/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef SCRIPTINITIALIZER_H
#define SCRIPTINITIALIZER_H

#include "initializer.h"
#include "preparator.h"

class ScriptInitializer : public Initializer
{
    Q_OBJECT
public:
    explicit ScriptInitializer(QObject *parent = nullptr);

    static Initializer *create();

signals:

public slots:
    void initialize();

private:
    QString _scriptFileName;
    QString _scriptDirName;
    QFile _scriptFile;
    QString _scriptContent;

    struct ParameterDesc {
        QString ref;
        QString name;
        QString content;

        QVariant value;
    };

    struct TreatmentDesc {
        QString ref;
        QString name;
        QString type;
        QString master;
        QString config;
        QMap<QString, ParameterDesc> parameters;
    };

    struct ConnectionDesc {
        QString ref;
        QString outputRef;
        QString inputRef;

        QString outputMasterElementName;
        QString outputTreatmentName;
        QString outputQualifierName;

        QString inputMasterElementName;
        QString inputTreatmentName;
        QString inputQualifierName;
    };

    struct PreparatorDesc {
        QString ref;
        QString name;
        QString type;
        QString code;
        QMap<QString, ParameterDesc> parameters;
        QMap<QString, TreatmentDesc> treatments;
        QStringList paths;

        Preparator *prep = nullptr;
    };

    struct ModelDesc {
        QString ref;
        QString name;
        QString type;
        QString code;
        QMap<QString, ParameterDesc> parameters;

        Model *model = nullptr;
    };

    struct SequenceDesc {
        QString ref;
        QString name;
        QString code;
        QMap<QString, TreatmentDesc> treatments;
        QList<ConnectionDesc> connections;
        PreparatorDesc preparator;
        QMap<QString, Treatment*> commonTreatments;
    };

    QMap<QString, ModelDesc> models;
    QMap<QString, SequenceDesc> sequences;

    void treatScript();
    void treatModels(const QStringRef &code);
    void treatModel(ModelDesc &model);
    void treatSequences(const QStringRef &code);
    void treatSequence(SequenceDesc &seq);
    void treatConnections(SequenceDesc &seq);
    void treatConnection(ConnectionDesc &conn);
    void treatPreparator(SequenceDesc &seq);
    template<typename T>
    void treatTreatments(T &objDesc);
    void treatTreatment(TreatmentDesc &treatment);
    void treatParameter(ParameterDesc &param);

    static QVariant treatVariable(const QString &var);

    template<typename T>
    static void checkAndInsert(T &obj, QMap<QString, T> &map);

    void buildModels();
    void buildModel(ModelDesc &model);
    void buildSequences();
    void buildSequence(SequenceDesc &seq);
    void buildPreparator(PreparatorDesc &preparator);
    Treatment *buildTreatment(TreatmentDesc &treatment, Preparator::PreparedTrack &preparedTrack, const QVector<Track *> &tracks, SequenceDesc &seq);
    void buildConnection(ConnectionDesc &conn, QMap<QString, Treatment*> &treatments, Preparator::PreparedTrack &preparedTrack, SequenceDesc &seq);
    Treatment *getTreatment(QString &masterElementName, QString &treatmentName, QMap<QString, Treatment*> &treatments, Preparator::PreparedTrack &preparedTrack, SequenceDesc &seq);

    static QRegularExpression commentRegEx;
    static QRegularExpression sequenceRegEx;

    static QRegularExpression preparatorRegEx;
    static QRegularExpression preparatorParamRegEx;
    static QRegularExpression preparatorPathRegEx;

    static QRegularExpression modelRegEx;
    static QRegularExpression modelParamRegEx;

    static QRegularExpression treatmentRegEx;
    static QRegularExpression treatmentTypeRegEx;
    static QRegularExpression treatmentMasterElementSortRegEx;
    static QRegularExpression treatmentParamRegEx;

    static QRegularExpression connectionsRegEx;
    static QRegularExpression connectionRegEx;
    static QRegularExpression connectionTreatmentNameRegEx;
    static QRegularExpression connectionMasterElementNameRegEx;
    static QRegularExpression connectionOutputQualifierRegEx;
    static QRegularExpression connectionInputQualifierRegEx;

    static QRegularExpression booleanRegEx;
    static QRegularExpression numberRegEx;
    static QRegularExpression stringRegEx;
    static QRegularExpression arrayRegEx;
    static QRegularExpression arraySplitterRegEx;

};

#endif // SCRIPTINITIALIZER_H
