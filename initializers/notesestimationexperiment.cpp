/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "notesestimationexperiment.h"

#include "modelsfactory.h"

#include "preparators/midifilepreparator.h"

NotesEstimationExperiment::NotesEstimationExperiment(QObject *parent) : Initializer(parent)
{

}

Initializer *NotesEstimationExperiment::create()
{
    return new NotesEstimationExperiment();
}

void NotesEstimationExperiment::initialize()
{
    bool ok = true;
    auto checkOk = [ok]{
        if (!ok)
            throw std::exception();
    };

    int sampleRate = args().at(0).toInt(&ok);
    checkOk();

    int frameSize = args().at(1).toInt(&ok);
    checkOk();

    int hopSize = args().at(2).toInt(&ok);
    checkOk();

    int hpcpSize = args().at(3).toInt(&ok);
    checkOk();

    int accumulation = args().at(4).toInt(&ok);
    checkOk();

    auto experimentCase = QMetaEnum::fromType<ExperimentCase>()
            .keyToValue(args().at(5).toStdString().c_str(), &ok);
    checkOk();

    QString outputDir = args().last() + '/' +
            QString::number(sampleRate) + "Hz" +
            QString::number(frameSize) + "f" +
            QString::number(hopSize) + "h_" +
            QString::number(hpcpSize) + "hpcp_" +
            QString::number(accumulation) + "acc_" +
            QMetaEnum::fromType<ExperimentCase>().valueToKey(experimentCase) + '/';
    QDir().mkpath(outputDir);


    //Preparing model
    Model *model = nullptr;
    switch (experimentCase) {
    case ExperimentCase::FOR_20_TRIALS_5_REDUC_PRELU:
        model = ModelsFactory::globalInstance().createModel("ConfigurableVarFfn");
        break;
    default:
        model = ModelsFactory::globalInstance().createModel("ConfigurableRectFfn");
        model->setProperty("inputSize", hpcpSize*(accumulation+1));
        model->setProperty("layersSize", hpcpSize*(accumulation+1));
        model->setProperty("outputSize", 128);
        break;
    }
    Melodium::instance()->addModel(model);
    //Layers
    switch (experimentCase) {
    case ExperimentCase::FOR_5_TRIALS_3_LAYERS_PRELU:
        model->setProperty("layers", repeat("PReLU", 3));
        break;
    case ExperimentCase::FOR_5_TRIALS_5_LAYERS_PRELU:
        model->setProperty("layers", repeat("PReLU", 5));
        break;
    case ExperimentCase::FOR_5_TRIALS_7_LAYERS_PRELU:
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_PRELU:
    case ExperimentCase::FOR_1000_TRIALS_7_LAYERS_PRELU:
        model->setProperty("layers", repeat("PReLU", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_BILINEARINTERPOLATION:
        model->setProperty("layers", repeat("BilinearInterpolation", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_FASTLSTM:
        model->setProperty("layers", repeat("FastLSTM", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_IDENTITY:
        model->setProperty("layers", repeat("IdentityLayer", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_LINEAR:
        model->setProperty("layers", repeat("Linear", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_LOGSOFTMAX:
        model->setProperty("layers", repeat("LogSoftMax", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_LSTM:
        model->setProperty("layers", repeat("LSTM", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_MAXPOOLING:
        model->setProperty("layers", repeat("MaxPooling", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_MEANPOOLING:
        model->setProperty("layers", repeat("MeanPooling", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_NEGATIVELOGLIKELIHOOD:
        model->setProperty("layers", repeat("NegativeLogLikelihood", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_RELU:
        model->setProperty("layers", repeat("ReLU", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_SIGMOID:
        model->setProperty("layers", repeat("Sigmoid", 7));
        break;
    case ExperimentCase::FOR_100_TRIALS_7_LAYERS_TANH:
        model->setProperty("layers", repeat("TanHLayer", 7));
        break;
    case ExperimentCase::FOR_5_TRIALS_10_LAYERS_PRELU:
    case ExperimentCase::FOR_100_TRIALS_10_LAYERS_PRELU:
    case ExperimentCase::FOR_1000_TRIALS_10_LAYERS_PRELU:
        model->setProperty("layers", repeat("PReLU", 10));
        break;
    case ExperimentCase::FOR_20_TRIALS_5_REDUC_PRELU:
        model->setProperty("layers", repeat("PReLU", 5));
        model->setProperty("layersOutputSizes", decrease(hpcpSize*accumulation, 127, 5));
        break;
    case ExperimentCase::FOR_20_TRIALS_7_REDUC_PRELU:
        model->setProperty("layers", repeat("PReLU", 7));
        model->setProperty("layersOutputSizes", decrease(hpcpSize*accumulation, 127, 7));
        break;
    }

    //Trials
    Treatment *firstTrainer = nullptr;
    Treatment *previous = nullptr;
    QVector<QPair<Treatment*, Treatment*>> steps;
    int trials = 0;
    bool saveAssignments = true;
    bool saveResults = true;
    bool saveDiff = true;
    switch (experimentCase) {
        case ExperimentCase::FOR_5_TRIALS_3_LAYERS_PRELU:
        case ExperimentCase::FOR_5_TRIALS_5_LAYERS_PRELU:
        case ExperimentCase::FOR_5_TRIALS_7_LAYERS_PRELU:
        case ExperimentCase::FOR_5_TRIALS_10_LAYERS_PRELU:
            trials = 5;
        break;
        case ExperimentCase::FOR_20_TRIALS_5_REDUC_PRELU:
        case ExperimentCase::FOR_20_TRIALS_7_REDUC_PRELU:
            trials = 20;
        break;
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_PRELU:
        case ExperimentCase::FOR_100_TRIALS_10_LAYERS_PRELU:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_BILINEARINTERPOLATION:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_FASTLSTM:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_IDENTITY:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_LINEAR:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_LOGSOFTMAX:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_LSTM:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_MAXPOOLING:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_MEANPOOLING:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_NEGATIVELOGLIKELIHOOD:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_RELU:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_SIGMOID:
        case ExperimentCase::FOR_100_TRIALS_7_LAYERS_TANH:
            trials = 100;
            saveAssignments = false;
            saveResults = false;
            saveDiff = false;
        break;
        case ExperimentCase::FOR_1000_TRIALS_7_LAYERS_PRELU:
        case ExperimentCase::FOR_1000_TRIALS_10_LAYERS_PRELU:
            trials = 1000;
            saveAssignments = false;
            saveResults = false;
            saveDiff = false;
        break;
    }

    for (int i=1 ; i <= trials ; ++i) {
        Treatment *trainer = TreatmentsFactory::globalInstance().createTreatment(
                    model->type().trainingTreatmentType().name(),
                    model, this);
        Melodium::instance()->addTreatment(trainer);
        if (firstTrainer == nullptr)
            firstTrainer = trainer;
        if (previous != nullptr)
            previous->addNext(trainer);

        Treatment *saver = TreatmentsFactory::globalInstance().createTreatment(
                    model->type().savingTreatmentType().name(),
                    model, this);
        saver->setParameter("filename", outputDir + "/" + QString::number(i) + "trainings.xml");
        Melodium::instance()->addTreatment(saver);
        trainer->addNext(saver);

        previous = saver;

        steps.append(QPair<Treatment*, Treatment*>(trainer, saver));
    }


    MidiFilePreparator prep(this);
    prep.setAnalysis(MidiFilePreparator::Tracks);
    prep.setTracksProperties(sampleRate, frameSize, hopSize);

    for (int i=6 ; i < args().size()-1 ; ++i)
        prep.add(args().at(i));

    prep.prepare();

    QList<Preparator::PreparedTrack> list = prep.preparedTracks();

    for (auto item: list) {
        item.treatment("MidiTracksAnalyst")->setParameter("presenceOnly", true);

        Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track(), this);
        framer->addInputsMap("signal", "audio");
        framer->setParameter("frameSize", frameSize);
        framer->setParameter("hopSize", hopSize);
        framer->setParameter("startFromZero", true);
        framer->setParameter("lastFrameToEndOfFile", true);
        Melodium::instance()->addTreatment(framer);
        item.treatment("MonoLoader")->addNext(framer);

        Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track(), this);
        windowing->addOutputsMap("frame", "frameWindow");
        windowing->setParameter("type", "blackmanharris92");
        windowing->setParameter("size", frameSize);
        Melodium::instance()->addTreatment(windowing);
        framer->addNext(windowing);

        Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track(), this);
        spectrum->addInputsMap("frame", "frameWindow");
        Melodium::instance()->addTreatment(spectrum);
        windowing->addNext(spectrum);

        Treatment *spectralPeaks = TreatmentsFactory::globalInstance().createTreatment("SpectralPeaks", item.track(), this);
        spectralPeaks->setParameter("sampleRate", sampleRate);
        spectralPeaks->setParameter("orderBy", "magnitude");
        spectralPeaks->setParameter("magnitudeThreshold", 1e-05);
        spectralPeaks->setParameter("minFrequency", 40);
        spectralPeaks->setParameter("maxFrequency", 5000);
        spectralPeaks->setParameter("maxPeaks", 10000);
        spectralPeaks->addOutputsMap("frequencies", "spectralpeaksfrequencies");
        spectralPeaks->addOutputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(spectralPeaks);
        spectrum->addNext(spectralPeaks);

        Treatment *hpcp = TreatmentsFactory::globalInstance().createTreatment("HPCP", item.track(), this);
        hpcp->setParameter("sampleRate", sampleRate);
        hpcp->setParameter("windowSize", 0.5);
        hpcp->setParameter("harmonics", 8);
        hpcp->setParameter("weightType", "cosine");
        hpcp->setParameter("size", hpcpSize);
        hpcp->addInputsMap("frequencies", "spectralpeaksfrequencies");
        hpcp->addInputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(hpcp);
        spectralPeaks->addNext(hpcp);

        Treatment *accumulator = TreatmentsFactory::globalInstance().createTreatment("Accumulator", item.track(), this);
        accumulator->setParameter("cumulation", accumulation);
        accumulator->addInputsMap("input", "hpcp");
        accumulator->addOutputsMap("output", "cumuledHpcp");
        Melodium::instance()->addTreatment(accumulator);
        hpcp->addNext(accumulator);

        Treatment *feeder = TreatmentsFactory::globalInstance().createTreatment(
                    model->type().feedingTreatmentType().name(),
                    model, item.track(), this);
        feeder->addInputsMap("data", "cumuledHpcp");
        feeder->addInputsMap("label", "midiTrackContent1");
        Melodium::instance()->addTreatment(feeder);
        accumulator->addNext(feeder);
        item.treatment("MidiTracksAnalyst")->addNext(feeder);
        feeder->addNext(firstTrainer);

        for (int i=1 ; i <= trials ; ++i) {
            Treatment *predictor = TreatmentsFactory::globalInstance().createTreatment(
                        model->type().predictingTreatmentType().name(),
                        model, item.track(), this);
            predictor->addInputsMap("data", "cumuledHpcp");
            predictor->addOutputsMap("assignments", "assignments"+QString::number(i));
            Melodium::instance()->addTreatment(predictor);
            steps.at(i-1).first->addNext(predictor);
            accumulator->addNext(predictor);
            //predictor->addNext(csvExport(item.track(), {"assignments"+QString::number(i)}, outputDir + "/" + QString::number(i) + "assignments.csv"));
            if (saveAssignments) predictor->addNext(imageExport(item.track(), "assignments"+QString::number(i), outputDir + "/" + QString::number(i) + "assignments.png"));
            if (i < trials)
                predictor->addNext(steps.at(i).first);

            Treatment *threshold = TreatmentsFactory::globalInstance().createTreatment("Thresholder", item.track(), this);
            threshold->addInputsMap("input", "assignments"+QString::number(i));
            threshold->addOutputsMap("output", "results"+QString::number(i));
            Melodium::instance()->addTreatment(threshold);
            predictor->addNext(threshold);

            if (saveResults) threshold->addNext(csvExport(item.track(), {"results"+QString::number(i)}, outputDir + "/" + QString::number(i) + "results.csv"));
            if (saveResults) threshold->addNext(imageExport(item.track(), "results"+QString::number(i), outputDir + "/" + QString::number(i) + "results.png"));

            Treatment *evaluator = TreatmentsFactory::globalInstance().createTreatment("Evaluator", QVector<Track*>({item.track()}), this);
            evaluator->addInputsMap("values", "results"+QString::number(i));
            evaluator->addInputsMap("ground", "midiTrackContent1");
            evaluator->setParameter("output", outputDir + '/' + QString::number(i) + "eval.csv");
            Melodium::instance()->addTreatment(evaluator);
            item.treatment("MidiTracksAnalyst")->addNext(evaluator);
            threshold->addNext(evaluator);

            if (saveDiff) {
                Treatment *differentiator = TreatmentsFactory::globalInstance().createTreatment("Differentiator", item.track(), this);
                differentiator->addInputsMap("values", "results"+QString::number(i));
                differentiator->addInputsMap("ground", "midiTrackContent1");
                differentiator->addOutputsMap("output", "diff"+QString::number(i));
                Melodium::instance()->addTreatment(differentiator);
                item.treatment("MidiTracksAnalyst")->addNext(differentiator);
                threshold->addNext(differentiator);

                differentiator->addNext(csvExport(item.track(), {"diff"+QString::number(i)}, outputDir + "/" + QString::number(i) + "diff.csv"));
                differentiator->addNext(imageExport(item.track(), "diff"+QString::number(i), outputDir + "/" + QString::number(i) + "diff.png"));
            }
        }
    }
}

QStringList NotesEstimationExperiment::repeat(const QString &string, size_t times)
{
    QStringList list;
    for (size_t i=0 ; i < times ; ++i)
        list << string;
    return list;
}

QList<QVariant> NotesEstimationExperiment::decrease(size_t start, size_t end, size_t steps)
{
    double step = (static_cast<double>(start) - static_cast<double>(end)) / static_cast<double>(steps);

    QList<QVariant> list;
    for (size_t i = 0 ; i < steps ; ++i) {
        list.append(static_cast<uint>(start-(step*(i+1))));
    }

    return list;
}
