/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef INSTRUMENTFILTERINGEXPERIMENTS_H
#define INSTRUMENTFILTERINGEXPERIMENTS_H

#include "initializer.h"
#include "modelsfactory.h"
#include "preparators/midifilepreparator.h"

class InstrumentFilteringExperiments : public Initializer
{
    Q_OBJECT
public:
    explicit InstrumentFilteringExperiments(QObject *parent = nullptr);

    static Initializer *create();

signals:

public slots:
    void initialize();

private:
    int _sampleRate;
    int _frameSize;
    int _hopSize;
    QString _window;
    QStringList _representations;
    int _cumulation;
    QString _method;
    QString _network;

    bool _spectrumRequested;
    bool _mfccRequested;
    bool _gfccRequested;


    Treatment *manageOriginalData(Preparator::PreparedTrack &item);
    Treatment *manageAlteredData(Preparator::PreparedTrack &item);
};

#endif // INSTRUMENTFILTERINGEXPERIMENTS_H
