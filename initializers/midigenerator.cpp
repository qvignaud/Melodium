#include "midigenerator.h"

MidiGenerator::MidiGenerator(QObject *parent) : Initializer(parent)
{

}

Initializer *MidiGenerator::create()
{
    return new MidiGenerator();
}

void MidiGenerator::initialize()
{
    bool ok = true;
    auto checkOk = [ok]{
        if (!ok)
            throw std::exception();
    };

    int lowNote = args().at(0).toInt(&ok);
    checkOk();

    int highNote = args().at(1).toInt(&ok);
    checkOk();

    float noteDuration = args().at(2).toFloat(&ok);
    checkOk();

    float silenceDuration = args().at(3).toFloat(&ok);
    checkOk();

    int firstVoice = args().at(4).toInt(&ok);
    checkOk();

    int lastVoice = args().at(5).toInt(&ok);
    checkOk();

    QString outDir = args().last();

    int sampleRate = 44100;
    int frameSize = 512;
    int hopSize = frameSize;

    int framesOn = (noteDuration * sampleRate) / frameSize;
    int framesOff = (silenceDuration * sampleRate) / frameSize;

    for (int voice = firstVoice ; voice <= lastVoice ; ++voice) {
        QString voiceLabel = QString::number(voice);
        while (voiceLabel.length() < 3)
            voiceLabel.prepend('0');

        Track *track = new Track(sampleRate, frameSize, hopSize, this);
        Melodium::instance()->addTrack(track);

        Treatment *trackFiller = TreatmentsFactory::globalInstance().createTreatment("MidiTracksFiller", track, this);
        trackFiller->setParameter("task", "ladder");
        trackFiller->setParameter("voice", voiceLabel);
        trackFiller->setParameter("framesOn", framesOn);
        trackFiller->setParameter("framesOff", framesOff);
        trackFiller->setParameter("lowNote", lowNote);
        trackFiller->setParameter("highNote", highNote);
        Melodium::instance()->addTreatment(trackFiller);

        Treatment *midiExporter = TreatmentsFactory::globalInstance().createTreatment("MidiExporter", track, this);
        midiExporter->setParameter("filename", outDir + '/' + voiceLabel + ".midi");
        Melodium::instance()->addTreatment(midiExporter);
        trackFiller->addNext(midiExporter);
    }
}
