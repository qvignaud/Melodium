/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "visualizerinitializer.h"

#include "preparators/audiofilepreparator.h"
#include "preparators/soundfontpreparator.h"

VisualizerInitializer::VisualizerInitializer(QObject *parent) : Initializer(parent)
{

}

Initializer *VisualizerInitializer::create()
{
    return new VisualizerInitializer();
}

void VisualizerInitializer::initialize()
{
    bool ok = true;
    auto checkOk = [ok]{
        if (!ok)
            throw std::exception();
    };

    int sampleRate = args().at(0).toInt(&ok);
    checkOk();

    int frameSize = args().at(1).toInt(&ok);
    checkOk();

    int hopSize = args().at(2).toInt(&ok);
    checkOk();

    QString window = args().at(3);

    //SoundFontPreparator prep(this);
    AudioFilePreparator prep(this);
    prep.setTracksProperties(sampleRate, frameSize, hopSize);
    for (int i=4 ; i < args().size()-1 ; ++i)
        prep.add(args().at(i));

    prep.prepare();

    QList<Preparator::PreparedTrack> list = prep.preparedTracks();

    QString outputDir = args().last() + '/' +
            QString::number(sampleRate) + "Hz" +
            QString::number(frameSize) + "f" +
            QString::number(hopSize) + "h_" + window + '/';
    QDir().mkpath(outputDir);

    auto exportCsv = [this](Track *track, Treatment *previous, QString data, QString path){
        Treatment *csv = TreatmentsFactory::globalInstance().createTreatment("CsvExporter", track, this);
        csv->setParameter("qualifiers", data);
        csv->setParameter("filename", path);

        Melodium::instance()->addTreatment(csv);
        previous->addNext(csv);
    };

    auto exportImage = [this](Track *track, Treatment *previous, QString data, QString path){
        Treatment *image = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", track, this);
        image->setParameter("filename", path);
        image->setParameter("autoScale", true);
        image->addInputsMap("data", data);

        Melodium::instance()->addTreatment(image);
        previous->addNext(image);
    };

    for (auto item: list) {
        QString baseName = QFileInfo(item.track()->audioFile()).baseName();

        Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track(), this);
        //framer->addInputsMap("signal", "audioSampleOutput");
        framer->addInputsMap("signal", "audio");
        framer->setParameter("frameSize", frameSize);
        framer->setParameter("hopSize", hopSize);
        framer->setParameter("startFromZero", true);
        framer->setParameter("lastFrameToEndOfFile", true);
        Melodium::instance()->addTreatment(framer);
        item.treatment("MonoLoader")->addNext(framer);

        Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track(), this);
        windowing->addOutputsMap("frame", "frameWindow");
        windowing->setParameter("type", window);
        windowing->setParameter("size", frameSize);
        Melodium::instance()->addTreatment(windowing);
        framer->addNext(windowing);

        Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track(), this);
        spectrum->addInputsMap("frame", "frameWindow");
        Melodium::instance()->addTreatment(spectrum);
        windowing->addNext(spectrum);

        Treatment *mfcc = TreatmentsFactory::globalInstance().createTreatment("MFCC", item.track(), this);
        mfcc->setParameter("inputSize", frameSize);
        mfcc->setParameter("sampleRate", sampleRate);
        mfcc->setParameter("highFrequencyBound", sampleRate/2);
        mfcc->addOutputsMap("bands", "mfccbands");
        Melodium::instance()->addTreatment(mfcc);
        spectrum->addNext(mfcc);
        exportCsv(item.track(), mfcc, "mfccbands", outputDir + baseName + ".mfccbands.csv");
        exportImage(item.track(), mfcc, "mfccbands", outputDir + baseName + ".mfccbands.png");
        exportCsv(item.track(), mfcc, "mfcc", outputDir + baseName + ".mfcc.csv");
        exportImage(item.track(), mfcc, "mfcc", outputDir + baseName + ".mfcc.png");

        Treatment *gfcc = TreatmentsFactory::globalInstance().createTreatment("GFCC", item.track(), this);
        gfcc->setParameter("inputSize", frameSize);
        gfcc->setParameter("sampleRate", sampleRate);
        gfcc->setParameter("highFrequencyBound", sampleRate/2);
        gfcc->addOutputsMap("bands", "gfccbands");
        Melodium::instance()->addTreatment(gfcc);
        spectrum->addNext(gfcc);
        exportCsv(item.track(), gfcc, "gfccbands", outputDir + baseName + ".gfccbands.csv");
        exportImage(item.track(), gfcc, "gfccbands", outputDir + baseName + ".gfccbands.png");
        exportCsv(item.track(), gfcc, "gfcc", outputDir + baseName + ".gfcc.csv");
        exportImage(item.track(), gfcc, "gfcc", outputDir + baseName + ".gfcc.png");

        Treatment *barkbands = TreatmentsFactory::globalInstance().createTreatment("BarkBands", item.track(), this);
        barkbands->setParameter("sampleRate", sampleRate);
        barkbands->addOutputsMap("bands", "barkbands");
        Melodium::instance()->addTreatment(barkbands);
        spectrum->addNext(barkbands);
        exportCsv(item.track(), barkbands, "barkbands", outputDir + baseName + ".barkbands.csv");
        exportImage(item.track(), barkbands, "barkbands", outputDir + baseName + ".barkbands.png");

        Treatment *spectralPeaks = TreatmentsFactory::globalInstance().createTreatment("SpectralPeaks", item.track(), this);
        spectralPeaks->setParameter("sampleRate", sampleRate);
        spectralPeaks->setParameter("orderBy", "magnitude");
        spectralPeaks->setParameter("magnitudeThreshold", 1e-05);
        spectralPeaks->setParameter("minFrequency", 40);
        spectralPeaks->setParameter("maxFrequency", 5000);
        spectralPeaks->setParameter("maxPeaks", 10000);
        spectralPeaks->addOutputsMap("frequencies", "spectralpeaksfrequencies");
        spectralPeaks->addOutputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(spectralPeaks);
        spectrum->addNext(spectralPeaks);
        exportCsv(item.track(), spectralPeaks, "spectralpeaksfrequencies", outputDir + baseName + ".spectralpeaksfrequencies.csv");
        //exportImage(item.track, spectralPeaks, "spectralpeaksfrequencies", outputDir + baseName + ".spectralpeaksfrequencies.png");
        exportCsv(item.track(), spectralPeaks, "spectralpeaksmagnitudes", outputDir + baseName + ".spectralpeaksmagnitudes.csv");
        //exportImage(item.track, spectralPeaks, "spectralpeaksmagnitudes", outputDir + baseName + ".spectralpeaksmagnitudes.png");

        Treatment *hpcp = TreatmentsFactory::globalInstance().createTreatment("HPCP", item.track(), this);
        hpcp->setParameter("sampleRate", sampleRate);
        hpcp->setParameter("windowSize", 0.5);
        hpcp->setParameter("harmonics", 8);
        hpcp->setParameter("weightType", "cosine");
        hpcp->setParameter("size", 120);
        hpcp->addInputsMap("frequencies", "spectralpeaksfrequencies");
        hpcp->addInputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(hpcp);
        spectralPeaks->addNext(hpcp);
        exportCsv(item.track(), hpcp, "hpcp", outputDir + baseName + ".hpcp.csv");
        exportImage(item.track(), hpcp, "hpcp", outputDir + baseName + ".hpcp.png");

        }
}
