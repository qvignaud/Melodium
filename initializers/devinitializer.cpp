/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "devinitializer.h"

//#include "models/pianoautoencoder.h"
#include "runnablescheme.h"

DevInitializer::DevInitializer(QObject *parent) : Initializer(parent)
{

}

Initializer *DevInitializer::create()
{
    return new DevInitializer();
}

void DevInitializer::initialize()
{
    /*MidiFilePreparator prep(this);
    prep.setTracksProperties(44100, 2048, 1024);

    prep.add("/tmp/midis");*/

    /*AudioFilePreparator prep(this);
    prep.setTracksProperties(44100, 2048, 1024);

    prep.add("/tmp/audio");*/

    int sampleRate = 44100;
    int frameSize = 2048;
    int hopSize = 1024;
    QString window = "blackmanharris92";

    SoundFontPreparator prep(this);
    prep.setTracksProperties(sampleRate, frameSize, hopSize);

    prep.add("/tmp/sf");

    prep.prepare();

    QList<Preparator::PreparedTrack> list = prep.preparedTracks();

    /*PianoAutoencoder *model = new PianoAutoencoder(this);
    Melodium::instance()->addModel(model);

    Treatment *training = model->trainingTreatment();
    Melodium::instance()->addTreatment(training);*/

    for (auto item: list) {
        //std::cout << item.track->audioFile().toStdString() << std::endl;
        /*Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track, this);
        QMap<QString, QString> framerInput;
        //framerInput.insert("signal", "audio");
        framerInput.insert("signal", "audioSampleOutput");
        framer->setInputsMap(framerInput);
        framer->setParameter("frameSize", 1024);
        framer->setParameter("hopSize", 512);
        framer->setParameter("startFromZero", true);
        framer->setParameter("lastFrameToEndOfFile", true);
        Melodium::instance()->addTreatment(framer);
        item.headTreatment->addNext(framer);

        Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track, this);
        QMap<QString, QString> windowingOutput;
        windowingOutput.insert("frame", "frameWindow");
        windowing->setOutputsMap(windowingOutput);
        windowing->setParameter("type", "hann");
        windowing->setParameter("size", 1024);
        Melodium::instance()->addTreatment(windowing);
        framer->addNext(windowing);

        Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track, this);
        QMap<QString, QString> spectrumInput;
        spectrumInput.insert("frame", "frameWindow");
        spectrum->setInputsMap(spectrumInput);
        Melodium::instance()->addTreatment(spectrum);
        windowing->addNext(spectrum);*/

        /*Treatment *mfcc = TreatmentsFactory::globalInstance().createTreatment("MFCC", item.track, this);
        Melodium::instance()->addTreatment(mfcc);
        spectrum->addNext(mfcc);

        Treatment *csv = TreatmentsFactory::globalInstance().createTreatment("CSVExporter", item.track, this);
        csv->setParameter("filename", item.track->audioFile() + ".csv");
        csv->setParameter("qualifiers", {"mfcc"});
        Melodium::instance()->addTreatment(csv);
        mfcc->addNext(csv);*/

        /*QMap<QString, QString> om;
        om.insert("midiTrackVoice1", "voice");
        om.insert("midiTrackContent1", "content");
        item.headTreatment->setOutputsMap(om);*/

        //item.headTreatment->setParameter("dontCountPresence", true);

        /*Treatment *csv = TreatmentsFactory::globalInstance().createTreatment("CSVExporter", item.track, this);
        csv->setParameter("filename", item.track->audioFile() + ".csv");
        csv->setParameter("qualifiers", QStringList({"content"}));
        Melodium::instance()->addTreatment(csv);
        item.headTreatment->addNext(csv);*/
        //spectrum->addNext(csv);

        /*Treatment *image = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", item.track, this);
        image->setParameter("filename", item.track->audioFile() + ".png");
        image->setParameter("autoScale", true);
        image->addInputsMap("data", "content");
        Melodium::instance()->addTreatment(image);
        item.headTreatment->addNext(image);
        //spectrum->addNext(image);*/

        Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track(), this);
        framer->addInputsMap("signal", "audioSampleOutput");
        //framer->addInputsMap("signal", "audio");
        framer->setParameter("frameSize", frameSize);
        framer->setParameter("hopSize", hopSize);
        framer->setParameter("startFromZero", true);
        framer->setParameter("lastFrameToEndOfFile", true);
        Melodium::instance()->addTreatment(framer);
        item.treatment("SoundFontAnalyst")->addNext(framer);

        Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track(), this);
        windowing->addOutputsMap("frame", "frameWindow");
        windowing->setParameter("type", window);
        windowing->setParameter("size", frameSize);
        Melodium::instance()->addTreatment(windowing);
        framer->addNext(windowing);

        Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track(), this);
        spectrum->addInputsMap("frame", "frameWindow");
        Melodium::instance()->addTreatment(spectrum);
        windowing->addNext(spectrum);

        Treatment *spectralPeaks = TreatmentsFactory::globalInstance().createTreatment("SpectralPeaks", item.track(), this);
        spectralPeaks->setParameter("sampleRate", sampleRate);
        spectralPeaks->setParameter("orderBy", "magnitude");
        spectralPeaks->setParameter("magnitudeThreshold", 1e-05);
        spectralPeaks->setParameter("minFrequency", 40);
        spectralPeaks->setParameter("maxFrequency", 5000);
        spectralPeaks->setParameter("maxPeaks", 10000);
        spectralPeaks->addOutputsMap("frequencies", "spectralpeaksfrequencies");
        spectralPeaks->addOutputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(spectralPeaks);
        spectrum->addNext(spectralPeaks);

        Treatment *hpcp = TreatmentsFactory::globalInstance().createTreatment("HPCP", item.track(), this);
        hpcp->setParameter("sampleRate", sampleRate);
        hpcp->setParameter("windowSize", 0.5);
        hpcp->setParameter("harmonics", 8);
        hpcp->setParameter("weightType", "cosine");
        hpcp->setParameter("size", 120);
        hpcp->addInputsMap("frequencies", "spectralpeaksfrequencies");
        hpcp->addInputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(hpcp);
        spectralPeaks->addNext(hpcp);

        /*Treatment *feed = model->feedingTreatment(item.track());
        feed->addInputsMap("data", "hpcp");
        //feed->addInputsMap("label", "content");
        Melodium::instance()->addTreatment(feed);
        //item.headTreatment->addNext(feed);
        hpcp->addNext(feed);

        feed->addNext(training);*/
    }

    /*RunnableScheme rs;
    rs.check();
    rs.printNotifications();
    rs.makeGraph("/tmp/graph.dot");*/
}
