/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "hpcptrainer.h"

#include "modelsfactory.h"

#include "preparators/audiofilepreparator.h"
#include "preparators/midifilepreparator.h"
#include "preparators/soundfontpreparator.h"

HpcpTrainer::HpcpTrainer(QObject *parent) : Initializer(parent)
{

}

Initializer *HpcpTrainer::create()
{
    return new HpcpTrainer();
}

void HpcpTrainer::initialize()
{
    bool ok = true;
    auto checkOk = [ok]{
        if (!ok)
            throw std::exception();
    };

    int sampleRate = args().at(0).toInt(&ok);
    checkOk();

    int frameSize = args().at(1).toInt(&ok);
    checkOk();

    int hopSize = args().at(2).toInt(&ok);
    checkOk();

    QString window = args().at(3);

    int harmonics = args().at(4).toInt(&ok);
    checkOk();

    QString weightType = args().at(5);

    int hpcpSize = args().at(6).toInt(&ok);
    checkOk();

    int accumulation = args().at(7).toInt(&ok);
    checkOk();

    QString activationFunction = args().at(8);

    int layersCount = args().at(9).toInt(&ok);
    checkOk();

    QString outputModelFile = args().last();

    Model *model = ModelsFactory::globalInstance().createModel("ConfigurableRectFfn");
    QStringList layers;
    for (int i=0 ; i < layersCount ; ++i)
        layers << activationFunction;
    model->setProperty("layers", layers);
    Melodium::instance()->addModel(model);
    Treatment *trainer = TreatmentsFactory::globalInstance().createTreatment(
                model->type().trainingTreatmentType().name(),
                model, this);
    trainer->setParameter("trainings", 20);
    trainer->setParameter("trainingOutput", "/tmp/trainingOutput.txt");
    Melodium::instance()->addTreatment(trainer);
    Treatment *saver = TreatmentsFactory::globalInstance().createTreatment(
                model->type().savingTreatmentType().name(),
                model, this);
    saver->setParameter("filename", outputModelFile);
    Melodium::instance()->addTreatment(saver);

    trainer->addNext(saver);

    /*Treatment *loader = TreatmentsFactory::globalInstance().createTreatment(
                model->type().loadingTreatmentType().name(),
                model, this);
    loader->setParameter("filename", outputModelFile);
    Melodium::instance()->addTreatment(loader);*/

    MidiFilePreparator prep(this);
        prep.setAnalysis(MidiFilePreparator::Tracks);
    //SoundFontPreparator prep(this);
    //AudioFilePreparator prep(this);
    prep.setTracksProperties(sampleRate, frameSize, hopSize);
    for (int i=8 ; i < args().size()-1 ; ++i)
        prep.add(args().at(i));

    prep.prepare();

    QList<Preparator::PreparedTrack> list = prep.preparedTracks();

    for (auto item: list) {
        Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track(), this);
        //framer->addInputsMap("signal", "audioSampleOutput");
        framer->addInputsMap("signal", "audio");
        framer->setParameter("frameSize", frameSize);
        framer->setParameter("hopSize", hopSize);
        framer->setParameter("startFromZero", true);
        framer->setParameter("lastFrameToEndOfFile", true);
        Melodium::instance()->addTreatment(framer);
        //item.treatment("SoundFontAnalyst")->addNext(framer);
        item.treatment("MonoLoader")->addNext(framer);

        Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track(), this);
        windowing->addOutputsMap("frame", "frameWindow");
        windowing->setParameter("type", window);
        windowing->setParameter("size", frameSize);
        Melodium::instance()->addTreatment(windowing);
        framer->addNext(windowing);

        Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track(), this);
        spectrum->addInputsMap("frame", "frameWindow");
        Melodium::instance()->addTreatment(spectrum);
        windowing->addNext(spectrum);

        Treatment *spectralPeaks = TreatmentsFactory::globalInstance().createTreatment("SpectralPeaks", item.track(), this);
        spectralPeaks->setParameter("sampleRate", sampleRate);
        spectralPeaks->setParameter("orderBy", "magnitude");
        spectralPeaks->setParameter("magnitudeThreshold", 1e-05);
        spectralPeaks->setParameter("minFrequency", 40);
        spectralPeaks->setParameter("maxFrequency", 5000);
        spectralPeaks->setParameter("maxPeaks", 10000);
        spectralPeaks->addOutputsMap("frequencies", "spectralpeaksfrequencies");
        spectralPeaks->addOutputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(spectralPeaks);
        spectrum->addNext(spectralPeaks);

        Treatment *hpcp = TreatmentsFactory::globalInstance().createTreatment("HPCP", item.track(), this);
        hpcp->setParameter("sampleRate", sampleRate);
        hpcp->setParameter("windowSize", 0.5);
        //hpcp->setParameter("harmonics", 8);
        hpcp->setParameter("harmonics", harmonics);
        //hpcp->setParameter("weightType", "cosine");
        hpcp->setParameter("weightType", weightType);
        //hpcp->setParameter("size", 120);
        hpcp->setParameter("size", hpcpSize);
        hpcp->addInputsMap("frequencies", "spectralpeaksfrequencies");
        hpcp->addInputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(hpcp);
        spectralPeaks->addNext(hpcp);

        Treatment *accumulator = TreatmentsFactory::globalInstance().createTreatment("Accumulator", item.track(), this);
        accumulator->setParameter("cumulation", accumulation);
        accumulator->addInputsMap("input", "hpcp");
        accumulator->addOutputsMap("output", "cumuledHpcp");
        Melodium::instance()->addTreatment(accumulator);
        hpcp->addNext(accumulator);

        Treatment *feeder = TreatmentsFactory::globalInstance().createTreatment(
                    model->type().feedingTreatmentType().name(),
                    model, item.track(), this);
        feeder->addInputsMap("data", "cumuledHpcp");
        //feeder->addInputsMap("label", "midiContent");
        feeder->addInputsMap("label", "midiTrackContent1");
        Melodium::instance()->addTreatment(feeder);
        accumulator->addNext(feeder);
        //item.treatment("SoundFontAnalyst")->addNext(feeder);
        item.treatment("MidiTracksAnalyst")->addNext(feeder);
        feeder->addNext(trainer);

        Treatment *predictor = TreatmentsFactory::globalInstance().createTreatment(
                    model->type().predictingTreatmentType().name(),
                    model, item.track(), this);
        predictor->addInputsMap("data", "cumuledHpcp");
        Melodium::instance()->addTreatment(predictor);
        trainer->addNext(predictor);
        //loader->addNext(predictor);
        accumulator->addNext(predictor);

        Treatment *differentiator = TreatmentsFactory::globalInstance().createTreatment("Differentiator", item.track(), this);
        differentiator->addInputsMap("values", "assignments");
        differentiator->addInputsMap("ground", "midiTrackContent1");
        differentiator->addOutputsMap("output", "diff");
        Melodium::instance()->addTreatment(differentiator);
        item.treatment("MidiTracksAnalyst")->addNext(differentiator);
        predictor->addNext(differentiator);

        differentiator->addNext(imageExport(item.track(), "diff",
                                            "/tmp/assignments/" + QFileInfo(item.track()->audioFile()).completeBaseName() + ".diff.png"));

        Treatment *exporter = TreatmentsFactory::globalInstance().createTreatment("CsvExporter", item.track(), this);
        exporter->setParameter("filename", "/tmp/assignments/" + QFileInfo(item.track()->audioFile()).completeBaseName() + ".csv");
        exporter->setParameter("qualifiers", {"assignments"});
        Melodium::instance()->addTreatment(exporter);
        predictor->addNext(exporter);

        Treatment *imagerTruth = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", item.track(), this);
        imagerTruth->setParameter("filename", "/tmp/assignments/" + QFileInfo(item.track()->audioFile()).completeBaseName() + ".truth.png");
        imagerTruth->setParameter("autoScale", true);
        //imagerTruth->addInputsMap("data", "midiContent");
        imagerTruth->addInputsMap("data", "midiTrackContent1");
        Treatment *imagerData = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", item.track(), this);
        imagerData->setParameter("filename", "/tmp/assignments/" + QFileInfo(item.track()->audioFile()).completeBaseName() + ".data.png");
        imagerData->setParameter("autoScale", true);
        imagerData->addInputsMap("data", "cumuledHpcp");
        Melodium::instance()->addTreatment(imagerData);
        accumulator->addNext(imagerData);

        Melodium::instance()->addTreatment(imagerTruth);
        //item.treatment("SoundFontAnalyst")->addNext(imagerTruth);
        item.treatment("MidiTracksAnalyst")->addNext(imagerTruth);
        Treatment *imagerAssignments = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", item.track(), this);
        imagerAssignments->setParameter("filename", "/tmp/assignments/" + QFileInfo(item.track()->audioFile()).completeBaseName() + ".assignments.png");
        imagerAssignments->setParameter("autoScale", true);
        imagerAssignments->addInputsMap("data", "assignments");
        Melodium::instance()->addTreatment(imagerAssignments);
        predictor->addNext(imagerAssignments);
    }
}
