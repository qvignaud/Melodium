/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "scriptinitializer.h"
#include <QRegularExpression>
#include <QDebug>

#include "modelsfactory.h"
#include "preparators/audiofilepreparator.h"
#include "preparators/midifilepreparator.h"
#include "preparators/soundfontpreparator.h"

QRegularExpression ScriptInitializer::commentRegEx("\\s*((\\/\\/|#)[^\\n]*|\\/\\*([^*\\/]|\\*[^\\/]|[^*]\\/)*\\*\\/)", QRegularExpression::UseUnicodePropertiesOption);
QRegularExpression ScriptInitializer::sequenceRegEx("sequence\\s+(?P<name>\\w+)\\s*(?P<code>{((?:[^{}]+|(?2))*+)})", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::DotMatchesEverythingOption);

QRegularExpression ScriptInitializer::preparatorRegEx("preparator\\s+(?P<name>\\w+)\\s*\\((?P<type>\\w+)\\)\\s*({(?P<code>(?:[^{}]+|(?2))*+)})", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::DotMatchesEverythingOption);
QRegularExpression ScriptInitializer::preparatorParamRegEx("(?P<name>\\w+)\\s+=\\s+(?P<content>[0-9]+|true|false)", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::DotMatchesEverythingOption);
QRegularExpression ScriptInitializer::preparatorPathRegEx("\"(?P<path>([^\"]|\\\\\")*)\"", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::DotMatchesEverythingOption);

QRegularExpression ScriptInitializer::modelRegEx("model\\s+(?P<name>\\w+)\\s*\\((?P<type>\\w+)\\)\\s*({(?P<code>(?:[^{}]+|(?2))*+)})", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::DotMatchesEverythingOption);
QRegularExpression ScriptInitializer::modelParamRegEx("(?P<name>\\w+)\\s+=\\s+(?P<content>\\\"([^\\\"\\\\]|\\\\\\\"|\\\\\\\\)*\\\"|-?[0-9]*\\.?[0-9]+|true|false|\\[[^\\]]*\\])", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::DotMatchesEverythingOption);

QRegularExpression ScriptInitializer::treatmentRegEx("^\\s*(?P<name>\\w+(?:\\[\\w+\\])?)\\s*\\((?P<config>((\\s*\\w+(\\[\\w+\\])?\\s*,?)?(\\s*\\w+\\s*=\\s*(\\\"([^\\\"\\\\]|\\\\\\\")*\\\"|-?[0-9]*\\.?[0-9]+|true|false|\\[.*\\])\\s*)?,?)*)\\)", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::MultilineOption);
QRegularExpression ScriptInitializer::treatmentTypeRegEx("^\\s*(\\w+(?:\\[\\w+\\])?)(\\s*,|$)", QRegularExpression::UseUnicodePropertiesOption);
QRegularExpression ScriptInitializer::treatmentMasterElementSortRegEx("^(\\w+)(?:\\[(\\w+)\\])?$", QRegularExpression::UseUnicodePropertiesOption);
QRegularExpression ScriptInitializer::treatmentParamRegEx("\\s*(?P<name>\\w+)\\s*=\\s*(?P<content>\\\"([^\\\"\\\\]|\\\\\\\"|\\\\\\\\)*\\\"|-?[0-9]*\\.?[0-9]+|true|false|\\[.*\\])\\s*(,|\\s*$)", QRegularExpression::UseUnicodePropertiesOption);

QRegularExpression ScriptInitializer::connectionsRegEx("([\\w.,\\[\\]]+)(\\s*-+>\\s*([\\w.,\\[\\]]+))+", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::DotMatchesEverythingOption);
QRegularExpression ScriptInitializer::connectionRegEx("([\\w.,\\[\\]]+)\\s*-+>\\s*([\\w.,\\[\\]]+)", QRegularExpression::UseUnicodePropertiesOption | QRegularExpression::DotMatchesEverythingOption);
QRegularExpression ScriptInitializer::connectionTreatmentNameRegEx("(?:\\w+\\[)?(\\w+)\\]?(?:\\.|$)", QRegularExpression::UseUnicodePropertiesOption);
QRegularExpression ScriptInitializer::connectionMasterElementNameRegEx("(\\w+)\\[", QRegularExpression::UseUnicodePropertiesOption);
QRegularExpression ScriptInitializer::connectionOutputQualifierRegEx("[\\w\\[\\]]+\\.(?:\\w+,)?(\\w+)", QRegularExpression::UseUnicodePropertiesOption);
QRegularExpression ScriptInitializer::connectionInputQualifierRegEx("[\\w\[\\]]+\\.(\\w+)", QRegularExpression::UseUnicodePropertiesOption);

QRegularExpression ScriptInitializer::booleanRegEx("^(true|false)$");
QRegularExpression ScriptInitializer::numberRegEx("^(-?[0-9]*\\.?[0-9]+)$");
QRegularExpression ScriptInitializer::stringRegEx("^\"(([^\"]|\\\\\")*)\"$");
QRegularExpression ScriptInitializer::arrayRegEx("^\\[(\\s*(-?[0-9]*\\.?[0-9]+|true|false|\"(([^\\\"]|\\\\\")*)\")\\s*,?\\s*)*\\]$");
QRegularExpression ScriptInitializer::arraySplitterRegEx("(\\[|,)\\s*(-?[0-9]*\\.?[0-9]+|true|false|\"([^\\\"\\\\]|\\\\\"|\\\\\\\\)*\")");

ScriptInitializer::ScriptInitializer(QObject *parent) : Initializer(parent)
{

}

Initializer *ScriptInitializer::create()
{
    return new ScriptInitializer();
}

void ScriptInitializer::initialize()
{
    _scriptFileName = args().at(0);
    _scriptDirName = QFileInfo(_scriptFileName).canonicalPath();

    _scriptFile.setFileName(_scriptFileName);

    if (_scriptFile.open(QIODevice::ReadOnly | QIODevice::Text)) {

        _scriptContent = QString(_scriptFile.readAll());

        treatScript();

        buildModels();
        buildSequences();
    }
    else
        throw std::invalid_argument("Script file cannot be open.");
}

void ScriptInitializer::treatScript()
{
    _scriptContent.remove(commentRegEx);
    treatModels(QStringRef(&_scriptContent));
    treatSequences(QStringRef(&_scriptContent));
}

void ScriptInitializer::treatModels(const QStringRef &code)
{
    auto it = modelRegEx.globalMatch(code);
    while (it.hasNext()) {
        auto match = it.next();

        ModelDesc model;
        model.ref = match.captured(0);
        model.name = match.captured("name");
        model.type = match.captured("type");
        model.code = match.captured("code");

        treatModel(model);

        checkAndInsert(model, models);
    }
}

void ScriptInitializer::treatModel(ScriptInitializer::ModelDesc &model)
{
    auto paramsIt = modelParamRegEx.globalMatch(model.code);
    while (paramsIt.hasNext()) {
        auto paramMatch = paramsIt.next();

        ParameterDesc param;
        param.ref = paramMatch.captured(0);
        param.name = paramMatch.captured("name");
        param.content = paramMatch.captured("content");

        treatParameter(param);

        checkAndInsert(param, model.parameters);
    }
}

void ScriptInitializer::treatSequences(const QStringRef &code)
{
    auto it = sequenceRegEx.globalMatch(code);
    while (it.hasNext()) {
        auto match = it.next();

        SequenceDesc seq;
        seq.ref = match.captured(0);
        seq.name = match.captured("name");
        seq.code = match.captured("code");

        treatSequence(seq);

        checkAndInsert(seq, sequences);
    }
}

void ScriptInitializer::treatSequence(SequenceDesc &seq)
{
    treatPreparator(seq);
    treatTreatments(seq);
    treatConnections(seq);
}

void ScriptInitializer::treatConnections(ScriptInitializer::SequenceDesc &seq)
{
    auto it = connectionsRegEx.globalMatch(seq.code);
    while (it.hasNext()) {
        auto match = it.next();

        int rightRemains = match.capturedRef(0).size();
        QRegularExpressionMatch connectionMatch;
        while (rightRemains > 0) {
            connectionMatch = connectionRegEx.match(match.capturedRef(0).right(rightRemains));

            if (connectionMatch.hasMatch()) {

                ConnectionDesc conn;
                conn.ref = connectionMatch.captured(0);
                conn.outputRef = connectionMatch.captured(1);
                conn.inputRef = connectionMatch.captured(2);

                treatConnection(conn);

                seq.connections.append(conn);

                rightRemains -= conn.ref.size() - conn.inputRef.size();
            }
            else
                break;
        }
    }
}

void ScriptInitializer::treatConnection(ScriptInitializer::ConnectionDesc &conn)
{
    conn.outputMasterElementName = connectionMasterElementNameRegEx.match(conn.outputRef).captured(1);
    conn.outputTreatmentName = connectionTreatmentNameRegEx.match(conn.outputRef).captured(1);
    conn.outputQualifierName = connectionOutputQualifierRegEx.match(conn.outputRef).captured(1);

    conn.inputMasterElementName = connectionMasterElementNameRegEx.match(conn.inputRef).captured(1);
    conn.inputTreatmentName = connectionTreatmentNameRegEx.match(conn.inputRef).captured(1);
    conn.inputQualifierName = connectionInputQualifierRegEx.match(conn.inputRef).captured(1);
}

void ScriptInitializer::treatPreparator(SequenceDesc &seq)
{
    auto match = preparatorRegEx.match(seq.code);
    if (match.hasMatch()) {
        seq.preparator.ref = match.captured(0);
        seq.preparator.name = match.captured("name");
        seq.preparator.type = match.captured("type");
        seq.preparator.code = match.captured("code");

        treatTreatments(seq.preparator);

        auto paramsIt = preparatorParamRegEx.globalMatch(seq.preparator.code);
        while (paramsIt.hasNext()) {
            auto paramMatch = paramsIt.next();

            ParameterDesc param;
            param.ref = paramMatch.captured(0);
            param.name = paramMatch.captured("name");
            param.content = paramMatch.captured("content");

            treatParameter(param);

            checkAndInsert(param, seq.preparator.parameters);
        }

        auto pathsIt = preparatorPathRegEx.globalMatch(seq.preparator.code);
        while (pathsIt.hasNext()) {
            auto pathMatch = pathsIt.next();

            seq.preparator.paths.append(pathMatch.captured("path"));
        }
    }
}

template<typename T>
void ScriptInitializer::treatTreatments(T &objDesc)
{
    auto it = treatmentRegEx.globalMatch(objDesc.code);
    while (it.hasNext()) {
        auto match = it.next();

        TreatmentDesc treatment;
        treatment.ref = match.captured(0);
        treatment.name = match.captured("name");
        treatment.config = match.captured("config");

        treatTreatment(treatment);

        checkAndInsert(treatment, objDesc.treatments);
    }
}

void ScriptInitializer::treatTreatment(TreatmentDesc &treatment)
{
    auto typeMatch = treatmentTypeRegEx.match(treatment.config);
    if (typeMatch.hasMatch())
        treatment.type = typeMatch.captured(1);
    else
        treatment.type = treatment.name;

    auto masterMatch = treatmentMasterElementSortRegEx.match(treatment.type);
    if (masterMatch.hasMatch() && masterMatch.lastCapturedIndex() == 2) {
        treatment.master = masterMatch.captured(1);
        treatment.type = masterMatch.captured(2);
    }

    auto paramsIt = treatmentParamRegEx.globalMatch(treatment.config);
    while (paramsIt.hasNext()) {
        auto match = paramsIt.next();

        ParameterDesc param;
        param.ref = match.captured(0);
        param.name = match.captured("name");
        param.content = match.captured("content");

        treatParameter(param);

        checkAndInsert(param, treatment.parameters);
    }
}

void ScriptInitializer::treatParameter(ScriptInitializer::ParameterDesc &param)
{
    QVariant value = treatVariable(param.content);

    if (!value.isValid()) {
        throw std::invalid_argument(QString("Invalid parameter: ..." + param.content + "...").toStdString());
    }

    param.value = value;
}

QVariant ScriptInitializer::treatVariable(const QString &var)
{
    QVariant value; // Invalid variant.

    auto booleanMatch = booleanRegEx.match(var);
    if (booleanMatch.hasMatch()) {
        if (booleanMatch.capturedRef(1) == "true")
            value = QVariant(true);
        else if (booleanMatch.capturedRef(1) == "false")
            value = QVariant(false);
    }

    auto numberMatch = numberRegEx.match(var);
    if (numberMatch.hasMatch()) {
        bool ok = true;

        int integerVal = numberMatch.capturedRef(1).toInt(&ok);
        if (ok) {
            value = QVariant(integerVal);
        }
        else {
            float floatingVal = numberMatch.capturedRef(1).toFloat(&ok);
            if (ok) {
                value = QVariant(floatingVal);
            }
        }
    }

    auto stringMatch = stringRegEx.match(var);
    if (stringMatch.hasMatch()) {
        value = QVariant(stringMatch.captured(1));
    }

    auto arrayMatch = arrayRegEx.match(var);
    if (arrayMatch.hasMatch()) {
        auto arrayVariables = arraySplitterRegEx.globalMatch(arrayMatch.capturedRef(0));

        QList<QVariant> listArray;
        bool valid = true;
        while (valid && arrayVariables.hasNext()) {
            auto currentVar = arrayVariables.next();
            QVariant currentVal = treatVariable(currentVar.captured(2));

            valid = currentVal.isValid();

            listArray.append(currentVal);
        }

        if (valid)
            value = QVariant(listArray);
    }

    return value;
}

void ScriptInitializer::buildModels()
{
    for (auto &model: models)
        buildModel(model);
}

void ScriptInitializer::buildModel(ScriptInitializer::ModelDesc &model)
{
    model.model = ModelsFactory::globalInstance().createModel(model.type);

    for (auto &param: model.parameters) {
        model.model->setProperty(param.name.toStdString().c_str(), param.value);
    }
}

void ScriptInitializer::buildSequences()
{
    for (auto &sequence: sequences)
        buildSequence(sequence);
}

void ScriptInitializer::buildSequence(ScriptInitializer::SequenceDesc &seq)
{
    buildPreparator(seq.preparator);

    seq.preparator.prep->prepare();

    QList<Preparator::PreparedTrack> preparedTracks = seq.preparator.prep->preparedTracks();
    QVector<Track*> tracks;
    tracks.reserve(preparedTracks.size());
    for (auto preparedTrack: preparedTracks)
        tracks.push_back(preparedTrack.track());

    for (auto preparedTrack: preparedTracks) {

        QMap<QString, Treatment*> treatments;
        for (auto &treatment: seq.treatments) {
            treatments.insert(treatment.name, buildTreatment(treatment, preparedTrack, tracks, seq));
        }

        for (auto &conn: seq.connections)
            buildConnection(conn, treatments, preparedTrack, seq);
    }
}

void ScriptInitializer::buildPreparator(ScriptInitializer::PreparatorDesc &preparator)
{
    //TODO PreparatorFactory
    if (preparator.type == "AudioFile") {
        preparator.prep = new AudioFilePreparator(this);
    }
    else if (preparator.type == "MidiFile") {
        auto prep = new MidiFilePreparator(this);
        prep->setAnalysis(MidiFilePreparator::Tracks); // TODO Remove when parameters are implemented.
        preparator.prep = prep;
    }
    else if (preparator.type == "SoundFont") {
        preparator.prep = new SoundFontPreparator(this);
    }
    else {
        throw std::invalid_argument(QString("No known type preparator \"" + preparator.type + "\".").toStdString());
    }

    bool ok = true;
    auto checkOk = [ok]{
        if (!ok)
            throw std::invalid_argument("Invalid preparator parameter.");
    };

    int sampleRate = preparator.parameters.value("sampleRate").value.toInt(&ok);
    checkOk();

    int frameSize = preparator.parameters.value("frameSize").value.toInt(&ok);
    checkOk();

    int hopSize = preparator.parameters.value("hopSize").value.toInt(&ok);
    checkOk();

    preparator.prep->setTracksProperties(sampleRate, frameSize, hopSize);

    for (QString path: preparator.paths)
        preparator.prep->add(path);

    //TODO Other preparator parameters.
}

Treatment *ScriptInitializer::buildTreatment(ScriptInitializer::TreatmentDesc &treatment, Preparator::PreparedTrack &preparedTrack, const QVector<Track*> &tracks, SequenceDesc &seq)
{
    Treatment *tr = nullptr;
    bool createdNow = false;
    if (treatment.master.isEmpty()) {
        TreatmentType::TrackOperation trackOperation = TreatmentType::MonoTrack;
        for (TreatmentType &type: TreatmentsFactory::availableTreatments()) {
            if (type.name() == treatment.type) {
                trackOperation = type.trackOperation();
                break;
            }
        }

        if (trackOperation != TreatmentType::MonoTrack) {
            tr = getTreatment(treatment.master, treatment.name, seq.commonTreatments, preparedTrack, seq);
        }

        if (tr == nullptr) {
            createdNow = true;

            switch (trackOperation) {
            case TreatmentType::MonoTrack:
                tr = TreatmentsFactory::globalInstance().createTreatment(treatment.type, preparedTrack.track(), this);
                break;
            case TreatmentType::MultiTrack:
                tr = TreatmentsFactory::globalInstance().createTreatment(treatment.type, tracks, this);
                break;
            case TreatmentType::NoTrack:
                tr = TreatmentsFactory::globalInstance().createTreatment(treatment.type, this);
                break;
            }

            if (trackOperation != TreatmentType::MonoTrack) {
                seq.commonTreatments.insert(treatment.name, tr);
            }
        }
    }
    else if (treatment.master == seq.preparator.name) {
        if (!preparedTrack.treatmentsNames().contains(treatment.type)) {
            throw std::invalid_argument("Preparator does not have \'" + treatment.type.toStdString() + "\'.");
        }

        tr = getTreatment(treatment.master, treatment.type, seq.commonTreatments, preparedTrack, seq);
        for (auto &param: treatment.parameters) {
            tr->setParameter(param.name, param.value);
        }
    }
    else {
        Model *model = models.value(treatment.master).model;
        if (model == nullptr) {
            throw std::invalid_argument("No model named \'" + treatment.master.toStdString() + "\'.");
        }

        TreatmentType::TrackOperation trackOperation = TreatmentType::MonoTrack;
        for (TreatmentType &type: TreatmentsFactory::availableTreatments()) {
            if (type.name() == treatment.type) {
                trackOperation = type.trackOperation();
                break;
            }
        }

        if (trackOperation != TreatmentType::MonoTrack) {
            tr = getTreatment(treatment.master, treatment.name, seq.commonTreatments, preparedTrack, seq);
        }

        if (tr == nullptr) {
            createdNow = true;

            switch (trackOperation) {
            case TreatmentType::MonoTrack:
                tr = TreatmentsFactory::globalInstance().createTreatment(treatment.type, model, preparedTrack.track(), this);
                break;
            case TreatmentType::MultiTrack:
                tr = TreatmentsFactory::globalInstance().createTreatment(treatment.type, model, tracks, this);
                break;
            case TreatmentType::NoTrack:
                tr = TreatmentsFactory::globalInstance().createTreatment(treatment.type, model, this);
                break;
            }

            if (trackOperation != TreatmentType::MonoTrack) {
                seq.commonTreatments.insert(treatment.name, tr);
            }
        }
    }

    if (createdNow) {

        for (auto &param: treatment.parameters) {
            tr->setParameter(param.name, param.value);
        }

        QVector<TreatmentType::DataInfo> outputs = tr->type().provide();
        for (auto output: outputs) {
            QString completeName = seq.name + '.' + treatment.name + '.' + output.name;
            tr->addOutputsMap(output.name, completeName);
        }

        Melodium::instance()->addTreatment(tr);
    }

    return tr;
}

void ScriptInitializer::buildConnection(ScriptInitializer::ConnectionDesc &conn, QMap<QString, Treatment *> &treatments, Preparator::PreparedTrack &preparedTrack, SequenceDesc &seq)
{
    Treatment *origin =
            getTreatment(conn.outputMasterElementName, conn.outputTreatmentName, treatments, preparedTrack, seq);
    Treatment *dest =
            getTreatment(conn.inputMasterElementName, conn.inputTreatmentName, treatments, preparedTrack, seq);

    if (origin == nullptr or dest == nullptr) {
        throw std::invalid_argument("Misidentified treatments for \'" + conn.outputTreatmentName.toStdString() + "\' to \'" +
                                    conn.inputTreatmentName.toStdString() + "\' connection.");
    }

    origin->addNext(dest);

    if (!conn.outputQualifierName.isNull() and !conn.inputQualifierName.isNull()) {
        dest->addInputsMap(conn.inputQualifierName,
                           origin->outputsMap().value(conn.outputQualifierName, conn.outputQualifierName));
    }
}

Treatment *ScriptInitializer::getTreatment(QString &masterElementName, QString &treatmentName, QMap<QString, Treatment *> &treatments, Preparator::PreparedTrack &preparedTrack, ScriptInitializer::SequenceDesc &seq)
{
    if (treatments.contains(treatmentName)) {
        return treatments.value(treatmentName, nullptr);
    }
    else if (seq.preparator.prep != nullptr and masterElementName == seq.preparator.name) {
        return preparedTrack.treatment(treatmentName);
    }
    else if (!masterElementName.isEmpty()) {
        return treatments.value(masterElementName + '[' + treatmentName + ']', nullptr);
    }
    else return nullptr;
}

template<typename T>
void ScriptInitializer::checkAndInsert(T &obj, QMap<QString, T> &map)
{
    if (map.contains(obj.name))
        throw std::invalid_argument("Name \"" + obj.name.toStdString() + "\" already declared.");
    else
        map.insert(obj.name, obj);
}
