#include "hpcpvisualizer.h"

#include "preparators/audiofilepreparator.h"
#include "preparators/soundfontpreparator.h"

HpcpVisualizer::HpcpVisualizer(QObject *parent) : Initializer(parent)
{

}

Initializer *HpcpVisualizer::create()
{
    return new HpcpVisualizer();
}

void HpcpVisualizer::initialize()
{
    bool ok = true;
    auto checkOk = [ok]{
        if (!ok)
            throw std::exception();
    };

    int sampleRate = args().at(0).toInt(&ok);
    checkOk();

    int frameSize = args().at(1).toInt(&ok);
    checkOk();

    int hopSize = args().at(2).toInt(&ok);
    checkOk();

    QString window = args().at(3);

    int harmonics = args().at(4).toInt(&ok);
    checkOk();

    QString weightType = args().at(5);

    int hpcpSize = args().at(6).toInt(&ok);
    checkOk();

    SoundFontPreparator prep(this);
    //AudioFilePreparator prep(this);
    prep.setTracksProperties(sampleRate, frameSize, hopSize);
    for (int i=4 ; i < args().size()-1 ; ++i)
        prep.add(args().at(i));

    prep.prepare();

    QList<Preparator::PreparedTrack> list = prep.preparedTracks();

    QString outputDir = args().last() + '/' +
            QString::number(sampleRate) + "Hz" +
            QString::number(frameSize) + "f" +
            QString::number(hopSize) + "h_" +
            window + '_' +
            QString::number(harmonics) + "hrm_" +
            weightType + '_' +
            QString::number(hpcpSize) + '/';
    QDir().mkpath(outputDir);

    auto exportCsv = [this](Track *track, Treatment *previous, QString data, QString path){
        Treatment *csv = TreatmentsFactory::globalInstance().createTreatment("CsvExporter", track, this);
        csv->setParameter("qualifiers", data);
        csv->setParameter("filename", path);

        Melodium::instance()->addTreatment(csv);
        previous->addNext(csv);
    };

    auto exportImage = [this](Track *track, Treatment *previous, QString data, QString path){
        Treatment *image = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", track, this);
        image->setParameter("filename", path);
        image->setParameter("autoScale", true);
        image->addInputsMap("data", data);

        Melodium::instance()->addTreatment(image);
        previous->addNext(image);
    };

    for (auto item: list) {
        QString baseName = QFileInfo(item.track()->audioFile()).completeBaseName();

        Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track(), this);
        framer->addInputsMap("signal", "audioSampleOutput");
        //framer->addInputsMap("signal", "audio");
        framer->setParameter("frameSize", frameSize);
        framer->setParameter("hopSize", hopSize);
        framer->setParameter("startFromZero", true);
        framer->setParameter("lastFrameToEndOfFile", true);
        Melodium::instance()->addTreatment(framer);
        item.treatment("SoundFontAnalyst")->addNext(framer);

        Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track(), this);
        windowing->addOutputsMap("frame", "frameWindow");
        windowing->setParameter("type", window);
        windowing->setParameter("size", frameSize);
        Melodium::instance()->addTreatment(windowing);
        framer->addNext(windowing);

        Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track(), this);
        spectrum->addInputsMap("frame", "frameWindow");
        Melodium::instance()->addTreatment(spectrum);
        windowing->addNext(spectrum);

        Treatment *spectralPeaks = TreatmentsFactory::globalInstance().createTreatment("SpectralPeaks", item.track(), this);
        spectralPeaks->setParameter("sampleRate", sampleRate);
        spectralPeaks->setParameter("orderBy", "magnitude");
        spectralPeaks->setParameter("magnitudeThreshold", 1e-05);
        spectralPeaks->setParameter("minFrequency", 40);
        spectralPeaks->setParameter("maxFrequency", 5000);
        spectralPeaks->setParameter("maxPeaks", 10000);
        spectralPeaks->addOutputsMap("frequencies", "spectralpeaksfrequencies");
        spectralPeaks->addOutputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(spectralPeaks);
        spectrum->addNext(spectralPeaks);
        exportCsv(item.track(), spectralPeaks, "spectralpeaksfrequencies", outputDir + baseName + ".spectralpeaksfrequencies.csv");
        //exportImage(item.track, spectralPeaks, "spectralpeaksfrequencies", outputDir + baseName + ".spectralpeaksfrequencies.png");
        exportCsv(item.track(), spectralPeaks, "spectralpeaksmagnitudes", outputDir + baseName + ".spectralpeaksmagnitudes.csv");
        //exportImage(item.track, spectralPeaks, "spectralpeaksmagnitudes", outputDir + baseName + ".spectralpeaksmagnitudes.png");

        Treatment *hpcp = TreatmentsFactory::globalInstance().createTreatment("HPCP", item.track(), this);
        hpcp->setParameter("sampleRate", sampleRate);
        hpcp->setParameter("windowSize", 0.5);
        //hpcp->setParameter("harmonics", 8);
        hpcp->setParameter("harmonics", harmonics);
        //hpcp->setParameter("weightType", "cosine");
        hpcp->setParameter("weightType", weightType);
        //hpcp->setParameter("size", 120);
        hpcp->setParameter("size", hpcpSize);
        hpcp->addInputsMap("frequencies", "spectralpeaksfrequencies");
        hpcp->addInputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(hpcp);
        spectralPeaks->addNext(hpcp);
        exportCsv(item.track(), hpcp, "hpcp", outputDir + baseName + ".hpcp.csv");
        exportImage(item.track(), hpcp, "hpcp", outputDir + baseName + ".hpcp.png");

        }
}
