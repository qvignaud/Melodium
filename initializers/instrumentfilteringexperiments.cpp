/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "instrumentfilteringexperiments.h"

InstrumentFilteringExperiments::InstrumentFilteringExperiments(QObject *parent) : Initializer(parent)
{

}

Initializer *InstrumentFilteringExperiments::create()
{
    return new InstrumentFilteringExperiments();
}

void InstrumentFilteringExperiments::initialize()
{
    bool ok = true;
    auto checkOk = [ok]{
        if (!ok)
            throw std::exception();
    };

    _sampleRate = args().at(0).toInt(&ok);
    checkOk();

    _frameSize = args().at(1).toInt(&ok);
    checkOk();

    _hopSize = args().at(2).toInt(&ok);
    checkOk();

    _window = args().at(3);

    _representations = args().at(4).split(',');
    _spectrumRequested = _representations.contains("spectrum", Qt::CaseInsensitive);
    _mfccRequested = _representations.contains("mfcc", Qt::CaseInsensitive);
    _gfccRequested = _representations.contains("gfcc", Qt::CaseInsensitive);

    _cumulation = args().at(5).toInt(&ok);
    checkOk();

    _method = args().at(5);

    _network = args().at(6);

    QString outputModelFile = args().last();

    MidiFilePreparator prep(this);
    prep.setAnalysis(MidiFilePreparator::Tracks);
    prep.setTracksProperties(_sampleRate, _frameSize, _hopSize);
    for (int i=7 ; i < args().size()-1 ; ++i)
        prep.add(args().at(i));
    prep.prepare();

    Model *model = ModelsFactory::globalInstance().createModel("ConfigurableRectFfn");
    Melodium::instance()->addModel(model);
    if (_network == "PReLU_3") {
        model->setProperty("layers", QStringList({"PReLU", "PReLU", "PReLU"}));
    }
    else if (_network == "PReLU_5") {
        model->setProperty("layers", QStringList({"PReLU", "PReLU", "PReLU", "PReLU", "PReLU"}));
    }
    else if (_network == "PReLU_10") {
        model->setProperty("layers", QStringList({"PReLU", "PReLU", "PReLU", "PReLU", "PReLU", "PReLU", "PReLU", "PReLU", "PReLU", "PReLU"}));
    }

    Treatment *trainer = TreatmentsFactory::globalInstance().createTreatment(
                model->type().trainingTreatmentType().name(),
                model, this);
    trainer->setParameter("trainings", 200);
    Melodium::instance()->addTreatment(trainer);
    Treatment *saver = TreatmentsFactory::globalInstance().createTreatment(
                model->type().savingTreatmentType().name(),
                model, this);
    saver->setParameter("filename", outputModelFile);
    Melodium::instance()->addTreatment(saver);
    trainer->addNext(saver);

    QList<Preparator::PreparedTrack> list = prep.preparedTracks();

    for (Preparator::PreparedTrack item: list) {

        Treatment *originalOutput = manageOriginalData(item);
        Treatment *alteredOutput = manageAlteredData(item);

        Treatment *originalAccumulator = TreatmentsFactory::globalInstance().createTreatment("Accumulator", item.track(), this);
        originalAccumulator->setParameter("cumulation", _cumulation);
        originalAccumulator->addInputsMap("input", "originalOutput");
        originalAccumulator->addOutputsMap("output", "cumuledOriginalOutput");
        Melodium::instance()->addTreatment(originalAccumulator);
        originalOutput->addNext(originalAccumulator);

        Treatment *alteredAccumulator = TreatmentsFactory::globalInstance().createTreatment("Accumulator", item.track(), this);
        alteredAccumulator->setParameter("cumulation", _cumulation);
        alteredAccumulator->addInputsMap("input", "alteredOutput");
        alteredAccumulator->addOutputsMap("output", "cumuledAlteredOutput");
        Melodium::instance()->addTreatment(alteredAccumulator);
        alteredOutput->addNext(alteredAccumulator);

        Treatment *feeder = TreatmentsFactory::globalInstance().createTreatment(
                    model->type().feedingTreatmentType().name(),
                    model, item.track(), this);
        feeder->addInputsMap("data", "cumuledAlteredOutput");
        feeder->addInputsMap("label", "cumuledOriginalOutput");
        Melodium::instance()->addTreatment(feeder);
        originalAccumulator->addNext(feeder);
        alteredAccumulator->addNext(feeder);
        feeder->addNext(trainer);

        Treatment *predictor = TreatmentsFactory::globalInstance().createTreatment(
                    model->type().predictingTreatmentType().name(),
                    model, item.track(), this);
        predictor->addInputsMap("data", "cumuledAlteredOutput");
        Melodium::instance()->addTreatment(predictor);
        trainer->addNext(predictor);
        alteredAccumulator->addNext(predictor);

        Treatment *imagerTruth = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", item.track(), this);
        imagerTruth->setParameter("filename", "/tmp/assignments/" + QFileInfo(item.track()->audioFile()).completeBaseName() + ".truth.png");
        imagerTruth->setParameter("autoScale", true);
        imagerTruth->addInputsMap("data", "cumuledOriginalOutput");
        Melodium::instance()->addTreatment(imagerTruth);
        originalAccumulator->addNext(imagerTruth);

        Treatment *imagerData = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", item.track(), this);
        imagerData->setParameter("filename", "/tmp/assignments/" + QFileInfo(item.track()->audioFile()).completeBaseName() + ".data.png");
        imagerData->setParameter("autoScale", true);
        imagerData->addInputsMap("data", "cumuledAlteredOutput");
        Melodium::instance()->addTreatment(imagerData);
        alteredAccumulator->addNext(imagerData);

        Treatment *imagerAssignments = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", item.track(), this);
        imagerAssignments->setParameter("filename", "/tmp/assignments/" + QFileInfo(item.track()->audioFile()).completeBaseName() + ".assignments.png");
        imagerAssignments->setParameter("autoScale", true);
        imagerAssignments->addInputsMap("data", "assignments");
        Melodium::instance()->addTreatment(imagerAssignments);
        predictor->addNext(imagerAssignments);
    }
}

Treatment *InstrumentFilteringExperiments::manageOriginalData(Preparator::PreparedTrack &item)
{
    Treatment *originalOutput = nullptr;

    Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track(), this);
    framer->addInputsMap("signal", "audio");
    framer->setParameter("frameSize", _frameSize);
    framer->setParameter("hopSize", _hopSize);
    framer->setParameter("startFromZero", true);
    framer->setParameter("lastFrameToEndOfFile", true);
    Melodium::instance()->addTreatment(framer);
    item.treatment("MonoLoader")->addNext(framer);

    Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track(), this);
    windowing->addOutputsMap("frame", "frameWindow");
    windowing->setParameter("type", _window);
    windowing->setParameter("size", _frameSize);
    Melodium::instance()->addTreatment(windowing);
    framer->addNext(windowing);

    Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track(), this);
    spectrum->addInputsMap("frame", "frameWindow");
    Melodium::instance()->addTreatment(spectrum);
    windowing->addNext(spectrum);

    Treatment *mfcc = nullptr;
    if (_mfccRequested) {
        mfcc = TreatmentsFactory::globalInstance().createTreatment("MFCC", item.track(), this);
        mfcc->setParameter("inputSize", _frameSize);
        mfcc->setParameter("sampleRate", _sampleRate);
        mfcc->setParameter("highFrequencyBound", _sampleRate/2);
        mfcc->addOutputsMap("bands", "mfccBands");
        Melodium::instance()->addTreatment(mfcc);
        spectrum->addNext(mfcc);
    }
    Treatment *gfcc = nullptr;
    if (_gfccRequested) {
        gfcc = TreatmentsFactory::globalInstance().createTreatment("GFCC", item.track(), this);
        gfcc->setParameter("inputSize", _frameSize);
        gfcc->setParameter("sampleRate", _sampleRate);
        gfcc->setParameter("highFrequencyBound", _sampleRate/2);
        gfcc->addOutputsMap("bands", "gfccBands");
        Melodium::instance()->addTreatment(gfcc);
        spectrum->addNext(gfcc);
    }

    if (_spectrumRequested && !(_mfccRequested || _gfccRequested)) {
        spectrum->addOutputsMap("spectrum", "originalOutput");
        originalOutput = spectrum;
    }
    else if (_mfccRequested && _gfccRequested) {
        Treatment *concat = TreatmentsFactory::globalInstance().createTreatment("Concatenator", item.track(), this);
        concat->addInputsMap("first", "mfccBands");
        concat->addInputsMap("second", "gfccBands");
        Melodium::instance()->addTreatment(concat);
        mfcc->addNext(concat);
        gfcc->addNext(concat);
        originalOutput = concat;

        if (_spectrumRequested) {
            originalOutput->addOutputsMap("output", "partialOutput");
            Treatment *concat = TreatmentsFactory::globalInstance().createTreatment("Concatenator", item.track(), this);
            concat->addInputsMap("first", "spectrum");
            concat->addInputsMap("second", "partialOutput");
            concat->addOutputsMap("output", "originalOutput");
            Melodium::instance()->addTreatment(concat);
            spectrum->addNext(concat);
            originalOutput->addNext(concat);
            originalOutput = concat;
        }
    }
    else if (_spectrumRequested && (_mfccRequested ^ _gfccRequested)) {
        Treatment *concat = TreatmentsFactory::globalInstance().createTreatment("Concatenator", item.track(), this);
        concat->addInputsMap("first", "spectrum");
        concat->addOutputsMap("output", "originalOutput");
        Melodium::instance()->addTreatment(concat);
        spectrum->addNext(concat);
        originalOutput = concat;

        if (_mfccRequested) {
            concat->addInputsMap("second", "mfccBands");
            mfcc->addNext(concat);
        }
        if (_gfccRequested) {
            concat->addInputsMap("second", "gfccBands");
            gfcc->addNext(concat);
        }
    }
    else if (_mfccRequested) {
        mfcc->addOutputsMap("bands", "originalOutput");
        originalOutput = mfcc;
    }
    else if (_gfccRequested) {
        gfcc->addOutputsMap("bands", "originalOutput");
        originalOutput = gfcc;
    }

    return originalOutput;
}

Treatment *InstrumentFilteringExperiments::manageAlteredData(Preparator::PreparedTrack &item)
{
    Treatment *alteredOutput = nullptr;

    // _method == "noise"
    Treatment *noise = TreatmentsFactory::globalInstance().createTreatment("NoiseAdder", item.track(), this);
    noise->addInputsMap("signal", "audio");
    noise->addOutputsMap("signal", "alteredAudio");
    Melodium::instance()->addTreatment(noise);
    item.treatment("MonoLoader")->addNext(noise);

    Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track(), this);
    framer->addInputsMap("signal", "alteredAudio");
    framer->setParameter("frameSize", _frameSize);
    framer->setParameter("hopSize", _hopSize);
    framer->setParameter("startFromZero", true);
    framer->setParameter("lastFrameToEndOfFile", true);
    framer->addOutputsMap("frame", "alteredFrame");
    Melodium::instance()->addTreatment(framer);
    noise->addNext(framer);

    Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track(), this);
    windowing->addInputsMap("frame", "alteredFrame");
    windowing->addOutputsMap("frame", "alteredFrameWindow");
    windowing->setParameter("type", _window);
    windowing->setParameter("size", _frameSize);
    Melodium::instance()->addTreatment(windowing);
    framer->addNext(windowing);

    Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track(), this);
    spectrum->addInputsMap("frame", "alteredFrameWindow");
    spectrum->addOutputsMap("spectrum", "alteredSpectrum");
    Melodium::instance()->addTreatment(spectrum);
    windowing->addNext(spectrum);

    Treatment *mfcc = nullptr;
    if (_mfccRequested) {
        mfcc = TreatmentsFactory::globalInstance().createTreatment("MFCC", item.track(), this);
        mfcc->addInputsMap("spectrum", "alteredSpectrum");
        mfcc->setParameter("inputSize", _frameSize);
        mfcc->setParameter("sampleRate", _sampleRate);
        mfcc->setParameter("highFrequencyBound", _sampleRate/2);
        mfcc->addOutputsMap("bands", "alteredMfcc");
        Melodium::instance()->addTreatment(mfcc);
        spectrum->addNext(mfcc);
    }
    Treatment *gfcc = nullptr;
    if (_gfccRequested) {
        gfcc = TreatmentsFactory::globalInstance().createTreatment("GFCC", item.track(), this);
        gfcc->addInputsMap("spectrum", "alteredSpectrum");
        gfcc->setParameter("inputSize", _frameSize);
        gfcc->setParameter("sampleRate", _sampleRate);
        gfcc->setParameter("highFrequencyBound", _sampleRate/2);
        gfcc->addOutputsMap("bands", "alteredGfcc");
        Melodium::instance()->addTreatment(gfcc);
        spectrum->addNext(gfcc);
    }

    if (_spectrumRequested && !(_mfccRequested || _gfccRequested)) {
        spectrum->addOutputsMap("spectrum", "alteredOutput");
        alteredOutput = spectrum;
    }
    else if (_mfccRequested && _gfccRequested) {
        Treatment *concat = TreatmentsFactory::globalInstance().createTreatment("Concatenator", item.track(), this);
        concat->addInputsMap("first", "alteredMfcc");
        concat->addInputsMap("second", "alteredGfcc");
        concat->addOutputsMap("output", "alteredOutput");
        Melodium::instance()->addTreatment(concat);
        mfcc->addNext(concat);
        gfcc->addNext(concat);
        alteredOutput = concat;

        if (_spectrumRequested) {
            alteredOutput->addOutputsMap("output", "alteredPartialOutput");
            Treatment *concat = TreatmentsFactory::globalInstance().createTreatment("Concatenator", item.track(), this);
            concat->addInputsMap("first", "alteredSpectrum");
            concat->addInputsMap("second", "alteredPartialOutput");
            concat->addOutputsMap("output", "alteredOutput");
            Melodium::instance()->addTreatment(concat);
            spectrum->addNext(concat);
            alteredOutput->addNext(concat);
            alteredOutput = concat;
        }
    }
    else if (_spectrumRequested && (_mfccRequested ^ _gfccRequested)) {
        Treatment *concat = TreatmentsFactory::globalInstance().createTreatment("Concatenator", item.track(), this);
        concat->addInputsMap("first", "alteredSpectrum");
        concat->addOutputsMap("output", "alteredOutput");
        Melodium::instance()->addTreatment(concat);
        spectrum->addNext(concat);
        alteredOutput = concat;

        if (_mfccRequested) {
            concat->addInputsMap("second", "alteredMfcc");
            mfcc->addNext(concat);
        }
        if (_gfccRequested) {
            concat->addInputsMap("second", "alteredGfcc");
            gfcc->addNext(concat);
        }
    }
    else if (_mfccRequested) {
        mfcc->addOutputsMap("bands", "alteredOutput");
        alteredOutput = mfcc;
    }
    else if (_gfccRequested) {
        gfcc->addOutputsMap("bands", "alteredOutput");
        alteredOutput = gfcc;
    }

    return alteredOutput;
}
