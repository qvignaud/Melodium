/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef NOTESESTIMATIONEXPERIMENT_H
#define NOTESESTIMATIONEXPERIMENT_H

#include "initializer.h"
#include <QMetaEnum>

class NotesEstimationExperiment : public Initializer
{
    Q_OBJECT
public:
    enum ExperimentCase {
        FOR_5_TRIALS_3_LAYERS_PRELU,
        FOR_5_TRIALS_5_LAYERS_PRELU,
        FOR_5_TRIALS_7_LAYERS_PRELU,
        FOR_100_TRIALS_7_LAYERS_PRELU,
        FOR_1000_TRIALS_7_LAYERS_PRELU,
        FOR_5_TRIALS_10_LAYERS_PRELU,
        FOR_100_TRIALS_10_LAYERS_PRELU,
        FOR_1000_TRIALS_10_LAYERS_PRELU,
        FOR_20_TRIALS_5_REDUC_PRELU,
        FOR_20_TRIALS_7_REDUC_PRELU,

        FOR_100_TRIALS_7_LAYERS_BILINEARINTERPOLATION,
        FOR_100_TRIALS_7_LAYERS_FASTLSTM,
        FOR_100_TRIALS_7_LAYERS_IDENTITY,
        FOR_100_TRIALS_7_LAYERS_LINEAR,
        FOR_100_TRIALS_7_LAYERS_LOGSOFTMAX,
        FOR_100_TRIALS_7_LAYERS_LSTM,
        FOR_100_TRIALS_7_LAYERS_MAXPOOLING,
        FOR_100_TRIALS_7_LAYERS_MEANPOOLING,
        FOR_100_TRIALS_7_LAYERS_NEGATIVELOGLIKELIHOOD,
        FOR_100_TRIALS_7_LAYERS_RELU,
        FOR_100_TRIALS_7_LAYERS_SIGMOID,
        FOR_100_TRIALS_7_LAYERS_TANH
    };
    Q_ENUM(ExperimentCase)

    explicit NotesEstimationExperiment(QObject *parent = nullptr);

    static Initializer *create();

signals:

public slots:
    void initialize();

private:
    static QStringList repeat(const QString &string, size_t times);
    static QList<QVariant> decrease(size_t start, size_t end, size_t steps);
};

#endif // NOTESESTIMATIONEXPERIMENT_H
