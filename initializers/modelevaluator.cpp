/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "modelevaluator.h"

#include "modelsfactory.h"

#include "preparators/midifilepreparator.h"

ModelEvaluator::ModelEvaluator(QObject *parent) : Initializer(parent)
{

}

Initializer *ModelEvaluator::create()
{
    return new ModelEvaluator();
}

void ModelEvaluator::initialize()
{
    bool ok = true;
    auto checkOk = [ok]{
        if (!ok)
            throw std::exception();
    };

    int sampleRate = args().at(0).toInt(&ok);
    checkOk();

    int frameSize = args().at(1).toInt(&ok);
    checkOk();

    int hopSize = args().at(2).toInt(&ok);
    checkOk();

    int hpcpSize = args().at(3).toInt(&ok);
    checkOk();

    int accumulation = args().at(4).toInt(&ok);
    checkOk();

    QString fileModel = args().at(5);

    QString layerFunction = args().at(6);

    int layers = args().at(7).toInt(&ok);
    checkOk();

    QString outputDir = args().last();
    QDir().mkpath(outputDir);

    Model *model = ModelsFactory::globalInstance().createModel("ConfigurableRectFfn");
    model->setProperty("inputSize", hpcpSize*(accumulation+1));
    model->setProperty("layersSize", hpcpSize*(accumulation+1));
    model->setProperty("outputSize", 128);
    {
        QStringList l;
        for (int i=0 ; i < layers ; ++i)
            l << layerFunction;
        model->setProperty("layers", l);
    }

    Melodium::instance()->addModel(model);
    Treatment *loader = TreatmentsFactory::globalInstance().createTreatment(
                model->type().loadingTreatmentType().name(),
                model, this);
    loader->setParameter("filename", fileModel);
    Melodium::instance()->addTreatment(loader);

    MidiFilePreparator prep(this);
    prep.setAnalysis(MidiFilePreparator::Tracks);
    prep.setTracksProperties(sampleRate, frameSize, hopSize);

    for (int i=8 ; i < args().size()-1 ; ++i)
        prep.add(args().at(i));

    prep.prepare();

    QList<Preparator::PreparedTrack> list = prep.preparedTracks();

    for (auto item: list) {
        QString baseName = QFileInfo(item.track()->originalMidiFile()).baseName();

        item.treatment("MidiTracksAnalyst")->setParameter("presenceOnly", true);

        Treatment *framer = TreatmentsFactory::globalInstance().createTreatment("FrameCutter", item.track(), this);
        framer->addInputsMap("signal", "audio");
        framer->setParameter("frameSize", frameSize);
        framer->setParameter("hopSize", hopSize);
        framer->setParameter("startFromZero", true);
        framer->setParameter("lastFrameToEndOfFile", true);
        Melodium::instance()->addTreatment(framer);
        item.treatment("MonoLoader")->addNext(framer);

        Treatment *windowing = TreatmentsFactory::globalInstance().createTreatment("Windowing", item.track(), this);
        windowing->addOutputsMap("frame", "frameWindow");
        windowing->setParameter("type", "blackmanharris92");
        windowing->setParameter("size", frameSize);
        Melodium::instance()->addTreatment(windowing);
        framer->addNext(windowing);

        Treatment *spectrum = TreatmentsFactory::globalInstance().createTreatment("Spectrum", item.track(), this);
        spectrum->addInputsMap("frame", "frameWindow");
        Melodium::instance()->addTreatment(spectrum);
        windowing->addNext(spectrum);

        Treatment *spectralPeaks = TreatmentsFactory::globalInstance().createTreatment("SpectralPeaks", item.track(), this);
        spectralPeaks->setParameter("sampleRate", sampleRate);
        spectralPeaks->setParameter("orderBy", "magnitude");
        spectralPeaks->setParameter("magnitudeThreshold", 1e-05);
        spectralPeaks->setParameter("minFrequency", 40);
        spectralPeaks->setParameter("maxFrequency", 5000);
        spectralPeaks->setParameter("maxPeaks", 10000);
        spectralPeaks->addOutputsMap("frequencies", "spectralpeaksfrequencies");
        spectralPeaks->addOutputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(spectralPeaks);
        spectrum->addNext(spectralPeaks);

        Treatment *hpcp = TreatmentsFactory::globalInstance().createTreatment("HPCP", item.track(), this);
        hpcp->setParameter("sampleRate", sampleRate);
        hpcp->setParameter("windowSize", 0.5);
        hpcp->setParameter("harmonics", 8);
        hpcp->setParameter("weightType", "cosine");
        hpcp->setParameter("size", hpcpSize);
        hpcp->addInputsMap("frequencies", "spectralpeaksfrequencies");
        hpcp->addInputsMap("magnitudes", "spectralpeaksmagnitudes");
        Melodium::instance()->addTreatment(hpcp);
        spectralPeaks->addNext(hpcp);

        Treatment *accumulator = TreatmentsFactory::globalInstance().createTreatment("Accumulator", item.track(), this);
        accumulator->setParameter("cumulation", accumulation);
        accumulator->addInputsMap("input", "hpcp");
        accumulator->addOutputsMap("output", "cumuledHpcp");
        Melodium::instance()->addTreatment(accumulator);
        hpcp->addNext(accumulator);

        Treatment *predictor = TreatmentsFactory::globalInstance().createTreatment(
                    model->type().predictingTreatmentType().name(),
                    model, item.track(), this);
        predictor->addInputsMap("data", "cumuledHpcp");
        Melodium::instance()->addTreatment(predictor);
        loader->addNext(predictor);
        accumulator->addNext(predictor);
        //predictor->addNext(csvExport(item.track(), {"assignments"}, outputDir + "/" + baseName + ".assignments.csv"));
        predictor->addNext(imageExport(item.track(), "assignments", outputDir + "/" + baseName + ".assignments.png"));

        Treatment *threshold = TreatmentsFactory::globalInstance().createTreatment("Thresholder", item.track(), this);
        threshold->addInputsMap("input", "assignments");
        threshold->addOutputsMap("output", "results");
        Melodium::instance()->addTreatment(threshold);
        predictor->addNext(threshold);

        threshold->addNext(csvExport(item.track(), {"results"}, outputDir + "/" + baseName + ".results.csv"));
        threshold->addNext(imageExport(item.track(), "results", outputDir + "/" + baseName + ".results.png"));

        Treatment *evaluator = TreatmentsFactory::globalInstance().createTreatment("Evaluator", QVector<Track*>({item.track()}), this);
        evaluator->addInputsMap("values", "results");
        evaluator->addInputsMap("ground", "midiTrackContent1");
        evaluator->setParameter("output", outputDir + '/' + baseName + ".eval.csv");
        Melodium::instance()->addTreatment(evaluator);
        item.treatment("MidiTracksAnalyst")->addNext(evaluator);
        threshold->addNext(evaluator);

        Treatment *differentiator = TreatmentsFactory::globalInstance().createTreatment("Differentiator", item.track(), this);
        differentiator->addInputsMap("values", "results");
        differentiator->addInputsMap("ground", "midiTrackContent1");
        differentiator->addOutputsMap("output", "diff");
        Melodium::instance()->addTreatment(differentiator);
        item.treatment("MidiTracksAnalyst")->addNext(differentiator);
        threshold->addNext(differentiator);

        differentiator->addNext(csvExport(item.track(), {"diff"}, outputDir + "/" + baseName + ".diff.csv"));
        differentiator->addNext(imageExport(item.track(), "diff", outputDir + "/" + baseName + ".diff.png"));

    }
}
