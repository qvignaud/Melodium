/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "treatmentsfactoryexception.h"

TreatmentsFactoryException::TreatmentsFactoryException(const Type type, const QString cause) :
    _type(type),
    _cause(cause)
{

}

void TreatmentsFactoryException::raise() const
{
    throw *this;
}

TreatmentsFactoryException *TreatmentsFactoryException::clone() const
{
    return new TreatmentsFactoryException(*this);
}

TreatmentsFactoryException::Type TreatmentsFactoryException::type() const
{
    return _type;
}

QString TreatmentsFactoryException::cause() const
{
    return _cause;
}

const char *TreatmentsFactoryException::what() const noexcept
{
    static std::string _stdCause = _cause.toStdString();
    return _stdCause.c_str();
}
