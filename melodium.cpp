/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "melodium.h"

#include "initializersfactory.h"
#include "modelsfactory.h"

#include "runnablescheme.h"

#include "scheduler.h"
#include "schedulers/monothreadscheduler.h"
#include "schedulers/multithreadscheduler.h"

Melodium::Melodium(int &argc, char **argv) : QGuiApplication(argc, argv), cout(stdout), cerr(stderr)
{
    essentia::init();

    setApplicationName("Mélodium");
    setApplicationVersion("proto-0");

    _cmdOptionsOk = true;
    _cmdInstanciateInitializer = true;

    _graph = false;

    _noExec = false;

    _scheduler = nullptr;

    _treatmentsFinished = 0;
    _treatmentsFailed = 0;

    initCommandLineParser();

    connect(this, &Melodium::treatmentFinished, this, &Melodium::incrementTreatmentsFinished);
    connect(this, &Melodium::treatmentFailed, this, &Melodium::printFailure);
    connect(this, &Melodium::treatmentFailed, this, &Melodium::incrementTreatmentsFailed);
}

Melodium::~Melodium()
{
    // Treatments, tracks and models are deleted through the Qt-parent/children procedure.

    essentia::shutdown();
}

int Melodium::exec()
{
    _commandLineParser.process(arguments());

    cmdOptionThreads();
    cmdOptionInfo();
    cmdOptionGraph();
    cmdOptionNoExec();

    if (_cmdOptionsOk && _cmdInstanciateInitializer) {
        QStringList positionalArgs = _commandLineParser.positionalArguments();

        if (positionalArgs.size() >= 1) {
            Initializer *init = InitializersFactory::globalInstance().createInitializer(positionalArgs.at(0));
            init->setParent(this);

            QStringList initializerArguments(positionalArgs);
            initializerArguments.removeFirst();
            init->setArgs(initializerArguments);

            if (init != nullptr) {

                try {
                    init->initialize();
                } catch (TrackException &te) {
                    cerr << "An error occured during initialization, ";
                    switch (te.type()) {
                    case TrackException::InvalidSampleRate:
                        cerr << static_cast<qint64>(te.invalidSize())
                             << "is not a valid sample rate."
                             << endl;
                        break;
                    case TrackException::InvalidFrameSize:
                        cerr << static_cast<qint64>(te.invalidSize())
                             << "is not a valid frame size."
                             << endl;
                        break;
                    case TrackException::InvalidHopSize:
                        cerr << static_cast<qint64>(te.invalidSize())
                             << "is not a valid hop size."
                             << endl;
                        break;
                    case TrackException::ZeroFrames:
                        cerr << "file '" << te.causeName()
                             << "' is too short ("
                             << static_cast<quint64>(te.invalidSize())
                             << " samples) and doesn't allow to create at least one frame."
                             << endl;
                        break;
                    default:
                        cerr << "the initializer failed to manage consistent qualifiers."
                             << endl;
                        break;

                    }

                    return EXIT_FAILURE;
                }

                RunnableScheme rs;
                rs.check();

                if (rs.hasErrors()) {
                    rs.printNotifications(cerr);
                    cerr << "Running cancelled." << endl;

                    return EXIT_FAILURE;
                }

                cout << _tracks.size() << " tracks to treat, "
                     << _treatments.size() << " treatments to proceed."
                     << endl;

                if (_graph) {
                    cout << "Generating execution graph." << endl;
                    RunnableScheme::makeGraph(_graphFilename);
                }

                if (!_noExec) {
                    initScheduler();

                    printProgress();

                    _scheduler->launch();
                }
                else {
                    cout << "No execution." << endl;
                    return EXIT_SUCCESS;
                }

            }
            else {
                cerr << "\"" << positionalArgs.at(0) << "\" is not an initializer." << endl;
                _commandLineParser.showHelp(EXIT_FAILURE);
            }
        }
        else {
            cerr << "No initializer provided." << endl;
            _commandLineParser.showHelp(EXIT_FAILURE);
        }
    }
    else if (_cmdOptionsOk && !_cmdInstanciateInitializer) {
        return EXIT_SUCCESS;
    }
    else if (!_cmdOptionsOk) {
        _commandLineParser.showHelp(EXIT_FAILURE);
    }

    return QCoreApplication::exec();
}

void Melodium::addTrack(Track *track)
{
    track->setParent(this);

    connect(track, &Track::originalMidiFileChanged, this, &Melodium::manageTrackOriginalMidiFileChanged);
    connect(track, &Track::audioFileChanged, this, &Melodium::manageTrackAudioFileChanged);
    connect(track, &Track::midiFileChanged, this, &Melodium::manageTrackMidiFileChanged);

    connect(track, &Track::qualifiersAdded, this, &Melodium::trackQualifiersAdded);
    connect(track, &Track::globalQualifiersAdded, this, &Melodium::trackGlobalQualifiersAdded);
    connect(track, &Track::globalRealQualifiersAdded, this, &Melodium::trackGlobalRealQualifiersAdded);
    connect(track, &Track::globalStringQualifiersAdded, this, &Melodium::trackGlobalStringQualifiersAdded);
    connect(track, &Track::simpleQualifiersAdded, this, &Melodium::trackSimpleQualifiersAdded);
    connect(track, &Track::simpleRealQualifiersAdded, this, &Melodium::trackSimpleRealQualifiersAdded);
    connect(track, &Track::simpleStringQualifiersAdded, this, &Melodium::trackSimpleStringQualifiersAdded);
    connect(track, &Track::detailedQualifiersAdded, this, &Melodium::trackDetailedQualifiersAdded);
    connect(track, &Track::detailedRealQualifiersAdded, this, &Melodium::trackDetailedRealQualifiersAdded);
    connect(track, &Track::detailedStringQualifiersAdded, this, &Melodium::trackDetailedStringQualifiersAdded);

    _tracks.append(track);

    emit trackAdded(track);
}

QList<Track *> Melodium::tracks() const
{
    return _tracks;
}

void Melodium::addTreatment(Treatment *treatment)
{
    treatment->setParent(this);

    connect(treatment, &Treatment::ready, this, &Melodium::treatmentReady);
    connect(treatment, &Treatment::started, this, &Melodium::treatmentStarted);
    connect(treatment, &Treatment::finished, this, &Melodium::treatmentFinished);
    connect(treatment, &Treatment::failed, this, &Melodium::treatmentFailed);

    _treatments.append(treatment);

    emit treatmentAdded(treatment);
}

QList<Treatment *> Melodium::treatments() const
{
    return _treatments;
}

void Melodium::addModel(Model *model)
{
    model->setParent(this);

    connect(model, &Model::feedingFinished, this, &Melodium::modelFeedingFinished);

    _models.append(model);

    emit modelAdded(model);
}

QList<Model *> Melodium::models() const
{
    return _models;
}

Melodium *Melodium::instance()
{
    return static_cast<Melodium *>(QCoreApplication::instance());
}

void Melodium::initCommandLineParser()
{
    _commandLineParser.setApplicationDescription("Mélodium © Quentin Vignaud 2018-2019\nComputes audio/musical data to extract and recover musical data from audio files using signal analysis and machine learning.");

    _commandLineParser.addPositionalArgument("initializer [params]", "Initializer to use and its parameters.");

    _commandLineParser.addOption({"list-initializers", "List available initializers."});
    _commandLineParser.addOption({"list-models", "List available models."});
    _commandLineParser.addOption({"list-treatments", "List available treatments. Can be 'audiorender', 'converter', 'analysis', 'feeder', 'trainer', 'classifier', 'exporter', or 'all'.", "role"});

    _commandLineParser.addOption({"info-treatments", "Get informations about treatments, separated by commas, or by role.", "name/role"});

    _commandLineParser.addOption({{"g", "graph"}, "Create the graph representing execution scheme, in DOT/GraphViz format.", "filename"});

    _commandLineParser.addOption({{"t", "threads"}, "Number of threads to work with. Default is " + QString::number(QThreadPool::globalInstance()->maxThreadCount()) + ".", "threads"});

    _commandLineParser.addOption({{"n", "noexec"}, "Do not execute anything, just initialize and check, or generate graph if used with --graph. It is implied by --list and --info."});

    _commandLineParser.addHelpOption();
    _commandLineParser.addVersionOption();
}

void Melodium::cmdOptionInfo()
{

    bool listInitializersSet = _commandLineParser.isSet("list-initializers");
    bool listModelsSet = _commandLineParser.isSet("list-models");
    bool listTreatmentsSet = _commandLineParser.isSet("list-treatments");
    bool infoTreatmentsSet = _commandLineParser.isSet("info-treatments");

    int count = 0;
    count += listInitializersSet;
    count += listModelsSet;
    count += listTreatmentsSet;
    count += infoTreatmentsSet;

    if (count <= 0) {
        return;
    }
    else if (count > 1) {
        cerr << "Incompatible options. Please use only once of '--list-*' and '--info-*' options." << endl;
        _cmdOptionsOk = false;
    }
    else {
        _cmdInstanciateInitializer = false;

        if (listInitializersSet) {
            QStringList initializers = InitializersFactory::availableInitializers();
            for (QString initializer: initializers)
                cout << initializer << endl;
        }
        else if (listModelsSet) {
            QList<ModelType> models = ModelsFactory::availableModels();
            for (ModelType model: models)
                cout << model.name() << endl;
        }
        else if (listTreatmentsSet or infoTreatmentsSet) {
            QStringList asked;
            if (listTreatmentsSet)
                asked = _commandLineParser.value("list-treatments").split(',');
            if (infoTreatmentsSet)
                asked = _commandLineParser.value("info-treatments").split(',');

            QStringList roles;
            roles
                    << "all"
                    << "audiorender"
                    << "converter"
                    << "analysis"
                    << "feeder"
                    << "trainer"
                    << "classifier"
                    << "exporter";
            bool isRole = false;
            for (QString role: roles)
                if (asked.contains(role, Qt::CaseInsensitive))
                    isRole = true;

            QList<TreatmentType> treatments;
            if (isRole) {
                for (QString role: asked) {
                    if (role.compare("all", Qt::CaseInsensitive) == 0)
                        treatments = TreatmentsFactory::availableTreatments();
                    else if (role.compare("audiorender", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::AudioRender));
                    else if (role.compare("converter", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::Converter));
                    else if (role.compare("analysis", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::Analysis));
                    else if (role.compare("modelfeeder", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::ModelFeeder));
                    else if (role.compare("modeltrainer", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::ModelTrainer));
                    else if (role.compare("modelpredictor", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::ModelPredictor));
                    else if (role.compare("modelsaver", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::ModelSaver));
                    else if (role.compare("modelloader", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::ModelLoader));
                    else if (role.compare("exporter", Qt::CaseInsensitive) == 0)
                        treatments.append(TreatmentsFactory::availableTreatments(TreatmentType::DataExporter));
                    else {
                        cerr << "'" << role << "' is not a role." << endl;
                        _cmdOptionsOk = false;
                        return;
                    }
                }
            }
            else {
                QList<TreatmentType> availableTreatments = TreatmentsFactory::availableTreatments();
                for (QString name: asked) {
                    bool found = false;
                    for (TreatmentType type: availableTreatments) {
                        if (type.name().compare(name, Qt::CaseInsensitive) == 0) {
                            treatments << type;
                            found = true;
                        }
                    }
                    if (!found) {
                        cerr << "'" << name << "' is not a treatment." << endl;
                        _cmdOptionsOk = false;
                        return;
                    }
                }
            }

            if (listTreatmentsSet)
                TreatmentInfoPrinter::simplePrint(treatments);
            else if (infoTreatmentsSet)
                TreatmentInfoPrinter::print(treatments);
        }
    }

}

void Melodium::cmdOptionGraph()
{
    if (_commandLineParser.isSet("graph")) {
        _graph = true;
        _graphFilename = _commandLineParser.value("graph");
        if (_graphFilename.isEmpty())
            _cmdOptionsOk = false;
    }
}

void Melodium::cmdOptionThreads()
{
    QString threads = _commandLineParser.value("threads");

    if (!threads.isEmpty()) {
        bool ok = true;
        int number = threads.toInt(&ok, 0);

        if (ok && number >= 1) {
            QThreadPool::globalInstance()->setMaxThreadCount(number);
        }
        else {
            cout << "'" << threads << "' is not a valid number of threads." << endl;
            _cmdOptionsOk = false;
        }
    }
}

void Melodium::cmdOptionNoExec()
{
    if (_commandLineParser.isSet("noexec")) {
        _noExec = true;
    }
}

void Melodium::initScheduler()
{
    if (QThreadPool::globalInstance()->maxThreadCount() == 1) {
        _scheduler = new MonoThreadScheduler(this);
        cout << "Processing in mono-thread mode." << endl;
    }
    else {
        _scheduler = new MultiThreadScheduler(this);
        cout << "Processing with " << QThreadPool::globalInstance()->maxThreadCount() << " treads." << endl;
    }

    connect(_scheduler, &Scheduler::finished, this, &Melodium::schedulerFinished);
}

void Melodium::printProgress()
{
    cout << '\r' << QString::number(_treatmentsFinished)
         << '/' << QString::number(_treatments.size())
         << " treatments finished, ";
    if (_treatmentsFailed > 0)
        cout << QString::number(_treatmentsFailed) << " failed, ";
    cout << QString::number(static_cast<double>(_treatmentsFinished) * 100.0 / static_cast<double>(_treatments.size()), 'f', 1)
         << "% done.";
    cout.flush();
}

void Melodium::printFailure(Treatment *treatment)
{
    cerr << "\nTreatment '" << treatment->type().name() << "' failed.\n";
    if (treatment->runnable()->initError()) {
        cerr << "Initialization failure: " << treatment->runnable()->initErrorMessage() << endl;
    }
    if (treatment->runnable()->workError()) {
        cerr << "Working failure: " << treatment->runnable()->workErrorMessage() << endl;
    }
    switch (treatment->type().trackOperation()) {
    case TreatmentType::MonoTrack:
        cerr << "Track informations: \n" << treatment->track()->info() << endl;
        break;
    case TreatmentType::MultiTrack:
        cerr << "Tracks informations: \n";
        for (auto track: treatment->tracks())
            cerr << track->info() << '\n';
        cerr.flush();
        break;
    case TreatmentType::NoTrack:
        break;

    }
}

void Melodium::manageTrackOriginalMidiFileChanged(QString originalMidiFile, Track *track)
{
    Q_UNUSED(originalMidiFile);
    emit trackOriginalMidiFileChanged(track);
}

void Melodium::manageTrackAudioFileChanged(QString audioFile, Track *track)
{
    Q_UNUSED(audioFile);
    emit trackAudioFileChanged(track);
}

void Melodium::manageTrackMidiFileChanged(QString midiFile, Track *track)
{
    Q_UNUSED(midiFile);
    emit trackMidiFileChanged(track);
}

void Melodium::incrementTreatmentsFinished()
{
    _treatmentsFinished++;
    printProgress();
}

void Melodium::incrementTreatmentsFailed()
{
    _treatmentsFailed++;
    printProgress();
}

void Melodium::schedulerFinished()
{
    cout << "\nAll treatments done." << endl;
    quit();
}

