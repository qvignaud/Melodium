/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef TREATMENTSFACTORY_H
#define TREATMENTSFACTORY_H

#include <QObject>
#include <QList>
#include <QStringList>
#include "treatmenttype.h"
#include "treatment.h"
#include "model.h"
#include "treatmentsfactoryexception.h"

/*!
 * \brief Treatments factory class.
 *
 * It is better in most cases use the globalInstance of the factory instead of instanciating a new one.
 */
class TreatmentsFactory : public QObject
{
    Q_OBJECT

public:

    /*!
     * \brief Instanciates a treatment factory.
     *
     * \note In most cases, it is better to use the globalInstance, unless it is wanted to have a separated factory to generate treatments hidden from the whole program.
     * \sa globalInstance
     */
    explicit TreatmentsFactory(QObject *parent=nullptr);
    ~TreatmentsFactory();

    /*!
     * \brief Creates the matching treatment.
     * \param name      Name of the treatment.
     * \param parent    Parent object of the treatment & runnable.
     * \return  The created treatment.
     *
     * This function instanciate both the treatment and the runnable linked to.
     */
    Treatment *createTreatment(const QString &name, QObject *parent = nullptr);
    /*!
     * \brief Creates the matching treatment.
     * \param name      Name of the treatment.
     * \param track     Track linked to the runnable.
     * \param parent    Parent object of the treatment & runnable.
     * \return  The created treatment.
     *
     * This function instanciate both the treatment and the runnable linked to.
     */
    Treatment *createTreatment(const QString &name, Track *track, QObject *parent = nullptr);
    /*!
     * \brief Creates the matching treatment.
     * \param name      Name of the treatment.
     * \param tracks    Tracks linked to the runnable.
     * \param parent    Parent object of the treatment & runnable.
     * \return  The created treatment.
     *
     * This function instanciate both the treatment and the runnable linked to.
     */
    Treatment *createTreatment(const QString &name, const QVector<Track *> &tracks, QObject *parent = nullptr);
    /*!
     * \brief Creates the matching treatment.
     * \param name      Name of the treatment.
     * \param model     Model working with the treatment.
     * \param parent    Parent object of the treatment & runnable.
     * \return  The created treatment.
     *
     * This function instanciate both the treatment and the runnable linked to.
     */
    Treatment *createTreatment(const QString &name, Model *model, QObject *parent = nullptr);
    /*!
     * \brief Creates the matching treatment.
     * \param name      Name of the treatment.
     * \param model     Model working with the treatment.
     * \param track     Track linked to the runnable.
     * \param parent    Parent object of the treatment & runnable.
     * \return  The created treatment.
     *
     * This function instanciate both the treatment and the runnable linked to.
     */
    Treatment *createTreatment(const QString &name, Model *model, Track *track, QObject *parent = nullptr);
    /*!
     * \brief Creates the matching treatment.
     * \param name      Name of the treatment.
     * \param model     Model working with the treatment.
     * \param tracks    Tracks linked to the runnable.
     * \param parent    Parent object of the treatment & runnable.
     * \return  The created treatment.
     *
     * This function instanciate both the treatment and the runnable linked to.
     */
    Treatment *createTreatment(const QString &name, Model *model, const QVector<Track *> &tracks, QObject *parent = nullptr);


    /*!
     * \brief Treatments the factory created.
     * \return List of treatments.
     */
    QList<Treatment *> treatments() const;
    /*!
     * \brief Treatments the factory created.
     * \param type Type of treatments to return.
     * \return List of treatments.
     * \overloads treatments()
     */
    QList<Treatment *> treatments(TreatmentType::Role role) const;

    /*!
     * \brief Treatments available.
     * \return A list of the available treatments.
     */
    static QList<TreatmentType> availableTreatments();
    /*!
     * \brief Treatments available.
     * \param type  Filter the type of treatments to return.
     * \return A list of available treatments matching with type.
     */
    static QList<TreatmentType> availableTreatments(TreatmentType::Role role);

    /*!
     * \brief Add a no-track treatment to the factory.
     * \param type Type of treatment.
     * \param initializer Initialization function that returns a new runnable to use in the treatment.
     */
    static void addAvailableTreatment(TreatmentType type, Runnable* (*initializer)(TreatmentType, QObject*));
    /*!
     * \brief Add a mono-track treatment to the factory.
     * \param type Type of treatment.
     * \param initializer Initialization function that returns a new runnable to use in the treatment.
     */
    static void addAvailableTreatment(TreatmentType type, Runnable* (*initializer)(TreatmentType, Track*, QObject*));
    /*!
     * \brief Add a multi-track treatment to the factory.
     * \param type Type of treatment.
     * \param initializer Initialization function that returns a new runnable to use in the treatment.
     */
    static void addAvailableTreatment(TreatmentType type, Runnable* (*initializer)(TreatmentType, const QVector<Track*>&, QObject*));
    /*!
     * \brief Add a no-track treatment from model to the factory.
     * \param type Type of treatment.
     * \param initializer Initialization function that returns a new runnable to use in the treatment.
     */
    static void addAvailableTreatment(TreatmentType type, Runnable* (Model::*initializer)(TreatmentType, QObject*));
    /*!
     * \brief Add a mono-track treatment from model to the factory.
     * \param type Type of treatment.
     * \param initializer Initialization function that returns a new runnable to use in the treatment.
     */
    static void addAvailableTreatment(TreatmentType type, Runnable* (Model::*initializer)(TreatmentType, Track*, QObject*));
    /*!
     * \brief Add a multi-track treatment from model to the factory.
     * \param type Type of treatment.
     * \param initializer Initialization function that returns a new runnable to use in the treatment.
     */
    static void addAvailableTreatment(TreatmentType type, Runnable* (Model::*initializer)(TreatmentType, const QVector<Track*>&, QObject*));

    /*!
     * \brief Get the global instance.
     * \return
     */
    static TreatmentsFactory& globalInstance();

private:
    /*!
     * \brief Union used as pointer to runnable initializer.
     */
    union Initializer {
        Runnable* (*noTrack)(TreatmentType, QObject*);
        Runnable* (*monoTrack)(TreatmentType, Track*, QObject*);
        Runnable* (*multiTrack)(TreatmentType, const QVector<Track*>&, QObject*);
        Runnable* (Model::*noTrackModel)(TreatmentType, QObject*);
        Runnable* (Model::*monoTrackModel)(TreatmentType, Track*, QObject*);
        Runnable* (Model::*multiTrackModel)(TreatmentType, const QVector<Track*>&, QObject*);
    };

    /*!
     * \brief Structure coupling available treatment with initializer function.
     */
    class AvailableTreatment : public TreatmentType {
    public:
        AvailableTreatment(TreatmentType &type);
        ///Initializer of the runnable.
        Initializer initializer;
    };

    /*!
     * \brief List of treatments created by the factory.
     */
    QList<Treatment *> _treatments;

    /*!
     * \brief Create and register treatment.
     * \param runnable
     * \return
     */
    Treatment *createTreatment(TreatmentType tt, Runnable *runnable);

    /*!
     * \brief List of the available treatments.
     */
    static QList<AvailableTreatment> _availableTreatments;
    /*!
     * \brief Tells if the treatment is registered.
     * \param name
     * \return
     */
    static bool registeredAvailableTreatment(const QString &name);
    /*!
     * \brief Return the avaliable treatment matching that name.
     * \param name
     * \return
     */
    static AvailableTreatment findAvailableTreatment(const QString &name);

private slots:
    /*!
     * \brief Handles destruction of a treatment.
     * \param treatment
     *
     * This function remove the treatment from the list of treatments created by the factory.
     */
    void treatmentDestroyed(QObject *treatment);
};


#endif // TREATMENTSFACTORY_H
