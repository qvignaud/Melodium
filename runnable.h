/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef RUNNABLE_H
#define RUNNABLE_H

#include <QObject>
#include <QRunnable>
#include <QVector>
#include <QMap>
#include "track.h"

/*!
 * \brief Base class for runnable objects.
 */
class Runnable : public QObject, public QRunnable
{
    Q_OBJECT
public:

    /*!
     * \brief Status of a runnable.
     */
    enum Status {
        ///The runnable is waiting to be launched.
        Waiting,
        ///The runnable is running.
        Running,
        ///The runnable finished successfully.
        Finished,
        ///The runnable failed.
        Failed
    };

    /*!
     * \brief Instanciates a new runnable.
     *
     * This constructor implies the runnable is no-track.
     *
     * \param name      Name of the runnable.
     * \param parent    Parent object.
     */
    Runnable(QString name, QObject *parent = nullptr);
    /*!
     * \brief Instanciates a new runnable.
     *
     * This constructor implies the runnable is mono-track.
     *
     * \param name      Name of the runnable.
     * \param track     Track to which the runnable is linked.
     * \param parent    Parent object.
     */
    Runnable(QString name, Track *track, QObject *parent = nullptr);
    /*!
     * \brief Instanciates a new runnable.
     *
     * \param name      Name of the runnable.
     * \param tracks    Track(s) to which the runnable is linked, can be empty if none.
     * \param parent    Parent object.
     */
    Runnable(QString name, const QVector<Track *> &tracks, QObject *parent = nullptr);

    /*!
     * \brief Name of what is processed by the runnable.
     * \return A string with the name.
     */
    QString name() const;

    /*!
     * \brief Tell the status of the runnable.
     * \return The status of the runnable.
     */
    Status status() const;

    /*!
     * \brief Get track linked to the runnable.
     * \return The track the runnable is linked to.
     * \note Calling this method on multi-track or no-tracks runnable is undefined behavior.
     */
    Track *track() const;

    /*!
     * \brief Get the tracks linked to the runnable.
     * \return Track(s) linked to the runnable, can be empty if none.
     */
    QVector<Track *> tracks() const;

    /*!
     * \brief Map of matching inputs.
     *
     * The key is the name of the input for the runnable, the value is the qualifier name to look for in track.
     */
    QMap<QString, QString> inputsMap() const;
    /*!
     * \brief Get the name used for an input.
     * \param ownName   Name used by the runnable.
     * \return Name of qualifier to use.
     */
    QString inputName(const QString &ownName) const;
    /*!
     * \brief Add an input map entry.
     * \param ownName       Name for the runnable.
     * \param externalName  Name of qualifier to use.
     */
    void addInputsMap(const QString &ownName, const QString &externalName);
    /*!
     * \brief Add entries into the input map.
     * \param inputsMap
     */
    void addInputsMap(const QMap<QString, QString> &inputsMap);
    /*!
     * \brief Set inputs map.
     * \param inputsMap
     */
    void setInputsMap(const QMap<QString, QString> &inputsMap);

    /*!
     * \brief Map of matching outputs.
     *
     * The key is the name of the output for the runnable, the value is the qualifier name to store in track.
     */
    QMap<QString, QString> outputsMap() const;
    /*!
     * \brief Get the name used for an output.
     * \param ownName   Name used by the runnable.
     * \return Name of qualifier to store.
     */
    QString outputName(const QString &ownName) const;
    /*!
     * \brief Add an output map entry.
     * \param ownName       Name for the runnable.
     * \param externalName  Name of qualifier to store.
     */
    void addOutputsMap(const QString &ownName, const QString &externalName);
    /*!
     * \brief Add entries in output maps.
     * \param outputsMap
     */
    void addOutputsMap(const QMap<QString, QString> &outputsMap);
    /*!
     * \brief Set ouputs map.
     * \param outputsMap
     */
    void setOutputsMap(const QMap<QString, QString> &outputsMap);

    /*!
     * \brief Run the... runnable.
     *
     */
    void run() final;

    /*!
     * \brief Tell if initialization error occured.
     * \return  true if error occured, else false.
     */
    bool initError() const;
    /*!
     * \brief Give the initialization error message.
     * \return The initialization error message, or an empty string.
     */
    QString initErrorMessage() const;

    /*!
     * \brief Tell if working error occured.
     * \return true if error occured, else false.
     */
    bool workError() const;
    /*!
     * \brief Give the working error message.
     * \return The working error message, or an empty string.
     */
    QString workErrorMessage() const;

signals:
    /*!
     * \brief Status of the runnable changed.
     * \param status    New status.
     */
    void statusChanged(Status status);
    /*!
     * \brief Runnable started.
     */
    void started();
    /*!
     * \brief Runnable finished.
     *
     * \info This signal is **not** emitted if the runnable fail.
     * \sa failed
     */
    void finished();
    /*!
     * \brief Runnable failed.
     */
    void failed();

protected:

    /*!
     * \brief Initialization function.
     *
     * Runnables have to make initialization in there.
     *
     * \return  true if all was successful, else false.
     */
    virtual bool init() = 0;
    /*!
     * \brief Working function.
     *
     * Runnables have to implement work here.
     *
     * \return  true if all was successful, else false.
     */
    virtual bool work() = 0;

    /*!
     * \brief Set initialization error message.
     * \param initErrorMessage  The error message.
     */
    void setInitErrorMessage(const QString &initErrorMessage);

    /*!
     * \brief Set working error message.
     * \param workErrorMessage  The error message.
     */
    void setWorkErrorMessage(const QString &workErrorMessage);

private:

    /*!
     * \brief Runnable name.
     */
    const QString _name;

    /*!
     * \brief Set the status of the runnable.
     * \param status
     */
    void setStatus(const Status status);

    /*!
     * \brief Status of the runnable.
     */
    Status _status;

    /*!
     * \brief Tracks the runnable is related to.
     */
    const QVector<Track *> _tracks;

    /*!
     * \brief Map of matching inputs.
     *
     * The key is the name of the input for the runnable, the value is the qualifier name to look for in track.
     */
    QMap<QString, QString> _inputsMap;

    /*!
     * \brief Map of matching outputs.
     *
     * The key is the name of the output for the runnable, the value is the qualifier name to store in track.
     */
    QMap<QString, QString> _outputsMap;

    /*!
     * \brief Initialization error mark.
     */
    bool _initError;
    /*!
     * \brief Initialization error message.
     */
    QString _initErrorMessage;

    /*!
     * \brief Working error mark.
     */
    bool _workError;
    /*!
     * \brief Working error message.
     */
    QString _workErrorMessage;
};

#endif // RUNNABLE_H
