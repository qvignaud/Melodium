/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef TREATMENT_H
#define TREATMENT_H

#include <QObject>
#include <QVector>
#include "treatmenttype.h"
#include "runnable.h"
#include "track.h"

/*!
 * \brief Treatment instance class.
 *
 * Wrap different kind of runnables and manage their execution.
 */
class Treatment : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Describe what to do with output qualifiers of the treatment.
     *
     * This is useful do tell what behavior must be followed to avoid unreasonable memory growth.
     * The NoClearing value should be only used for debug purposes, as qualifiers will then stay in associated track until the end of the program (or something else remove it).
     */
    enum QualifiersClearance {
        /// Qualifiers aren't cleared.
        NoClearing,
        /// Qualifiers are cleared after all (directly) next treatments are finished.
        ClearAfterNextTreatmentsFinished
    };

    /*!
     * \brief Instanciates a new treatment.
     * \param type      Type of runnable.
     * \param parent    Parent object.
     */
    explicit Treatment(const TreatmentType type, Runnable *runnable, QObject *parent = nullptr);
    ~Treatment();

    /*!
     * \brief Get the type of treatment.
     * \return
     */
    const TreatmentType type() const;

    /*!
     * \brief Get the track treatment is related to.
     * \return The track linked to this treatment.
     *
     * \note This is a convenience function equivalent to runnable()->track().
     * \warning Calling this method on multi-track or no-tracks treatment is undefined behavior.
     */
    Track *track() const;

    /*!
     * \brief Get the tracks treatment is related to.
     * \return The tracks linked to this treatment.
     *
     * \note This is a convenience function equivalent to runnable()->tracks().
     */
    QVector<Track *> tracks() const;

    /*!
     * \brief Get the runnable this treatment corresponds to.
     * \return The runnable designated by this treatment.
     */
    Runnable *runnable() const;

    /*!
     * \brief Get the value of a parameter.
     * \param name
     * \return Parameter asked.
     *
     * \note To obtain default value of a parameter, use type() instead.
     */
    QVariant parameter(const QString &name) const;
    /*!
     * \brief Set value of a parameter.
     * \param name
     * \param value
     */
    void setParameter(const QString &name, const QVariant &value);

    /*!
     * \brief Map of matching inputs.
     *
     * The key is the name of the input for the runnable, the value is the qualifier name to look for in track.
     * \see Runnable::inputsMap
     */
    QMap<QString, QString> inputsMap() const;
    /*!
     * \brief Add an input map entry.
     * \param ownName       Name for the runnable.
     * \param externalName  Name of qualifier to use.
     * \see  Runnable::addInputsMap
     */
    void addInputsMap(const QString &ownName, const QString &externalName);
    /*!
     * \brief Add entries into the input map.
     * \param inputsMap
     * \see  Runnable::addInputsMap
     */
    void addInputsMap(const QMap<QString, QString> &inputsMap);
    /*!
     * \brief Set inputs map.
     * \param inputsMap
     * \see Runnable::setInputsMap
     */
    void setInputsMap(const QMap<QString, QString> &inputsMap);

    /*!
     * \brief Map of matching outputs.
     *
     * The key is the name of the output for the runnable, the value is the qualifier name to store in track.
     * \see Runnable::outputsMap
     */
    QMap<QString, QString> outputsMap() const;
    /*!
     * \brief Add an output map entry.
     * \param ownName       Name for the runnable.
     * \param externalName  Name of qualifier to store.
     * \see Runnable::addOutputsMap
     */
    void addOutputsMap(const QString &ownName, const QString &externalName);
    /*!
     * \brief Add entries in output maps.
     * \param outputsMap
     * \see Runnable::addOutputsMap
     */
    void addOutputsMap(const QMap<QString, QString> &outputsMap);
    /*!
     * \brief Set ouputs map.
     * \param outputsMap
     * \see Runnable::setOutputsMap
     */
    void setOutputsMap(const QMap<QString, QString> &outputsMap);

    /*!
     * \brief Get number of treatments prior that one.
     * \return
     */
    unsigned int previousCount() const;

    /*!
     * \brief Get number of treatments having to be finished before that one can be run.
     * \return
     */
    unsigned int previousRemaining() const;

    /*!
     * \brief Tells if the treatment is ready to execute.
     * \return
     *
     * This is a convenience function equivalent to test if previousRemaining() equals 0 and the runnable is waiting.
     * \sa previousRemaining
     * \sa Runnable::status
     */
    bool isReady() const;

    /*!
     * \brief Get treatments directly following the current one.
     * \return A vector of treatments that have to be done when this one is finished.
     */
    QVector<Treatment *> next() const;
    /*!
     * \brief Get number of treatments directly following that one.
     * \return
     */
    unsigned int nextCount() const;
    /*!
     * \brief Set the whole list of treatments to do next that one.
     * \param next
     *
     * \note This list replace the previous one.
     */
    void setNext(const QVector<Treatment *> &next);
    /*!
     * \brief Add a treatment to do after that one is finished.
     * \param treatment
     */
    void addNext(Treatment *treatment);

    /*!
     * \brief Tell behavior that will be used for clearing qualifiers.
     * \default ClearAfterNextTreatmentsFinished
     */
    QualifiersClearance qualifiersClearance() const;
    /*!
     * \brief Set output qualifiers clearance behavior.
     * \param qualifiersClearance
     */
    void setQualifiersClearance(const QualifiersClearance &qualifiersClearance);

public slots:
    /*!
     * \brief Remove a treatment from the next list.
     * \param treatment typed as QObject for use with destroyed(), will be internally casted to Treatment.
     */
    void removeNext(QObject *treatment);
    /*!
     * \brief Clear
     */
    void clearNext();

signals:
    /*!
     * \brief All previous treatments has finished.
     */
    void ready(Treatment *treatment);

    /*!
     * \brief The treatment has started.
     * \param treatment
     *
     * \sa Runnable::started
     */
    void started(Treatment *treatment);
    /*!
     * \brief The treatment has finished.
     * \param treatment
     *
     * \note This signal is **not** emitted when the treatment fail.
     *
     * \sa failed
     * \sa Runnable::finished
     */
    void finished(Treatment *treatment);
    /*!
     * \brief The treatment has failed.
     * \param treatment
     *
     * \sa Runnable::failed
     */
    void failed(Treatment *treatment);

private:

    /*!
     * \brief Type of treatment.
     */
    const TreatmentType _type;

    /*!
     * \brief Runnable the treatment is related to.
     */
    Runnable *const _runnable;

    /*!
     * \brief Number of previous treaments required.
     */
    unsigned int _previous;

    /*!
     * \brief Number of previous treatments remaining.
     *
     * This number cannot exceed _previous, and 0 means all previous work is done and treatment can be run.
     */
    unsigned int _previousRemaining;

    /*!
     * \brief Vector of treatments to do after the current one finish.
     */
    QVector<Treatment *> _next;

    /*!
     * \brief Number of next treatments that finished.
     */
    unsigned int _nextFinished;

    /*!
     * \brief Tell behavior to use.
     * \default ClearAfterNextTreatmentsFinished
     */
    QualifiersClearance _qualifiersClearance;

    /*!
     * \brief Clear the qualifiers the treatment added in its associated track.
     */
    void clearQualifiers();

private slots:
    /*!
     * \brief Manage the start of the linked runnable.
     */
    void manageRunnableStarted();
    /*!
     * \brief Manage the end of the linked runnable.
     *
     * If there is no next treatments it calls clearQualifiers().
     */
    void manageRunnableFinished();
    /*!
     * \brief Manage the failure of the linked runnable.
     */
    void manageRunnableFailed();

    /*!
     * \brief Manage the finishing of a following treatment.
     * \param next
     *
     * If all next treatments have finished, it calls clearQualifiers().
     */
    void manageNextFinished(Treatment *next);
};

#endif // TREATMENT_H
