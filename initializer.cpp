/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "initializer.h"

Initializer::Initializer(QObject *parent) : QObject(parent)
{

}

QStringList Initializer::args() const
{
    return _args;
}

void Initializer::setArgs(const QStringList &args)
{
    _args = args;
}

Treatment *Initializer::csvExport(Track *track, const QStringList &qualifiers, const QString &filename)
{
    Treatment *exporter = TreatmentsFactory::globalInstance().createTreatment("CsvExporter", track);
    exporter->setParameter("qualifiers", qualifiers);
    exporter->setParameter("filename", filename);
    Melodium::instance()->addTreatment(exporter);

    return exporter;
}

Treatment *Initializer::imageExport(Track *track, const QString &data, const QString &filename)
{
    Treatment *imager = TreatmentsFactory::globalInstance().createTreatment("ImageExporter", track);
    imager->setParameter("filename", filename);
    imager->setParameter("autoScale", true);
    imager->addInputsMap("data", data);
    Melodium::instance()->addTreatment(imager);

    return imager;
}
