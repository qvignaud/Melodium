/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MODELTYPE_H
#define MODELTYPE_H

#include <QObject>
#include "treatmenttype.h"

class ModelType
{
    Q_GADGET
public:
    /*!
     * \brief Abilities of the model.
     */
    enum ModelAbility {
        Feeding =       1 << 0,
        Training =      1 << 1,
        Predicting =    1 << 2,
        Saving =        1 << 3,
        Loading =       1 << 4
    };
    Q_ENUM(ModelAbility)
    Q_DECLARE_FLAGS(ModelAbilities, ModelAbility)

    ModelType(QString name, ModelAbilities abilities);

    QString name() const;

    ModelAbilities abilities() const;

    QString description() const;
    void setDescription(const QString &description);

    TreatmentType feedingTreatmentType() const;
    void setFeedingTreatmentType(const TreatmentType &feedingTreatmentType);

    TreatmentType trainingTreatmentType() const;
    void setTrainingTreatmentType(const TreatmentType &trainingTreatmentType);

    TreatmentType predictingTreatmentType() const;
    void setPredictingTreatmentType(const TreatmentType &predictingTreatmentType);

    TreatmentType savingTreatmentType() const;
    void setSavingTreatmentType(const TreatmentType &savingTreatmentType);

    TreatmentType loadingTreatmentType() const;
    void setLoadingTreatmentType(const TreatmentType &loadingTreatmentType);

private:
    QString _name;
    ModelAbilities _abilities;
    QString _description;

    TreatmentType _feedingTreatmentType;
    TreatmentType _trainingTreatmentType;
    TreatmentType _predictingTreatmentType;
    TreatmentType _savingTreatmentType;
    TreatmentType _loadingTreatmentType;

};

#endif // MODELTYPE_H
