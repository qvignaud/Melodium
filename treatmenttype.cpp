/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "treatmenttype.h"

TreatmentType::TreatmentType(QString name, Role role, TrackOperation trackOperation) :
    _name(name),
    _role(role),
    _trackOperation(trackOperation)
{

}

QString TreatmentType::name() const
{
    return _name;
}

TreatmentType::Role TreatmentType::role() const
{
    return _role;
}

TreatmentType::TrackOperation TreatmentType::trackOperation() const
{
    return _trackOperation;
}

bool TreatmentType::isFromModel() const
{
    return (_role == ModelFeeder or
            _role == ModelTrainer or
            _role == ModelPredictor or
            _role == ModelSaver or
            _role == ModelLoader);
}

QString TreatmentType::description() const
{
    return _description;
}

void TreatmentType::setDescription(const QString &description)
{
    _description = description;
}

QVector<TreatmentType::DataInfo> TreatmentType::require() const
{
    return _require;
}

void TreatmentType::addRequire(const TreatmentType::DataInfo &require)
{
    _require.append(require);
}

void TreatmentType::setRequire(const QVector<DataInfo> &require)
{
    _require = require;
}

QVector<TreatmentType::ParamInfo> TreatmentType::parameters() const
{
    return _parameters;
}

void TreatmentType::addParameter(const TreatmentType::ParamInfo &parameter)
{
    _parameters.append(parameter);
}

void TreatmentType::setParameters(const QVector<ParamInfo> &parameters)
{
    _parameters = parameters;
}

QVector<TreatmentType::DataInfo> TreatmentType::provide() const
{
    return _provide;
}

void TreatmentType::addProvide(const TreatmentType::DataInfo &provide)
{
    _provide.append(provide);
}

void TreatmentType::setProvide(const QVector<DataInfo> &provide)
{
    _provide = provide;
}

#include <QMetaEnum>
std::ostream &operator<<(std::ostream &out, const TreatmentType::Role role)
{
    return out << QMetaEnum::fromType<TreatmentType::Role>().valueToKey(role);
}

std::ostream &operator<<(std::ostream &out, const TreatmentType::DataType type)
{
    return out << QMetaEnum::fromType<TreatmentType::DataType>().valueToKey(type);
}
