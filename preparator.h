/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef PREPARATOR_H
#define PREPARATOR_H

#include <QObject>
#include <QHash>
#include <QFileInfo>
#include <QDir>
#include "melodium.h"
#include "treatmentsfactory.h"
#include "preparatorexception.h"

/*!
 * \brief Abstract class for preparators.
 *
 * A preparator is aimed to take files and prepare the work required to use them as track.
 */
class Preparator : public QObject
{
    Q_OBJECT
protected:
    /*!
     * \brief Combine a track and the treatments preparing it.
     */
    struct PreparingTrack {
        /*!
         * \brief Track prepared.
         */
        Track *track;
        /*!
         * \brief Treatments exposed by the preparator.
         *
         * Use Treatment::addNext on them to append treatments to do after the track is prepared.
         */
        QHash<QString, Treatment*> treatments;
    };
public:

    class PreparedTrack : private PreparingTrack {
    public:
        PreparedTrack(const PreparingTrack &pt);

        Track *track() const;
        Treatment *treatment(const QString &name) const;
        QList<QString> treatmentsNames() const;
    };

    /*!
     * \brief Preparator constructor.
     * \param parent
     */
    explicit Preparator(QObject *parent = nullptr);

    /*!
     * \brief Add a filename or directory to prepare.
     * \param string
     */
    void add(const QString &string);
    /*!
     * \brief Add a file (or directory) to prepare.
     * \param file
     */
    void add(QFileInfo &file);

    /*!
     * \brief Get the files to prepare/prepared.
     * \return
     */
    QFileInfoList filesList() const;

    /*!
     * \brief Set the properties of the tracks which will be created and prepared.
     * \param sampleRate
     * \param frameSize
     * \param hopSize
     */
    void setTracksProperties(int sampleRate, int frameSize, int hopSize);

    /*!
     * \brief Sample rate of tracks.
     * \return
     * \sa Track::sampleRate
     */
    int tracksSampleRate() const;

    /*!
     * \brief Frame size of tracks.
     * \return
     * \sa Track::frameSize
     */
    int tracksFrameSize() const;

    /*!
     * \brief Hop size of tracks.
     * \return
     * \sa Track::hopSize
     */
    int tracksHopSize() const;

    /*!
     * \brief Get the list of prepared tracks.
     * \return
     *
     * This list is empty until prepare() is called.
     */
    QList<PreparedTrack> preparedTracks() const;

signals:

public slots:
    /*!
     * \brief Prepare tracks.
     */
    virtual void prepare() = 0;

protected:

    /*!
     * \brief List of files to prepare/prepared.
     */
    QFileInfoList _filesList;

    /*!
     * \brief Sample rate of tracks.
     * \sa Track::sampleRate
     */
    int _tracksSampleRate;
    /*!
     * \brief Frame size of tracks.
     * \sa Track::frameSize
     */
    int _tracksFrameSize;
    /*!
     * \brief Hop size of tracks.
     * \sa Track::hopSize
     */
    int _tracksHopSize;

    /*!
     * \brief List of prepared tracks.
     */
    QList<PreparedTrack> _preparedTracks;

    /*!
     * \brief Instanciates a new track.
     * \return new Track.
     *
     * This is a convenience function, equivalent to new Track(_tracksSampleRate, _tracksFrameSize, _tracksHopSize).
     */
    Track *newTrack();

    /*!
     * \brief Recurses directories in files list.
     *
     * Look for all directories in _filesList, and expand them with their contained files, recusively.
     *
     * This is a convenience function.
     */
    void recurseDirectories();


};

#endif // PREPARATOR_H
