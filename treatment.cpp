/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "treatment.h"
#include "melodium.h"

Treatment::Treatment(const TreatmentType type, Runnable *runnable, QObject *parent) : QObject(parent), _type(type), _runnable(runnable)
{
    _previous = 0;
    _previousRemaining = 0;
    _nextFinished = 0;
    _qualifiersClearance = QualifiersClearance::ClearAfterNextTreatmentsFinished;

    QVector<TreatmentType::ParamInfo> params = _type.parameters();
    for (auto param: params) {
        setParameter(param.name, param.defaultValue);
    }

    connect(_runnable, &Runnable::started, this, &Treatment::manageRunnableStarted, Qt::QueuedConnection);
    connect(_runnable, &Runnable::finished, this, &Treatment::manageRunnableFinished, Qt::QueuedConnection);
    connect(_runnable, &Runnable::failed, this, &Treatment::manageRunnableFailed, Qt::QueuedConnection);
}

Treatment::~Treatment()
{
    if (!Melodium::instance()->closingDown()) {
        clearNext();
        delete _runnable;
    }
}

const TreatmentType Treatment::type() const
{
    return _type;
}

Track *Treatment::track() const
{
    return _runnable->track();
}

QVector<Track *> Treatment::tracks() const
{
    return _runnable->tracks();
}

Runnable *Treatment::runnable() const
{
    return _runnable;
}

QVariant Treatment::parameter(const QString &name) const
{
    return _runnable->property(name.toStdString().c_str());
}

void Treatment::setParameter(const QString &name, const QVariant &value)
{
    _runnable->setProperty(name.toStdString().c_str(), value);
}

QMap<QString, QString> Treatment::inputsMap() const
{
    return _runnable->inputsMap();
}

void Treatment::addInputsMap(const QString &ownName, const QString &externalName)
{
    _runnable->addInputsMap(ownName, externalName);
}

void Treatment::addInputsMap(const QMap<QString, QString> &inputsMap)
{
    _runnable->addInputsMap(inputsMap);
}

void Treatment::setInputsMap(const QMap<QString, QString> &inputsMap)
{
    _runnable->setInputsMap(inputsMap);
}

QMap<QString, QString> Treatment::outputsMap() const
{
    return _runnable->outputsMap();
}

void Treatment::addOutputsMap(const QString &ownName, const QString &externalName)
{
    _runnable->addOutputsMap(ownName, externalName);
}

void Treatment::addOutputsMap(const QMap<QString, QString> &outputsMap)
{
    _runnable->addOutputsMap(outputsMap);
}

void Treatment::setOutputsMap(const QMap<QString, QString> &outputsMap)
{
    _runnable->setOutputsMap(outputsMap);
}

unsigned int Treatment::previousCount() const
{
    return _previous;
}

unsigned int Treatment::previousRemaining() const
{
    return _previousRemaining;
}

bool Treatment::isReady() const
{
    return _previousRemaining == 0 && _runnable->status() == Runnable::Waiting;
}

QVector<Treatment *> Treatment::next() const
{
    return _next;
}

unsigned int Treatment::nextCount() const
{
    return static_cast<unsigned int>(_next.size());
}

void Treatment::setNext(const QVector<Treatment *> &next)
{
    clearNext();
    for (auto t: next) {
        addNext(t);
    }
}

void Treatment::addNext(Treatment *treatment)
{
    if (_next.contains(treatment))
        return;

    _next.append(treatment);
    treatment->_previous++;
    if (_runnable->status() != Runnable::Finished)
        treatment->_previousRemaining++;

    connect(treatment, &Treatment::finished, this, &Treatment::manageNextFinished);
    connect(treatment, &QObject::destroyed, this, &Treatment::removeNext);
}

void Treatment::removeNext(QObject *treatment)
{
    Treatment *castedTreatment = qobject_cast<Treatment*>(treatment);
    if (_next.removeAll(castedTreatment) > 0) {
        castedTreatment->_previous--;
        if (_runnable->status() != Runnable::Finished)
            castedTreatment->_previousRemaining--;
    }
}

void Treatment::clearNext()
{
    auto next = _next;
    for (auto n: next)
        removeNext(n);
}

Treatment::QualifiersClearance Treatment::qualifiersClearance() const
{
    return _qualifiersClearance;
}

void Treatment::setQualifiersClearance(const QualifiersClearance &qualifiersClearance)
{
    _qualifiersClearance = qualifiersClearance;
}

void Treatment::clearQualifiers()
{
    QVector<TreatmentType::DataInfo> qualifiersProvided(_type.provide());

    // Replacing according outputsMap.
    auto outputsMap = _runnable->outputsMap();
    for (auto output = outputsMap.begin() ;
         output != outputsMap.end() ;
         output++) {
        for (int i=0 ; i < qualifiersProvided.size() ; ++i) {
            if (qualifiersProvided.at(i).name == output.key())
                qualifiersProvided[i].name = output.value();
        }
    }

    for (auto qualifier: qualifiersProvided) {
        switch (qualifier.type) {
            case TreatmentType::DataType::REAL:
            track()->clearGlobalRealQualifier(qualifier.name);
            break;
            case TreatmentType::DataType::STRING:
            track()->clearGlobalStringQualifier(qualifier.name);
            break;
            case TreatmentType::DataType::VECTOR_REAL:
            track()->clearSimpleRealQualifier(qualifier.name);
            break;
            case TreatmentType::DataType::VECTOR_STRING:
            track()->clearSimpleStringQualifier(qualifier.name);
            break;
            case TreatmentType::DataType::VECTOR_VECTOR_REAL:
            track()->clearDetailedRealQualifier(qualifier.name);
            break;
            case TreatmentType::DataType::VECTOR_VECTOR_STRING:
            track()->clearDetailedStringQualifier(qualifier.name);
            break;
            default:
            break;
        }
    }
}

void Treatment::manageRunnableStarted()
{
    emit started(this);
}

void Treatment::manageRunnableFinished()
{
    for (auto t: _next) {
        t->_previousRemaining--;
        if (t->_previousRemaining == 0) emit t->ready(t);
    }

    if (_qualifiersClearance == ClearAfterNextTreatmentsFinished and
            _next.size() == 0)
        clearQualifiers();

    emit finished(this);
}

void Treatment::manageRunnableFailed()
{
    emit failed(this);
}

void Treatment::manageNextFinished(Treatment *next)
{
    Q_UNUSED(next)
    _nextFinished++;
    if (_qualifiersClearance == ClearAfterNextTreatmentsFinished and
            _nextFinished == static_cast<unsigned int>(_next.size()))
        clearQualifiers();
}
