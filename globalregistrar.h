/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef GLOBALREGISTRAR_H
#define GLOBALREGISTRAR_H

/*!
 * \brief Class registering initializers, treatments, and all stuff having factory.
 *
 * Register initializers in InitializersFactory.
 * Register treatments in TreatmentsFactory.
 */
class GlobalRegistrar
{
public:
    ///Do registration of all elements.
    static void registerAll();

private:
    GlobalRegistrar() {}

    ///Do registration of intializers.
    static void registerInitializers();

    ///Do registration of models.
    static void registerModels();

    ///Do registration of treatments.
    static void registerTreatments();
    // All of these following are treatments.
    ///Register converters.
    static void registerConverters();
    ///Registering audio renderers.
    static void registerAudioRenderers();
    ///Registering analysts.
    static void registerAnalysts();
    ///Registering exporters.
    static void registerExporters();

};

#endif // GLOBALREGISTRAR_H
