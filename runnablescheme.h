/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef RUNNABLESCHEME_H
#define RUNNABLESCHEME_H

#include <QObject>
#include "melodium.h"
#include "treatmentinfoprinter.h"

/*!
 * \brief Check and print execution/runnable scheme.
 */
class RunnableScheme : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Type of notification.
     */
    enum NotificationType {
        Error,
        Warning,
        Notice
    };

    /*!
     * \brief Notification from checking procedure.
     */
    struct Notification {
        /*!
         * \brief Notification
         * \param type
         * \param info
         */
        Notification(NotificationType type, QString info);
        /// Type of notification.
        NotificationType type;
        /// Information (formatted for console)  about the notification.
        QString info;
    };

    /*!
     * \brief Instanciates a RunnableScheme.
     * \param parent
     */
    explicit RunnableScheme(QObject *parent = nullptr);

    /*!
     * \brief Check if the Melodium treatments are well setted up.
     */
    void check();

    /*!
     * \brief Tells if there are errors.
     * \return
     */
    bool hasErrors() const;

    /*!
     * \brief Tells if there are warnings.
     * \return
     */
    bool hasWarnings() const;

    /*!
     * \brief List of notification made by the checking procedure.
     * \return
     */
    QList<Notification> notifications() const;

    /*!
     * \brief Print all notifications in stream.
     * \param stream
     */
    void printNotifications(QTextStream &stream) const;

    /*!
     * \brief Print a notification.
     * \param notification
     * \param cout
     */
    static void printNotification(Notification notification, QTextStream &cout);

    /*!
     * \brief Create the graph of execution and write it in the specified file.
     * \param filename
     */
    static void makeGraph(const QString &filename);

signals:

public slots:

private:
    /// Tells if there are errors.
    bool _hasErrors;
    /// Tells if there are warnings.
    bool _hasWarnings;
    /// Notifications.
    QList<Notification> _notifications;

    /*!
     * \brief Format input or output name to make it fancy for user.
     * \param usedName
     * \param originalName
     * \param dataType
     * \return
     */
    static QString formatIoName(QString usedName, QString originalName, TreatmentType::DataType dataType);
};

#endif // RUNNABLESCHEME_H
