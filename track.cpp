/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "track.h"

Track::Track(int sampleRate, int frameSize, int hopSize, QObject *parent) : QObject(parent), _sampleRate(sampleRate), _frameSize(frameSize), _hopSize(hopSize)
{
    if (sampleRate <= 0)
        throw TrackException(TrackException::InvalidSampleRate, sampleRate);
    if (frameSize <= 0)
        throw TrackException(TrackException::InvalidFrameSize, frameSize);
    if (hopSize <= 0 or hopSize > frameSize)
        throw TrackException(TrackException::InvalidHopSize, hopSize);

    _mutex = new QMutex(QMutex::Recursive);
    _length = 0;
    _samples = 0;
    _frames = 0;
}

Track::~Track()
{
    delete _mutex;
}

quint64 Track::length() const
{
    QMutexLocker locker(_mutex);
    return _length;
}

void Track::setLength(quint64 length)
{
    if (_length != 0)
        throw std::logic_error("Track has already defined measurements.");

    _length = length;
    _samples = (static_cast<quint64>(_sampleRate) * _length) / 1000;
    _frames = _samples / static_cast<quint64>(_hopSize);
}

quint64 Track::samples() const
{
    QMutexLocker locker(_mutex);
    return _samples;
}

void Track::setSamples(quint64 samples)
{
    if (_samples != 0)
        throw std::logic_error("Track has already defined measurements.");

    _samples = samples;
    _length = (_samples * 1000) / static_cast<quint64>(_sampleRate);
    _frames = _samples / static_cast<quint64>(_hopSize);
}

int Track::sampleRate() const
{
    QMutexLocker locker(_mutex);
    return _sampleRate;
}

int Track::frameSize() const
{
    QMutexLocker locker(_mutex);
    return _frameSize;
}

int Track::hopSize() const
{
    QMutexLocker locker(_mutex);
    return _hopSize;
}

quint64 Track::frames() const
{
    QMutexLocker locker(_mutex);
    return _frames;
}

void Track::setFrames(quint64 frames)
{
    if (_frames != 0)
        throw std::logic_error("Track has already defined measurements.");

    _frames = frames;
    _samples = _frames * static_cast<quint64>(_hopSize);
    _length = (_samples * 1000) / static_cast<quint64>(_sampleRate);
}

bool Track::hasOriginalMidiFile() const
{
    QMutexLocker locker(_mutex);
    return (!_originalMidiFile.isEmpty());
}

QString Track::originalMidiFile() const
{
    QMutexLocker locker(_mutex);
    return _originalMidiFile;
}

bool Track::setOriginalMidiFile(const QString originalMidiFile)
{
    QMutexLocker locker(_mutex);

    if (Track::isMidiFile(originalMidiFile)) {
        _originalMidiFile = originalMidiFile;
        emit originalMidiFileChanged(_originalMidiFile, this);
        return true;
    }
    else return false;
}

bool Track::hasAudioFile() const
{
    QMutexLocker locker(_mutex);
    return (!_audioFile.isEmpty());
}

QString Track::audioFile() const
{
    QMutexLocker locker(_mutex);
    return _audioFile;
}

bool Track::setAudioFile(const QString audioFile)
{
    QMutexLocker locker(_mutex);

    if (Track::isAudioFile(audioFile)) {
        _audioFile = audioFile;

        quint64 _originalSample = 0;
        quint64 _originalRate = 0;
        if (Track::isWaveFile(_audioFile)) {
            TagLib::RIFF::WAV::File file(_audioFile.toStdString().c_str());
            _originalSample = file.audioProperties()->sampleFrames();
            _originalRate = static_cast<quint64>(file.audioProperties()->sampleRate());
        }
        else if (Track::isFlacFile(_audioFile)) {
            TagLib::FLAC::File file(_audioFile.toStdString().c_str());
            _originalSample = file.audioProperties()->sampleFrames();
            _originalRate = static_cast<quint64>(file.audioProperties()->sampleRate());
        }
        setSamples(((_originalSample * static_cast<quint64>(_sampleRate)) / _originalRate) - 1);

        if (_frames == 0)
            throw TrackException(TrackException::ZeroFrames, audioFile, _samples);

        emit audioFileChanged(_audioFile, this);
        return true;
    }
    else return false;
}

bool Track::hasMidiFile() const
{
    QMutexLocker locker(_mutex);
    return (!_midiFile.isEmpty());
}

QString Track::midiFile() const
{
    QMutexLocker locker(_mutex);
    return _midiFile;
}

bool Track::setMidiFile(const QString midiFile)
{
    QMutexLocker locker(_mutex);

    if (Track::isMidiFile(midiFile)) {
        _midiFile = midiFile;
        emit midiFileChanged(_midiFile, this);
        return true;
    }
    else return false;
}

bool Track::setFile(const QString fileName)
{
    QMutexLocker locker(_mutex);
    if (fileName.endsWith(".result.midi")) return setMidiFile(fileName);
    else if (isMidiFile(fileName)) return setOriginalMidiFile(fileName);
    else return setAudioFile(fileName);
}

QMap<QString, float> Track::globalRealQualifiers() const
{
    QMutexLocker locker(_mutex);
    return _globalRealQualifiers;
}

const QMap<QString, float> &Track::globalRealQualifiers(QMutex *&mutex) const
{
    _mutex->lock();
    mutex = _mutex;
    return _globalRealQualifiers;
}

QMap<QString, std::string> Track::globalStringQualifiers() const
{
    QMutexLocker locker(_mutex);
    return _globalStringQualifiers;
}

const QMap<QString, std::string> &Track::globalStringQualifiers(QMutex *&mutex) const
{
    _mutex->lock();
    mutex = _mutex;
    return _globalStringQualifiers;
}

QMap<QString, std::vector<float> > Track::simpleRealQualifiers() const
{
    QMutexLocker locker(_mutex);
    return _simpleRealQualifiers;
}

const QMap<QString, std::vector<float> > &Track::simpleRealQualifiers(QMutex *&mutex) const
{
    _mutex->lock();
    mutex = _mutex;
    return _simpleRealQualifiers;
}

QMap<QString, std::vector<std::string> > Track::simpleStringQualifiers() const
{
    QMutexLocker locker(_mutex);
    return _simpleStringQualifiers;
}

const QMap<QString, std::vector<std::string> > &Track::simpleStringQualifiers(QMutex *&mutex) const
{
    _mutex->lock();
    mutex = _mutex;
    return _simpleStringQualifiers;
}

QMap<QString, std::vector<std::vector<float> > > Track::detailedRealQualifiers() const
{
    QMutexLocker locker(_mutex);
    return _detailedRealQualifiers;
}

const QMap<QString, std::vector<std::vector<float> > > &Track::detailedRealQualifiers(QMutex *&mutex) const
{
    _mutex->lock();
    mutex = _mutex;
    return _detailedRealQualifiers;
}

QMap<QString, std::vector<std::vector<std::string> > > Track::detailedStringQualifiers() const
{
    QMutexLocker locker(_mutex);
    return _detailedStringQualifiers;
}

const QMap<QString, std::vector<std::vector<std::string> > > &Track::detailedStringQualifiers(QMutex *&mutex) const
{
    _mutex->lock();
    mutex = _mutex;
    return _detailedStringQualifiers;
}

void Track::storeQualifier(const QString &name, float value)
{
    QMutexLocker locker(_mutex);

    _globalRealQualifiers.insert(name, value);

    emit qualifiersAdded(this);
    emit globalQualifiersAdded(this);
    emit globalRealQualifiersAdded(this);
}

void Track::storeQualifier(const QString &name, const std::string &value)
{
    QMutexLocker locker(_mutex);

    _globalStringQualifiers.insert(name, value);

    emit qualifiersAdded(this);
    emit globalQualifiersAdded(this);
    emit globalStringQualifiersAdded(this);
}

void Track::storeQualifier(const QString &name, const std::vector<float> &value)
{
    QMutexLocker locker(_mutex);

    if (value.size() != _frames
            && value.size() != _samples) {
        throw TrackException(TrackException::InvalidQualifierLength,
                             name,
                             value.size());
    }

    _simpleRealQualifiers.insert(name, value);

    emit qualifiersAdded(this);
    emit simpleQualifiersAdded(this);
    emit simpleRealQualifiersAdded(this);
}

void Track::storeQualifier(const QString &name, const std::vector<std::string> &value)
{
    QMutexLocker locker(_mutex);

    if (value.size() != _frames
            && value.size() != _samples) {
        throw TrackException(TrackException::InvalidQualifierLength,
                             name,
                             value.size());
    }

    _simpleStringQualifiers.insert(name, value);

    emit qualifiersAdded(this);
    emit simpleQualifiersAdded(this);
    emit simpleStringQualifiersAdded(this);
}

void Track::storeQualifier(const QString &name, const std::vector<std::vector<float> > &value, bool dontCheckConstantDepth)
{
    QMutexLocker locker(_mutex);

    if (value.size() != _frames
            && value.size() != _samples) {
        throw TrackException(TrackException::InvalidQualifierLength,
                             name,
                             value.size());
    }

    size_t size = value.at(0).size();
    for (size_t i = 0 ; !dontCheckConstantDepth and i < value.size() ; i++) {
        if (value.at(i).size() != size) {
            throw TrackException(TrackException::QualifierDepthNonConstant,
                                 name,
                                 value.size());
        }
    }

    _detailedRealQualifiers.insert(name, value);

    emit qualifiersAdded(this);
    emit detailedQualifiersAdded(this);
    emit detailedRealQualifiersAdded(this);
}

void Track::storeQualifier(const QString &name, const std::vector<std::vector<std::string> > &value, bool dontCheckConstantDepth)
{
    QMutexLocker locker(_mutex);

    if (value.size() != _frames
            && value.size() != _samples) {
        throw TrackException(TrackException::InvalidQualifierLength,
                             name,
                             value.size());
    }

    size_t size = value.at(0).size();
    for (size_t i = 0 ; !dontCheckConstantDepth and i < value.size() ; i++) {
        if (value.at(i).size() != size) {
            throw TrackException(TrackException::QualifierDepthNonConstant,
                                 name,
                                 value.size());
        }
    }

    _detailedStringQualifiers.insert(name, value);

    emit qualifiersAdded(this);
    emit detailedQualifiersAdded(this);
    emit detailedStringQualifiersAdded(this);
}

void Track::storeQualifiers(const QMap<QString, float> &qualifiers)
{
    QMutexLocker locker(_mutex);

    _globalRealQualifiers.unite(qualifiers);

    emit qualifiersAdded(this);
    emit globalQualifiersAdded(this);
    emit globalRealQualifiersAdded(this);
}

void Track::storeQualifiers(const QMap<QString, std::string> &qualifiers)
{
    QMutexLocker locker(_mutex);

    _globalStringQualifiers.unite(qualifiers);

    emit qualifiersAdded(this);
    emit globalQualifiersAdded(this);
    emit globalStringQualifiersAdded(this);
}

void Track::storeQualifiers(const QMap<QString, std::vector<float> > &qualifiers)
{
    QMutexLocker locker(_mutex);

    for (auto it = qualifiers.begin() ; it != qualifiers.end() ; ++it) {
        if (it.value().size() != _frames
                && it.value().size() != _samples) {
            throw TrackException(TrackException::InvalidQualifierLength,
                                 it.key(),
                                 it.value().size());
        }
    }

    for (auto it = qualifiers.begin() ; it != qualifiers.end() ; ++it) {
        _simpleRealQualifiers.insert(it.key(), it.value());
    }

    emit qualifiersAdded(this);
    emit simpleQualifiersAdded(this);
    emit simpleRealQualifiersAdded(this);
}

void Track::storeQualifiers(const QMap<QString, std::vector<std::string> > &qualifiers)
{
    QMutexLocker locker(_mutex);

    for (auto it = qualifiers.begin() ; it != qualifiers.end() ; ++it) {
        if (it.value().size() != _frames
                && it.value().size() != _samples) {
            throw TrackException(TrackException::InvalidQualifierLength,
                                 it.key(),
                                 it.value().size());
        }
    }

    for (auto it = qualifiers.begin() ; it != qualifiers.end() ; ++it) {
        _simpleStringQualifiers.insert(it.key(), it.value());
    }

    emit qualifiersAdded(this);
    emit simpleQualifiersAdded(this);
    emit simpleStringQualifiersAdded(this);
}

void Track::storeQualifiers(const QMap<QString, std::vector<std::vector<float> > > &qualifiers, bool dontCheckConstantDepth)
{
    QMutexLocker locker(_mutex);

    for (auto it = qualifiers.begin() ; it != qualifiers.end() ; ++it) {
        if (it.value().size() != _frames
                && it.value().size() != _samples) {
            throw TrackException(TrackException::InvalidQualifierLength,
                                 it.key(),
                                 it.value().size());
        }

        size_t size = it.value().at(0).size();
        for (size_t i = 0 ; !dontCheckConstantDepth and i < it.value().size() ; i++) {
            if (it.value().at(i).size() != size) {
                throw TrackException(TrackException::QualifierDepthNonConstant,
                                     it.key(),
                                     it.value().size());
            }
        }
    }

    for (auto it = qualifiers.begin() ; it != qualifiers.end() ; ++it) {
        _detailedRealQualifiers.insert(it.key(), it.value());
    }

    emit qualifiersAdded(this);
    emit detailedQualifiersAdded(this);
    emit detailedRealQualifiersAdded(this);
}

void Track::storeQualifiers(const QMap<QString, std::vector<std::vector<std::string> > > &qualifiers, bool dontCheckConstantDepth)
{
    QMutexLocker locker(_mutex);

    for (auto it = qualifiers.begin() ; it != qualifiers.end() ; ++it) {
        if (it.value().size() != _frames
                && it.value().size() != _samples) {
            throw TrackException(TrackException::InvalidQualifierLength,
                                 it.key(),
                                 it.value().size());
        }

        size_t size = it.value().at(0).size();
        for (size_t i = 0 ; !dontCheckConstantDepth and i < it.value().size() ; i++) {
            if (it.value().at(i).size() != size) {
                throw TrackException(TrackException::QualifierDepthNonConstant,
                                     it.key(),
                                     it.value().size());
            }
        }
    }

    for (auto it = qualifiers.begin() ; it != qualifiers.end() ; ++it) {
        _detailedStringQualifiers.insert(it.key(), it.value());
    }

    emit qualifiersAdded(this);
    emit detailedQualifiersAdded(this);
    emit detailedStringQualifiersAdded(this);
}

void Track::clearGlobalRealQualifier(const QString &name)
{
    QMutexLocker locker(_mutex);

    _globalRealQualifiers.remove(name);
}

void Track::clearGlobalStringQualifier(const QString &name)
{
    QMutexLocker locker(_mutex);

    _globalStringQualifiers.remove(name);
}

void Track::clearSimpleRealQualifier(const QString &name)
{
    QMutexLocker locker(_mutex);

    _simpleRealQualifiers.remove(name);
}

void Track::clearSimpleStringQualifier(const QString &name)
{
    QMutexLocker locker(_mutex);

    _simpleStringQualifiers.remove(name);
}

void Track::clearDetailedRealQualifier(const QString &name)
{
    QMutexLocker locker(_mutex);

    _detailedRealQualifiers.remove(name);
}

void Track::clearDetailedStringQualifier(const QString &name)
{
    QMutexLocker locker(_mutex);

    _detailedStringQualifiers.remove(name);
}

bool Track::match(int sampleRate, int frameSize, int hopSize, const QString &fileName) const
{
    QMutexLocker locker(_mutex);
    if (    sampleRate == _sampleRate &&
            frameSize == _frameSize &&
            hopSize == _hopSize) {

        if (    fileName == _originalMidiFile ||
                fileName == _audioFile ||
                fileName == _midiFile) {
            return true;
        }
        else {
            QFileInfo testedFile(fileName);
            if ( QFileInfo(_originalMidiFile).baseName() == testedFile.baseName() ||
                 QFileInfo(_audioFile).baseName() == testedFile.baseName() ||
                 QFileInfo(_midiFile).baseName() == testedFile.baseName()) {
                return true;
            }
            else return false;
        }
    }
    else return false;
}

QString Track::info() const
{
    auto nameOrNone = [](QString name) -> QString {
        if (name.isEmpty()) return "[None]";
        else return QFileInfo(name).fileName();
    };

    QString informations;
    informations += "Sample rate: " + QString::number(_sampleRate) + "Hz, ";
    informations += "Frame size: " + QString::number(_frameSize) + ", ";
    informations += "Hop size: " + QString::number(_hopSize) + '\n';

    informations += "Original MIDI file: " + nameOrNone(_originalMidiFile) + '\n';
    informations += "Audio file: " + nameOrNone(_audioFile) + '\n';
    informations += "MIDI file: " + nameOrNone(_midiFile) + '\n';

    informations += "Length: " + QString::number(_length) + "ms, ";
    informations += "Samples: " + QString::number(_samples) + ", ";
    informations += "Frames: " + QString::number(_frames) + '\n';

    informations += "Global real qualifiers: ";
    for (QString name: _globalRealQualifiers.keys())
        informations += name + ' ';
    informations += '\n';

    informations += "Global string qualifiers: ";
    for (QString name: _globalStringQualifiers.keys())
        informations += name + ' ';
    informations += '\n';

    informations += "Simple real qualifiers: ";
    for (QString name: _simpleRealQualifiers.keys())
        informations += name + ' ';
    informations += '\n';

    informations += "Simple string qualifiers: ";
    for (QString name: _simpleStringQualifiers.keys())
        informations += name + ' ';
    informations += '\n';

    informations += "Detailed real qualifiers: ";
    for (QString name: _detailedRealQualifiers.keys())
        informations += name + ' ';
    informations += '\n';

    informations += "Detailed string qualifiers: ";
    for (QString name: _detailedStringQualifiers.keys())
        informations += name + ' ';
    informations += '\n';

    return informations;
}

bool Track::isMidiFile(const QString &fileName)
{
    return Track::fileExistsAndMatchType({"audio/midi"}, fileName);
}

bool Track::isAudioFile(const QString &fileName)
{
    return Track::fileExistsAndMatchType({"audio/wave", "audio/wav", "audio/x-wav", "audio/vnd.wave", "audio/flac"}, fileName);
}

bool Track::isFlacFile(const QString &fileName)
{
    return Track::fileExistsAndMatchType({"audio/flac"}, fileName);
}

bool Track::isWaveFile(const QString &fileName)
{
    return Track::fileExistsAndMatchType({"audio/wave", "audio/wav", "audio/x-wav", "audio/vnd.wave"}, fileName);
}

bool Track::fileExistsAndMatchType(const QList<QString> &mimeName, const QString &fileName)
{
    QFile file(fileName);

    if (file.exists()) {
        QMimeDatabase mimeDb;

        QMimeType mimeType = mimeDb.mimeTypeForFileNameAndData(fileName, &file);

        return mimeName.contains(mimeType.name());
    }
    else return false;
}




