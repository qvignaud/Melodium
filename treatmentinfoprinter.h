/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef TREATMENTINFOPRINTER_H
#define TREATMENTINFOPRINTER_H

#include <QTextStream>
#include "treatmenttype.h"

/*!
 * \brief Treatment info printer.
 *
 * Convenience class.
 */
class TreatmentInfoPrinter
{
public:
    /*!
     * \brief Print treatments to std::cout.
     * \param tts
     */
    static void print(const QList<TreatmentType> &tts);
    /*!
     * \brief Print treatment to std::cout.
     * \param tt
     */
    static void print(const TreatmentType &tt);
    /*!
     * \brief Print one line of informations about treatments to std::cout.
     * \param tts
     */
    static void simplePrint(const QList<TreatmentType> &tts);
    /*!
     * \brief Print one line of informations about treatment to std::cout.
     * \param tt
     */
    static void simplePrint(const TreatmentType &tt);

    /*!
     * \brief Get role name.
     * \param role
     * \return
     */
    static QString roleName(const TreatmentType::Role role);
    /*!
     * \brief Get track operation name.
     * \param to
     * \return
     */
    static QString trackOperationName(const TreatmentType::TrackOperation to);
    /*!
     * \brief Get datatype name.
     * \param dt
     * \return
     */
    static QString dataTypeName(const TreatmentType::DataType dt);
    /*!
     * \brief Get string describing values range.
     * \param dt
     * \param lv
     * \return
     */
    static QString valuesString(const TreatmentType::DataType dt, const QList<QVariant> &lv);

private:
    TreatmentInfoPrinter() {}

    ///Text stream to std::cout.
    static QTextStream cout;

};

#endif // TREATMENTINFOPRINTER_H
