/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "preparator.h"

Preparator::Preparator(QObject *parent) : QObject(parent)
{
    _tracksSampleRate = 0;
    _tracksFrameSize = 0;
    _tracksHopSize = 0;
}

void Preparator::add(const QString &string)
{
    _filesList << QFileInfo(string);
}

void Preparator::add(QFileInfo &file)
{
    _filesList << file;
}

QFileInfoList Preparator::filesList() const
{
    return _filesList;
}

void Preparator::setTracksProperties(int sampleRate, int frameSize, int hopSize)
{
    _tracksSampleRate = sampleRate;
    _tracksFrameSize = frameSize;
    _tracksHopSize = hopSize;
}

int Preparator::tracksSampleRate() const
{
    return _tracksSampleRate;
}

int Preparator::tracksFrameSize() const
{
    return _tracksFrameSize;
}

int Preparator::tracksHopSize() const
{
    return _tracksHopSize;
}

QList<Preparator::PreparedTrack> Preparator::preparedTracks() const
{
    return _preparedTracks;
}

Track *Preparator::newTrack()
{
    return new Track(_tracksSampleRate, _tracksFrameSize, _tracksHopSize);
}

void Preparator::recurseDirectories()
{
    // Including other directories.
    for (int i=0 ; i < _filesList.size() ; ++i) {
        auto &fileInfo = _filesList.at(i);
        if (fileInfo.isDir()) {
            _filesList << QDir(fileInfo.absoluteFilePath()).entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot | QDir::Files);
        }
    }
}

Preparator::PreparedTrack::PreparedTrack(const PreparingTrack &pt)
{
    PreparingTrack::track = pt.track;
    PreparingTrack::treatments = pt.treatments;
}

Track *Preparator::PreparedTrack::track() const
{
    return PreparingTrack::track;
}

Treatment *Preparator::PreparedTrack::treatment(const QString &name) const
{
    Treatment *treatment = PreparingTrack::treatments.value(name, nullptr);
    if (treatment == nullptr)
        throw PreparatorException(PreparatorException::UnkownPreparationTreatment,
                                  "No treatment '" + name + "' available in preparation.");
    return treatment;
}

QList<QString> Preparator::PreparedTrack::treatmentsNames() const
{
    return PreparingTrack::treatments.keys();
}
