/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef TRACKEXCEPTION_H
#define TRACKEXCEPTION_H

#include <QException>

/*!
 * \brief Specialized exception for Track.
 */
class TrackException : public QException
{
public:
    /*!
     * \brief Type of exception.
     */
    enum Type {
        /// The sample rate is invalid (less than 0).
        InvalidSampleRate,
        /// The frame size is invalid (less than 0).
        InvalidFrameSize,
        /// The hop size is invalid (less than 0 or larger than frame size).
        InvalidHopSize,
        /// The track has zero frames (because of a too small file).
        ZeroFrames,
        /// A qualifier have an invalid length (non equal to samples or frames).
        InvalidQualifierLength,
        /// A qualifier don't have a constant depth.
        QualifierDepthNonConstant
    };

    /*!
     * \brief Build exception based on invalid size only.
     * \param type
     * \param invalidSize
     */
    TrackException(const Type type, const __int128 invalidSize);
    /*!
     * \brief Build exception about an invalid thing.
     * \param type
     * \param causeName
     * \param invalidSize
     */
    TrackException(const Type type, const QString causeName, const __int128 invalidSize);

    void raise() const override;
    TrackException *clone() const override;

    /// Type of exception.
    Type type() const;

    /*!
     * \brief Get the name of the invalid thing that caused exception.
     * \return  name of qualifier, or file name/path, or empty string if not about qualifier of file.
     */
    QString causeName() const;

    /*!
     * \brief Get the invalid size that caused the exeption.
     * \return  size or length, but will be 0 if not about a size problem.
     */
    __int128 invalidSize() const;

private:
    /// Type of exception.
    const Type _type;
    /// Name of the invalid thing.
    const QString _causeName;
    /// Invalid size that caused the exception.
    const __int128 _invalidSize;
};

#endif // TRACKEXCEPTION_H
