/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <QObject>
#include "melodium.h"

/*!
 * \brief Abstract class for schedulers.
 */
class Scheduler : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates scheduler.
     * \param parent
     */
    explicit Scheduler(QObject *parent);

    /*!
     * \brief Launch the execution.
     *
     * This method do nothing if the execution is already launched.
     */
    void launch();

    /*!
     * \brief Tells if the schedule is launched.
     * \return
     */
    bool isLaunched() const;

signals:
    /*!
     * \brief The scheduler is launched.
     *
     * This signal can be emitted only once.
     */
    void launched();
    /*!
     * \brief All treatments scheduled are finished.
     *
     * When all treatments are finished this signal is emitted.
     *
     * \note This signal can be emitted multiple times, if treatments are added after all others already finished.
     */
    void finished();

protected:
    /*!
     * \brief Do the execution of treatments.
     *
     * This method have to be implemented by subclasses.
     *
     * This method is called *only once* by launch().
     *
     * Subclasses don't need to emit launched(), but have to implement finished() emission.
     * It must be considered than treatments can be added in Melodium *after* the scheduler is launched,
     * so having a slot connected to Melodium::treatmentAdded can be a good solution.
     *
     */
    virtual void work() = 0;

private:
    /// Store state of scheduler.
    bool _launched;
};

#endif // SCHEDULER_H
