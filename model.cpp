/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "model.h"

Model::Model(const ModelType &type, QObject *parent) : QObject(parent),
    _type(type)
{
    _feedersCreated = 0;
    _feedersFinished = 0;
}

Model::~Model()
{

}

ModelType Model::type() const
{
    return _type;
}

Runnable *Model::feedingRunnable(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    Runnable *feeder = createFeedingRunnable(track, parent);
    connect(feeder, &Runnable::finished, this, &Model::manageFeederFinished);

    _feedersCreated++;

    return feeder;
}

Runnable *Model::trainingRunnable(TreatmentType type, QObject *parent)
{
    Q_UNUSED(type)
    return createTrainingRunnable(parent);
}

Runnable *Model::predictionRunnable(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return createPredictionRunnable(track, parent);
}

Runnable *Model::savingRunnable(TreatmentType type, QObject *parent)
{
    Q_UNUSED(type)
    return createSavingRunnable(parent);
}

Runnable *Model::loadingRunnable(TreatmentType type, QObject *parent)
{
    Q_UNUSED(type)
    return createLoadingRunnable(parent);
}

bool Model::isFeedingFinished() const
{
    return (_feedersCreated > 0 && _feedersCreated == _feedersFinished);
}

Runnable *Model::createFeedingRunnable(Track *track, QObject *parent)
{
    Q_UNUSED(track)
    Q_UNUSED(parent)
    throw std::exception();
}

Runnable *Model::createTrainingRunnable(QObject *parent)
{
    Q_UNUSED(parent)
    throw std::exception();
}

Runnable *Model::createPredictionRunnable(Track *track, QObject *parent)
{
    Q_UNUSED(track)
    Q_UNUSED(parent)
    throw std::exception();
}

Runnable *Model::createSavingRunnable(QObject *parent)
{
    Q_UNUSED(parent)
    throw std::exception();
}

Runnable *Model::createLoadingRunnable(QObject *parent)
{
    Q_UNUSED(parent)
    throw std::exception();
}

void Model::manageFeederFinished()
{
    _feedersFinished++;
    if (isFeedingFinished())
        emit feedingFinished(this);
}
