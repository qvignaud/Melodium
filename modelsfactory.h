/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MODELSFACTORY_H
#define MODELSFACTORY_H

#include <QObject>
#include <QList>
#include <QStringList>
#include "model.h"
#include "treatmentsfactory.h"

/*!
 * \brief Models factory class.
 */
class ModelsFactory : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a factory.
     * \param parent
     */
    explicit ModelsFactory(QObject *parent = nullptr);
    ~ModelsFactory();

    /*!
     * \brief Creates the matching model.
     * \param name
     * \return The created model, or nullptr if the name doesn't match anything.
     */
    Model *createModel(const QString &name);

    /*!
     * \brief Models the factory created.
     * \return
     */
    QList<Model *> models() const;

    /*!
     * \brief Models available.
     * \return
     */
    static QList<ModelType> availableModels();

    /*!
     * \brief Add a model to the factory.
     * \param type  Type of the model.
     * \param initializer   Creation function returning a new model.
     */
    static void addAvailableModel(const ModelType &type, Model* (*initializer)());

    /*!
     * \brief Get the global instance.
     * \return
     */
    static ModelsFactory& globalInstance();

private:
    /*!
     * \brief Class coupling available model with initialisation function.
     */
    class AvailableModel : public ModelType {
    public:
        AvailableModel(const ModelType &type);
        /// Initialisation function of the model.
        Model* (*initializer)() = nullptr;
    };

    /*!
     * \brief List of the models created by the factory.
     */
    QList<Model *> _models;

    /*!
     * \brief List of avaliable models.
     */
    static QList<AvailableModel> _availableModels;

private slots:
    void modelDestroyed(QObject *model);
};

#endif // MODELSFACTORY_H
