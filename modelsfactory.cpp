/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "modelsfactory.h"

QList<ModelsFactory::AvailableModel> ModelsFactory::_availableModels = QList<ModelsFactory::AvailableModel>();

ModelsFactory::ModelsFactory(QObject *parent) : QObject(parent)
{

}

ModelsFactory::~ModelsFactory()
{

}

Model *ModelsFactory::createModel(const QString &name)
{
    for (auto am: ModelsFactory::_availableModels) {
        if (am.name().compare(name, Qt::CaseInsensitive) == 0) {
            Model *model = am.initializer();

            connect(model, &QObject::destroyed, this, &ModelsFactory::modelDestroyed);
            _models << model;

            return model;
        }
    }

    return nullptr;
}

QList<Model *> ModelsFactory::models() const
{
    return _models;
}

QList<ModelType> ModelsFactory::availableModels()
{
    QList<ModelType> list;
    for (auto am: ModelsFactory::_availableModels)
        list << am;
    return list;
}

void ModelsFactory::addAvailableModel(const ModelType &type, Model *(*initializer)())
{
    AvailableModel am(type);
    am.initializer = initializer;
    ModelsFactory::_availableModels << am;

    if (am.abilities().testFlag(ModelType::Feeding))
        TreatmentsFactory::globalInstance()
                .addAvailableTreatment(am.feedingTreatmentType(),
                                       &Model::feedingRunnable);
    if (am.abilities().testFlag(ModelType::Training))
        TreatmentsFactory::globalInstance()
                .addAvailableTreatment(am.trainingTreatmentType(),
                                       &Model::trainingRunnable);
    if (am.abilities().testFlag(ModelType::Predicting))
        TreatmentsFactory::globalInstance()
                .addAvailableTreatment(am.predictingTreatmentType(),
                                       &Model::predictionRunnable);
    if (am.abilities().testFlag(ModelType::Saving))
        TreatmentsFactory::globalInstance()
                .addAvailableTreatment(am.savingTreatmentType(),
                                       &Model::savingRunnable);
    if (am.abilities().testFlag(ModelType::Loading))
        TreatmentsFactory::globalInstance()
                .addAvailableTreatment(am.loadingTreatmentType(),
                                       &Model::loadingRunnable);
}

ModelsFactory &ModelsFactory::globalInstance()
{
    static ModelsFactory instance;
    return instance;
}

void ModelsFactory::modelDestroyed(QObject *model)
{
    _models.removeAll(static_cast<Model *>(model));
}

ModelsFactory::AvailableModel::AvailableModel(const ModelType &type) : ModelType (type)
{
    initializer = nullptr;
}
