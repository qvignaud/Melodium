/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef INITIALIZERSFACTORY_H
#define INITIALIZERSFACTORY_H

#include <QObject>
#include <QList>
#include "initializer.h"

/*!
 * \brief Initializers factory class.
 */
class InitializersFactory : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a factory.
     * \param parent
     */
    explicit InitializersFactory(QObject *parent = nullptr);
    ~InitializersFactory();

    /*!
     * \brief Creates the matching initializer.
     * \param name
     * \return The created initializer, or nullptr if name doesn't match anything.
     *
     */
    Initializer *createInitializer(const QString &name);

    /*!
     * \brief Initializers the factory created.
     * \return
     */
    QList<Initializer *> initializers() const;

    /*!
     * \brief Initializers available.
     * \return
     */
    static QStringList availableInitializers();

    /*!
     * \brief Add an initializer to the factory.
     * \param name Name of the initializer.
     * \param initializer Creation function the returns a new initializer.
     */
    static void addAvailableInitializer(QString name, Initializer* (*initializer)());

    /*!
     * \brief Get the global instance.
     * \return
     */
    static InitializersFactory& globalInstance();

private:
    /*!
     * \brief Structure coupling available initializer with init function.
     */
    struct AvailableInitializer {
        /// Name of the initializer.
        QString name;
        /// Initialization (creation) function of the initializer.
        Initializer* (*initializer)() = nullptr;
    };

    /*!
     * \brief List of initializers created by the factory.
     */
    QList<Initializer *> _initializers;

    /*!
     * \brief List of available initializers.
     */
    static QList<AvailableInitializer> _availableInitializers;

private:
    void initializerDestroyed(QObject *initializer);
};

#endif // INITIALIZERSFACTORY_H
