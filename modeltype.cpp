/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "modeltype.h"

ModelType::ModelType(QString name, ModelAbilities abilities) :
    _name(name),
    _abilities(abilities),
    _feedingTreatmentType("", TreatmentType::ModelFeeder),
    _trainingTreatmentType("", TreatmentType::ModelTrainer),
    _predictingTreatmentType("", TreatmentType::ModelPredictor),
    _savingTreatmentType("", TreatmentType::ModelSaver),
    _loadingTreatmentType("", TreatmentType::ModelLoader)
{

}

QString ModelType::name() const
{
    return _name;
}

ModelType::ModelAbilities ModelType::abilities() const
{
    return _abilities;
}

QString ModelType::description() const
{
    return _description;
}

void ModelType::setDescription(const QString &description)
{
    _description = description;
}

TreatmentType ModelType::feedingTreatmentType() const
{
    return _feedingTreatmentType;
}

void ModelType::setFeedingTreatmentType(const TreatmentType &feedingTreatmentType)
{
    _feedingTreatmentType = feedingTreatmentType;
}

TreatmentType ModelType::trainingTreatmentType() const
{
    return _trainingTreatmentType;
}

void ModelType::setTrainingTreatmentType(const TreatmentType &trainingTreatmentType)
{
    _trainingTreatmentType = trainingTreatmentType;
}

TreatmentType ModelType::predictingTreatmentType() const
{
    return _predictingTreatmentType;
}

void ModelType::setPredictingTreatmentType(const TreatmentType &predictingTreatmentType)
{
    _predictingTreatmentType = predictingTreatmentType;
}

TreatmentType ModelType::savingTreatmentType() const
{
    return _savingTreatmentType;
}

void ModelType::setSavingTreatmentType(const TreatmentType &savingTreatmentType)
{
    _savingTreatmentType = savingTreatmentType;
}

TreatmentType ModelType::loadingTreatmentType() const
{
    return _loadingTreatmentType;
}

void ModelType::setLoadingTreatmentType(const TreatmentType &loadingTreatmentType)
{
    _loadingTreatmentType = loadingTreatmentType;
}
