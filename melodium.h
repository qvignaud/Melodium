/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MELODIUM_H
#define MELODIUM_H

#include <QGuiApplication>
#include <QTextStream>
#include <QCommandLineParser>
#include <QThreadPool>
#include <QList>
#include <QFileInfo>
#include <essentia.h>
#include "model.h"
#include "track.h"
#include "treatmenttype.h"
#include "treatment.h"
#include "treatmentsfactory.h"
#include "treatmentinfoprinter.h"

using namespace std;
// There to avoid circular reference.
class Scheduler;

/*!
 * \brief The Mélodium main class
 *
 * This class manage Mélodium data and execution.
 */
class Melodium : public QGuiApplication
{
    Q_OBJECT

public:
    /*!
     * \brief Program initialization.
     * \param argc  From main argc.
     * \param argv  From main argv.
     */
    Melodium(int &argc, char **argv);
    ~Melodium();

    /*!
     * \brief Main function.
     * \return Exit code.
     */
    int exec();

    /*!
     * \brief Add the track for management.
     * \param track
     *
     * Melodium took the parenthood of the track,
     * and connects the track to concerned signals and
     * slots and add it to the list of managed tracks.
     */
    void addTrack(Track *track);

    /*!
     * \brief Tracks managed.
     * \return A list of tracks.
     */
    QList<Track *> tracks() const;

    /*!
     * \brief Add treatment for management.
     * \param treatment
     *
     * Melodium took the parenthood of the treatment,
     * and connects the treatment to concerned signals and
     * slots and add it to the list of managed treatments.
     */
    void addTreatment(Treatment *treatment);

    /*!
     * \brief Treatments managed.
     * \return A list of treatments.
     */
    QList<Treatment *> treatments() const;

    /*!
     * \brief Add model for management.
     * \param model
     *
     * Melodium took the parenthood of the model,
     * and connects the model to concerned signals and
     * slots and add it to the list of managed models.
     */
    void addModel(Model *model);

    /*!
     * \brief Models managed.
     * \return A list of models.
     */
    QList<Model *> models() const;

    /*!
     * \brief Return a pointer to the Mélodium application main class.
     * \return
     *
     * \warning Calling this function when application hasn't been allocated will cause undefined behavior (likely SEGFAULT).
     */
    static Melodium *instance();

signals:
    /*!
     * \brief A track has been added in Mélodium.
     * \param track
     * \sa addTrack
     */
    void trackAdded(Track *track);
    /*!
     * \brief A track has its original MIDI file changed.
     * \param track
     * \sa Track::originalMidiFileChanged
     */
    void trackOriginalMidiFileChanged(Track *track);
    /*!
     * \brief A track has its audio file changed.
     * \param track
     * \sa Track::audioFileChanged
     */
    void trackAudioFileChanged(Track *track);
    /*!
     * \brief A track has its MIDI file changed
     * \param track
     * \sa Track::midiFileChanged
     */
    void trackMidiFileChanged(Track *track);

    /*!
     * \brief A track has qualifiers added.
     * \param track
     * \sa Track::qualifiersAdded
     */
    void trackQualifiersAdded(Track *track);
    /*!
     * \brief A track has global qualifiers added.
     * \param track
     * \sa Track::globalQualifiersAdded
     */
    void trackGlobalQualifiersAdded(Track *track);
    /*!
     * \brief A track has real qualifiers added.
     * \param track
     * \sa Track::globalRealQualifiersAdded
     */
    void trackGlobalRealQualifiersAdded(Track *track);
    /*!
     * \brief A track has string qualifiers added.
     * \param track
     * \sa Track::globalStringQualifiersAdded
     */
    void trackGlobalStringQualifiersAdded(Track *track);
    /*!
     * \brief A track has simple qualifiers added.
     * \param track
     * \sa Track::simpleQualifiersAdded
     */
    void trackSimpleQualifiersAdded(Track *track);
    /*!
     * \brief A track has simple real qualifiers added.
     * \param track
     * \sa Track::simpleRealQualifiersAdded
     */
    void trackSimpleRealQualifiersAdded(Track *track);
    /*!
     * \brief A track has simple string qualifiers added.
     * \param track
     * \sa Track::simpleStringQualifiersAdded
     */
    void trackSimpleStringQualifiersAdded(Track *track);
    /*!
     * \brief A track has detailed qualifiers added.
     * \param track
     * \sa Track::detailedQualifiersAdded
     */
    void trackDetailedQualifiersAdded(Track *track);
    /*!
     * \brief A track has detailed real qualifiers added.
     * \param track
     * \sa Track::detailedRealQualifiersAdded
     */
    void trackDetailedRealQualifiersAdded(Track *track);
    /*!
     * \brief A track has detailed string qualifiers added.
     * \param track
     * \sa Track::detailedStringQualifiersAdded
     */
    void trackDetailedStringQualifiersAdded(Track *track);

    /*!
     * \brief A treatment has been added in Mélodium.
     * \param treatment
     * \sa addTreatment
     */
    void treatmentAdded(Treatment *treatment);
    /*!
     * \brief A treatment is ready to execute.
     * \param treatment
     * \sa Treatment::ready
     */
    void treatmentReady(Treatment *treatment);
    /*!
     * \brief A treatment started.
     * \param treatment
     * \sa Treatment::started
     */
    void treatmentStarted(Treatment *treatment);
    /*!
     * \brief A treatment finished.
     * \param treatement
     * \sa Treatment::finished
     */
    void treatmentFinished(Treatment *treatement);
    /*!
     * \brief A treatment failed.
     * \param treatment
     * \sa Treatment::failed
     */
    void treatmentFailed(Treatment *treatment);

    /*!
     * \brief A treatment has been added in Mélodium.
     * \param model
     */
    void modelAdded(Model *model);
    /*!
     * \brief A model has been totally fed.
     * \param model
     *
     * \sa Model::feedingFinished
     */
    void modelFeedingFinished(Model *model);

private:
    /*!
     * \brief Standard console output stream.
     */
    QTextStream cout;
    /*!
     * \brief Standard console error stream.
     */
    QTextStream cerr;
    /*!
     * \brief Command line parser.
     */
    QCommandLineParser _commandLineParser;
    /*!
     * \brief Initialize the command line parser.
     * \sa _commandLineParser
     */
    void initCommandLineParser();

    /*!
     * \brief Stand if command line options are valid or not.
     *
     * true if all is fine, else false.
     */
    bool _cmdOptionsOk;
    /*!
     * \brief Stand if the program should instanciate an initializer or not.
     *
     * \default true
     */
    bool _cmdInstanciateInitializer;

    /*!
     * \brief Manage information printing about treatments and other.
     */
    void cmdOptionInfo();

    /*!
     * \brief Stands if the program should make the execution graph.
     *
     * \default false
     */
    bool _graph;
    /*!
     * \brief Filename where to write graph.
     */
    QString _graphFilename;
    /*!
     * \brief Manage graph option.
     */
    void cmdOptionGraph();

    /*!
     * \brief Manage number of threads according to the command line.
     */
    void cmdOptionThreads();

    /*!
     * \brief Stands if the execution should not be finally done.
     *
     * \default false
     */
    bool _noExec;
    /*!
     * \brief Manage noexec option.
     */
    void cmdOptionNoExec();

    /*!
     * \brief Tracks managed.
     */
    QList<Track*> _tracks;

    /*!
     * \brief Treatments managed.
     */
    QList<Treatment*> _treatments;

    /*!
     * \brief Models managed.
     */
    QList<Model*> _models;

    /*!
     * \brief Scheduler used to execute treatments.
     */
    Scheduler *_scheduler;
    /*!
     * \brief Initialize scheduler.
     *
     * According threads number and user commands, instanciates the "best" scheduler.
     */
    void initScheduler();

    unsigned int _treatmentsFinished;
    unsigned int _treatmentsFailed;

    void printProgress();

    void printFailure(Treatment *treatment);

private slots:
    /*!
     * \brief Manage change of original MIDI file.
     * \param originalMidiFile
     * \param track
     *
     * \sa Track::originalMidiFileChanged
     */
    void manageTrackOriginalMidiFileChanged(QString originalMidiFile, Track *track);
    /*!
     * \brief Manage change of audio file.
     * \param audioFile
     * \param track
     *
     * \sa Track::audioFileChanged
     */
    void manageTrackAudioFileChanged(QString audioFile, Track *track);
    /*!
     * \brief Manage change of MIDI file.
     * \param midiFile
     * \param track
     *
     * \sa Track::midiFileChanged
     */
    void manageTrackMidiFileChanged(QString midiFile, Track *track);

    void incrementTreatmentsFinished();
    void incrementTreatmentsFailed();

    void schedulerFinished();

};

#endif // MELODIUM_H
