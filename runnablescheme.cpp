/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "runnablescheme.h"

RunnableScheme::Notification::Notification(RunnableScheme::NotificationType type, QString info) :
    type(type),
    info(info)
{
}

RunnableScheme::RunnableScheme(QObject *parent) : QObject(parent)
{
    _hasErrors = false;
    _hasWarnings = false;
}

void RunnableScheme::check()
{
    auto treatments = Melodium::instance()->treatments();
    QMap<Treatment*, QVector<Treatment*>> previousList;

    for (auto treatment: treatments)
        if (!previousList.contains(treatment))
            previousList.insert(treatment, QVector<Treatment*>());


    for (auto treatment: treatments)
        for (auto next: treatment->next())
            previousList.find(next).value().append(treatment);


    // Detect inputs satisfaction.
    for (auto treatmentIterator = previousList.begin() ;
         treatmentIterator != previousList.end() ;
         ++treatmentIterator) {
        auto treatment = treatmentIterator.key();
        auto inputs = treatment->type().require();

        for (auto previous: treatmentIterator.value()) {
            for (auto output: previous->type().provide()) {
                for (int i=0 ; i < inputs.size() ; ++i) {
                    auto input = inputs.at(i);
                    if (treatment->runnable()->inputName(input.name) ==
                            previous->runnable()->outputName(output.name)) {
                        if (input.type != output.type) {
                            // ERROR: types don't match.
                            _hasErrors = true;
                            _notifications.append(Notification(Error, previous->type().name() + " -> " + treatment->type().name() + ": " +
                                                               formatIoName(previous->runnable()->outputName(output.name), output.name, output.type) + " and " +
                                                               formatIoName(treatment->runnable()->inputName(input.name), input.name, input.type) +
                                                               " don't have same type."));
                        }
                        inputs.remove(i);
                        --i;
                    }
                }
            }
        }

        if (inputs.size() != 0) {
            // ERROR: all entries aren't satisfied.
            _hasErrors = true;
            for (auto input: inputs)
                _notifications.append(Notification(Error, treatment->type().name() + ": " +
                                                   formatIoName(treatment->runnable()->inputName(input.name), input.name, input.type) +
                                                   " is not set."));
        }
    }

    // Detect unused outputs.
    for (auto treatment: treatments) {
        auto outputs = treatment->type().provide();

        auto nexts = treatment->next();
        for (auto next: nexts) {
            auto inputs = treatment->type().require();
            for (auto input: inputs)
                for (int i=0 ; i < outputs.size() ; ++i) {
                    if (treatment->runnable()->outputName(outputs.at(i).name) ==
                            next->runnable()->inputName(input.name)) {
                        outputs.remove(i);
                        break;
                    }
                }
        }

        for (auto output: outputs) {
            _notifications.append(Notification(Notice, treatment->type().name() + ": " +
                                               formatIoName(treatment->runnable()->outputName(output.name), output.name, output.type) +
                                               " is not used."));
        }
    }
}

bool RunnableScheme::hasErrors() const
{
    return _hasErrors;
}

bool RunnableScheme::hasWarnings() const
{
    return _hasWarnings;
}

QList<RunnableScheme::Notification> RunnableScheme::notifications() const
{
    return _notifications;
}

void RunnableScheme::printNotifications(QTextStream &stream) const
{
    for (auto notif: _notifications)
        printNotification(notif, stream);
}

void RunnableScheme::printNotification(RunnableScheme::Notification notification, QTextStream &cout)
{
    switch (notification.type) {
    case RunnableScheme::Error:
        cout << "\033[31mError\033[39m: ";
        break;
    case RunnableScheme::Warning:
        cout << "\033[33mWarning\033[39m: ";
        break;
    case RunnableScheme::Notice:
        cout << "\033[35mNotice\033[39m: ";
        break;
    }

    cout << notification.info << endl;
}

void RunnableScheme::makeGraph(const QString &filename)
{
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly))
        return; //Maybe throw error.

    const char tab = '\t';
    auto treatmentName = [](const Treatment *treatment){
        QString name("t_" + treatment->type().name().toLower());
        name += '_' + QString::number(reinterpret_cast<intptr_t>(treatment), 16);
        return name;
    };

    QTextStream out(&file);
    out.setCodec("UTF-8");
    out << "digraph melodiumScheme {" << endl;
    out << tab << "rankdir=LR;" << endl;
    out << tab << "node [shape=box];" << endl << endl;

    auto treatments = Melodium::instance()->treatments();
    for (Treatment *treatment: treatments) {

        out << tab << treatmentName(treatment)
                    << " [label=<<TABLE BORDER=\"0\"><TR><TD COLSPAN=\"2\">" << treatment->type().name() << "</TD></TR>";

        QVector<TreatmentType::ParamInfo> params = treatment->type().parameters();
        for (auto param: params) {
            out << "<TR><TD ALIGN=\"RIGHT\"><I>" << param.name
                << "</I>:</TD><TD ALIGN=\"LEFT\">";
            if (param.type == TreatmentType::STRING)
                out << "<I>'" << treatment->parameter(param.name).toString() << "'</I></TD></TR>";
            else
                out << treatment->parameter(param.name).toString() << "</TD></TR>";

        }

        out << "</TABLE>>];" << endl;

        for (Treatment *next: treatment->next()) {
            out << tab << treatmentName(treatment) << " -> " << treatmentName(next) << ';' << endl;
        }
        out << endl;
    }

    out << "}";
    out.flush();
    file.close();

}

QString RunnableScheme::formatIoName(QString usedName, QString originalName, TreatmentType::DataType dataType)
{
    QString fn("\033[3m" + originalName + "\033[23m ");
    if (usedName != originalName) {
        fn += '[' + usedName + "] ";
    }
    fn += "\033[2m" + TreatmentInfoPrinter::dataTypeName(dataType) + "\033[22m";

    return fn;
}
