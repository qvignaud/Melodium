# Mélodium

A musical information retriever, developed for research in MIR (*Musical Information Retrieval*) using signal analysis and machine learning.

This project is **under development** for Linux and MacOS platforms only, but might compile and work properly on other systems, as WSL, or pure Windows. Cross-platform compatibility on purpose could come in future.

## Compilation

### Requirements
Compiling Mélodium will require the following components:
- Qt 5.12 or higher
- Essentia 2.1 or higher
- Taglib 1.9 or higher
- ALSA library

In order to work, Mélodium also require the following components to be available on your system:
- FFmpeg
- FluidSynth *or* Timidity++
- Fluid R3 GM & GS soundfonts

### Step-by-step (for Debian-like)

#### Step 1
If you're lucky enough (or that readme became bit old), Qt 5.12 and Essentia 2.1 could be directly available in your system package manager.

For Qt type `apt-get install qtbase5-dev`, be careful it is Qt **5.12** or higher.
Else see [Qt project](https://doc.qt.io/qt-5/gettingstarted.html).

For Essentia, try `apt-get install libessentia-dev`.
Else follow [its documentation](https://essentia.upf.edu/documentation/).
See also the following issue to avoid segmentation faults coming from Essentia: qvignaud/Melodium#1

Taglib and ALSA are base components of most distributions, but you'll probably still need development packages, so `apt-get install libtag1-dev libasound2-dev`.

If you are foolhardy, just type `apt-get install qt5base-dev libessentia-dev libtag1-dev libasound2-dev` and let destiny decide.

#### Step 2

Clone locally Mélodium, in the folder you want,
`git clone --recurse-submodules https://gitlab.com/qvignaud/Melodium.git`.

#### Step 3

In the cloned folder, `qmake`, then `make`…

#### Step 4

Depending the usage you will have, Mélodium could call tools like FFmpeg, FluidSynth, or Timidity++, and work with soundfonds Fluid R3.

The easiest way is to simply make their installation thought package manager: `apt-get install ffmpeg fluidsynth timidity fluid-soundfont-gm fluid-soundfont-gs`.

#### Step 5

You're done! You can use Mélodium inside build directory with the command `melodium`.

## Documentation

The documentation can be generated using Doxygen. Just call `doxygen` inside the `doc` directory.

Doxygen should be available in your package manager, and you can also check its project site at [http://www.doxygen.nl/](http://www.doxygen.nl/).

## License

This software is free and open-source, under the EUPL licence.

Why this one specifically? Well, as this project have a particular relationship with cultural world, probably more than most other softwares, it is important to have a strong legal basis covering also the notion of artwork.
In the same way, as *no culture is more important than another*, it was important to have a licence readable and understanble by most of people. The EUPL is available and *legally valid* in 23 languages, covering a large number of people.

Then, the legal part:
> Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence"); You may not use this work except in compliance with the Licence. You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/software/page/eupl
>
>Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and limitations under the Licence.

And do not worry, this licence is explicitly compatible with the ones mentionned in its appendix, including most of the common open-source licences.
