/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "preparatorexception.h"

PreparatorException::PreparatorException(const Type type, const QString cause) :
    _type(type),
    _cause(cause)
{

}

void PreparatorException::raise() const
{
    throw *this;
}

PreparatorException *PreparatorException::clone() const
{
    return new PreparatorException(*this);
}

PreparatorException::Type PreparatorException::type() const
{
    return _type;
}

QString PreparatorException::cause() const
{
    return _cause;
}
