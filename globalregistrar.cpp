/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "globalregistrar.h"

#include "initializersfactory.h"

#include "initializers/devinitializer.h"
#include "initializers/hpcptrainer.h"
#include "initializers/visualizerinitializer.h"
#include "initializers/hpcpvisualizer.h"
#include "initializers/midigenerator.h"
#include "initializers/notesestimationexperiment.h"
#include "initializers/instrumentfilteringexperiments.h"
#include "initializers/modelevaluator.h"
#include "initializers/scriptinitializer.h"

#include "modelsfactory.h"

#include "models/devmodel.h"
#include "models/ffnrect3hl.h"
#include "models/configurablerectffn.h"
#include "models/configurablevarffn.h"
#include "models/sparseautoencoder.h"

#include "treatmentsfactory.h"

#include "runnables/converters/ffmpegconverter.h"

#include "runnables/audiorenderer/fluidsynthrender.h"
#include "runnables/audiorenderer/timidityrender.h"

#include "runnables/analysts/accumulator.h"
#include "runnables/analysts/concatenator.h"
#include "runnables/analysts/differentiator.h"
#include "runnables/analysts/thresholder.h"
#include "runnables/analysts/evaluator.h"
#include "runnables/analysts/essentiaregistrar.h"
#include "runnables/analysts/miditracksanalyst.h"
#include "runnables/analysts/midivoicesanalyst.h"
#include "runnables/analysts/miditracksfiller.h"
#include "runnables/analysts/soundfontanalyst.h"
#include "runnables/analysts/scaler.h"

#include "runnables/exporters/csvexporter.h"
#include "runnables/exporters/imageexporter.h"
#include "runnables/exporters/jsonexporter.h"
#include "runnables/exporters/midiexporter.h"

void GlobalRegistrar::registerAll()
{
    registerInitializers();
    registerModels();
    registerTreatments();
}

void GlobalRegistrar::registerInitializers()
{
    InitializersFactory::addAvailableInitializer("dev", &DevInitializer::create);
    InitializersFactory::addAvailableInitializer("hpcptrainer", &HpcpTrainer::create);
    InitializersFactory::addAvailableInitializer("hpcpvisualizer", &HpcpVisualizer::create);
    InitializersFactory::addAvailableInitializer("visualizer", &VisualizerInitializer::create);
    InitializersFactory::addAvailableInitializer("midigenerator", &MidiGenerator::create);
    InitializersFactory::addAvailableInitializer("notesestimationexperiment", &NotesEstimationExperiment::create);
    InitializersFactory::addAvailableInitializer("instrumentfilteringexperiments", &InstrumentFilteringExperiments::create);
    InitializersFactory::addAvailableInitializer("modelevaluator", &ModelEvaluator::create);
    InitializersFactory::addAvailableInitializer("script", &ScriptInitializer::create);
}

void GlobalRegistrar::registerModels()
{
    ModelsFactory::addAvailableModel(DevModel::modelType(), &DevModel::create);
    ModelsFactory::addAvailableModel(FfnRect3hl::modelType(), &FfnRect3hl::create);
    ModelsFactory::addAvailableModel(ConfigurableRectFfn::modelType(), &ConfigurableRectFfn::create);
    ModelsFactory::addAvailableModel(ConfigurableVarFfn::modelType(), &ConfigurableVarFfn::create);
    ModelsFactory::addAvailableModel(SparseAutoencoder::modelType(), &SparseAutoencoder::create);
}

void GlobalRegistrar::registerTreatments()
{
    registerConverters();
    registerAudioRenderers();
    registerAnalysts();
    registerExporters();
}

void GlobalRegistrar::registerConverters()
{
    TreatmentsFactory::addAvailableTreatment(FfmpegConverter::treatmentType(), &FfmpegConverter::create);
}

void GlobalRegistrar::registerAudioRenderers()
{
    TreatmentsFactory::addAvailableTreatment(TimidityRender::treatmentType(), &TimidityRender::create);
    TreatmentsFactory::addAvailableTreatment(FluidsynthRender::treatmentType(), &FluidsynthRender::create);
}

void GlobalRegistrar::registerAnalysts()
{
    TreatmentsFactory::addAvailableTreatment(Accumulator::treatmentType(), &Accumulator::create);
    TreatmentsFactory::addAvailableTreatment(Concatenator::treatmentType(), &Concatenator::create);
    TreatmentsFactory::addAvailableTreatment(Differentiator::treatmentType(), &Differentiator::create);
    TreatmentsFactory::addAvailableTreatment(Thresholder::treatmentType(), &Thresholder::create);
    TreatmentsFactory::addAvailableTreatment(Evaluator::treatmentType(), &Evaluator::create);
    EssentiaRegistrar::registerEssentiaTreatments();
    TreatmentsFactory::addAvailableTreatment(MidiTracksAnalyst::treatmentType(), &MidiTracksAnalyst::create);
    TreatmentsFactory::addAvailableTreatment(MidiVoicesAnalyst::treatmentType(), &MidiVoicesAnalyst::create);
    TreatmentsFactory::addAvailableTreatment(SoundFontAnalyst::treatmentType(), &SoundFontAnalyst::create);
    TreatmentsFactory::addAvailableTreatment(MidiTracksFiller::treatmentType(), &MidiTracksFiller::create);
    TreatmentsFactory::addAvailableTreatment(Scaler::treatmentType(), &Scaler::create);
}

void GlobalRegistrar::registerExporters()
{
    TreatmentsFactory::addAvailableTreatment(CsvExporter::treatmentType(), &CsvExporter::create);
    TreatmentsFactory::addAvailableTreatment(ImageExporter::treatmentType(), &ImageExporter::create);
    TreatmentsFactory::addAvailableTreatment(JsonExporter::treatmentType(), &JsonExporter::create);
    TreatmentsFactory::addAvailableTreatment(MidiExporter::treatmentType(), &MidiExporter::create);
}
