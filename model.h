/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include "modeltype.h"
#include "treatment.h"

/*!
 * \brief Base class for models.
 */
class Model : public QObject
{
    Q_OBJECT
public:

    /*!
     * \brief Instanciates an new model.
     * \param parent
     */
    explicit Model(const ModelType &type, QObject *parent = nullptr);
    ~Model();

    /*!
     * \brief Get the type of model.
     * \return
     */
    ModelType type() const;

    /*!
     * \brief Get a new feeding runnable for the model.
     * \param track
     * \param parent
     * \return
     */
    Runnable *feedingRunnable(TreatmentType type, Track *track, QObject *parent=nullptr);
    /*!
     * \brief Get the training runnable for the model.
     * \param parent
     * \return
     */
    Runnable *trainingRunnable(TreatmentType type, QObject *parent=nullptr);
    /*!
     * \brief Get a new prediction runnable for the model.
     * \param track
     * \param parent
     * \return
     */
    Runnable *predictionRunnable(TreatmentType type, Track *track, QObject *parent=nullptr);
    /*!
     * \brief Get a saving runnable for the model.
     * \param parent
     * \return
     */
    Runnable *savingRunnable(TreatmentType type, QObject *parent=nullptr);
    /*!
     * \brief Get a loader runnable for the model.
     * \param parent
     * \return
     */
    Runnable *loadingRunnable(TreatmentType type, QObject *parent=nullptr);

    /*!
     * \brief Tells if feeding is finished.
     * \return
     */
    bool isFeedingFinished() const;

signals:
    /*!
     * \brief Feeding is finished.
     *
     * This signal is emitted when all feeding treatments are finished.
     * It generally means that the training can be done, depending of the model kind.
     */
    void feedingFinished(Model *model);

protected:
    /*!
     * \brief This function have to return a feeding runnable for the model.
     * \param track
     * \param parent
     * \return
     */
    virtual Runnable *createFeedingRunnable(Track *track, QObject *parent=nullptr);
    /*!
     * \brief This function have to return a training runnable for the model.
     * \param track
     * \param parent
     * \return
     */
    virtual Runnable *createTrainingRunnable(QObject *parent=nullptr);
    /*!
     * \brief This function have to return a prediction runnable for the model.
     * \param track
     * \param parent
     * \return
     */
    virtual Runnable *createPredictionRunnable(Track *track, QObject *parent=nullptr);
    /*!
     * \brief This function have to return a saving runnable for the model.
     * \param parent
     * \return
     */
    virtual Runnable *createSavingRunnable(QObject *parent=nullptr);
    /*!
     * \brief This function have to return a loading runnable for the model.
     * \param parent
     * \return
     */
    virtual Runnable *createLoadingRunnable(QObject *parent=nullptr);

private:
    /// Type of the model.
    const ModelType _type;

    /// Number of feeding treatments created.
    unsigned int _feedersCreated;
    /// Number of feeding treatments finished.
    unsigned int _feedersFinished;

private slots:
    /// Slot incrementing finished feeders and emitting signal if needed.
    void manageFeederFinished();
};

#endif // MODEL_H
