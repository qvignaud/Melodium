/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "treatmentinfoprinter.h"

QTextStream TreatmentInfoPrinter::cout(stdout);

void TreatmentInfoPrinter::print(const QList<TreatmentType> &tts)
{
    for (auto tt: tts)
        print(tt);
}

void TreatmentInfoPrinter::print(const TreatmentType &tt)
{
    cout << "\033[1m" << tt.name() << "\033[21m [" << roleName(tt.role()) << ", \033[3m" << trackOperationName(tt.trackOperation()) << "\033[23m] :" << endl;

    cout << "\t\033[4mDescription:\033[24m" << endl;
    cout << "\t\t" << tt.description().replace('\n', "\n\t\t") << endl;

    cout << "\t\033[4mParameters:\033[24m ";
    if (tt.parameters().empty()) cout << "None" << endl;
    else {
        cout << endl;

        for (auto param: tt.parameters()) {
            cout << "\t\t"
                 << "\033[2m" + dataTypeName(param.type) + "\033[22m "
                 << "\033[3m" + param.name + "\033[23m"
                 << ':' << endl
                 << "\t\t\t"
                 << param.description
                 << endl
                 << "\t\t\t"
                 << "Default: "
                 << qSetFieldWidth(23)
                 << left
                    ;

            if (param.defaultValue.isNull())
                cout << "None";
            else
                if (param.type == TreatmentType::STRING)
                    cout << "'" + param.defaultValue.toString() + "' ";
                else if (param.type == TreatmentType::VECTOR_STRING)
                    cout << qSetFieldWidth(0)
                         << "{"
                         << param.defaultValue.toStringList()
                            .replaceInStrings(QRegExp("^"), "'")
                            .replaceInStrings(QRegExp("$"), "'")
                            .join(", ")
                         << "}"
                         << endl
                         << "\t\t\t";
                else
                    cout << "\033[3m" + param.defaultValue.toString() + "\033[23m ";

            cout << qSetFieldWidth(0)
                 << "Admitted values: "
                 << valuesString(param.type, param.values)
                 << endl << endl
                    ;
        }
    }

    cout << "\t\033[4mRequire:\033[24m ";
    if (tt.require().empty()) cout << "Nothing" << endl;
    else {
        cout << endl;

        for (auto req: tt.require()) {
            cout << "\t\t"
                 << "\033[2m" + dataTypeName(req.type) + "\033[22m "
                 << "\033[3m" + req.name + "\033[23m"
                 << ':' << endl
                 << "\t\t\t"
                 << req.description
                 << endl << endl
                    ;
        }
    }

    cout << "\t\033[4mProvide:\033[24m ";
    if (tt.provide().empty()) cout << "Nothing" << endl;
    else {
        cout << endl;

        for (auto pro: tt.provide()) {
            cout << "\t\t"
                 << "\033[2m" + dataTypeName(pro.type) + "\033[22m "
                 << "\033[3m" + pro.name + "\033[23m"
                 << ':' << endl
                 << "\t\t\t"
                 << pro.description
                 << endl << endl
                    ;
        }
    }
}

void TreatmentInfoPrinter::simplePrint(const QList<TreatmentType> &tts)
{
    for (auto tt: tts)
        simplePrint(tt);
}

void TreatmentInfoPrinter::simplePrint(const TreatmentType &tt)
{
    cout << "\033[1m" << tt.name() << "\033[21m [" << roleName(tt.role()) << "]" << endl;
}

QString TreatmentInfoPrinter::roleName(const TreatmentType::Role role)
{
    switch (role) {
    case TreatmentType::AudioRender:
        return "Audio render";
    case TreatmentType::Converter:
        return "Converter";
    case TreatmentType::Analysis:
        return "Analyst";
    case TreatmentType::ModelFeeder:
        return "Model feeder";
    case TreatmentType::ModelTrainer:
        return "Model trainer";
    case TreatmentType::ModelPredictor:
        return "Model predictor";
    case TreatmentType::ModelSaver:
        return "Model saver";
    case TreatmentType::ModelLoader:
        return "Model loader";
    case TreatmentType::DataExporter:
        return "Data exporter";
    }
    return "";
}

QString TreatmentInfoPrinter::trackOperationName(const TreatmentType::TrackOperation to)
{
    switch (to) {
    case TreatmentType::MonoTrack:
        return "Mono-track";
    case TreatmentType::MultiTrack:
        return "Multi-tracks";
    case TreatmentType::NoTrack:
        return "Track-independant";

    }
    return "";
}

QString TreatmentInfoPrinter::dataTypeName(const TreatmentType::DataType dt)
{
    switch (dt) {
    case TreatmentType::NONE:
        return "None";
    case TreatmentType::BOOL:
        return "Bool";
    case TreatmentType::INT:
        return "Integer";
    case TreatmentType::REAL:
        return "Real";
    case TreatmentType::STRING:
        return "String";
    case TreatmentType::VECTOR_REAL:
        return "Vector<Real>";
    case TreatmentType::VECTOR_STRING:
        return "Vector<String>";
    case TreatmentType::VECTOR_VECTOR_REAL:
        return "Vector²<Real>";
    case TreatmentType::VECTOR_VECTOR_STRING:
        return "Vector²<String>";
    }
    return "";
}

QString TreatmentInfoPrinter::valuesString(const TreatmentType::DataType dt, const QList<QVariant> &lv)
{
    QStringList out;
    QString pre;
    QString post;

    if (lv.empty()) return QString();

    if (dt == TreatmentType::BOOL) {
        pre = '{';
        for (auto v: lv) out += "\033[3m" + v.toString() + "\033[23m";
        post = '}';
    }
    else if (dt == TreatmentType::INT) {
        if (lv.size() == 2) {
            pre = '[';
            post = ']';
        }
        else {
            pre = '{';
            post = '}';
        }
        for (auto v: lv) out += "\033[3m" + QString::number(v.toInt()) + "\033[23m";
    }
    else if (dt == TreatmentType::REAL) {
        if (lv.size() == 2) {
            pre = '[';
            post = ']';
        }
        else {
            pre = '{';
            post = '}';
        }
        for (auto v: lv) out += "\033[3m" + QString::number(v.toDouble()) + "\033[23m";
    }
    else if (dt == TreatmentType::STRING or
             dt == TreatmentType::VECTOR_STRING) {
        pre = '{';
        for (auto v: lv) out += '\'' + v.toString() + '\'';
        post = '}';
    }

    return pre + out.join(", ") + post;
}
