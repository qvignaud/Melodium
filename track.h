/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef TRACK_H
#define TRACK_H

#include <QtGlobal>
#include <string>
#include <vector>
#include <QObject>
#include <QMap>
#include <QVector>
#include <QMutex>
#include <QMutexLocker>
#include <QFile>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QMimeType>
#include <wavfile.h>
#include <flacfile.h>
#include "trackexception.h"

/*!
 * \brief Track representation and management class.
 *
 * This class manage a track inside the Mélodium environment.
 *
 * A track is related to a music/audio signal, and defined by a sample rate, frame and hop sizes.
 * Qualifiers of a track can be of two types, either real or string, in three categories:
 * - global qualifiers, qualifiyng the whole track;
 * - simple qualifiers, qualifying each tick by a single value;
 * - detailed qualifiers, qualifying each tick with multiples values.
 *
 * Non-global qualifiers defines each sample or frame (vectors are sized according number of samples or frames).
 * In the case of detailed qualifiers, each subvector have to be qualified by the same number of values.
 *
 * A track must have at least one of these files to have meaning:
 * - original MIDI file (used to be rendered or for evaluation);
 * - audio file (in lossless format, only FLAC or WAVE with PCM);
 * - MIDI file (result from process).
 *
 *
 * \note    This class is thread-safe.
 * \sa TrackException
 */
class Track : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Creates a new track.
     * \param sampleRate    Sample rate.
     * \param frameSize     Size of the frame.
     * \param hopSize       Size of the hop.
     * \param parent        Parent object.
     *
     * A track is defined by all these parameters.
     *
     * \note If treatments has to be done with different ones another track is needed, even if files are the same.
     *
     * \sa TrackException
     */
    explicit Track(int sampleRate, int frameSize, int hopSize, QObject *parent = nullptr);
    ~Track();

    /*!
     * \brief Length of the track, in milliseconds.
     * \return Length.
     */
    quint64 length() const;
    /*!
     * \brief Set the length of the track.
     * \param length
     *
     * Computes samples and frames too.
     *
     * \note This can only be called once, as for setSamples and setFrames.
     */
    void setLength(quint64 length);

    /*!
     * \brief Samples in the track.
     * \return Samples.
     */
    quint64 samples() const;
    /*!
     * \brief Set number of samples in the track.
     * \param samples
     *
     * Computes length and frames too.
     *
     * \note This can only be called once, as for setLength and setFrames.
     */
    void setSamples(quint64 samples);

    /*!
     * \brief Sample rate of the track.
     * \return Sample rate.
     */
    int sampleRate() const;

    /*!
     * \brief Frame size of the track.
     * \return Frame size.
     */
    int frameSize() const;

    /*!
     * \brief Hop size of the track.
     * \return Hop size.
     */
    int hopSize() const;

    /*!
     * \brief Frames in the tracks.
     * \return Frame count.
     */
    quint64 frames() const;
    /*!
     * \brief Set number of frames in the track.
     * \param frames
     *
     * Computes length and samples too.
     *
     * \note This can only be called once, as for setLength and setSamples.
     */
    void setFrames(quint64 frames);

    /*!
     * \brief Tell if the track has an original MIDI file.
     * \return  true if the track has an original MIDI file, false if not.
     */
    bool hasOriginalMidiFile() const;

    /*!
     * \brief Original MIDI file of the track.
     * \return  Original MIDI file.
     */
    QString originalMidiFile() const;
    /*!
     * \brief Set the original MIDI file of the track.
     * \param originalMidiFile
     * \return true if the file is a MIDI file, else false.
     */
    bool setOriginalMidiFile(const QString originalMidiFile);

    /*!
     * \brief Tell if the track has an audio file.
     * \return true if the track has an audio file, else false.
     */
    bool hasAudioFile() const;

    /*!
     * \brief Audio file of the track.
     * \return Audio file.
     */
    QString audioFile() const;
    /*!
     * \brief Set the audio file of the track.
     * \param audioFile
     * \return true if the audio file is a FLAC or WAVE file, else false.
     */
    bool setAudioFile(const QString audioFile);

    /*!
     * \brief Tell if the track has a resulting MIDI file.
     * \return true if the track has a MIDI file, else false.
     */
    bool hasMidiFile() const;

    /*!
     * \brief MIDI file of the track.
     * \return MIDI file.
     */
    QString midiFile() const;
    /*!
     * \brief Set the MIDI file of the track.
     * \param midiFile
     * \return true if the file is a MIDI file, else false.
     */
    bool setMidiFile(const QString midiFile);

    /*!
     * \brief Set the file to the track.
     * \param fileName
     *
     * This is a convenience function.
     * The fileName will be checked as follow:
     * - if it ends with ".result.midi" and is MIDI file, it will be used as MIDI file;
     * - if it is a MIDI file, it will be used as original MIDI file;
     * - if it is an audio file, it will be used as audio file.
     *
     * \return true if the file is matching one of the case, else false.
     */
    bool setFile(const QString fileName);

    /*!
     * \brief Global real qualifiers of the track.
     * \return A copy of global real qualifiers.
     * \warning Qualifiers can be heavy to copy, see returning reference alternative for optimization.
     */
    QMap<QString, float> globalRealQualifiers() const;
    /*!
     * \brief Global real qualifiers of the track.
     * \param mutex Reference to a mutex pointer, to use for unlock after read.
     * \return A reference to global real qualifiers.
     * \warning Don't forgot to call mutex->unlock() after reading what needed from the map.
     */
    const QMap<QString, float>& globalRealQualifiers(QMutex *&mutex) const;

    /*!
     * \brief Global string qualifiers of the track.
     * \return A copy of global string qualifiers.
     * \warning Qualifiers can be heavy to copy, see returning reference alternative for optimization.
     */
    QMap<QString, std::string> globalStringQualifiers() const;
    /*!
     * \brief Global string qualifiers of the track.
     * \param mutex Reference to a mutex pointer, to use for unlock after read.
     * \return A reference to global string qualifiers.
     * \warning Don't forgot to call mutex->unlock() after reading what needed from the map.
     */
    const QMap<QString, std::string>& globalStringQualifiers(QMutex *&mutex) const;

    /*!
     * \brief Simple real qualifiers of the track.
     * \return A copy of simple real qualifiers.
     * \warning Qualifiers can be heavy to copy, see returning reference alternative for optimization.
     */
    QMap<QString, std::vector<float> > simpleRealQualifiers() const;
    /*!
     * \brief Simple real qualifiers of the track.
     * \param mutex Reference to a mutex pointer, to use for unlock after read.
     * \return A reference to simple real qualifiers.
     * \warning Don't forgot to call mutex->unlock() after reading what needed from the map.
     */
    const QMap<QString, std::vector<float> >& simpleRealQualifiers(QMutex *&mutex) const;

    /*!
     * \brief Simple string qualifiers of the track.
     * \return A copy of simple string qualifiers.
     * \warning Qualifiers can be heavy to copy, see returning reference alternative for optimization.
     */
    QMap<QString, std::vector<std::string> > simpleStringQualifiers() const;
    /*!
     * \brief Simple string qualifiers of the track.
     * \param mutex Reference to a mutex pointer, to use for unlock after read.
     * \return A reference to simple string qualifiers.
     * \warning Don't forgot to call mutex->unlock() after reading what needed from the map.
     */
    const QMap<QString, std::vector<std::string> >& simpleStringQualifiers(QMutex *&mutex) const;

    /*!
     * \brief Detailed real qualifiers of the track.
     * \return A copy of detailed real qualifiers.
     * \warning Qualifiers can be heavy to copy, see returning reference alternative for optimization.
     */
    QMap<QString, std::vector<std::vector<float> > > detailedRealQualifiers() const;
    /*!
     * \brief Detailed real qualifiers of the track.
     * \param mutex Reference to a mutex pointer, to use for unlock after read.
     * \return A reference to detailed real qualifiers.
     * \warning Don't forgot to call mutex->unlock() after reading what needed from the map.
     */
    const QMap<QString, std::vector<std::vector<float> > >& detailedRealQualifiers(QMutex *&mutex) const;

    /*!
     * \brief Detailed string qualifiers of the track.
     * \return A copy of detailed string qualifiers.
     * \warning Qualifiers can be heavy to copy, see returning reference alternative for optimization.
     */
    QMap<QString, std::vector<std::vector<std::string> > > detailedStringQualifiers() const;
    /*!
     * \brief Detailed string qualifiers of the track.
     * \param mutex Reference to a mutex pointer, to use for unlock after read.
     * \return A reference to detailed string qualifiers.
     * \warning Don't forgot to call mutex->unlock() after reading what needed from the map.
     */
    const QMap<QString, std::vector<std::vector<std::string> > >& detailedStringQualifiers(QMutex *&mutex) const;

    /*!
     * \brief Store a qualifier in the track.
     * \param name  Name of the qualifier.
     * \param value Value associated.
     *
     * Qualifier have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifier(const QString &name, float value);
    /*!
     * \brief Store a qualifier in the track.
     * \param name  Name of the qualifier.
     * \param value Value associated.
     *
     * Qualifier have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifier(const QString &name, const std::string &value);
    /*!
     * \brief Store a qualifier in the track.
     * \param name  Name of the qualifier.
     * \param value Value associated.
     *
     * Qualifier have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifier(const QString &name, const std::vector<float> &value);
    /*!
     * \brief Store a qualifier in the track.
     * \param name  Name of the qualifier.
     * \param value Value associated.
     *
     * Qualifier have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifier(const QString &name, const std::vector<std::string> &value);
    /*!
     * \brief Store a qualifier in the track.
     * \param name  Name of the qualifier.
     * \param value Value associated.
     * \param dontCheckConstantDepth    Explicitly tell the method to not check if all subvectors have same size.
     * \warning If dontCheckConstantDepth is set to true, no TrackException::QualifierDepthNonConstant will be emitted, but treatments using this qualifier could fail if they're not explicitly designed to deal with it.
     *
     * Qualifier have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifier(const QString &name, const std::vector<std::vector<float>> &value, bool dontCheckConstantDepth=false);
    /*!
     * \brief Store a qualifier in the track.
     * \param name  Name of the qualifier.
     * \param value Value associated.
     * \param dontCheckConstantDepth    Explicitly tell the method to not check if all subvectors have same size.
     * \warning If dontCheckConstantDepth is set to true, no TrackException::QualifierDepthNonConstant will be emitted, but treatments using this qualifier could fail if they're not explicitly designed to deal with it.
     *
     * Qualifier have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifier(const QString &name, const std::vector<std::vector<std::string>> &value, bool dontCheckConstantDepth=false);

    /*!
     * \brief Stores the qualifiers for the track.
     * \param qualifiers
     *
     * Qualifiers have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifiers(const QMap<QString, float> &qualifiers);
    /*!
     * \brief Stores the qualifiers for the track.
     * \param qualifiers
     *
     * Qualifiers have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifiers(const QMap<QString, std::string> &qualifiers);
    /*!
     * \brief Stores the qualifiers for the track.
     * \param qualifiers
     *
     * Qualifiers have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifiers(const QMap<QString, std::vector<float>> &qualifiers);
    /*!
     * \brief Stores the qualifiers for the track.
     * \param qualifiers
     *
     * Qualifiers have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifiers(const QMap<QString, std::vector<std::string>> &qualifiers);
    /*!
     * \brief Stores the qualifiers for the track.
     * \param qualifiers
     * \param dontCheckConstantDepth    Explicitly tell the method to not check if all subvectors have same size.
     * \warning If dontCheckConstantDepth is set to true, no TrackException::QualifierDepthNonConstant will be emitted, but treatments using these qualifiers could fail if they're not explicitly designed to deal with it.
     *
     * Qualifiers have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifiers(const QMap<QString, std::vector<std::vector<float>>> &qualifiers, bool dontCheckConstantDepth=false);
    /*!
     * \brief Stores the qualifiers for the track.
     * \param qualifiers
     * \param dontCheckConstantDepth    Explicitly tell the method to not check if all subvectors have same size.
     * \warning If dontCheckConstantDepth is set to true, no TrackException::QualifierDepthNonConstant will be emitted, but treatments using these qualifiers could fail if they're not explicitly designed to deal with it.
     *
     * Qualifiers have to match with the characteristics of the track to be stored (length/ticks, sample rate, frame and hop sizes).
     * \sa TrackException
     */
    void storeQualifiers(const QMap<QString, std::vector<std::vector<std::string>>> &qualifiers, bool dontCheckConstantDepth=false);

    /*!
     * \brief Clear the named qualifier.
     * \param name
     *
     * This removes the entry of the qualifier map.
     */
    void clearGlobalRealQualifier(const QString &name);
    /*!
     * \brief Clear the named qualifier.
     * \param name
     *
     * This removes the entry of the qualifier map.
     */
    void clearGlobalStringQualifier(const QString &name);
    /*!
     * \brief Clear the named qualifier.
     * \param name
     *
     * This removes the entry of the qualifier map.
     */
    void clearSimpleRealQualifier(const QString &name);
    /*!
     * \brief Clear the named qualifier.
     * \param name
     *
     * This removes the entry of the qualifier map.
     */
    void clearSimpleStringQualifier(const QString &name);
    /*!
     * \brief Clear the named qualifier.
     * \param name
     *
     * This removes the entry of the qualifier map.
     */
    void clearDetailedRealQualifier(const QString &name);
    /*!
     * \brief Clear the named qualifier.
     * \param name
     *
     * This removes the entry of the qualifier map.
     */
    void clearDetailedStringQualifier(const QString &name);

    /*!
     * \brief Say if the track match the characteristics.
     * \param sampleRate
     * \param frameSize
     * \param hopSize
     * \param fileName
     * \return true if matching, else false.
     * \note This is a convenience function to make comparison between tracks.
     */
    bool match(int sampleRate, int frameSize, int hopSize, const QString &fileName) const;

    /*!
     * \brief Get human-readable summary informations about the track.
     * \return
     */
    QString info() const;

    /*!
     * \brief Say if a file is a MIDI file.
     * \param fileName
     * \return true if file exists and has MIME type of MIDI file, else false.
     */
    static bool isMidiFile(const QString &fileName);
    /*!
     * \brief Say if a file is an audio file.
     * \param fileName
     * \return true if file exists and has MIME type of FLAC or WAVE, else false.
     */
    static bool isAudioFile(const QString &fileName);
    /*!
     * \brief Say if a file is a FLAC file.
     * \param fileName
     * \return true if file exists and has MIME type of FLAC file, else false.
     */
    static bool isFlacFile(const QString &fileName);
    /*!
     * \brief Say if a file is a WAVE file.
     * \param fileName
     * \return true if file exists and has MIME type of WAVE file, else false.
     */
    static bool isWaveFile(const QString &fileName);

signals:
    /*!
     * \brief The original MIDI file has changed.
     * \param originalMidiFile
     * \param track
     */
    void originalMidiFileChanged(QString originalMidiFile, Track *track);
    /*!
     * \brief The audio file has changed.
     * \param audioFile
     * \param track
     */
    void audioFileChanged(QString audioFile, Track *track);
    /*!
     * \brief The MIDI file changed.
     * \param midiFile
     * \param track
     */
    void midiFileChanged(QString midiFile, Track *track);

    /*!
     * \brief Qualifiers has been added.
     * \param track
     */
    void qualifiersAdded(Track *track);
    /*!
     * \brief Global qualifiers has been added.
     * \param track
     */
    void globalQualifiersAdded(Track *track);
    /*!
     * \brief Global real qualifiers has been added.
     * \param track
     */
    void globalRealQualifiersAdded(Track *track);
    /*!
     * \brief Global string qualifiers has been added.
     * \param track
     */
    void globalStringQualifiersAdded(Track *track);
    /*!
     * \brief Simple qualifiers has been added.
     * \param track
     */
    void simpleQualifiersAdded(Track *track);
    /*!
     * \brief Simple real qualifiers has been added.
     * \param track
     */
    void simpleRealQualifiersAdded(Track *track);
    /*!
     * \brief Simple string qualifiers has been added.
     * \param track
     */
    void simpleStringQualifiersAdded(Track *track);
    /*!
     * \brief Detailed qualifiers has been added.
     * \param track
     */
    void detailedQualifiersAdded(Track *track);
    /*!
     * \brief Detailed real qualifiers has been added.
     * \param track
     */
    void detailedRealQualifiersAdded(Track *track);
    /*!
     * \brief Detailed string qualifiers has been added.
     * \param track
     */
    void detailedStringQualifiersAdded(Track *track);

public slots:

private:
    /*!
     * \brief Mutex for thread-safe work.
     */
    QMutex *_mutex;

    /*!
     * \brief Original MIDI file.
     */
    QString _originalMidiFile;
    /*!
     * \brief Audio file.
     */
    QString _audioFile;
    /*!
     * \brief MIDI file.
     */
    QString _midiFile;

    /*!
     * \brief Length of the track, in milliseconds.
     */
    quint64 _length;
    /*!
     * \brief Samples in the track.
     */
    quint64 _samples;
    /*!
     * \brief Sample rate.
     */
    const int _sampleRate;
    /*!
     * \brief Frame size.
     */
    const int _frameSize;
    /*!
     * \brief Hop size.
     */
    const int _hopSize;
    /*!
     * \brief Frames count.
     */
    quint64 _frames;

    /*!
     * \brief Global real qualifiers.
     */
    QMap<QString, float> _globalRealQualifiers;
    /*!
     * \brief Global string qualifiers.
     */
    QMap<QString, std::string> _globalStringQualifiers;
    /*!
     * \brief Simple real qualifiers.
     */
    QMap<QString, std::vector<float>> _simpleRealQualifiers;
    /*!
     * \brief Simple string qualifiers.
     */
    QMap<QString, std::vector<std::string>> _simpleStringQualifiers;
    /*!
     * \brief Detailed real qualifiers.
     */
    QMap<QString, std::vector<std::vector<float>>> _detailedRealQualifiers;
    /*!
     * \brief Detailed string qualifiers.
     */
    QMap<QString, std::vector<std::vector<std::string>>> _detailedStringQualifiers;

    /*!
     * \brief Say if a file exists and match the MIME type.
     * \param mimeName
     * \param fileName
     * \note This function uses the MIME database which is dependant of the platorm or the Qt distribution.
     * \return true if yes, else false.
     */
    static bool fileExistsAndMatchType(const QList<QString> &mimeName, const QString &fileName);

};

#endif // TRACK_H
