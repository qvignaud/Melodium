/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef INITIALIZER_H
#define INITIALIZER_H

#include "melodium.h"

/*!
 * \brief Base class for initializers.
 */
class Initializer : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates Initializer
     */
    explicit Initializer(QObject *parent = nullptr);

    /*!
     * \brief Get arguments given to the initializer.
     * \return
     */
    QStringList args() const;
    /*!
     * \brief Set arguments for the intializer.
     * \param args
     */
    void setArgs(const QStringList &args);

signals:

public slots:
    /*!
     * \brief Make initialization.
     */
    virtual void initialize() = 0;

protected:
    static Treatment *csvExport(Track *track, const QStringList &qualifiers, const QString &filename);
    static Treatment *imageExport(Track *track, const QString &data, const QString &filename);

private:
    /*!
     * \brief Arguments given.
     */
    QStringList _args;
};

#endif // INITIALIZER_H
