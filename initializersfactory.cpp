/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "initializersfactory.h"

QList<InitializersFactory::AvailableInitializer> InitializersFactory::_availableInitializers = QList<InitializersFactory::AvailableInitializer>();

InitializersFactory::InitializersFactory(QObject *parent) : QObject(parent)
{

}

InitializersFactory::~InitializersFactory()
{

}

Initializer *InitializersFactory::createInitializer(const QString &name)
{
    for (auto ai: InitializersFactory::_availableInitializers) {
        if (ai.name.compare((name), Qt::CaseInsensitive) == 0) {
            Initializer *initializer = ai.initializer();

            connect(initializer, &QObject::destroyed, this, &InitializersFactory::initializerDestroyed);
            _initializers << initializer;

            return initializer;
        }
    }

    return nullptr;
}

QList<Initializer *> InitializersFactory::initializers() const
{
    return _initializers;
}

QStringList InitializersFactory::availableInitializers()
{
    QStringList list;
    for (auto ai: InitializersFactory::_availableInitializers)
        list << ai.name;
    return list;
}

void InitializersFactory::addAvailableInitializer(QString name, Initializer *(*initializer)())
{
    AvailableInitializer ai;
    ai.name = name;
    ai.initializer = initializer;
    InitializersFactory::_availableInitializers << ai;
}

InitializersFactory &InitializersFactory::globalInstance()
{
    static InitializersFactory instance;
    return instance;
}

void InitializersFactory::initializerDestroyed(QObject *initializer)
{
    _initializers.removeAll(static_cast<Initializer *>(initializer));
}
