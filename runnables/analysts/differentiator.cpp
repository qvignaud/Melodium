#include "differentiator.h"

Differentiator::Differentiator(Track *track, QObject *parent) : Runnable("Differentiator", track, parent)
{

}

TreatmentType Differentiator::treatmentType()
{
    TreatmentType tt("Differentiator", TreatmentType::Analysis);

    tt.setDescription("Make a difference between two bidimentionnal vectors.");

    TreatmentType::DataInfo values;
    values.name = "values";
    values.description = "Data vector to differenciate from ground.";
    values.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(values);

    TreatmentType::DataInfo ground;
    ground.name = "ground";
    ground.description = "Ground data, substracted to values.";
    ground.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(ground);

    TreatmentType::DataInfo output;
    output.name = "output";
    output.description = "Output vector.";
    output.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addProvide(output);

    return tt;
}

Runnable *Differentiator::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new Differentiator(track, parent);
}

bool Differentiator::init()
{
    return true;
}

bool Differentiator::work()
{
    QMutex *mutex = nullptr;
    auto &qualifiers = track()->detailedRealQualifiers(mutex);
    auto values = qualifiers.value(inputName("values"));
    auto ground = qualifiers.value(inputName("ground"));
    mutex->unlock();

    if (    values.size() != ground.size() or
            values.at(0).size() != ground.at(0).size())
        throw std::logic_error("Values and ground don't have same dimensions.");

    std::vector<std::vector<float>> output;

    for (size_t i = 0 ; i < values.size() ; ++i) {
        std::vector<float> diff;
        for (size_t j = 0 ; j < values.at(i).size() ; ++j) {
            diff.push_back(values.at(i).at(j) - ground.at(i).at(j));
        }
        output.push_back(diff);
    }

    track()->storeQualifier(outputName("output"), output);

    return true;
}
