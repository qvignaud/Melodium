/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef EVALUATOR_H
#define EVALUATOR_H

#include "runnable.h"
#include "treatmenttype.h"

class Evaluator : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(QString output READ output WRITE setOutput)
public:
    explicit Evaluator(const QVector<Track *> &tracks, QObject *parent = nullptr);

    QString output() const;
    void setOutput(const QString &output);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new evaluator.
     * \param track
     * \param parent
     * \return An evaluator.
     *
     * \note This function is to give for registering Evaluator in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, const QVector<Track *> &tracks, QObject *parent);

protected:
    /*!
     * \brief Initialize evaluation.
     * \return
     */
    bool init();
    /*!
     * \brief Process evaluation.
     * \return
     */
    bool work();

private:
    QString _output;

    struct Counts {
        qulonglong realPositives = 0;
        qulonglong realNegatives = 0;
        qulonglong truePositives = 0;
        qulonglong trueNegatives = 0;
        qulonglong falsePositives = 0;
        qulonglong falseNegatives = 0;
    };

    struct Rates : public Counts {
        Rates(const Counts counts);

        double TPR();
        double TNR();
        double PPV();
        double NPV();
        double FNR();
        double FPR();
        double FDR();
        double FOR();

        double accuracy();
        double f1();
        double MCC();
        double BM();
        double MK();

        double toDouble(qulonglong integer);
    };
};

#endif // EVALUATOR_H
