/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "miditracksanalyst.h"

MidiTracksAnalyst::MidiTracksAnalyst(Track *track, QObject *parent) : MidiAnalyst("MidiTracksAnalyst", track, parent)
{
}

MidiTracksAnalyst::~MidiTracksAnalyst()
{
}

TreatmentType MidiTracksAnalyst::treatmentType()
{
    TreatmentType tt("MidiTracksAnalyst", TreatmentType::Analysis);

    tt.setDescription("Make MIDI analysis of the whole tracks and store the musical data of frames. The analysis is based on the original MIDI file.");

    TreatmentType::ParamInfo paramPresenceOnly;

    paramPresenceOnly.name = "presenceOnly";
    paramPresenceOnly.description = "Set if only the presence of a note should be notified (0 or 1), and not its velocity.";
    paramPresenceOnly.type = TreatmentType::BOOL;
    paramPresenceOnly.defaultValue = false;
    paramPresenceOnly.values = {true, false};

    tt.addParameter(paramPresenceOnly);

    QVector<TreatmentType::DataInfo> outputs;
    for (int i=0 ; i < 16 ; ++i) {
        TreatmentType::DataInfo output;

        output.name = "midiTrackVoice" + QString::number(i+1);
        output.description = "Code of the voice attributed to the MIDI track #" + QString::number(i+1) + ", in NNN-decimal form, starting at 0 (not 1).";
        output.type = TreatmentType::DataType::STRING;

        outputs.append(output);
    }
    for (int i=0 ; i < 16 ; ++i) {
        TreatmentType::DataInfo output;

        output.name = "midiTrackContent" + QString::number(i+1);
        output.description = "Content of the MIDI track #" + QString::number(i+1) + ", from note 0 to 127, by velocity.";
        output.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;

        outputs.append(output);
    }

    tt.setProvide(outputs);

    return tt;
}

Runnable *MidiTracksAnalyst::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new MidiTracksAnalyst(track, parent);
}

bool MidiTracksAnalyst::work()
{
    QList<int> tracks = _file->tracks();
    for (int midiTrack: tracks) {
        QList<QMidiEvent*> events = _file->eventsForTrack(midiTrack);

        // We look for the first event with valid voice.
        bool validVoice = false;
        for (QMidiEvent *event: events) {
            if (event->voice() != -1) {
                QString mTVName = "midiTrackVoice" + QString::number(midiTrack+1);
                _trackVoices.insert(outputName(mTVName),
                                    threeDigitsNumber(event->voice()).toStdString());
                validVoice = true;
                break;
            }
        }
        //If the is no voice associated with this track.
        if (!validVoice) continue;

        // Allocation of well-sized content table.
        std::vector<std::vector<float>> trackContent(track()->frames(),
                                                     std::vector<float>(128, 0.0f));

        // Trace all notes found.
        for (QMidiEvent *event: events) {
            if (event->type() == QMidiEvent::NoteOn) {
                traceNote(event, events, trackContent);
            }
        }

        QString mTCName = "midiTrackContent" + QString::number(midiTrack+1);
        _tracksContent.insert(outputName(mTCName),
                             trackContent);
    }

    track()->storeQualifiers(_trackVoices);
    track()->storeQualifiers(_tracksContent);

    _trackVoices.clear();
    _tracksContent.clear();

    return true;
}
