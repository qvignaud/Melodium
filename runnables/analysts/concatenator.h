/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef CONCATENATOR_H
#define CONCATENATOR_H

#include "runnable.h"
#include "treatmenttype.h"

class Concatenator : public Runnable
{
    Q_OBJECT
public:
    explicit Concatenator(Track *track, QObject *parent = nullptr);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new concatenator.
     * \param track
     * \param parent
     * \return A concatenator.
     *
     * \note This function is to give for registering Concatenator in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Initialize concatenation.
     * \return
     */
    bool init();
    /*!
     * \brief Process concatenation.
     * \return
     */
    bool work();
};

#endif // CONCATENATOR_H
