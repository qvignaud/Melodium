/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MIDIANALYST_H
#define MIDIANALYST_H

#include "runnable.h"
#include "treatmenttype.h"
#include <QMidiFile.h>

/*!
 * \brief Make analysis of original MIDI file associated with track.
 */
class MidiAnalyst : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(bool presenceOnly READ presenceOnly WRITE setPresenceOnly)
public:
    /*!
     * \brief Instanciates new MidiAnalyst.
     * \param name
     * \param track
     * \param parent
     */
    explicit MidiAnalyst(QString name, Track *track, QObject *parent = nullptr);
    ~MidiAnalyst();

    /*!
     * \brief Tells if only presence of note should be notified, not its velocity.
     * \return
     */
    bool presenceOnly() const;
    /*!
     * \brief Set if only presence of note should be notified, instead of velocity.
     * \param presenceOnly
     */
    void setPresenceOnly(bool presenceOnly);

protected:
    /*!
     * \brief Initialize MIDI file reader.
     * \return
     */
    bool init();

    ///MIDI file reader.
    QMidiFile *_file;

    /*!
     * \brief Tells if only presence of note should be notified, not its velocity.
     */
    bool _presenceOnly;

    /*!
     * \brief Tell what frame mache with what tick.
     * \param tick
     * \return
     */
    quint64 tickToFrame(qint32 tick);
    /*!
     * \brief Trace a note in the table vector.
     * \param noteOn
     * \param events
     * \param table
     */
    void traceNote(QMidiEvent *noteOn, QList<QMidiEvent*> &events, std::vector<std::vector<float>> &table);
    /*!
     * \brief Fill a table-MIDI-sheet with a value.
     * \param table
     * \param note
     * \param frameStart
     * \param frameStop
     * \param value
     */
    static void fillValue(std::vector<std::vector<float>> &table, int note, size_t frameStart, size_t frameStop, float value);

    /*!
     * \brief threeDigitsNumber
     * \param voice
     * \return
     */
    static QString threeDigitsNumber(int number);
};

#endif // MIDIANALYST_H
