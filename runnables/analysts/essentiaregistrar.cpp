/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "essentiaregistrar.h"

bool EssentiaRegistrar::_treatmentsLoaded = false;
QList<TreatmentType> EssentiaRegistrar::_treatmentsAvailable = QList<TreatmentType>();

void EssentiaRegistrar::registerEssentiaTreatments()
{
    auto &list = treatmentsAvailable();
    for (auto treatment: list) {

        switch (_treatmentsExecutionKind.value(treatment.name())) {
            case STRAIGHT:
                TreatmentsFactory::addAvailableTreatment(treatment, &EssentiaStraightAnalyst::create);
            break;
            case NESTED:
                TreatmentsFactory::addAvailableTreatment(treatment, &EssentiaNestedAnalyst::create);
            break;
            case NESTED_OUT:
                TreatmentsFactory::addAvailableTreatment(treatment, &EssentiaNestedOutAnalyst::create);
            break;
        }
    }
}

QList<TreatmentType>& EssentiaRegistrar::treatmentsAvailable()
{
    if (!_treatmentsLoaded) {
        QList<TreatmentType> finalTreatments;
        QList<TreatmentType> baseTreatments = basicTreatmentTypes();

        for (auto baseTreatment: baseTreatments) {
            if (fillTreatmentDetails(baseTreatment))
                finalTreatments.append(baseTreatment);
        }

        _treatmentsAvailable = finalTreatments;
        _treatmentsLoaded = true;
    }


    return _treatmentsAvailable;
}

QList<TreatmentType> EssentiaRegistrar::basicTreatmentTypes()
{
    auto &factory = essentia::standard::AlgorithmFactory::instance();
    std::vector<std::string> names = factory.keys();

    QList<TreatmentType> treatments;

    for (auto &name: names) {
        auto &infos = factory.getInfo(name);
        TreatmentType tt(QString::fromStdString(name), TreatmentType::Analysis);

        tt.setDescription('[' + QString::fromStdString(infos.category) + "] " + QString::fromStdString(infos.description));

        treatments.append(tt);

        // Maybe do something with category.
        //std::cout << infos.name << ": " << infos.category << std::endl;
    }

    return treatments;
}

bool EssentiaRegistrar::fillTreatmentDetails(TreatmentType &tt)
{
    auto &factory = essentia::standard::AlgorithmFactory::instance();

    bool success = true;

    essentia::standard::Algorithm *algo = nullptr;

    ExecutionKind execKind = STRAIGHT;
    bool inspectParameters = false;
    bool requireSignal = false;
    bool requireFrame = false;
    bool provideFrame = false;
    if (factory.getInfo(tt.name().toStdString()).category == "Spectral" or
            factory.getInfo(tt.name().toStdString()).category == "Tonal")
        execKind = NESTED;
    else if (factory.getInfo(tt.name().toStdString()).category == "Standard")
        inspectParameters = true;

    try {
        algo = factory.create(tt.name().toStdString());
    }
    catch (essentia::EssentiaException &ex) {
        success = false;
    }

    if (success) {
        tt.setRequire(inputInfos(algo->inputs(), &algo->inputDescription));
        tt.setProvide(outputInfos(algo->outputs(), &algo->outputDescription));

        tt.setParameters(paramInfos(algo->parameterDescription.keys(), &algo->parameterDescription, &algo->parameterRange, algo->defaultParameters()));

        auto req = tt.require();
        for (int i=0 ; i < req.size() && success ; i++) {
            success = req.at(i).type != TreatmentType::NONE;

            if (execKind == NESTED && req.at(i).type == TreatmentType::VECTOR_REAL)
                req[i].type = TreatmentType::VECTOR_VECTOR_REAL;

            if (inspectParameters && req.at(i).name == "signal")
                requireSignal = true;

            if (inspectParameters && req.at(i).name == "frame")
                requireFrame = true;
        }

        auto pro = tt.provide();
        for (int i=0 ; i < pro.size() && success ; i++) {
            success = pro.at(i).type != TreatmentType::NONE;

            if (execKind == NESTED && pro.at(i).type == TreatmentType::REAL)
                pro[i].type = TreatmentType::VECTOR_REAL;
            if (execKind == NESTED && pro.at(i).type == TreatmentType::VECTOR_REAL)
                pro[i].type = TreatmentType::VECTOR_VECTOR_REAL;

            if (inspectParameters && pro.at(i).name == "frame")
                provideFrame = true;
        }

        if (inspectParameters && requireSignal && provideFrame) {
            execKind = NESTED_OUT;
            for (int i=0 ; i < pro.size() ; i++)
                if (pro.at(i).name == "frame")
                    pro[i].type = TreatmentType::VECTOR_VECTOR_REAL;
        }

        if (inspectParameters && requireFrame && provideFrame) {
            execKind = NESTED;

            for (int i=0 ; i < req.size() ; i++)
                if (req.at(i).type == TreatmentType::VECTOR_REAL)
                    req[i].type = TreatmentType::VECTOR_VECTOR_REAL;

            for (int i=0 ; i < pro.size() ; i++)
                if (pro.at(i).type == TreatmentType::VECTOR_REAL)
                    pro[i].type = TreatmentType::VECTOR_VECTOR_REAL;
        }

        tt.setRequire(req);
        tt.setProvide(pro);

        auto par = tt.parameters();
        for (int i=0 ; i < par.size() && success ; i++) {
            success = par.at(i).type != TreatmentType::NONE;
        }
    }

    if (success) _treatmentsExecutionKind.insert(tt.name(), execKind);

    return success;
}

QVector<TreatmentType::DataInfo> EssentiaRegistrar::inputInfos(essentia::OrderedMap<essentia::standard::InputBase> inputs, essentia::DescriptionMap *descriptions)
{
    QVector<TreatmentType::DataInfo> infos;
    for (auto &input: inputs) {
        TreatmentType::DataInfo info;

        info.name = QString::fromStdString(input.first);
        info.description = QString::fromStdString(descriptions->at(input.first));
        info.type = dataType(&input.second->typeInfo());

        infos.append(info);
    }

    return infos;
}

QVector<TreatmentType::DataInfo> EssentiaRegistrar::outputInfos(essentia::OrderedMap<essentia::standard::OutputBase> outputs, essentia::DescriptionMap *descriptions)
{
    QVector<TreatmentType::DataInfo> infos;
    for (auto &output: outputs) {
        TreatmentType::DataInfo info;

        info.name = QString::fromStdString(output.first);
        info.description = QString::fromStdString(descriptions->at(output.first));
        info.type = dataType(&output.second->typeInfo());

        infos.append(info);
    }

    return infos;
}

TreatmentType::DataType EssentiaRegistrar::dataType(const std::type_info *ti)
{
    if (*ti == typeid(float)) return TreatmentType::REAL;
    else if (*ti == typeid(std::string)) return TreatmentType::STRING;
    else if (*ti == typeid(std::vector<float>)) return TreatmentType::VECTOR_REAL;
    else if (*ti == typeid(std::vector<std::string>)) return TreatmentType::VECTOR_STRING;
    else if (*ti == typeid(std::vector<std::vector<float>>)) return TreatmentType::VECTOR_VECTOR_REAL;
    else if (*ti == typeid(std::vector<std::vector<std::string>>)) return TreatmentType::VECTOR_VECTOR_STRING;
    else return TreatmentType::NONE;
}

QVector<TreatmentType::ParamInfo> EssentiaRegistrar::paramInfos(std::vector<std::string> names, essentia::DescriptionMap *descriptions, essentia::DescriptionMap *ranges, essentia::ParameterMap defaultValues)
{
    QVector<TreatmentType::ParamInfo> params;
    for (auto name: names) {
        TreatmentType::ParamInfo param;

        param.name = QString::fromStdString(name);
        param.description = QString::fromStdString(descriptions->at(name));
        param.type = paramType(defaultValues.at(name).type());
        param.defaultValue = paramValue(defaultValues.at(name));
        param.values = paramRange(ranges->at(name), defaultValues.at(name).type());

        params.append(param);
    }

    return params;
}

TreatmentType::DataType EssentiaRegistrar::paramType(essentia::Parameter::ParamType type)
{
    switch (type) {
    case essentia::Parameter::BOOL:
        return TreatmentType::BOOL;
    case essentia::Parameter::INT:
        return TreatmentType::INT;
    case essentia::Parameter::REAL:
        return TreatmentType::REAL;
    case essentia::Parameter::STRING:
        return TreatmentType::STRING;
    case essentia::Parameter::VECTOR_REAL:
        return TreatmentType::VECTOR_REAL;
    default:
        return TreatmentType::NONE;
    }
}

QVariant EssentiaRegistrar::paramValue(essentia::Parameter param)
{
    try {
        switch (param.type()) {
        case essentia::Parameter::BOOL:
            return QVariant(param.toBool());
        case essentia::Parameter::INT:
            return QVariant(param.toInt());
        case essentia::Parameter::REAL:
            return QVariant(param.toReal());
        case essentia::Parameter::STRING:
            return QVariant(QString::fromStdString(param.toString()));
        case essentia::Parameter::VECTOR_REAL:
            return QVariant::fromValue(QVector<float>::fromStdVector(param.toVectorReal()));
        default:
            return QVariant();
        }
    }
    catch (essentia::EssentiaException &ex) {
        return QVariant();
    }
}

QList<QVariant> EssentiaRegistrar::paramRange(std::string range, essentia::Parameter::ParamType type)
{
    QList<QVariant> list;
    QStringList stringList;

    if (!range.empty()) {
        range.erase(0, 1);
        range.erase(range.size() - 1);
        stringList = QString::fromStdString(range).split(',');
    }

    for (QString item: stringList) {
        switch (type) {
        case essentia::Parameter::BOOL:
            list << QVariant(item == "false" ? false : true);
            break;
        case essentia::Parameter::INT:
            if (item == "-inf") list << QVariant(std::numeric_limits<int>::min());
            else if (item == "inf") list << QVariant(std::numeric_limits<int>::max());
            else list << QVariant(item.toInt());
            break;
        case essentia::Parameter::REAL:
            if (item == "-inf") list << QVariant(-std::numeric_limits<double>::infinity());
            else if (item == "inf") list << QVariant(std::numeric_limits<double>::infinity());
            else list << QVariant(item.toDouble());
            break;
        case essentia::Parameter::STRING:
            list << QVariant(item);
            break;
        default:
            break;
        }
    }

    return list;
}

/*
 * This function could actually be used if too much exceptions to the fillTreatmentsDetails
 * algorithm are found.
 */
/*QMap<QString, EssentiaRegistrar::ExecutionKind> executionKindMap() {
    QMap<QString, EssentiaRegistrar::ExecutionKind> map;
    map.insert("MonoLoader", EssentiaRegistrar::STRAIGHT);
    return map;
}*/

QMap<QString, EssentiaRegistrar::ExecutionKind> EssentiaRegistrar::_treatmentsExecutionKind = QMap<QString, EssentiaRegistrar::ExecutionKind>();

