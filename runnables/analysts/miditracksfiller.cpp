/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "miditracksfiller.h"

MidiTracksFiller::MidiTracksFiller(Track *track, QObject *parent) : Runnable("MidiTracksFiller", track, parent),
    _voice("000"),
    _task("ladder"),
    _framesOn(1),
    _framesOff(1),
    _lowNote(0),
    _highNote(127)
{
}

QString MidiTracksFiller::task() const
{
    return _task;
}

void MidiTracksFiller::setTask(const QString &task)
{
    _task = task;
}

QString MidiTracksFiller::voice() const
{
    return _voice;
}

void MidiTracksFiller::setVoice(const QString &voice)
{
    _voice = voice;
}

int MidiTracksFiller::highNote() const
{
    return _highNote;
}

void MidiTracksFiller::setHighNote(int highNote)
{
    _highNote = highNote;
}

int MidiTracksFiller::lowNote() const
{
    return _lowNote;
}

void MidiTracksFiller::setLowNote(int lowNote)
{
    _lowNote = lowNote;
}

int MidiTracksFiller::framesOff() const
{
    return _framesOff;
}

void MidiTracksFiller::setFramesOff(int framesOff)
{
    _framesOff = framesOff;
}

int MidiTracksFiller::framesOn() const
{
    return _framesOn;
}

void MidiTracksFiller::setFramesOn(int framesOn)
{
    _framesOn = framesOn;
}

TreatmentType MidiTracksFiller::treatmentType()
{
    TreatmentType tt("MidiTracksFiller", TreatmentType::Analysis);

    tt.setDescription("Generate and fill MIDI-like tracks.");

    TreatmentType::ParamInfo task;
    task.name = "task";
    task.type = TreatmentType::STRING;
    task.values = {"ladder"};
    task.defaultValue = "ladder";
    tt.addParameter(task);

    TreatmentType::ParamInfo voice;
    voice.name = "voice";
    voice.type = TreatmentType::STRING;
    voice.values = {"000", "127"};
    voice.defaultValue = "000";
    tt.addParameter(voice);

    TreatmentType::ParamInfo framesOn;
    framesOn.name = "framesOn";
    framesOn.type = TreatmentType::INT;
    framesOn.values = {0, std::numeric_limits<int>::max()};
    framesOn.defaultValue = 1;
    tt.addParameter(framesOn);

    TreatmentType::ParamInfo framesOff;
    framesOff.name = "framesOff";
    framesOff.type = TreatmentType::INT;
    framesOff.values = {0, std::numeric_limits<int>::max()};
    framesOff.defaultValue = 1;
    tt.addParameter(framesOff);

    TreatmentType::ParamInfo lowNote;
    lowNote.name = "lowNote";
    lowNote.type = TreatmentType::INT;
    lowNote.values = {0, 127};
    lowNote.defaultValue = 1;
    tt.addParameter(lowNote);

    TreatmentType::ParamInfo highNote;
    highNote.name = "highNote";
    highNote.type = TreatmentType::INT;
    highNote.values = {0, 127};
    highNote.defaultValue = 1;
    tt.addParameter(highNote);

    QVector<TreatmentType::DataInfo> outputs;
    for (int i=0 ; i < 16 ; ++i) {
        TreatmentType::DataInfo output;

        output.name = "midiTrackVoice" + QString::number(i+1);
        output.description = "Code of the voice attributed to the MIDI track #" + QString::number(i+1) + ", in NNN-decimal form, starting at 0 (not 1).";
        output.type = TreatmentType::DataType::STRING;

        outputs.append(output);
    }
    for (int i=0 ; i < 16 ; ++i) {
        TreatmentType::DataInfo output;

        output.name = "midiTrackContent" + QString::number(i+1);
        output.description = "Content of the MIDI track #" + QString::number(i+1) + ", from note 0 to 127, by velocity.";
        output.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;

        outputs.append(output);
    }

    tt.setProvide(outputs);

    return tt;
}

Runnable *MidiTracksFiller::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new MidiTracksFiller(track, parent);
}

bool MidiTracksFiller::init()
{
    return true;
}

bool MidiTracksFiller::work()
{
    std::vector<std::vector<float> > mainContent;

    if (_task == "ladder") {
        mainContent = ladder(80, _framesOn, _framesOff, _lowNote, _highNote);
    }

    track()->setFrames(mainContent.size());

    track()->storeQualifier(outputName("midiTrackContent1"), mainContent);
    track()->storeQualifier(outputName("midiTrackVoice1"), _voice.toStdString());

    for (int i=2 ; i <= 16 ; ++i) {
        QString number = QString::number(i);
        track()->storeQualifier(outputName("midiTrackVoice" + number), "000");
        track()->storeQualifier(outputName("midiTrackContent" + number), full(0, mainContent.size()));
    }

    return true;
}

std::vector<std::vector<float> > MidiTracksFiller::full(float value, size_t frames)
{
    return std::vector<std::vector<float>>(frames, std::vector<float>(128, value));
}

std::vector<std::vector<float> > MidiTracksFiller::ladder(float value, size_t framesOn, size_t framesOff, size_t low, size_t high)
{
    std::vector<std::vector<float>> track((framesOn + framesOff) * (1+high-low), std::vector<float>(128, 0));

    size_t frame = 0;
    for (size_t note=low ; note <= high ; ++note) {

        for (size_t i=0 ; i < framesOn ; ++i) {
            track.at(frame).at(note) = value;
            ++frame;
        }

        frame += framesOff;
    }

    return track;
}
