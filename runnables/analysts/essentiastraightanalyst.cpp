/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "essentiastraightanalyst.h"

EssentiaStraightAnalyst::EssentiaStraightAnalyst(TreatmentType treatment, Track *track, QObject *parent) : EssentiaAnalyst(treatment, track, parent)
{

}

Runnable *EssentiaStraightAnalyst::create(TreatmentType treatment, Track *track, QObject *parent)
{
    return new EssentiaStraightAnalyst(treatment, track, parent);
}

bool EssentiaStraightAnalyst::work()
{
    //We set up all INPUTS for the algorithm.
    for (auto input: _treatment.require()) {
        QMutex *mutex = nullptr;

        if (input.type == TreatmentType::REAL) {
            essentia::Real num = track()->globalRealQualifiers(mutex)
                    .value(inputName(input.name));
            auto it = _globalRealInput.insert(input.name.toStdString(), num);
            _algo->input(it.key()).set(it.value());
        }
        else if (input.type == TreatmentType::STRING) {
            std::string str = track()->globalStringQualifiers(mutex)
                    .value(inputName(input.name));
            auto it = _globalStringInput.insert(input.name.toStdString(), str);
            _algo->input(it.key()).set(it.value());
        }
        else if (input.type == TreatmentType::VECTOR_REAL) {
            std::vector<essentia::Real> nums = track()->simpleRealQualifiers(mutex)
                    .value(inputName(input.name));
            auto it = _simpleRealInput.insert(input.name.toStdString(), nums);
            _algo->input(it.key()).set(it.value());
        }
        else if (input.type == TreatmentType::VECTOR_STRING) {
            std::vector<std::string> strs = track()->simpleStringQualifiers(mutex)
                    .value(inputName(input.name));
            auto it = _simpleStringInput.insert(input.name.toStdString(), strs);
            _algo->input(it.key()).set(it.value());
        }
        else if (input.type == TreatmentType::VECTOR_VECTOR_REAL) {
            std::vector<std::vector<essentia::Real>> nums = track()->detailedRealQualifiers(mutex)
                    .value(inputName(input.name));
            auto it = _detailedRealInput.insert(input.name.toStdString(), nums);
            _algo->input(it.key()).set(it.value());
        }
        else if (input.type == TreatmentType::VECTOR_VECTOR_STRING) {
            std::vector<std::vector<std::string>> strs = track()->detailedStringQualifiers(mutex)
                    .value(inputName(input.name));
            auto it = _detailedStringInput.insert(input.name.toStdString(), strs);
            _algo->input(it.key()).set(it.value());
        }

        if (mutex != nullptr) mutex->unlock();
    }

    //We set up all OUTPUTS for the algorithm.
    for (auto output: _treatment.provide()) {
        if (output.type == TreatmentType::REAL) {
            auto it = _globalRealOutput.insert(output.name.toStdString(), 0.0);
            _algo->output(it.key()).set(it.value());
        }
        else if (output.type == TreatmentType::STRING) {
            auto it = _globalStringOutput.insert(output.name.toStdString(), std::string());
            _algo->output(it.key()).set(it.value());
        }
        else if (output.type == TreatmentType::VECTOR_REAL) {
            auto it = _simpleRealOutput.insert(output.name.toStdString(), std::vector<essentia::Real>());
            _algo->output(it.key()).set(it.value());
        }
        else if (output.type == TreatmentType::VECTOR_STRING) {
            auto it = _simpleStringOutput.insert(output.name.toStdString(), std::vector<std::string>());
            _algo->output(it.key()).set(it.value());
        }
        else if (output.type == TreatmentType::VECTOR_VECTOR_REAL) {
            auto it = _detailedRealOutput.insert(output.name.toStdString(), std::vector<std::vector<essentia::Real>>());
            _algo->output(it.key()).set(it.value());
        }
        else if (output.type == TreatmentType::VECTOR_VECTOR_STRING) {
            auto it = _detailedStringOutput.insert(output.name.toStdString(), std::vector<std::vector<std::string>>());
            _algo->output(it.key()).set(it.value());
        }
    }

    _algo->compute();

    //We store all outputs.
    QMap<QString, float> globalRealQualifiers;
    for (auto qualifier = _globalRealOutput.begin() ; qualifier != _globalRealOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        globalRealQualifiers.insert(outputName(name), qualifier.value());
    }
    if (globalRealQualifiers.size()) track()->storeQualifiers(globalRealQualifiers);

    QMap<QString, std::string> globalStringQualifiers;
    for (auto qualifier = _globalStringOutput.begin() ; qualifier != _globalStringOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        globalStringQualifiers.insert(outputName(name), qualifier.value());
    }
    if (globalStringQualifiers.size()) track()->storeQualifiers(globalStringQualifiers);

    QMap<QString, std::vector<float>> simpleRealQualifiers;
    for (auto qualifier = _simpleRealOutput.begin() ; qualifier != _simpleRealOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        if (qualifier.value().size() != track()->frames() and
                qualifier.value().size() != track()->samples()) {

            if (qualifier.value().size() > track()->frames() and
                    qualifier.value().size() > track()->samples())
                qualifier.value().resize(track()->samples());

            else if (qualifier.value().size() > track()->frames() and
                     qualifier.value().size() < track()->samples())
                qualifier.value().resize(track()->frames());

        }

        simpleRealQualifiers.insert(outputName(name), qualifier.value());
    }
    if (simpleRealQualifiers.size()) track()->storeQualifiers(simpleRealQualifiers);

    QMap<QString, std::vector<std::string>> simpleStringQualifiers;
    for (auto qualifier = _simpleStringOutput.begin() ; qualifier != _simpleStringOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        if (qualifier.value().size() != track()->frames() and
                qualifier.value().size() != track()->samples()) {

            if (qualifier.value().size() > track()->frames() and
                    qualifier.value().size() > track()->samples())
                qualifier.value().resize(track()->samples());

            else if (qualifier.value().size() > track()->frames() and
                     qualifier.value().size() < track()->samples())
                qualifier.value().resize(track()->frames());

        }

        simpleStringQualifiers.insert(outputName(name), qualifier.value());
    }
    if (simpleStringQualifiers.size()) track()->storeQualifiers(simpleStringQualifiers);

    QMap<QString, std::vector<std::vector<float>>> detailedRealQualifiers;
    for (auto qualifier = _detailedRealOutput.begin() ; qualifier != _detailedRealOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        if (qualifier.value().size() != track()->frames() and
                qualifier.value().size() != track()->samples()) {

            if (qualifier.value().size() > track()->frames() and
                    qualifier.value().size() > track()->samples())
                qualifier.value().resize(track()->samples());

            else if (qualifier.value().size() > track()->frames() and
                     qualifier.value().size() < track()->samples())
                qualifier.value().resize(track()->frames());

        }

        detailedRealQualifiers.insert(outputName(name), qualifier.value());
    }
    if (detailedRealQualifiers.size()) track()->storeQualifiers(detailedRealQualifiers);

    QMap<QString, std::vector<std::vector<std::string>>> detailedStringQualifiers;
    for (auto qualifier = _detailedStringOutput.begin() ; qualifier != _detailedStringOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        if (qualifier.value().size() != track()->frames() and
                qualifier.value().size() != track()->samples()) {

            if (qualifier.value().size() > track()->frames() and
                    qualifier.value().size() > track()->samples())
                qualifier.value().resize(track()->samples());

            else if (qualifier.value().size() > track()->frames() and
                     qualifier.value().size() < track()->samples())
                qualifier.value().resize(track()->frames());

        }

        detailedStringQualifiers.insert(outputName(name), qualifier.value());
    }
    if (detailedStringQualifiers.size()) track()->storeQualifiers(detailedStringQualifiers);

    clearAll();
    return true;
}
