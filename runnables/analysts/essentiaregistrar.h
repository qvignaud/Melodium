/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef ESSENTIAREGISTRAR_H
#define ESSENTIAREGISTRAR_H

#include <typeinfo>
#include <limits>
#include <algorithmfactory.h>
#include <QVector>
#include "treatmenttype.h"
#include "treatmentsfactory.h"
#include "essentiastraightanalyst.h"
#include "essentianestedanalyst.h"
#include "essentianestedoutanalyst.h"

/*!
 * \brief Registrar for treaments provided by Essentia.
 *
 * Register them in TreatmentsFactory.
 */
class EssentiaRegistrar
{

public:
    ///Kind of exection the algorithm need.
    enum ExecutionKind {
        ///Algorithm executes with EssentiaStraightAnalyst.
        STRAIGHT,
        ///Algorithm executes with EssentiaNestedAnalyst.
        NESTED,
        ///Algorithm executes with EssentiaNestedOutAnalyst.
        NESTED_OUT
    };

    /*!
     * \brief Do the registration of Essentia algorithms.
     */
    static void registerEssentiaTreatments();

    /*!
     * \brief Get the list of available treatments for Mélodium.
     * \return
     */
    static QList<TreatmentType> &treatmentsAvailable();

private:
    EssentiaRegistrar() {}

    ///Treatments already been loaded.
    static bool _treatmentsLoaded;
    ///List of treatment available.
    static QList<TreatmentType> _treatmentsAvailable;
    ///Map of treatments with their execution kind.
    static QMap<QString, ExecutionKind> _treatmentsExecutionKind;

    ///Returns treatments filled with their names and descriptions.
    static QList<TreatmentType> basicTreatmentTypes();
    ///Fill all treatment details, and specify if it can be used in Mélodium.
    static bool fillTreatmentDetails(TreatmentType &tt);
    ///Return inputs required.
    static QVector<TreatmentType::DataInfo> inputInfos(essentia::OrderedMap<essentia::standard::InputBase> inputs, essentia::DescriptionMap *descriptions);
    ///Return outpus provided.
    static QVector<TreatmentType::DataInfo> outputInfos(essentia::OrderedMap<essentia::standard::OutputBase> outputs, essentia::DescriptionMap *descriptions);
    ///Return datatype of Essentia input/output.
    static TreatmentType::DataType dataType(const std::type_info *ti);
    ///Return parameters proposed by algorithms.
    static QVector<TreatmentType::ParamInfo> paramInfos(std::vector<std::string> names, essentia::DescriptionMap *descriptions, essentia::DescriptionMap *ranges, essentia::ParameterMap defaultValues);
    ///Return datatype of Essentia parameter.
    static TreatmentType::DataType paramType(essentia::Parameter::ParamType type);
    ///Return parameter as QVariant.
    static QVariant paramValue(essentia::Parameter param);
    ///Return parameter range as list of QVariant.
    static QList<QVariant> paramRange(std::string range, essentia::Parameter::ParamType type);
};

#endif // ESSENTIAREGISTRAR_H
