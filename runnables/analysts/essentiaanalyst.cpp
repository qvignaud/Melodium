/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "essentiaanalyst.h"

QMutex EssentiaAnalyst::_algoCreationDeletionMutex;

EssentiaAnalyst::EssentiaAnalyst(TreatmentType treatment, Track *track, QObject *parent) : Runnable(treatment.name(), track, parent), _treatment(treatment)
{
    _algo = nullptr;
    for (auto param: _treatment.parameters()) {
        setProperty(param.name.toStdString().c_str(), param.defaultValue);
    }
}

bool EssentiaAnalyst::init()
{
    essentia::ParameterMap paramMap;

    for (auto param: _treatment.parameters()) {
        switch (param.type) {
        case TreatmentType::BOOL:
            paramMap.add(param.name.toStdString(),
                         property(param.name.toStdString().c_str()).toBool());
            break;
        case TreatmentType::INT:
            paramMap.add(param.name.toStdString(),
                         property(param.name.toStdString().c_str()).toInt());
            break;
        case TreatmentType::REAL:
            paramMap.add(param.name.toStdString(),
                         property(param.name.toStdString().c_str()).toDouble());
            break;
        case TreatmentType::STRING:
            paramMap.add(param.name.toStdString(),
                         property(param.name.toStdString().c_str()).toString().toStdString());
            break;
        case TreatmentType::VECTOR_REAL:
            paramMap.add(param.name.toStdString(),
                         property(param.name.toStdString().c_str()).toBool());
            break;
        default:
            //Theorical error.
            break;
        }
    }

    _algoCreationDeletionMutex.lock();
    _algo = essentia::standard::AlgorithmFactory::instance().create(_treatment.name().toStdString());
    _algo->configure(paramMap);
    _algoCreationDeletionMutex.unlock();

    return true;
}

void EssentiaAnalyst::clearAll()
{
    _globalRealInput.clear();
    _globalStringInput.clear();
    _simpleRealInput.clear();
    _simpleStringInput.clear();
    _detailedRealInput.clear();
    _detailedStringInput.clear();

    _globalRealOutput.clear();
    _globalStringOutput.clear();
    _simpleRealOutput.clear();
    _simpleStringOutput.clear();
    _detailedRealOutput.clear();
    _detailedStringOutput.clear();

    _algoCreationDeletionMutex.lock();
    delete _algo;
    _algoCreationDeletionMutex.unlock();
}
