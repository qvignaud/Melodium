/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "thresholder.h"

Thresholder::Thresholder(Track *track, QObject *parent) : Runnable("Thresholder", track, parent)
{
    _low = 0;
    _high = 1;
    _threshold = .5;
}

float Thresholder::threshold() const
{
    return _threshold;
}

void Thresholder::setThreshold(float threshold)
{
    if (status() == Runnable::Waiting)
        _threshold = threshold;
}

float Thresholder::high() const
{
    return _high;
}

void Thresholder::setHigh(float high)
{
    if (status() == Runnable::Waiting)
        _high = high;
}

float Thresholder::low() const
{
    return _low;
}

void Thresholder::setLow(float low)
{
    if (status() == Runnable::Waiting)
        _low = low;
}

TreatmentType Thresholder::treatmentType()
{
    TreatmentType tt("Thresholder", TreatmentType::Analysis);

    tt.setDescription("Threshold data of a bidimentionnal vector.");

    TreatmentType::ParamInfo low;
    low.name = "low";
    low.description = "Low value to store when under threshold.";
    low.type = TreatmentType::DataType::REAL;
    low.values = {std::numeric_limits<float>::lowest(), std::numeric_limits<float>::max()};
    low.defaultValue = 0;
    tt.addParameter(low);

    TreatmentType::ParamInfo high;
    high.name = "high";
    high.description = "High value to store when equal or over threshold.";
    high.type = TreatmentType::DataType::REAL;
    high.values = {std::numeric_limits<float>::lowest(), std::numeric_limits<float>::max()};
    high.defaultValue = 1;
    tt.addParameter(high);

    TreatmentType::ParamInfo threshold;
    threshold.name = "threshold";
    threshold.description = "Threshold value, values under it will be turned to 'low', other to 'high'.";
    threshold.type = TreatmentType::DataType::REAL;
    threshold.values = {std::numeric_limits<float>::lowest(), std::numeric_limits<float>::max()};
    threshold.defaultValue = .5;
    tt.addParameter(threshold);

    TreatmentType::DataInfo input;
    input.name = "input";
    input.description = "Data vector to threshold.";
    input.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(input);

    TreatmentType::DataInfo output;
    output.name = "output";
    output.description = "Output vector.";
    output.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addProvide(output);

    return tt;
}

Runnable *Thresholder::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new Thresholder(track, parent);
}

bool Thresholder::init()
{
    return true;
}

bool Thresholder::work()
{
    QMutex *mutex = nullptr;
    std::vector<std::vector<float>> input = track()->detailedRealQualifiers(mutex).value(inputName("input"));
    mutex->unlock();

    std::vector<std::vector<float>> output;
    for (auto in: input) {
        std::vector<float> out;
        for (float i: in) {
            out.push_back((i >= _threshold) ? _high : _low);
        }
        output.push_back(out);
    }

    track()->storeQualifier(outputName("output"), output);

    return true;
}
