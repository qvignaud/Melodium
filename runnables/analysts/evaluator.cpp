#include "evaluator.h"

#include <QTextStream>
#include <cmath>

Evaluator::Evaluator(const QVector<Track *> &tracks, QObject *parent) : Runnable("Evaluator", tracks, parent)
{

}

QString Evaluator::output() const
{
    return _output;
}

void Evaluator::setOutput(const QString &output)
{
    if (status() == Runnable::Waiting)
        _output = output;
}

TreatmentType Evaluator::treatmentType()
{
    TreatmentType tt("Evaluator", TreatmentType::Analysis, TreatmentType::TrackOperation::MultiTrack);

    tt.setDescription("Make an evaluation of a bidimentionnal vector compared to ground truth.");

    TreatmentType::DataInfo values;
    values.name = "values";
    values.description = "Data vector to evaluate from ground truth.";
    values.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(values);

    TreatmentType::DataInfo ground;
    ground.name = "ground";
    ground.description = "Ground truth.";
    ground.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(ground);

    TreatmentType::ParamInfo output;
    output.name = "output";
    output.description = "Output path to use for saving evaluation results.";
    output.type = TreatmentType::DataType::STRING;
    tt.addParameter(output);

    return tt;
}

Runnable *Evaluator::create(TreatmentType type, const QVector<Track *> &tracks, QObject *parent)
{
    Q_UNUSED(type)
    return new Evaluator(tracks, parent);
}

bool Evaluator::init()
{
    return true;
}

bool Evaluator::work()
{
    Counts globalCounts;

    bool countsInitialized = false;
    std::vector<Rates> counts;

    auto treatedTracks = tracks();
    QMutex *mutex = nullptr;
    for (auto track: treatedTracks) {
        auto &detailedQualifiers = track->detailedRealQualifiers(mutex);
        auto values = detailedQualifiers.value(inputName("values"));
        auto ground = detailedQualifiers.value(inputName("ground"));
        mutex->unlock();

        if (    values.size() != ground.size() or
                values.at(0).size() != ground.at(0).size())
            throw std::logic_error("Values and ground don't have same dimensions.");

        if (!countsInitialized) {
            counts = std::vector<Rates>(values.at(0).size(), Rates(Counts()));
        }

        for (size_t f = 0 ; f < values.size() ; ++f) {
            for (size_t v = 0 ; v < values.at(f).size() ; ++v) {
                float fValue = values.at(f).at(v);
                float fTruth = ground.at(f).at(v);

                bool value = static_cast<int>(fValue) >= 1;
                bool truth = static_cast<int>(fTruth) >= 1;

                if (value == truth) {
                    if (truth == true) {
                        globalCounts.truePositives++;
                        globalCounts.realPositives++;
                        counts.at(v).truePositives++;
                        counts.at(v).realPositives++;
                    }
                    else {
                        globalCounts.trueNegatives++;
                        globalCounts.realNegatives++;
                        counts.at(v).trueNegatives++;
                        counts.at(v).realNegatives++;
                    }
                }
                else {
                    if (truth == true) {
                        globalCounts.falseNegatives++;
                        globalCounts.realPositives++;
                        counts.at(v).falseNegatives++;
                        counts.at(v).realPositives++;
                    }
                    else {
                        globalCounts.falsePositives++;
                        globalCounts.realNegatives++;
                        counts.at(v).falsePositives++;
                        counts.at(v).realNegatives++;
                    }
                }
            }
        }
    }

    Rates globalRates(globalCounts);

    QFile outputFile(_output);
    if (outputFile.open(QIODevice::WriteOnly)) {
        QTextStream out(&outputFile);

        auto writeOut = [&out](QString label, Rates rates){
            out << label << ":P:" << rates.realPositives << endl;
            out << label << ":N:" << rates.realNegatives << endl;
            out << label << ":TP:" << rates.truePositives << endl;
            out << label << ":TN:" << rates.trueNegatives << endl;
            out << label << ":FP:" << rates.falsePositives << endl;
            out << label << ":FN:" << rates.falseNegatives << endl;

            out << label << ":TPR:" << rates.TPR() << endl;
            out << label << ":TNR:" << rates.TNR() << endl;
            out << label << ":PPV:" << rates.PPV() << endl;
            out << label << ":NPV:" << rates.NPV() << endl;
            out << label << ":FNR:" << rates.FNR() << endl;
            out << label << ":FPR:" << rates.FPR() << endl;
            out << label << ":FDR:" << rates.FDR() << endl;
            out << label << ":FOR:" << rates.FOR() << endl;
            out << label << ":ACC:" << rates.accuracy() << endl;
            out << label << ":F1:" << rates.f1() << endl;
            out << label << ":MCC:" << rates.MCC() << endl;
            out << label << ":BM:" << rates.BM() << endl;
            out << label << ":MK:" << rates.MK() << endl;

            out << endl;
        };

        writeOut("global", globalRates);
        for (size_t i=0 ; i < counts.size() ; ++i)
            writeOut(QString::number(i), counts.at(i));
    }

    return true;
}


Evaluator::Rates::Rates(const Evaluator::Counts counts) :
    Counts (counts)
{

}

double Evaluator::Rates::TPR()
{
    return toDouble(truePositives) / toDouble(realPositives);
}

double Evaluator::Rates::TNR()
{
    return toDouble(trueNegatives) / toDouble(realNegatives);
}

double Evaluator::Rates::PPV()
{
    return toDouble(truePositives) / toDouble(truePositives + falsePositives);
}

double Evaluator::Rates::NPV()
{
    return toDouble(trueNegatives) / toDouble(trueNegatives + falseNegatives);
}

double Evaluator::Rates::FNR()
{
    return toDouble(falseNegatives) / toDouble(realPositives);
}

double Evaluator::Rates::FPR()
{
    return toDouble(falsePositives) / toDouble(realNegatives);
}

double Evaluator::Rates::FDR()
{
    return toDouble(falsePositives) / toDouble(falsePositives + truePositives);
}

double Evaluator::Rates::FOR()
{
    return toDouble(falseNegatives) / toDouble(falseNegatives + trueNegatives);
}

double Evaluator::Rates::accuracy()
{
    return toDouble(truePositives + trueNegatives) / toDouble(realPositives + realNegatives);
}

double Evaluator::Rates::f1()
{
    return toDouble(2 * truePositives) / toDouble(2 * truePositives + falsePositives + falseNegatives);
}

double Evaluator::Rates::MCC()
{
    return (toDouble(truePositives * trueNegatives) - toDouble(falsePositives * falseNegatives)) /
            sqrt(toDouble(truePositives + falsePositives) *
                 toDouble(truePositives + falseNegatives) *
                 toDouble(trueNegatives + falsePositives) *
                 toDouble(trueNegatives + falseNegatives));
}

double Evaluator::Rates::BM()
{
    return TPR() + TNR() - 1;
}

double Evaluator::Rates::MK()
{
    return PPV() + NPV() - 1;
}

double Evaluator::Rates::toDouble(qulonglong integer)
{
    return static_cast<double>(integer);
}
