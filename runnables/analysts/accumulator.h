/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef ACCUMULATOR_H
#define ACCUMULATOR_H

#include "runnable.h"
#include "treatmenttype.h"

class Accumulator : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(int cumulation READ cumulation WRITE setCumulation)
public:
    explicit Accumulator(Track *track, QObject *parent = nullptr);

    int cumulation() const;
    void setCumulation(int cumulation);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new accumulator.
     * \param track
     * \param parent
     * \return An accumulator.
     *
     * \note This function is to give for registering Accumulator in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Initialize accumulation.
     * \return
     */
    bool init();
    /*!
     * \brief Process accumulation.
     * \return
     */
    bool work();

private:
    int _cumulation;
};

#endif // ACCUMULATOR_H
