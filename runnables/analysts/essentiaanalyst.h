/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef ESSENTIAANALYST_H
#define ESSENTIAANALYST_H

#include <QObject>
#include <QMap>
#include "runnable.h"
#include "treatmenttype.h"
#include <algorithm.h>
#include <algorithmfactory.h>

/*!
 * \brief Essentia algorithms execution base class.
 */
class EssentiaAnalyst : public Runnable
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a new analyst.
     * \param treatment
     * \param track
     * \param parent
     */
    explicit EssentiaAnalyst(TreatmentType treatment, Track *track, QObject *parent = nullptr);

signals:

public slots:

protected:
    ///Initialize algorithm and configure it with parameters.
    bool init();

    ///Treatment type defining the alogithm.
    const TreatmentType _treatment;

    ///Essentia algorithm.
    essentia::standard::Algorithm *_algo;

    QMap<std::string, essentia::Real> _globalRealInput;
    QMap<std::string, std::string> _globalStringInput;
    QMap<std::string, std::vector<essentia::Real>> _simpleRealInput;
    QMap<std::string, std::vector<std::string>> _simpleStringInput;
    QMap<std::string, std::vector<std::vector<essentia::Real>>> _detailedRealInput;
    QMap<std::string, std::vector<std::vector<std::string>>> _detailedStringInput;

    QMap<std::string, essentia::Real> _globalRealOutput;
    QMap<std::string, std::string> _globalStringOutput;
    QMap<std::string, std::vector<essentia::Real>> _simpleRealOutput;
    QMap<std::string, std::vector<std::string>> _simpleStringOutput;
    QMap<std::string, std::vector<std::vector<essentia::Real>>> _detailedRealOutput;
    QMap<std::string, std::vector<std::vector<std::string>>> _detailedStringOutput;

    /*!
     * \brief Clear all local inputs and outputs, and plan algorithm deletion.
     *
     * This method should be called at the end of work() method.
     */
    void clearAll();

private:
    static QMutex _algoCreationDeletionMutex;

};

#endif // ESSENTIAANALYST_H
