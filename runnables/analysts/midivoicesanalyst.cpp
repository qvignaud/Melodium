/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "midivoicesanalyst.h"

MidiVoicesAnalyst::MidiVoicesAnalyst(Track *track, QObject *parent) : MidiAnalyst("MidiVoicesAnalyst", track, parent)
{
    _dontCountPresence = false;
}

MidiVoicesAnalyst::~MidiVoicesAnalyst()
{

}

TreatmentType MidiVoicesAnalyst::treatmentType()
{
    TreatmentType tt("MidiVoicesAnalyst", TreatmentType::Analysis);

    tt.setDescription("Make analysis of MIDI voices and store the musical data of frames. The analysis is based on the original MIDI file.");

    QVector<TreatmentType::ParamInfo> params;
    TreatmentType::ParamInfo paramPresenceOnly;

    paramPresenceOnly.name = "presenceOnly";
    paramPresenceOnly.description = "Set if only the presence of a note should be notified (0 or 1), and not its velocity.";
    paramPresenceOnly.type = TreatmentType::BOOL;
    paramPresenceOnly.defaultValue = false;
    paramPresenceOnly.values = {true, false};

    params.append(paramPresenceOnly);

    TreatmentType::ParamInfo paramDontCountPresence;

    paramPresenceOnly.name = "dontCountPresence";
    paramPresenceOnly.description = "Do not count number of notes present, only if at least once is per frame.";
    paramPresenceOnly.type = TreatmentType::BOOL;
    paramPresenceOnly.defaultValue = false;
    paramPresenceOnly.values = {true, false};

    params.append(paramPresenceOnly);

    tt.setParameters(params);

    QVector<TreatmentType::DataInfo> outputs;
    TreatmentType::DataInfo outputPresence;

    outputPresence.name = "midiVoicePresenceNNN";
    outputPresence.description = "Tells if a voice is playing at each frame, and how many notes (unless presenceOnly is activated, in which case it only tells if at least one note is played).";
    outputPresence.type = TreatmentType::DataType::VECTOR_REAL;

    outputs.append(outputPresence);

    TreatmentType::DataInfo outputContent;

    outputContent.name = "midiVoiceContentNNN";
    outputContent.description = "Content of the MIDI voice NNN, from note 0 to 127, by velocity.";
    outputContent.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;

    outputs.append(outputContent);

    tt.setProvide(outputs);

    return tt;
}

Runnable *MidiVoicesAnalyst::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new MidiVoicesAnalyst(track, parent);
}

bool MidiVoicesAnalyst::work()
{
    for (int voice = 0 ; voice < 128 ; ++voice) {
        QList<QMidiEvent*> events = _file->events(voice);

        if (events.empty())
            continue;

        // Allocation of well-sized content table.
        std::vector<std::vector<float>> voiceContent(track()->frames(),
                                                     std::vector<float>(128, 0.0f));
        std::vector<float> voicePresence(track()->frames(), 0.0f);

        // Trace all notes found.
        for (QMidiEvent *event: events) {
            if (event->type() == QMidiEvent::NoteOn) {
                traceNote(event, events, voiceContent);
            }
        }

        QString mVCName = "midiVoiceContent" + threeDigitsNumber(voice);
        _voicesContent.insert(outputName(mVCName),
                             voiceContent);

        for (size_t i = 0 ; i < voicePresence.size() ; ++i) {
            int played = 0;
            for (size_t n = 0 ; n < 128 ; ++n) {
                // We are working with floating-point values so strict comparison as integer
                // is required.
                if (static_cast<short>(voiceContent[i][n]) != 0)
                    ++played;
            }

            if (_dontCountPresence && played > 0)
                voicePresence[i] = 1;
            else
                voicePresence[i] = static_cast<float>(played);
        }

        QString mVPName = "midiVoicePresence" + threeDigitsNumber(voice);
        _voicesPresence.insert(outputName(mVPName),
                               voicePresence);
    }

    track()->storeQualifiers(_voicesPresence);
    track()->storeQualifiers(_voicesContent);

    _voicesPresence.clear();
    _voicesContent.clear();

    return true;
}

bool MidiVoicesAnalyst::dontCountPresence() const
{
    return _dontCountPresence;
}

void MidiVoicesAnalyst::setDontCountPresence(bool dontCountPresence)
{
    _dontCountPresence = dontCountPresence;
}
