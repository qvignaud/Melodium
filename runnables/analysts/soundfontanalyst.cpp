/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "soundfontanalyst.h"

SoundFontAnalyst::SoundFontAnalyst(Track *track, QObject *parent) : Runnable("SoundFontAnalyst", track, parent)
{

}

TreatmentType SoundFontAnalyst::treatmentType()
{
    TreatmentType tt("SoundFontAnalyst", TreatmentType::Analysis);

    tt.setDescription("Make sound font analysis. This treatment mimic MIDI analysts with soundfont samples, the outputs are similar to MidiTracksAnalyst. It always behaves like if \'presenceOnly\' is enabled (without considering velocity).");

    TreatmentType::DataInfo audioIntput;
    audioIntput.name = "audio";
    audioIntput.description = "Raw audio signal of the sample.";
    audioIntput.type = TreatmentType::DataType::VECTOR_REAL;
    tt.addRequire(audioIntput);

    TreatmentType::DataInfo audioSampleOutput;
    audioSampleOutput.name = "audioSampleOutput";
    audioSampleOutput.description = "Treated audio sample, according to soundfont properties (currently equal to audio signal).";
    audioSampleOutput.type = TreatmentType::DataType::VECTOR_REAL;
    tt.addProvide(audioSampleOutput);

    TreatmentType::DataInfo midiVoice;
    midiVoice.name = "midiVoice";
    midiVoice.description = "MIDI code of the voice in NNN-decimal form, starting at 0 (not 1).";
    midiVoice.type = TreatmentType::DataType::STRING;
    tt.addProvide(midiVoice);

    TreatmentType::DataInfo midiContent;
    midiContent.name = "midiContent";
    midiContent.description = "MIDI content, from note 0 to 127, 0 for abscence and 1 for presence.";
    midiContent.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addProvide(midiContent);

    return tt;
}

Runnable *SoundFontAnalyst::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new SoundFontAnalyst(track, parent);
}

bool SoundFontAnalyst::init()
{
    return true;
}

bool SoundFontAnalyst::work()
{
    unsigned int note = 0;
    QString voice(property("voice").toString());

    // Finding note.
    QVariant key(property("key"));
    QVariant pitch_keycenter(property("pitch_keycenter"));
    bool ok = false;
    if (key.isValid()) {
        note = key.toUInt(&ok);
    }
    if (pitch_keycenter.isValid() and !ok) {
        note = pitch_keycenter.toUInt(&ok);
    }
    if (!ok)
        return false;


    track()->storeQualifier(outputName("midiVoice"),
                          voice.toStdString());

    // Create and setup one-frame vector.
    std::vector<float> frameVector(128, 0.0f);
    frameVector.at(note) = 1.0f;
    // Expand it to whole track.
    std::vector<std::vector<float>> fullVector(track()->frames(), frameVector);

    track()->storeQualifier(outputName("midiContent"),
                            fullVector);


    QMutex *mutex = nullptr;
    std::vector<float> audio = track()->simpleRealQualifiers(mutex).value(inputName("audio"));
    mutex->unlock();

    track()->storeQualifier(outputName("audioSampleOutput"), audio);

    return true;
}


