/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef SCALER_H
#define SCALER_H

#include "runnable.h"
#include "treatmenttype.h"

class Scaler : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(float minScale READ minScale WRITE setMinScale)
    Q_PROPERTY(float maxScale READ maxScale WRITE setMaxScale)
public:
    explicit Scaler(Track *track, QObject *parent = nullptr);

    float minScale() const;
    void setMinScale(float minScale);

    float maxScale() const;
    void setMaxScale(float maxScale);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new scaler.
     * \param track
     * \param parent
     * \return An scaler.
     *
     * \note This function is to give for registering Scaler in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Initialize scaling.
     * \return
     */
    bool init();
    /*!
     * \brief Process scaling.
     * \return
     */
    bool work();

private:
    float _minScale;
    float _maxScale;
};

#endif // SCALER_H
