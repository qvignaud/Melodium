/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "essentianestedanalyst.h"

EssentiaNestedAnalyst::EssentiaNestedAnalyst(TreatmentType treatment, Track *track, QObject *parent) : EssentiaAnalyst(treatment, track, parent)
{

}

Runnable *EssentiaNestedAnalyst::create(TreatmentType treatment, Track *track, QObject *parent)
{
    return new EssentiaNestedAnalyst(treatment, track, parent);
}

bool EssentiaNestedAnalyst::work()
{
    //We set up all INPUTS for the algorithm.
    auto inputs = _treatment.require();
    size_t inputSize = 0;
    bool sizeError = false;
    for (auto input: inputs) {
        QMutex *mutex = nullptr;

        if (input.type == TreatmentType::VECTOR_REAL) {
            std::vector<essentia::Real> nums = track()->simpleRealQualifiers(mutex)
                    .value(inputName(input.name));
            _simpleRealInput.insert(input.name.toStdString(), nums);
            if (inputSize == 0) inputSize = nums.size();
            else if (inputSize != nums.size()) sizeError = true;
        }
        else if (input.type == TreatmentType::VECTOR_STRING) {
            std::vector<std::string> strs = track()->simpleStringQualifiers(mutex)
                    .value(inputName(input.name));
            _simpleStringInput.insert(input.name.toStdString(), strs);
            if (inputSize == 0) inputSize = strs.size();
            else if (inputSize != strs.size()) sizeError = true;
        }
        else if (input.type == TreatmentType::VECTOR_VECTOR_REAL) {
            std::vector<std::vector<essentia::Real>> nums = track()->detailedRealQualifiers(mutex)
                    .value(inputName(input.name));
            _detailedRealInput.insert(input.name.toStdString(), nums);
            if (inputSize == 0) inputSize = nums.size();
            else if (inputSize != nums.size()) sizeError = true;
        }
        else if (input.type == TreatmentType::VECTOR_VECTOR_STRING) {
            std::vector<std::vector<std::string>> strs = track()->detailedStringQualifiers(mutex)
                    .value(inputName(input.name));
            _detailedStringInput.insert(input.name.toStdString(), strs);
            if (inputSize == 0) inputSize = strs.size();
            else if (inputSize != strs.size()) sizeError = true;
        }

        if (mutex != nullptr) mutex->unlock();
    }

    //All inputs must have same size.
    if (sizeError == true) {
        delete _algo;
        return false;
    }

    //We set up all OUTPUTS for the algorithm.
    auto outputs = _treatment.provide();
    for (auto output: outputs) {
        if (output.type == TreatmentType::VECTOR_REAL) {
            _simpleRealOutput.insert(output.name.toStdString(), std::vector<essentia::Real>(inputSize, 0.0));
        }
        else if (output.type == TreatmentType::VECTOR_STRING) {
            _simpleStringOutput.insert(output.name.toStdString(), std::vector<std::string>(inputSize, std::string()));
        }
        else if (output.type == TreatmentType::VECTOR_VECTOR_REAL) {
            _detailedRealOutput.insert(output.name.toStdString(), std::vector<std::vector<essentia::Real>>(inputSize, std::vector<essentia::Real>()));
        }
        else if (output.type == TreatmentType::VECTOR_VECTOR_STRING) {
            _detailedStringOutput.insert(output.name.toStdString(), std::vector<std::vector<std::string>>(inputSize, std::vector<std::string>()));
        }
    }

    //Execute step by step.
    for (size_t i=0 ; i < inputSize ; i++) {

        for (auto it = _simpleRealInput.begin() ; it != _simpleRealInput.end() ; it++) {
            _algo->input(it.key()).set(it.value().at(i));
        }

        for (auto it = _simpleStringInput.begin() ; it != _simpleStringInput.end() ; it++) {
            _algo->input(it.key()).set(it.value().at(i));
        }

        for (auto it = _detailedRealInput.begin() ; it != _detailedRealInput.end() ; it++) {
            _algo->input(it.key()).set(it.value().at(i));
        }

        for (auto it = _detailedStringInput.begin() ; it != _detailedStringInput.end() ; it++) {
            _algo->input(it.key()).set(it.value().at(i));
        }

        for (auto it = _simpleRealOutput.begin() ; it != _simpleRealOutput.end() ; it++) {
            _algo->output(it.key()).set(it.value().at(i));
        }

        for (auto it = _simpleStringOutput.begin() ; it != _simpleStringOutput.end() ; it++) {
            _algo->output(it.key()).set(it.value().at(i));
        }

        for (auto it = _detailedRealOutput.begin() ; it != _detailedRealOutput.end() ; it++) {
            _algo->output(it.key()).set(it.value().at(i));
        }

        for (auto it = _detailedStringOutput.begin() ; it != _detailedStringOutput.end() ; it++) {
            _algo->output(it.key()).set(it.value().at(i));
        }

        _algo->compute();
    }

    //We store outputs.
    QMap<QString, std::vector<float>> simpleRealQualifiers;
    for (auto qualifier = _simpleRealOutput.begin() ; qualifier != _simpleRealOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        simpleRealQualifiers.insert(outputName(name), qualifier.value());
    }
    if (simpleRealQualifiers.size()) track()->storeQualifiers(simpleRealQualifiers);

    QMap<QString, std::vector<std::string>> simpleStringQualifiers;
    for (auto qualifier = _simpleStringOutput.begin() ; qualifier != _simpleStringOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        simpleStringQualifiers.insert(outputName(name), qualifier.value());
    }
    if (simpleStringQualifiers.size()) track()->storeQualifiers(simpleStringQualifiers);

    QMap<QString, std::vector<std::vector<float>>> detailedRealQualifiers;
    for (auto qualifier = _detailedRealOutput.begin() ; qualifier != _detailedRealOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        detailedRealQualifiers.insert(outputName(name), qualifier.value());
    }
    if (detailedRealQualifiers.size()) track()->storeQualifiers(detailedRealQualifiers, true);

    QMap<QString, std::vector<std::vector<std::string>>> detailedStringQualifiers;
    for (auto qualifier = _detailedStringOutput.begin() ; qualifier != _detailedStringOutput.end() ; qualifier++) {
        QString name = QString::fromStdString(qualifier.key());
        detailedStringQualifiers.insert(outputName(name), qualifier.value());
    }
    if (detailedStringQualifiers.size()) track()->storeQualifiers(detailedStringQualifiers);

    clearAll();
    return true;
}
