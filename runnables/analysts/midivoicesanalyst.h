/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MIDIVOICESANALYST_H
#define MIDIVOICESANALYST_H

#include "midianalyst.h"

/*!
 * \brief Make by-voice analysis of original MIDI file associated with track.
 */
class MidiVoicesAnalyst : public MidiAnalyst
{
    Q_OBJECT
    Q_PROPERTY(bool dontCountPresence READ dontCountPresence WRITE setDontCountPresence)
public:
    /*!
     * \brief Instanciates new MidiVoicesAnalyst
     * \param track
     * \param parent
     */
    explicit MidiVoicesAnalyst(Track *track, QObject *parent = nullptr);
    ~MidiVoicesAnalyst();

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new MIDI tracks analyst.
     * \param track
     * \param parent
     * \return An MIDI tracks analyst.
     *
     * \note This function is to give for registering MidiTracksAnalyst in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

    /*!
     * \brief Tells if presence shouldn't be counted or not.
     * \return
     */
    bool dontCountPresence() const;
    /*!
     * \brief Set if count of presence should be ignored or not.
     * \param dontCountPresence
     */
    void setDontCountPresence(bool dontCountPresence);

protected:
    /*!
     * \brief Read and filter MIDI data.
     * \return
     */
    bool work();

private:
    /*!
     * \brief Store presence of voices.
     */
    QMap<QString, std::vector<float>> _voicesPresence;
    /*!
     * \brief Store content of voices.
     */
    QMap<QString, std::vector<std::vector<float>>> _voicesContent;

    /// Tells if presence shouldn't be counted or not.
    bool _dontCountPresence;
};

#endif // MIDIVOICESANALYST_H
