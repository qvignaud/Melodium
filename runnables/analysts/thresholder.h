/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef THRESHOLDER_H
#define THRESHOLDER_H

#include "runnable.h"
#include "treatmenttype.h"

class Thresholder : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(float low READ low WRITE setLow)
    Q_PROPERTY(float high READ high WRITE setHigh)
    Q_PROPERTY(float threshold READ threshold WRITE setThreshold)
public:
    explicit Thresholder(Track *track, QObject *parent = nullptr);

    float low() const;
    void setLow(float low);

    float high() const;
    void setHigh(float high);

    float threshold() const;
    void setThreshold(float threshold);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new thresholder.
     * \param track
     * \param parent
     * \return A thresholder.
     *
     * \note This function is to give for registering Thresholder in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Initialize threshold.
     * \return
     */
    bool init();
    /*!
     * \brief Process threshold.
     * \return
     */
    bool work();

private:
    float _low;
    float _high;
    float _threshold;
};

#endif // THRESHOLDER_H
