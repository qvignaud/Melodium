/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "scaler.h"

Scaler::Scaler(Track *track, QObject *parent) : Runnable("Scaler", track, parent)
{
    _minScale = 0;
    _maxScale = 1;
}

float Scaler::minScale() const
{
    return _minScale;
}

void Scaler::setMinScale(float minScale)
{
    if (status() == Runnable::Waiting)
        _minScale = minScale;
}

float Scaler::maxScale() const
{
    return _maxScale;
}

void Scaler::setMaxScale(float maxScale)
{
    if (status() == Runnable::Waiting)
        _maxScale = maxScale;
}

TreatmentType Scaler::treatmentType()
{
    TreatmentType tt("Scaler", TreatmentType::Analysis);

    tt.setDescription("Scale bidimentionnal data between a set of values.");

    TreatmentType::ParamInfo min;
    min.name = "minScale";
    min.type = TreatmentType::REAL;
    min.description = "Minimum value of output scale.";
    min.defaultValue = 0;
    tt.addParameter(min);

    TreatmentType::ParamInfo max;
    max.name = "maxScale";
    max.type = TreatmentType::REAL;
    max.description = "Maximum value of output scale";
    max.defaultValue = 1;
    tt.addParameter(max);

    TreatmentType::DataInfo input;
    input.name = "input";
    input.description = "Data vector to cumulate.";
    input.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(input);

    TreatmentType::DataInfo output;
    output.name = "output";
    output.description = "Output vector.";
    output.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addProvide(output);

    return tt;
}

Runnable *Scaler::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new Scaler(track, parent);
}

bool Scaler::init()
{
    if (minScale() > maxScale())
        throw std::invalid_argument("Minimum scale cannot be greater than maximum scale.");

    return true;
}

bool Scaler::work()
{
    std::vector<std::vector<float>> data;
    QMutex *mutex = nullptr;
    data = track()->detailedRealQualifiers(mutex).value(inputName("input"));
    mutex->unlock();

    float min = std::numeric_limits<float>::max();
    float max = std::numeric_limits<float>::lowest();

    for (auto &frame: data) {
        for (float value: frame) {
            if (value < min)
                min = value;
            if (value > max)
                max = value;
        }
    }

    int xSize = static_cast<int>(data.size());
    int ySize = static_cast<int>(data.at(0).size());

    for (int x=0 ; x < xSize ; ++x) {
        for (int y=0 ; y < ySize ; ++y) {

            float value = data.at(x).at(y);
            value -= min;
            value *= (maxScale() - minScale());
            value /= (max - min);
            value += minScale();
            // or: value = minScale() + ( ((value - min)*(maxScale() - minScale() )) / (max - min) );

            data.at(x).at(y) = value;
        }
    }

    track()->storeQualifier(outputName("output"), data);

    return true;
}
