/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MIDITRACKSFILLER_H
#define MIDITRACKSFILLER_H

#include "runnable.h"
#include "treatmenttype.h"

/*!
 * \brief Make a MIDI generation of track.
 */
class MidiTracksFiller : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(QString voice READ voice WRITE setVoice)
    Q_PROPERTY(QString task READ task WRITE setTask)
    Q_PROPERTY(int framesOn READ framesOn WRITE setFramesOn)
    Q_PROPERTY(int framesOff READ framesOff WRITE setFramesOff)
    Q_PROPERTY(int lowNote READ lowNote WRITE setLowNote)
    Q_PROPERTY(int highNote READ highNote WRITE setHighNote)
public:
    /*!
     * \brief Instanciates a new MidiTracksFiller.
     * \param track
     * \param parent
     */
    explicit MidiTracksFiller(Track *track, QObject *parent = nullptr);

    /*!
     * \brief Voice used to generate MIDI.
     * \return
     */
    QString voice() const;
    /*!
     * \brief Set the MIDI voice.
     * \param voice
     */
    void setVoice(const QString &voice);

    /*!
     * \brief Generation task to do.
     * \return
     */
    QString task() const;
    /*!
     * \brief Set the task to use for generation.
     * \param task
     */
    void setTask(const QString &task);

    /*!
     * \brief Number of frames where note is played.
     * \return
     */
    int framesOn() const;
    /*!
     * \brief Set number of frames where note is played.
     * \param framesOn
     */
    void setFramesOn(int framesOn);

    /*!
     * \brief Number of frame of "silence".
     * \return
     */
    int framesOff() const;
    /*!
     * \brief Set number of frames for silence.
     * \param framesOff
     */
    void setFramesOff(int framesOff);

    /*!
     * \brief Lowest note to use.
     * \return
     */
    int lowNote() const;
    /*!
     * \brief Set the lowest note to use.
     * \param lowNote
     */
    void setLowNote(int lowNote);

    /*!
     * \brief Highest note to use.
     * \return
     */
    int highNote() const;
    /*!
     * \brief Set the highest note to use.
     * \param highNote
     */
    void setHighNote(int highNote);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new MIDI tracks filler.
     * \param track
     * \param parent
     * \return An MIDI tracks filler.
     *
     * \note This function is to give for registering MidiTracksFiller in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

signals:

public slots:

protected:
    /*!
     * \brief Initialize the filler.
     * \return
     */
    bool init();
    /*!
     * \brief Generate MIDI tracks.
     * \return
     */
    bool work();

private:
    ///MIDI voice to use.
    QString _voice;
    ///Generation task to do.
    QString _task;

    ///Frames with note on.
    int _framesOn;
    ///Frames with "silence".
    int _framesOff;
    ///Lowest note.
    int _lowNote;
    ///Highest note.
    int _highNote;

    /*!
     * \brief Generates a full track filled with one value.
     * \param value
     * \param frames
     * \return
     */
    static std::vector<std::vector<float>> full(float value, size_t frames);
    /*!
     * \brief Generates a track with notes doing a "ladder".
     * \param value
     * \param framesOn
     * \param framesOff
     * \param low
     * \param high
     * \return
     */
    static std::vector<std::vector<float>> ladder(float value, size_t framesOn, size_t framesOff, size_t low=0, size_t high=127);
};

#endif // MIDITRACKSFILLER_H
