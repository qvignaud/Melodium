/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "accumulator.h"

Accumulator::Accumulator(Track *track, QObject *parent) : Runnable("Accumulator", track, parent)
{

}

int Accumulator::cumulation() const
{
    return _cumulation;
}

void Accumulator::setCumulation(int cumulation)
{
    if (status() == Runnable::Waiting)
        _cumulation = cumulation;
}

TreatmentType Accumulator::treatmentType()
{
    TreatmentType tt("Accumulator", TreatmentType::Analysis);

    tt.setDescription("Accumulates data of multiples frames of bidimensionnal vector.");

    TreatmentType::ParamInfo cumulation;
    cumulation.name = "cumulation";
    cumulation.type = TreatmentType::DataType::INT;
    cumulation.description = "Number of preceding iterations to cumulate.";
    cumulation.values = {0, std::numeric_limits<int>::max()};
    cumulation.defaultValue = 0;
    tt.addParameter(cumulation);

    TreatmentType::DataInfo input;
    input.name = "input";
    input.description = "Data vector to cumulate.";
    input.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(input);

    TreatmentType::DataInfo output;
    output.name = "output";
    output.description = "Output vector.";
    output.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addProvide(output);

    return tt;
}

Runnable *Accumulator::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new Accumulator(track, parent);
}

bool Accumulator::init()
{
    return true;
}

bool Accumulator::work()
{
    QMutex *mutex = nullptr;
    auto input = track()->detailedRealQualifiers(mutex).value(inputName("input"));
    mutex->unlock();

    size_t length = input.at(0).size();

    QVector<std::vector<float>> buffer(_cumulation, std::vector<float>(length, 0));
    std::vector<float> concatened;
    std::vector<std::vector<float>> output;

    for (auto frame: input) {
        concatened.clear();

        // Making rotation in buffer.
        buffer.append(frame);
        for (auto bufferedFrame: buffer) {
            for (size_t i=0 ; i < bufferedFrame.size() ; ++i)
                concatened.push_back(bufferedFrame.at(i));
        }
        buffer.removeFirst();

        output.push_back(concatened);
    }

    track()->storeQualifier(outputName("output"), output);

    return true;
}
