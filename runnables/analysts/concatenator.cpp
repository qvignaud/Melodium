/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "concatenator.h"

Concatenator::Concatenator(Track *track, QObject *parent) : Runnable("Concatenator", track, parent)
{

}

TreatmentType Concatenator::treatmentType()
{
    TreatmentType tt("Concatenator", TreatmentType::Analysis);

    tt.setDescription("Concatenator data from two bidimensionnal real vectors into one.");

    TreatmentType::DataInfo first;
    first.name = "first";
    first.description = "First data vector to concatene.";
    first.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(first);

    TreatmentType::DataInfo second;
    second.name = "second";
    second.description = "Second data vector to concatene.";
    second.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(second);

    TreatmentType::DataInfo output;
    output.name = "output";
    output.description = "Output vector, containing first and second data.";
    output.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addProvide(output);

    return tt;
}

Runnable *Concatenator::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new Concatenator(track, parent);
}

bool Concatenator::init()
{
    return true;
}

bool Concatenator::work()
{
    QMutex *mutex = nullptr;
    auto &vectors = track()->detailedRealQualifiers(mutex);
    auto &first = vectors.value(inputName("first"));
    auto &second = vectors.value(inputName("second"));

    if (first.size() != second.size())
        throw std::logic_error("Qualifiers don't have same length.");

    std::vector<std::vector<float>> output(first.size(),
                                           std::vector<float>(
                                               first.at(0).size()+second.at(0).size(),
                                               0)
                                           );

    for (size_t i=0 ; i < first.size() ; ++i) {
        for (size_t f=0 ; f < first.at(i).size() ; ++f)
            output.at(i).at(f) = first.at(i).at(f);
        for (size_t s=0 ; s < second.at(i).size() ; ++s)
            output.at(i).at(first.at(i).size() + s) = second.at(i).at(s);
    }
    mutex->unlock();

    track()->storeQualifier(outputName("output"), output);

    return true;
}
