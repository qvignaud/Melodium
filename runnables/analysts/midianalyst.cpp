/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "midianalyst.h"

MidiAnalyst::MidiAnalyst(QString name, Track *track, QObject *parent) : Runnable(name, track, parent)
{
    _file = new QMidiFile();
    _presenceOnly = false;
}

MidiAnalyst::~MidiAnalyst()
{
    delete _file;
}

bool MidiAnalyst::presenceOnly() const
{
    return _presenceOnly;
}

void MidiAnalyst::setPresenceOnly(bool presenceOnly)
{
    _presenceOnly = presenceOnly;
}

bool MidiAnalyst::init()
{
    if (_file->load(track()->originalMidiFile()))
        return true;
    else
        return false;
}

quint64 MidiAnalyst::tickToFrame(qint32 tick)
{
    double time = static_cast<double>(_file->timeFromTick(tick)) * 1000.0;
    quint64 frame = static_cast<quint64>((static_cast<double>(track()->frames()) / static_cast<double>(track()->length())) * time);
    if (frame > track()->frames())
        frame = track()->frames();
    return frame;
}

void MidiAnalyst::traceNote(QMidiEvent *noteOn, QList<QMidiEvent *> &events, std::vector<std::vector<float> > &table)
{
    QList<QMidiEvent*> meaningFullEvents;

    for (QMidiEvent *event: events) {
        if (event->note() == noteOn->note() &&
                event->tick() > noteOn->tick()) {

            meaningFullEvents.append(event);

            if (event->type() == QMidiEvent::NoteOff)
                break;
        }
    }

    QMidiEvent *previousEvent = noteOn;
    for (QMidiEvent *currentEvent: meaningFullEvents) {
        if (!(currentEvent->isNoteEvent() || currentEvent->type() == QMidiEvent::KeyPressure))
            continue;

        quint64 frameStart =  tickToFrame(previousEvent->tick());
        quint64 frameStop = tickToFrame(currentEvent->tick());

        float value;
        if (_presenceOnly)
            value = 1;
        else if (previousEvent->isNoteEvent())
            value = previousEvent->velocity();
        else //if (previousEvent->type() == QMidiEvent::KeyPressure)
            value = previousEvent->amount();

        fillValue(table, noteOn->note().note(), frameStart, frameStop, value);

        previousEvent = currentEvent;
    }
}

void MidiAnalyst::fillValue(std::vector<std::vector<float> > &table, int note, size_t frameStart, size_t frameStop, float value)
{
    size_t y = static_cast<size_t>(note);

    for (size_t x = frameStart ; x < frameStop ; ++x) {
        table[x][y] = value;
    }
}

QString MidiAnalyst::threeDigitsNumber(int number)
{
    QString string = QString::number(number);

    while (string.length() < 3)
        string = '0' + string;

    return string;
}
