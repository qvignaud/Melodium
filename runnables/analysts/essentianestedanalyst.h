/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef ESSENTIANESTEDANALYST_H
#define ESSENTIANESTEDANALYST_H

#include <QObject>
#include "essentiaanalyst.h"

/*!
 * \brief Executes Essentia algorithm in nested mode.
 */
class EssentiaNestedAnalyst : public EssentiaAnalyst
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a new EssentiaNestedAnalyst.
     * \param treatment
     * \param track
     * \param parent
     */
    explicit EssentiaNestedAnalyst(TreatmentType treatment, Track *track, QObject *parent = nullptr);

    /*!
     * \brief Create a new Essentia analyst.
     * \param treatment
     * \param track
     * \param parent
     * \return
     *
     * \note This function is to give to TreatmentsFactory.
     */
    static Runnable *create(TreatmentType treatment, Track *track, QObject *parent);

signals:

public slots:

protected:
    /*!
     * \brief Process the algorithm in nested mode.
     * \return
     */
    bool work();
};

#endif // ESSENTIANESTEDANALYST_H
