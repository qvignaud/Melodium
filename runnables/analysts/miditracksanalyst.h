/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MIDITRACKSANALYST_H
#define MIDITRACKSANALYST_H

#include "midianalyst.h"

/*!
 * \brief Make by-track analysis of original MIDI file associated with track.
 */
class MidiTracksAnalyst : public MidiAnalyst
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates new MidiTrackAnalyst.
     * \param track
     * \param parent
     */
    explicit MidiTracksAnalyst(Track *track, QObject *parent = nullptr);
    ~MidiTracksAnalyst();

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new MIDI tracks analyst.
     * \param track
     * \param parent
     * \return An MIDI tracks analyst.
     *
     * \note This function is to give for registering MidiTracksAnalyst in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Read and filter MIDI data.
     * \return
     */
    bool work();

private:
    /*!
     * \brief Store which voice is attributed to which track, on hexadecimal representation.
     */
    QMap<QString, std::string> _trackVoices;
    /*!
     * \brief Store the content of the tracks.
     *
     * Each frame contains the 128 MIDI notes, 0 means a note isn't played, else it is the velocity of the play.
     */
    QMap<QString, std::vector<std::vector<float>>> _tracksContent;

};

#endif // MIDITRACKSANALYST_H
