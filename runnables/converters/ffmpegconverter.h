/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef FFMPEGCONVERTER_H
#define FFMPEGCONVERTER_H

#include <QProcess>
#include "runnable.h"
#include "treatmenttype.h"

/*!
 * \brief Audio converter based of FFMPEG.
 *
 * This class allows to convert any file supported by FFMPEG (audio as video) in FLAC format.
 */
class FfmpegConverter : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(QString inputFilename READ inputFilename WRITE setInputFilename)
    Q_PROPERTY(QString outputFilename READ outputFilename WRITE setOutputFilename)

public:
    /*!
     * \brief Instanciates a FfmpegConverter
     * \param track             Track to use.
     * \param inputFilename     Input file to convert.
     * \param outputFilename    Ouput file to generate (flac).
     * \param parent            Parent object.
     */
    explicit FfmpegConverter(Track *track, QString inputFilename, QString outputFilename, QObject *parent = nullptr);
    /*!
     * \brief Instanciates a FfmpegConverter
     * \param track     Track to use.
     * \param parent    Parent object.
     */
    explicit FfmpegConverter(Track *track, QObject *parent = nullptr);
    ~FfmpegConverter();

    /*!
     * \brief Input file name to convert.
     * \return
     */
    QString inputFilename() const;
    /*!
     * \brief Set the input file name to convert.
     * \param inputFilename
     *
     * Can be any file type supported by FFmpeg.
     */
    void setInputFilename(const QString &inputFilename);

    /*!
     * \brief Output file name.
     * \return
     */
    QString outputFilename() const;
    /*!
     * \brief Set the output file name.
     * \param outputFilename    Name of the file, extension should be '.flac' but won't be forced by this method.
     *
     * The file will be in FLAC format.
     */
    void setOutputFilename(const QString &outputFilename);

    /*!
     * \brief Tell if an error occured.
     * \return true if error occured, else false.
     */
    bool hasError() const;
    /*!
     * \brief Tell the process error.
     * \return The process error.
     */
    QProcess::ProcessError error() const;

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new audio converter.
     * \param track
     * \param parent
     * \return An Ffmpeg converter.
     *
     * \note This function is to give for registering FfmpegConverter in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Initialize conversion.
     */
    bool init();

    /*!
     * \brief Make conversion.
     *
     * \warning If filename is not specified, not supported by FFmpeg, or invalid, the execution will fail.
     */
    bool work();

private:

    /*!
     * \brief Input file name.
     */
    QString _inputFilename;
    /*!
     * \brief Output file name.
     */
    QString _outputFilename;

    /*!
     * \brief Process hosting FFmpeg.
     */
    QProcess *_process;

    /*!
     * \brief Error mark.
     */
    bool _error;
};

#endif // FFMPEGCONVERTER_H
