/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "ffmpegconverter.h"

FfmpegConverter::FfmpegConverter(Track *track, QString inputFilename, QString outputFilename, QObject *parent) : FfmpegConverter(track, parent)
{
    _inputFilename = inputFilename;
    _outputFilename = outputFilename;
}

FfmpegConverter::FfmpegConverter(Track *track, QObject *parent) : Runnable("FfmpegConverter", track, parent)
{

}

FfmpegConverter::~FfmpegConverter()
{
    delete _process;
}

QString FfmpegConverter::inputFilename() const
{
    return _inputFilename;
}

void FfmpegConverter::setInputFilename(const QString &inputFilename)
{
    if (status() == Runnable::Waiting)
        _inputFilename = inputFilename;
}

QString FfmpegConverter::outputFilename() const
{
    return _outputFilename;
}

void FfmpegConverter::setOutputFilename(const QString &outputFilename)
{
    if (status() == Runnable::Waiting)
        _outputFilename = outputFilename;
}

bool FfmpegConverter::hasError() const
{
    return _error;
}

QProcess::ProcessError FfmpegConverter::error() const
{
    if (_process == nullptr) return QProcess::UnknownError;
    else return _process->error();
}

TreatmentType FfmpegConverter::treatmentType()
{
    TreatmentType tt("FfmpegConverter", TreatmentType::Converter);

    tt.setDescription("Convert any file supported by FFmpeg (audio as video) into FLAC file.");

    QVector<TreatmentType::ParamInfo> params;

    TreatmentType::ParamInfo inputFilename;
    inputFilename.name = "inputFilename";
    inputFilename.type = TreatmentType::STRING;
    inputFilename.description = "Input file name to convert. Can be any format supported by FFmpeg.";
    params.append(inputFilename);

    TreatmentType::ParamInfo outputFilename;
    outputFilename.name = "outputFilename";
    outputFilename.type = TreatmentType::STRING;
    outputFilename.description = "Output file, will be FLAC, whatever extension is used.";
    params.append(outputFilename);

    tt.setParameters(params);

    return tt;
}

Runnable *FfmpegConverter::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new FfmpegConverter(track, parent);
}

bool FfmpegConverter::init()
{
    _process = new QProcess();
    _process->setProgram("ffmpeg");

    QStringList args;
    args << "-i"
         << _inputFilename
         << "-c:a"
         << "flac"
         << _outputFilename
            ;

    _process->setArguments(args);

    return true;
}

bool FfmpegConverter::work()
{
    _process->start(QIODevice::ReadOnly);
    //Waiting indefinitely for process to finish.
    _process->waitForFinished(-1);

    track()->setAudioFile(_outputFilename);

    return !(_error = (_process->exitCode() != EXIT_SUCCESS || _process->exitStatus() != QProcess::NormalExit));

}
