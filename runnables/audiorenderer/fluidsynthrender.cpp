/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "fluidsynthrender.h"

FluidsynthRender::FluidsynthRender(Track *track, QString flacFilename, QObject *parent) : FluidsynthRender(track, parent)
{
    setFlacFilename(flacFilename);
}

FluidsynthRender::FluidsynthRender(Track *track, QObject *parent) : AudioRender("FluidsynthRender", track, parent)
{
    _process = nullptr;
    _error = false;

    _soundFonts
            << "/usr/share/sounds/sf2/FluidR3_GM.sf2"
            << "/usr/share/sounds/sf2/FluidR3_GS.sf2";
}

FluidsynthRender::~FluidsynthRender()
{
    delete _process;
}

bool FluidsynthRender::hasError() const
{
    return _error;
}

QProcess::ProcessError FluidsynthRender::error() const
{
    if (_process == nullptr) return QProcess::UnknownError;
    else return _process->error();
}

QStringList FluidsynthRender::soundFonts() const
{
    return _soundFonts;
}

void FluidsynthRender::setSoundFonts(const QStringList &soundFonts)
{
    if (status() == Runnable::Waiting)
        _soundFonts = soundFonts;
}

TreatmentType FluidsynthRender::treatmentType()
{
    TreatmentType tt("FluidsynthRender", TreatmentType::AudioRender);

    tt.setDescription("Render original MIDI file in audio using Fluidsynth. Require the presence of soundfonts FluidR3_GM.sf2 and FluidR3_GS.sf2 inside /usr/share/sounds/sf2 to work.");

    TreatmentType::ParamInfo inputFilename;
    inputFilename.name = "flacFilename";
    inputFilename.type = TreatmentType::STRING;
    inputFilename.description = "File to produce, will be FLAC format.";
    tt.addParameter(inputFilename);

    TreatmentType::ParamInfo soundFonts;
    soundFonts.name = "soundFonts";
    soundFonts.type = TreatmentType::DataType::VECTOR_STRING;
    soundFonts.description = "Soundfonts to use for render.";
    soundFonts.defaultValue = QStringList({"/usr/share/sounds/sf2/FluidR3_GM.sf2", "/usr/share/sounds/sf2/FluidR3_GS.sf2"});
    tt.addParameter(soundFonts);

    return tt;
}

Runnable *FluidsynthRender::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new FluidsynthRender(track, parent);
}

bool FluidsynthRender::init()
{
    _process = new QProcess();
    _process->setProgram("fluidsynth");

    QStringList args;
    args << "--no-midi-in"
         << "--audio-file-type=flac"
         << "--fast-render=" + flacFilename()
         << _soundFonts
         << track()->originalMidiFile();

    _process->setArguments(args);

    return true;
}

bool FluidsynthRender::work()
{
    _process->start(QIODevice::ReadOnly);
    //Waiting indefinitely for process to finish.
    _process->waitForFinished(-1);

    track()->setAudioFile(flacFilename());

    return !(_error = (_process->exitCode() != EXIT_SUCCESS || _process->exitStatus() != QProcess::NormalExit));
}
