/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef AUDIORENDER_H
#define AUDIORENDER_H

#include <QObject>
#include "runnable.h"

/*!
 * \brief Audio render abstraction class.
 * \abstract
 */
class AudioRender : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(QString flacFilename READ flacFilename WRITE setFlacFilename)

public:
    /*!
     * \brief Instanciates a new audio render.
     * \param name          Name of the renderer.
     * \param track         Track to render.
     * \param flacFilename  FLAC file to create.
     * \param parent        Parent object.
     */
    explicit AudioRender(QString name, Track *track, QString flacFilename, QObject *parent = nullptr);
    /*!
     * \brief Instanciates a new audio render.
     * \param name          Name of the renderer.
     * \param track         Track to render.
     * \param parent        Parent object.
     */
    explicit AudioRender(QString name, Track *track, QObject *parent = nullptr);
    ~AudioRender();

    /*!
     * \brief FLAC filename
     * \return The filename used to store render.
     */
    QString flacFilename() const;
    /*!
     * \brief Set the FLAC filename.
     * \param filename
     */
    void setFlacFilename(const QString& filename);

protected:
    /*!
     * \brief Initialize render.
     */
    virtual bool init() = 0;

    /*!
     * \brief Make render.
     *
     * \warning If FLAC filename is not specified or invalid, or no track is linked, the execution will fail.
     */
    virtual bool work() = 0;

private:
    /*!
     * \brief FLAC filename.
     */
    QString _flacFilename;

};

#endif // AUDIORENDER_H
