/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "timidityrender.h"

TimidityRender::TimidityRender(Track *track, QString flacFilename, QObject *parent) : TimidityRender(track, parent)
{
    setFlacFilename(flacFilename);
}

TimidityRender::TimidityRender(Track *track, QObject *parent) : AudioRender("TimidityRender", track, parent)
{
    _process = nullptr;
    _error = false;
}

TimidityRender::~TimidityRender()
{
    delete _process;
}

bool TimidityRender::hasError() const
{
    return _error;
}

QProcess::ProcessError TimidityRender::error() const
{
    if (_process == nullptr) return QProcess::UnknownError;
    else return _process->error();
}

TreatmentType TimidityRender::treatmentType()
{
    TreatmentType tt("TimidityRender", TreatmentType::AudioRender);

    tt.setDescription("Render original MIDI file in audio using Timidity++.");

    TreatmentType::ParamInfo inputFilename;
    inputFilename.name = "flacFilename";
    inputFilename.type = TreatmentType::STRING;
    inputFilename.description = "File to produce, will be FLAC format.";
    tt.addParameter(inputFilename);

    return tt;
}

Runnable *TimidityRender::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new TimidityRender(track, parent);
}

bool TimidityRender::init()
{
    _process = new QProcess();
    _process->setProgram("timidity");

    QStringList args;
    args << "-OF"
         << "--output-mono"
         << track()->originalMidiFile()
         << "-o"
         << flacFilename();

    _process->setArguments(args);

    return true;
}

bool TimidityRender::work()
{
    _process->start(QIODevice::ReadOnly);
    //Waiting indefinitely for process to finish.
    _process->waitForFinished(-1);

    track()->setAudioFile(flacFilename());

    return !(_error = (_process->exitCode() != EXIT_SUCCESS || _process->exitStatus() != QProcess::NormalExit));
}
