/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef FLUIDSYNTHRENDER_H
#define FLUIDSYNTHRENDER_H

#include <QProcess>
#include "audiorender.h"
#include "treatmenttype.h"

/*!
 * \brief Audio render of track management class.
 *
 * This class uses Fluidsynth to create an audio render from a MIDI file.
 */
class FluidsynthRender : public AudioRender
{
    Q_OBJECT
    Q_PROPERTY(QStringList soundFonts READ soundFonts WRITE setSoundFonts)
public:
    /*!
     * \brief Instanciates a new audio render.
     * \param track         Track to render.
     * \param flacFilename  FLAC file to create.
     * \param parent        Parent object.
     */
    explicit FluidsynthRender(Track *track, QString flacFilename, QObject *parent = nullptr);
    /*!
     * \brief Instanciates a new audio render.
     * \param track         Track to render.
     * \param parent        Parent object.
     */
    explicit FluidsynthRender(Track *track, QObject *parent = nullptr);
    ~FluidsynthRender();

    /*!
     * \brief Sound fonts used for render.
     * \return
     */
    QStringList soundFonts() const;
    /*!
     * \brief Set sound fonts to use for render.
     * \param soundFonts
     */
    void setSoundFonts(const QStringList &soundFonts);

    /*!
     * \brief Tell if an error occured.
     * \return true if error occured, else false.
     */
    bool hasError() const;
    /*!
     * \brief Tell the process error.
     * \return The process error.
     */
    QProcess::ProcessError error() const;

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new audio render.
     * \param track
     * \param parent
     * \return An Fluidsynth audio render.
     *
     * \note This function is to give for registering FluidsynthRender in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Initialize render.
     */
    bool init();

    /*!
     * \brief Make render.
     *
     * \warning If FLAC filename is not specified or invalid, the execution will fail.
     * \warning The actual process require SoundFonts `/usr/share/sounds/sf2/FluidR3_GM.sf2` and `/usr/share/sounds/sf2/FluidR3_GS.sf2` to work.
     */
    bool work();

private:
    /*!
     * \brief Process hosting Fluidsynth.
     */
    QProcess *_process;

    /*!
     * \brief Error mark.
     */
    bool _error;

    /*!
     * \brief Soundfonts to use.
     */
    QStringList _soundFonts;

};

#endif // FLUIDSYNTHRENDER_H
