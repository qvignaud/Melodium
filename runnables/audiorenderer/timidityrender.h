/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef TIMIDITYRENDER_H
#define TIMIDITYRENDER_H

#include <QProcess>
#include "audiorender.h"
#include "treatmenttype.h"

/*!
 * \brief Audio render of track management class.
 *
 * This class uses Timidity++ to create an audio render from a MIDI file.
 */
class TimidityRender : public AudioRender
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a new audio render.
     * \param track         Track to render.
     * \param flacFilename  FLAC file to create.
     * \param parent        Parent object.
     */
    explicit TimidityRender(Track *track, QString flacFilename, QObject *parent = nullptr);
    /*!
     * \brief Instanciates a new audio render.
     * \param track         Track to render.
     * \param parent        Parent object.
     */
    explicit TimidityRender(Track *track, QObject *parent = nullptr);
    ~TimidityRender();

    /*!
     * \brief Tell if an error occured.
     * \return true if error occured, else false.
     */
    bool hasError() const;
    /*!
     * \brief Tell the process error.
     * \return The process error.
     */
    QProcess::ProcessError error() const;

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new audio render.
     * \param type
     * \param track
     * \param parent
     * \return An Timidity audio render.
     *
     * \note This function is to give for registering TimidityRender in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Initialize render.
     */
    bool init();

    /*!
     * \brief Make render.
     *
     * \warning If FLAC filename is not specified or invalid, or no track is linked, the execution will fail.
     */
    bool work();

private:
    /*!
     * \brief Process hosting Timidity++.
     */
    QProcess *_process;

    /*!
     * \brief Error mark.
     */
    bool _error;

};

#endif // TIMIDITYRENDER_H
