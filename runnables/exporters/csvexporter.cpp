/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "csvexporter.h"

CsvExporter::CsvExporter(Track *track, QString &filename, QObject *parent) : CsvExporter(track, parent)
{
    _filename = filename;
}

CsvExporter::CsvExporter(Track *track, QObject *parent) : Runnable("CsvExporter", track, parent)
{

}

QString CsvExporter::filename() const
{
    return _filename;
}

void CsvExporter::setFilename(const QString &filename)
{
    if (status() == Runnable::Waiting)
        _filename = filename;
}

QStringList CsvExporter::qualifiers() const
{
    return _qualifiers;
}

void CsvExporter::setQualifiers(const QStringList &qualifiers)
{
    if (status() == Runnable::Waiting)
        _qualifiers = qualifiers;
}

TreatmentType CsvExporter::treatmentType()
{
    TreatmentType tt("CsvExporter", TreatmentType::DataExporter);

    tt.setDescription("Exports frames data of the track in CSV.");

    QVector<TreatmentType::ParamInfo> params;

    TreatmentType::ParamInfo filename;
    filename.name = "filename";
    filename.type = TreatmentType::STRING;
    filename.description = "File to write.";
    params.append(filename);

    TreatmentType::ParamInfo qualifiers;
    qualifiers.name = "qualifiers";
    qualifiers.type = TreatmentType::VECTOR_STRING;
    qualifiers.description = "List of qualifiers to export. They must be frame-sized.";
    params.append(qualifiers);

    tt.setParameters(params);

    return tt;
}

Runnable *CsvExporter::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type);
    return new CsvExporter(track, parent);
}

bool CsvExporter::init()
{
    return true;
}

bool CsvExporter::work()
{
    QFile file(_filename);

    auto lf = [&file]{
        file.write("\n");
    };

    if (file.open(QIODevice::WriteOnly)) {
        QMutex *mutex = nullptr;
        QStringList header;

        auto &simpleReal = track()->simpleRealQualifiers(mutex);
        for (auto item = simpleReal.begin() ; item != simpleReal.end() ; ++item) {
            if (!_qualifiers.contains(item.key(), Qt::CaseInsensitive)) continue;
            if (item.value().size() == track()->frames()) {
                header << item.key();
            }
        }
        mutex->unlock();

        auto &simpleString = track()->simpleStringQualifiers(mutex);
        for (auto item = simpleString.begin() ; item != simpleString.end() ; ++item) {
            if (!_qualifiers.contains(item.key(), Qt::CaseInsensitive)) continue;
            if (item.value().size() == track()->frames()) {
                header << item.key();
            }
        }
        mutex->unlock();

        auto &detailedReal = track()->detailedRealQualifiers(mutex);
        for (auto item = detailedReal.begin() ; item != detailedReal.end() ; ++item) {
            if (!_qualifiers.contains(item.key(), Qt::CaseInsensitive)) continue;
            if (item.value().size() == track()->frames()) {
                for (size_t i = 0 ; i < item.value().at(0).size() ; i++) {
                    header << item.key() + '_' + QString::number(i);
                }
            }
        }
        mutex->unlock();

        auto &detailedString = track()->detailedStringQualifiers(mutex);
        for (auto item = detailedString.begin() ; item != detailedString.end() ; ++item) {
            if (!_qualifiers.contains(item.key(), Qt::CaseInsensitive)) continue;
            if (item.value().size() == track()->frames()) {
                for (size_t i = 0 ; i < item.value().at(0).size() ; i++) {
                    header << item.key() + '_' + QString::number(i);
                }
            }
        }
        mutex->unlock();

        file.write(header.join(',').toUtf8());
        lf();

        for (quint64 frame = 0 ; frame < track()->frames() ; ++frame) {
            QStringList line;

            auto &simpleReal = track()->simpleRealQualifiers(mutex);
            for (auto item = simpleReal.begin() ; item != simpleReal.end() ; ++item) {
                if (!_qualifiers.contains(item.key(), Qt::CaseInsensitive)) continue;
                if (item.value().size() == track()->frames()) {
                    line << QString::number(item.value().at(frame));
                }
            }
            mutex->unlock();

            auto &simpleString = track()->simpleStringQualifiers(mutex);
            for (auto item = simpleString.begin() ; item != simpleString.end() ; ++item) {
                if (!_qualifiers.contains(item.key(), Qt::CaseInsensitive)) continue;
                if (item.value().size() == track()->frames()) {
                    line << item.value().at(frame).c_str();
                }
            }
            mutex->unlock();

            auto &detailedReal = track()->detailedRealQualifiers(mutex);
            for (auto item = detailedReal.begin() ; item != detailedReal.end() ; ++item) {
                if (!_qualifiers.contains(item.key(), Qt::CaseInsensitive)) continue;
                if (item.value().size() == track()->frames()) {
                    for (size_t i = 0 ; i < item.value().at(frame).size() ; i++) {
                        line << QString::number(item.value().at(frame).at(i));
                    }
                }
            }
            mutex->unlock();

            auto &detailedString = track()->detailedStringQualifiers(mutex);
            for (auto item = detailedString.begin() ; item != detailedString.end() ; ++item) {
                if (!_qualifiers.contains(item.key(), Qt::CaseInsensitive)) continue;
                if (item.value().size() == track()->frames()) {
                    for (size_t i = 0 ; i < item.value().at(0).size() ; i++) {
                        line << item.value().at(frame).at(i).c_str();
                    }
                }
            }
            mutex->unlock();

            file.write(line.join(',').toUtf8());
            lf();
        }

        file.close();
        return true;
    }
    else return false;
}
