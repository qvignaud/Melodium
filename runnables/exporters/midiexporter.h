/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MIDIEXPORTER_H
#define MIDIEXPORTER_H

#include "runnable.h"
#include "treatmenttype.h"
#include "QMidi/src/QMidiFile.h"

/*!
 * \brief MIDI exporter class.
 */
class MidiExporter : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(QString filename READ filename WRITE setFilename)
public:
    /*!
     * \brief Instanciates a new Midi exporter.
     * \param track
     * \param parent
     */
    explicit MidiExporter(Track *track, QObject *parent = nullptr);

    /*!
     * \brief Name of the MIDI file.
     * \return
     */
    QString filename() const;
    /*!
     * \brief Set the MIDI filename.
     * \param filename
     *
     * \note This function does nothing if the exporter is launched.
     */
    void setFilename(const QString &filename);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new MIDI exporter.
     * \param track
     * \param parent
     * \return A MIDI exporter.
     *
     * \note This function is to give for registering MidiExporter in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

signals:

public slots:

protected:
    /*!
     * \brief Initialize export.
     */
    bool init();

    /*!
     * \brief Make export.
     */
    bool work();

private:
    ///File to write in.
    QString _filename;
    ///MIDI file manager.
    QMidiFile _midi;

    ///Get the MIDI tick matching the given frame.
    qint32 frameToTick(size_t frame);

};

#endif // MIDIEXPORTER_H
