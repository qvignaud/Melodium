/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "midiexporter.h"

MidiExporter::MidiExporter(Track *track, QObject *parent) : Runnable("MidiExporter", track, parent)
{

}

QString MidiExporter::filename() const
{
    return _filename;
}

void MidiExporter::setFilename(const QString &filename)
{
    _filename = filename;
}

TreatmentType MidiExporter::treatmentType()
{
    TreatmentType tt("MidiExporter", TreatmentType::DataExporter);

    TreatmentType::ParamInfo filename;
    filename.name = "filename";
    filename.type = TreatmentType::STRING;
    filename.description = "File to write.";
    tt.addParameter(filename);

    QVector<TreatmentType::DataInfo> inputs;
    for (int i=0 ; i < 16 ; ++i) {
        TreatmentType::DataInfo input;

        input.name = "midiTrackVoice" + QString::number(i+1);
        input.description = "Code of the voice attributed to the MIDI track #" + QString::number(i+1) + ", in NNN-decimal form, starting at 0 (not 1).";
        input.type = TreatmentType::DataType::STRING;

        inputs.append(input);
    }
    for (int i=0 ; i < 16 ; ++i) {
        TreatmentType::DataInfo input;

        input.name = "midiTrackContent" + QString::number(i+1);
        input.description = "Content of the MIDI track #" + QString::number(i+1) + ", from note 0 to 127, by velocity.";
        input.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;

        inputs.append(input);
    }

    tt.setRequire(inputs);

    return tt;
}

Runnable *MidiExporter::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new MidiExporter(track, parent);
}

bool MidiExporter::init()
{
    _midi.clear();

    double frames = track()->frames();
    double length = track()->length();
    length /= 1000;
    double resolution = frames / length;
    _midi.setResolution(static_cast<int>(resolution));

    return true;
}

bool MidiExporter::work()
{
    for (int trackNum = 0 ; trackNum < 16 ; ++trackNum) {
        bool conversionOk = true;
        int trackVoice = QString::fromStdString(track()->globalStringQualifiers()
                .value("midiTrackVoice" + QString::number(trackNum+1)))
                .toInt(&conversionOk, 10);
        if (!conversionOk)
            throw std::invalid_argument("Voice for track " + std::to_string(trackNum+1) + " is invalid or undefined.");

        QMutex *mutex = nullptr;
        auto trackContent = track()->detailedRealQualifiers(mutex).value("midiTrackContent" + QString::number(trackNum+1));
        mutex->unlock();

        if (_midi.createTrack() != trackNum)
            return false; // It means there is a very strange problem…

        /*
         * For each musical note, we create the equivalent MIDI notes present in the detailed matrix.
         */
        for (int note = 0 ; note <= 127 ; ++note) {
            int velocity = 0;
            size_t start = 0;
            for (size_t frame = 0 ; frame < trackContent.size() ; ++frame) {
                int currentVelocity = trackContent.at(frame).at(note);
                if (currentVelocity != velocity) {

                    //End note.
                    if (velocity != 0) {
                        _midi.createNote(trackNum, frameToTick(start), frameToTick(frame), trackVoice, note, velocity, 64);
                    }

                    velocity = currentVelocity;
                    start = frame;
                }
            }
        }
    }

    return _midi.save(_filename);
}

qint32 MidiExporter::frameToTick(size_t frame)
{
    float time = ((static_cast<float>(track()->length()) / 1000.0) * static_cast<float>(frame))
            / static_cast<float>(track()->frames());
    return _midi.tickFromTime(time);
}
