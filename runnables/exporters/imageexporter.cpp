/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "imageexporter.h"
#include <QImage>

ImageExporter::ImageExporter(Track *track, QObject *parent) : Runnable("ImageExporter", track, parent)
{
    _autoScale = false;
    _minScale = 0;
    _maxScale = 1;
}

QString ImageExporter::filename() const
{
    return _filename;
}

void ImageExporter::setFilename(const QString &filename)
{
    if (status() == Runnable::Waiting)
        _filename = filename;
}

bool ImageExporter::autoScale() const
{
    return _autoScale;
}

void ImageExporter::setAutoScale(bool autoScale)
{
    if (status() == Runnable::Waiting)
        _autoScale = autoScale;
}

float ImageExporter::maxScale() const
{
    return _maxScale;
}

void ImageExporter::setMaxScale(float maxScale)
{
    if (status() == Runnable::Waiting)
        _maxScale = maxScale;
}

float ImageExporter::minScale() const
{
    return _minScale;
}

void ImageExporter::setMinScale(float minScale)
{
    if (status() == Runnable::Waiting)
        _minScale = minScale;
}

TreatmentType ImageExporter::treatmentType()
{
    TreatmentType tt("ImageExporter", TreatmentType::DataExporter);

    tt.setDescription("Exports data as image.");

    QVector<TreatmentType::ParamInfo> params;

    TreatmentType::ParamInfo filename;
    filename.name = "filename";
    filename.type = TreatmentType::STRING;
    filename.description = "File to write.";
    params.append(filename);

    TreatmentType::ParamInfo autoScale;
    autoScale.name = "autoScale";
    autoScale.type = TreatmentType::BOOL;
    autoScale.description = "Make auto scaling of colors according to the minimum and maximum value of data.";
    autoScale.defaultValue = false;
    params.append(autoScale);

    TreatmentType::ParamInfo min;
    min.name = "minScale";
    min.type = TreatmentType::REAL;
    min.description = "Scaling to this value as black.";
    min.defaultValue = 0;
    params.append(min);

    TreatmentType::ParamInfo max;
    max.name = "maxScale";
    max.type = TreatmentType::REAL;
    max.description = "Scaling to this value as white.";
    max.defaultValue = 1;
    params.append(max);

    tt.setParameters(params);

    TreatmentType::DataInfo data;
    data.name = "data";
    data.type = TreatmentType::VECTOR_VECTOR_REAL;
    data.description = "Qualifier to store as image.";
    tt.addRequire(data);

    return tt;
}

Runnable *ImageExporter::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type)
    return new ImageExporter(track, parent);
}

bool ImageExporter::init()
{
    return true;
}

bool ImageExporter::work()
{
    std::vector<std::vector<float>> data;
    QMutex *mutex = nullptr;
    data = track()->detailedRealQualifiers(mutex).value(inputName("data"));
    mutex->unlock();

    if (_autoScale)
        autoScale(data);

    int xSize = static_cast<int>(data.size());
    int ySize = static_cast<int>(data.at(0).size());

    QImage image(xSize,
                 ySize,
                 QImage::Format_RGB32);

    image.setText("Minimum", QString::number(_minScale));
    image.setText("Maximum", QString::number(_maxScale));

    for (int x=0 ; x < xSize ; ++x) {
        for (int y=0 ; y < ySize ; ++y) {
            int value = 255 - scaleRange256(data.at(x).at(y));
            image.setPixel(x, ySize-y-1, qRgb(value, value, value));
        }
    }

    image.save(_filename);

    return true;
}

void ImageExporter::autoScale(const std::vector<std::vector<float> > &data)
{
    float min = std::numeric_limits<float>::max();
    float max = std::numeric_limits<float>::lowest();

    for (auto &frame: data) {
        for (float value: frame) {
            if (value < min)
                min = value;
            if (value > max)
                max = value;
        }
    }

    _minScale = min;
    _maxScale = max;
}

int ImageExporter::scaleRange256(float value)
{
    value -= _minScale;
    value /= (_maxScale - _minScale);
    value *= 255;
    
    int intValue = static_cast<int>(value);
    if (intValue > 255) intValue = 255;
    if (intValue < 0) intValue = 0;

    return intValue;
}

