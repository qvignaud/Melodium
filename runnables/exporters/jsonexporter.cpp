/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "jsonexporter.h"

JsonExporter::JsonExporter(Track *track, QString &filename, QObject *parent) : JsonExporter(track, parent)
{
    _filename = filename;
}

JsonExporter::JsonExporter(Track *track, QObject *parent) : Runnable("JsonExporter", track, parent)
{

}

QString JsonExporter::filename() const
{
    return _filename;
}

void JsonExporter::setFilename(const QString &filename)
{
    if (status() == Runnable::Waiting)
        _filename = filename;
}

QStringList JsonExporter::qualifiers() const
{
    return _qualifiers;
}

void JsonExporter::setQualifiers(const QStringList &qualifiers)
{
    if (status() == Runnable::Waiting)
        _qualifiers = qualifiers;
}

TreatmentType JsonExporter::treatmentType()
{
    TreatmentType tt("JsonExporter", TreatmentType::DataExporter);

    tt.setDescription("Exports data of the track in JSON.");

    QVector<TreatmentType::ParamInfo> params;

    TreatmentType::ParamInfo filename;
    filename.name = "filename";
    filename.type = TreatmentType::STRING;
    filename.description = "File to write.";
    params.append(filename);

    TreatmentType::ParamInfo qualifiers;
    qualifiers.name = "qualifiers";
    qualifiers.type = TreatmentType::VECTOR_STRING;
    qualifiers.description = "List of qualifiers to export.";
    params.append(qualifiers);

    tt.setParameters(params);

    return tt;
}

Runnable *JsonExporter::create(TreatmentType type, Track *track, QObject *parent)
{
    Q_UNUSED(type);
    return new JsonExporter(track, parent);
}

bool JsonExporter::init()
{
    return true;
}

#include <iostream>
bool JsonExporter::work()
{
    QJsonObject obj;
    QMutex *mutex = nullptr;

    QJsonObject global;
    auto &globalReal = track()->globalRealQualifiers(mutex);
    for (auto item = globalReal.begin() ; item != globalReal.end() ; ++item) {
        if (_qualifiers.contains(item.key(), Qt::CaseInsensitive))
            global.insert(item.key(), item.value());
    }
    mutex->unlock();

    auto &globalString = track()->globalStringQualifiers(mutex);
    for (auto item = globalString.begin() ; item != globalString.end() ; ++item) {
        if (_qualifiers.contains(item.key(), Qt::CaseInsensitive))
            global.insert(item.key(), QString(item.value().c_str()));
    }
    mutex->unlock();
    obj.insert("global", global);

    QJsonObject simple;
    auto &simpleReal = track()->simpleRealQualifiers(mutex);
    for (auto item = simpleReal.begin() ; item != simpleReal.end() ; ++item) {
        if (_qualifiers.contains(item.key(), Qt::CaseInsensitive)) {
            QJsonArray array;
            for (auto val: item.value())
                array << val;
            simple.insert(item.key(), array);
        }

    }
    mutex->unlock();

    auto &simpleString = track()->simpleStringQualifiers(mutex);
    for (auto item = simpleString.begin() ; item != simpleString.end() ; ++item) {
        if (_qualifiers.contains(item.key(), Qt::CaseInsensitive)) {
            QJsonArray array;
            for (auto val: item.value())
                array << val.c_str();
            simple.insert(item.key(), array);
        }
    }
    mutex->unlock();
    obj.insert("simple", simple);

    QJsonObject detailed;
    auto &detailedReal = track()->detailedRealQualifiers(mutex);
    for (auto item = detailedReal.begin() ; item != detailedReal.end() ; ++item) {
        if (_qualifiers.contains(item.key(), Qt::CaseInsensitive)) {
            QJsonArray array;
            for (auto vec: item.value()) {
                QJsonArray subArray;
                for (auto val: vec)
                    subArray << val;
                array << subArray;
            }
            detailed.insert(item.key(), array);
        }
    }
    mutex->unlock();

    auto &detailedString = track()->detailedStringQualifiers(mutex);
    for (auto item = detailedString.begin() ; item != detailedString.end() ; ++item) {
        if (_qualifiers.contains(item.key(), Qt::CaseInsensitive)) {
            QJsonArray array;
            for (auto vec: item.value()) {
                QJsonArray subArray;
                for (auto val: vec)
                    subArray << val.c_str();
                array << subArray;
            }
            detailed.insert(item.key(), array);
        }
    }
    mutex->unlock();
    obj.insert("detailed", detailed);

    QFile file(_filename);
    if (file.open(QIODevice::WriteOnly)) {
        file.write(QJsonDocument(obj).toJson(QJsonDocument::Compact));
        file.close();
        return true;
    } else return false;
}
