/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef IMAGEEXPORTER_H
#define IMAGEEXPORTER_H

#include "runnable.h"
#include "treatmenttype.h"

/*!
 * \brief Export data as image.
 */
class ImageExporter : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(QString filename READ filename WRITE setFilename)
    Q_PROPERTY(bool autoScale READ autoScale WRITE setAutoScale)
    Q_PROPERTY(float minScale READ minScale WRITE setMinScale)
    Q_PROPERTY(float maxScale READ maxScale WRITE setMaxScale)
public:
    /*!
     * \brief Instanciates a new image exporter.
     * \param track
     * \param parent
     */
    explicit ImageExporter(Track *track, QObject *parent = nullptr);

    /*!
     * \brief Filename of image to save.
     * \return
     */
    QString filename() const;
    /*!
     * \brief Set filename to save.
     * \param filename
     *
     * \note This function does nothing if the exporter is launched.
     */
    void setFilename(const QString &filename);

    /*!
     * \brief Tells if the exporter will autoscale color on values.
     * \return
     */
    bool autoScale() const;
    /*!
     * \brief Set if the exporter will auto scale colors.
     * \param autoScale
     */
    void setAutoScale(bool autoScale);

    /*!
     * \brief Minimum value equivalent to white.
     * \return
     */
    float minScale() const;
    /*!
     * \brief Set minimum value as white.
     * \param minScale
     */
    void setMinScale(float minScale);

    /*!
     * \brief Maximum value as black.
     * \return
     */
    float maxScale() const;
    /*!
     * \brief Set maximum value as black.
     * \param maxScale
     */
    void setMaxScale(float maxScale);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new image exporter.
     * \param track
     * \param parent
     * \return An image exporter.
     *
     * \note This function is to give for registering ImageExporter in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Return true.
     * \return
     */
    bool init();
    /*!
     * \brief Make export.
     * \return
     */
    bool work();

private:
    /// File to write in.
    QString _filename;

    /// Autoscale enabled or not.
    bool _autoScale;
    /// Value as white.
    float _minScale;
    /// Value as black.
    float _maxScale;

    /*!
     * \brief Setup minimum and maximum scale value according to data.
     * \param data
     */
    void autoScale(const std::vector<std::vector<float>> &data);
    /*!
     * \brief Tells intensity to use for value, according to scale range.
     * \param value
     * \return
     */
    int scaleRange256(float value);
};

#endif // IMAGEEXPORTER_H
