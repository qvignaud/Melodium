/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef CSVEXPORTER_H
#define CSVEXPORTER_H

#include "runnable.h"
#include "treatmenttype.h"

/*!
 * \brief CSV exporter class.
 */
class CsvExporter : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(QString filename READ filename WRITE setFilename)
    Q_PROPERTY(QStringList qualifiers READ qualifiers WRITE setQualifiers)
public:
    /*!
     * \brief Instanciates a new CSV exporter class to specified file.
     * \param track
     * \param filename
     * \param parent
     */
    explicit CsvExporter(Track *track, QString &filename, QObject *parent = nullptr);
    /*!
     * \brief Instanciates a new CSV exporter.
     * \param track
     * \param parent
     */
    explicit CsvExporter(Track *track, QObject *parent = nullptr);

    /*!
     * \brief Filename to write in.
     * \return
     */
    QString filename() const;
    /*!
     * \brief Set filename to write in.
     * \param filename
     *
     * \note This function does nothing if the exporter is launched.
     */
    void setFilename(const QString &filename);

    /*!
     * \brief List of qualifiers to export.
     * \return
     */
    QStringList qualifiers() const;
    /*!
     * \brief Set the list of qualifiers to export.
     * \param qualifiers
     *
     * \note This function does nothing if the exporter is launched.
     */
    void setQualifiers(const QStringList &qualifiers);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

    /*!
     * \brief Create a new CSV exporter.
     * \param track
     * \param parent
     * \return An CSV exporter.
     *
     * \note This function is to give for registering CsvExporter in TreatmentsFactory.
     */
    static Runnable *create(TreatmentType type, Track *track, QObject *parent);

protected:
    /*!
     * \brief Initialize export.
     */
    bool init();

    /*!
     * \brief Make export.
     */
    bool work();

private:
    ///File to write in.
    QString _filename;
    ///Qualifiers to export.
    QStringList _qualifiers;
};

#endif // CSVEXPORTER_H
