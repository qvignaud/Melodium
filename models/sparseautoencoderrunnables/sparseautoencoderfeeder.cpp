/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "sparseautoencoderfeeder.h"
#include "models/sparseautoencoder.h"

SparseAutoencoderFeeder::SparseAutoencoderFeeder(Track *track, SparseAutoencoder *ae, QObject *parent) :
    Runnable("SparseAutoencoderFeeder", track, parent),
    _ae(ae)
{

}

TreatmentType SparseAutoencoderFeeder::treatmentType()
{
    TreatmentType tt("SparseAutoencoderFeeder", TreatmentType::ModelFeeder);

    TreatmentType::DataInfo data;
    data.name = "data";
    data.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(data);

    return  tt;
}

bool SparseAutoencoderFeeder::init()
{
    QMutex *mutex = nullptr;
    auto &qualifiers = track()->detailedRealQualifiers(mutex);
    _data = qualifiers.value(inputName("data"));
    mutex->unlock();

    return true;
}

bool SparseAutoencoderFeeder::work()
{

    const size_t data_columns = _data.size();
    const size_t data_rows = _data.at(0).size();
    arma::mat data_matrix(data_rows, data_columns);
    for (size_t x = 0 ; x < data_columns ; ++x) {
        for (size_t y = 0 ; y < data_rows ; ++y) {
            data_matrix(y, x) = _data.at(x).at(y);
        }
    }

    arma::mat fakeLabel = {0.0};

    _data.clear();

    _ae->feed(data_matrix, fakeLabel);

    return true;
}
