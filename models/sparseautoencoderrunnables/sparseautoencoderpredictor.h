/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef SPARSEAUTOENCODERPREDICTOR_H
#define SPARSEAUTOENCODERPREDICTOR_H

#include "runnable.h"
#include "treatmenttype.h"

class SparseAutoencoder;

class SparseAutoencoderPredictor : public Runnable
{
    Q_OBJECT
public:
    explicit SparseAutoencoderPredictor(Track *track, SparseAutoencoder *ae, QObject *parent = nullptr);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

protected:
    /// Autoencoder connected to this feeder.
    SparseAutoencoder *const _ae;

    virtual bool init();
    virtual bool work();

private:
    std::vector<std::vector<float>> _data;
};

#endif // SPARSEAUTOENCODERPREDICTOR_H
