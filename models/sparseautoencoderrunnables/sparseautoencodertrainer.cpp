/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "sparseautoencodertrainer.h"
#include "models/sparseautoencoder.h"

SparseAutoencoderTrainer::SparseAutoencoderTrainer(SparseAutoencoder *ae, QObject *parent) :
    Runnable("SparseAutoencoderTrainer", parent),
    _ae(ae)
{

}

QString SparseAutoencoderTrainer::trainingOutput() const
{
    return _trainingOutput;
}

void SparseAutoencoderTrainer::setTrainingOutput(const QString &trainingOutput)
{
    if (status() == Waiting)
        _trainingOutput = trainingOutput;
}

TreatmentType SparseAutoencoderTrainer::treatmentType()
{
    TreatmentType tt("SparseAutoencoderTrainer", TreatmentType::ModelTrainer, TreatmentType::NoTrack);

    TreatmentType::ParamInfo trainingOutput;
    trainingOutput.name = "trainingOutput";
    trainingOutput.description = "File where writing training stats output. Leave empty to not have output.";
    trainingOutput.type = TreatmentType::STRING;
    trainingOutput.defaultValue = "";
    tt.addParameter(trainingOutput);

    return  tt;
}

bool SparseAutoencoderTrainer::init()
{
    if (!_trainingOutput.isEmpty()) {
        _fileOutput.setFileName(_trainingOutput);

        if (!_fileOutput.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Unbuffered))
            return false;
    }

    return _ae->isFeedingFinished();
}

bool SparseAutoencoderTrainer::work()
{
    bool produceOutput = !_trainingOutput.isEmpty();

    auto start = std::chrono::steady_clock::now();
    _ae->train();
    auto end = std::chrono::steady_clock::now();

    if (produceOutput) {
        std::chrono::duration<double, std::milli> duration = end - start;

        QString outputString;
        outputString += QString::number(duration.count(), 'f', 6) + "ms";
        outputString += '\n';

        _fileOutput.write(outputString.toUtf8());
        _fileOutput.flush();
    }

    _fileOutput.close();

    return true;
}
