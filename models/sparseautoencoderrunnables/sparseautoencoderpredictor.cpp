/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "sparseautoencoderpredictor.h"
#include "models/sparseautoencoder.h"

SparseAutoencoderPredictor::SparseAutoencoderPredictor(Track *track, SparseAutoencoder *ae, QObject *parent) :
    Runnable("SparseAutoencoderPredictor", track, parent),
    _ae(ae)
{

}

TreatmentType SparseAutoencoderPredictor::treatmentType()
{
    TreatmentType tt("SparseAutoencoderPredictor", TreatmentType::ModelPredictor);

    TreatmentType::DataInfo data;
    data.name = "data";
    data.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(data);

    TreatmentType::DataInfo features;
    features.name = "features";
    features.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addProvide(features);

    return  tt;
}

bool SparseAutoencoderPredictor::init()
{
    QMutex *mutex = nullptr;
    auto &qualifiers = track()->detailedRealQualifiers(mutex);
    _data = qualifiers.value(inputName("data"));
    mutex->unlock();

    return true;
}

bool SparseAutoencoderPredictor::work()
{

    const size_t data_columns = _data.size();
    const size_t data_rows = _data.at(0).size();
    arma::mat data_matrix(data_rows, data_columns);
    for (size_t x = 0 ; x < data_columns ; ++x) {
        for (size_t y = 0 ; y < data_rows ; ++y) {
            data_matrix(y, x) = _data.at(x).at(y);
        }
    }

    arma::mat features_matrix;

    _ae->predict(data_matrix, features_matrix);

    std::vector<std::vector<float>> features(features_matrix.n_cols,
                                                std::vector<float>(features_matrix.n_rows, 0));
    for (size_t c = 0 ; c < features_matrix.n_cols ; ++c) {
        for (size_t r = 0 ; r < features_matrix.n_rows ; ++r) {
            features.at(c).at(r) = features_matrix(r, c);
        }
    }

    track()->storeQualifier(outputName("features"), features);

    return true;
}
