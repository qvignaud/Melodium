/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef FFNRECT3HL_H
#define FFNRECT3HL_H

#include "ffnmodel.h"

class FfnRect3hl : public FfnModel
{
    Q_OBJECT
public:
    explicit FfnRect3hl(QObject *parent = nullptr);
    ~FfnRect3hl();

    static Model *create();

    static ModelType modelType();

signals:

public slots:


protected:
    virtual void building();
};

#endif // FFNRECT3HL_H
