/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "configurablerectffn.h"

ConfigurableRectFfn::ConfigurableRectFfn(QObject *parent) : FfnModel(modelType(), parent)
{

}

QStringList ConfigurableRectFfn::layers() const
{
    return _layers;
}

void ConfigurableRectFfn::setLayers(const QStringList &layers)
{
    _layers = layers;
}

unsigned int ConfigurableRectFfn::outputSize() const
{
    return _outputSize;
}

void ConfigurableRectFfn::setOutputSize(unsigned int outputSize)
{
    _outputSize = outputSize;
}

unsigned int ConfigurableRectFfn::layersSize() const
{
    return _layersSize;
}

void ConfigurableRectFfn::setLayersSize(unsigned int layersSize)
{
    _layersSize = layersSize;
}

unsigned int ConfigurableRectFfn::inputSize() const
{
    return _inputSize;
}

void ConfigurableRectFfn::setInputSize(unsigned int inputSize)
{
    _inputSize = inputSize;
}

Model *ConfigurableRectFfn::create()
{
    return new ConfigurableRectFfn();
}

ModelType ConfigurableRectFfn::modelType()
{
    ModelType mt("ConfigurableRectFfn", ModelType::ModelAbilities(ModelType::Feeding | ModelType::Training | ModelType::Predicting | ModelType::Saving | ModelType::Loading));
    mt.setDescription("Configurable rectangle fast-forward network, layers sized as the input data, and reduced at the end to labels.");

    mt.setFeedingTreatmentType(MlpackModelFeeder::treatmentType());
    mt.setTrainingTreatmentType(MlpackModelTrainer::treatmentType());
    mt.setPredictingTreatmentType(MlpackModelPredictor::treatmentType());
    mt.setSavingTreatmentType(MlpackModelSaver::treatmentType());
    mt.setLoadingTreatmentType(MlpackModelLoader::treatmentType());

    return mt;
}

void ConfigurableRectFfn::building()
{
    checkBuilt();

    _ffn.Add<mlpack::ann::Linear<>>(_inputSize, _layersSize);
    for (QString layer: _layers) {
        if (layer == "BilinearInterpolation")
            _ffn.Add<mlpack::ann::BilinearInterpolation<>>();
        else if (layer == "FastLSTM")
            _ffn.Add<mlpack::ann::FastLSTM<>>();
        else if (layer == "IdentityLayer")
            _ffn.Add<mlpack::ann::IdentityLayer<>>();
        else if (layer == "Linear")
            _ffn.Add<mlpack::ann::Linear<>>(_layersSize, _layersSize);
        else if (layer == "LogSoftMax")
            _ffn.Add<mlpack::ann::LogSoftMax<>>();
        else if (layer == "LSTM")
            _ffn.Add<mlpack::ann::LSTM<>>();
        else if (layer == "MaxPooling")
            _ffn.Add<mlpack::ann::MaxPooling<>>();
        else if (layer == "MeanPooling")
            _ffn.Add<mlpack::ann::MeanPooling<>>();
        else if (layer == "NegativeLogLikelihood")
            _ffn.Add<mlpack::ann::NegativeLogLikelihood<>>();
        else if (layer == "PReLU")
            _ffn.Add<mlpack::ann::PReLU<>>();
        else if (layer == "ReLU")
            _ffn.Add<mlpack::ann::ReLULayer<>>();
        else if (layer == "Sigmoid")
            _ffn.Add<mlpack::ann::SigmoidLayer<>>();
        else if (layer == "TanHLayer")
            _ffn.Add<mlpack::ann::TanHLayer<>>();
        else
            throw std::invalid_argument("'" + layer.toStdString() + "' is not a know layer type.");
    }
    _ffn.Add<mlpack::ann::Linear<>>(_layersSize, _outputSize);
}

