/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "ffnrect3hl.h"

FfnRect3hl::FfnRect3hl(QObject *parent) : FfnModel(modelType(), parent)
{

}

FfnRect3hl::~FfnRect3hl()
{

}

Model *FfnRect3hl::create()
{
    return new FfnRect3hl();
}

ModelType FfnRect3hl::modelType()
{
    ModelType mt("FfnRect3hl", ModelType::ModelAbilities(ModelType::Feeding | ModelType::Training | ModelType::Predicting | ModelType::Saving | ModelType::Loading));
    mt.setDescription("Fast-forward network with 3 hidden layers, all sized as input vector.");

    mt.setFeedingTreatmentType(MlpackModelFeeder::treatmentType());
    mt.setTrainingTreatmentType(MlpackModelTrainer::treatmentType());
    mt.setPredictingTreatmentType(MlpackModelPredictor::treatmentType());
    mt.setSavingTreatmentType(MlpackModelSaver::treatmentType());
    mt.setLoadingTreatmentType(MlpackModelLoader::treatmentType());

    return mt;
}

void FfnRect3hl::building()
{
    checkBuilt();
    _ffn.Add<mlpack::ann::Linear<>>(_data.n_rows, _data.n_rows);
    _ffn.Add<mlpack::ann::PReLU<>>();
    _ffn.Add<mlpack::ann::PReLU<>>();
    _ffn.Add<mlpack::ann::PReLU<>>();
    _ffn.Add<mlpack::ann::Linear<>>(_data.n_rows, _labels.n_rows);
}
