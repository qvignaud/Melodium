#include "configurablevarffn.h"

ConfigurableVarFfn::ConfigurableVarFfn(QObject *parent) : FfnModel(modelType(), parent)
{

}

QStringList ConfigurableVarFfn::layers() const
{
    return _layers;
}

void ConfigurableVarFfn::setLayers(const QStringList &layers)
{
    _layers = layers;
}

QList<QVariant> ConfigurableVarFfn::layersOutputSizes() const
{
    return _layersOutputSizes;
}

void ConfigurableVarFfn::setLayersOutputSizes(const QList<QVariant> &layersOutputSizes)
{
    _layersOutputSizes = layersOutputSizes;
}

Model *ConfigurableVarFfn::create()
{
    return new ConfigurableVarFfn();
}

ModelType ConfigurableVarFfn::modelType()
{
    ModelType mt("ConfigurableVarFfn", ModelType::ModelAbilities(ModelType::Feeding | ModelType::Training | ModelType::Predicting | ModelType::Saving | ModelType::Loading));
    mt.setDescription("Configurable fast-forward network, output of the layers setted to custom sizes (using linear layer).");

    mt.setFeedingTreatmentType(MlpackModelFeeder::treatmentType());
    mt.setTrainingTreatmentType(MlpackModelTrainer::treatmentType());
    mt.setPredictingTreatmentType(MlpackModelPredictor::treatmentType());
    mt.setSavingTreatmentType(MlpackModelSaver::treatmentType());
    mt.setLoadingTreatmentType(MlpackModelLoader::treatmentType());

    return mt;
}

void ConfigurableVarFfn::building()
{
    checkBuilt();

    if (_layers.size() != _layersOutputSizes.size())
        throw std::invalid_argument("Layers and output sizes don't have same lenght.");

    _ffn.Add<mlpack::ann::Linear<>>(_data.n_rows, _data.n_rows);
    size_t previousSize = _data.n_rows;
    for (int i=0 ; i < _layers.size() ; ++i) {
        const QString &layer = _layers.at(i);
        if (layer == "BilinearInterpolation")
            _ffn.Add<mlpack::ann::BilinearInterpolation<>>();
        else if (layer == "FastLSTM")
            _ffn.Add<mlpack::ann::FastLSTM<>>();
        else if (layer == "IdentityLayer")
            _ffn.Add<mlpack::ann::IdentityLayer<>>();
        else if (layer == "Linear")
            _ffn.Add<mlpack::ann::Linear<>>(_data.n_rows, _data.n_rows);
        else if (layer == "LogSoftMax")
            _ffn.Add<mlpack::ann::LogSoftMax<>>();
        else if (layer == "LSTM")
            _ffn.Add<mlpack::ann::LSTM<>>();
        else if (layer == "MaxPooling")
            _ffn.Add<mlpack::ann::MaxPooling<>>();
        else if (layer == "MeanPooling")
            _ffn.Add<mlpack::ann::MeanPooling<>>();
        else if (layer == "NegativeLogLikelihood")
            _ffn.Add<mlpack::ann::NegativeLogLikelihood<>>();
        else if (layer == "PReLU")
            _ffn.Add<mlpack::ann::PReLU<>>();
        else if (layer == "ReLU")
            _ffn.Add<mlpack::ann::ReLULayer<>>();
        else if (layer == "Sigmoid")
            _ffn.Add<mlpack::ann::SigmoidLayer<>>();
        else if (layer == "TanHLayer")
            _ffn.Add<mlpack::ann::TanHLayer<>>();
        else
            throw std::invalid_argument("'" + layer.toStdString() + "' is not a know layer type.");

        size_t outSize = _layersOutputSizes.at(i).toUInt();
        _ffn.Add<mlpack::ann::Linear<>>(previousSize, outSize);
        previousSize = outSize;

    }

    _ffn.Add<mlpack::ann::Linear<>>(previousSize, _labels.n_rows);
}
