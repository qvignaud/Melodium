/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef SPARSEAUTOENCODER_H
#define SPARSEAUTOENCODER_H

#include "mlpackmodel.h"
#include <mlpack/methods/sparse_autoencoder/sparse_autoencoder.hpp>
#include "sparseautoencoderrunnables/sparseautoencoderfeeder.h"
#include "sparseautoencoderrunnables/sparseautoencodertrainer.h"
#include "sparseautoencoderrunnables/sparseautoencoderpredictor.h"

class SparseAutoencoder : public MlpackModel
{
    Q_OBJECT
    Q_PROPERTY(unsigned int visibleSize READ visibleSize WRITE setVisibleSize)
    Q_PROPERTY(unsigned int hiddenSize READ hiddenSize WRITE setHiddenSize)
    Q_PROPERTY(float lambda READ lambda WRITE setLambda)
    Q_PROPERTY(float beta READ beta WRITE setBeta)
    Q_PROPERTY(float rho READ rho WRITE setRho)
public:
    explicit SparseAutoencoder(QObject *parent = nullptr);
    ~SparseAutoencoder();

    unsigned int visibleSize() const;
    void setVisibleSize(unsigned int visibleSize);

    unsigned int hiddenSize() const;
    void setHiddenSize(unsigned int hiddenSize);

    float lambda() const;
    void setLambda(float lambda);

    float beta() const;
    void setBeta(float beta);

    float rho() const;
    void setRho(float rho);

    static Model *create();

    static ModelType modelType();

protected:

    virtual Runnable *createFeedingRunnable(Track *track, QObject *parent);
    virtual Runnable *createTrainingRunnable(QObject *parent);
    virtual Runnable *createPredictionRunnable(Track *track, QObject *parent);

    mlpack::nn::SparseAutoencoder *_autoEncoder = nullptr;
    mlpack::nn::SparseAutoencoderFunction *_autoEncoderFunction = nullptr;

    virtual void training();
    virtual void predicting(arma::mat &data, arma::mat &assignments);
    virtual void saving(const std::string &filename);
    virtual void loading(const std::string &filename);

private:
    unsigned int _visibleSize;
    unsigned int _hiddenSize;
    float _lambda;
    float _beta;
    float _rho;
};

#endif // SPARSEAUTOENCODER_H
