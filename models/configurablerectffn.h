/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef CONFIGURABLERECTFFN_H
#define CONFIGURABLERECTFFN_H

#include "ffnmodel.h"

class ConfigurableRectFfn : public FfnModel
{
    Q_OBJECT
    Q_PROPERTY(QStringList layers READ layers WRITE setLayers)
    Q_PROPERTY(unsigned int inputSize READ inputSize WRITE setInputSize)
    Q_PROPERTY(unsigned int layersSize READ layersSize WRITE setLayersSize)
    Q_PROPERTY(unsigned int outputSize READ outputSize WRITE setOutputSize)
public:
    explicit ConfigurableRectFfn(QObject *parent = nullptr);

    QStringList layers() const;
    void setLayers(const QStringList &layers);

    unsigned int inputSize() const;
    void setInputSize(unsigned int inputSize);

    unsigned int layersSize() const;
    void setLayersSize(unsigned int layersSize);

    unsigned int outputSize() const;
    void setOutputSize(unsigned int outputSize);

    static Model *create();

    static ModelType modelType();

signals:

public slots:

protected:
    virtual void building();

private:
    QStringList _layers;
    unsigned int _inputSize;
    unsigned int _layersSize;
    unsigned int _outputSize;

};

#endif // CONFIGURABLERECTFFN_H
