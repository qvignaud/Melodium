/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "devmodel.h"

DevModel::DevModel(QObject *parent) : MlpackModel(modelType(), parent)
{
}

DevModel::~DevModel()
{

}

Model *DevModel::create()
{
    return new DevModel();
}

ModelType DevModel::modelType()
{
    ModelType mt("DevModel", ModelType::ModelAbilities(ModelType::Feeding | ModelType::Training | ModelType::Predicting | ModelType::Saving | ModelType::Loading));
    mt.setDescription("Development model. Do NOT use in production.");

    mt.setFeedingTreatmentType(MlpackModelFeeder::treatmentType());
    mt.setTrainingTreatmentType(MlpackModelTrainer::treatmentType());
    mt.setPredictingTreatmentType(MlpackModelPredictor::treatmentType());
    mt.setSavingTreatmentType(MlpackModelSaver::treatmentType());
    mt.setLoadingTreatmentType(MlpackModelLoader::treatmentType());

    return mt;
}

void DevModel::training()
{
    /*
    // Load the data from data.csv (hard-coded).  Use CLI for simple command-line
    // parameter handling.
    arma::mat data;
    data::Load("data.csv", data, true);
    // Use templates to specify that we want a NeighborSearch object which uses
    // the Manhattan distance.
    NeighborSearch<NearestNeighborSort, ManhattanDistance> nn(data);
    // Create the object we will store the nearest neighbors in.
    arma::Mat<size_t> neighbors;
    arma::mat distances; // We need to store the distance too.
    // Compute the neighbors.
    nn.Search(1, neighbors, distances);
    // Write each neighbor and distance using Log.
    for (size_t i = 0; i < neighbors.n_elem; ++i)
    {
      std::cout << "Nearest neighbor of point " << i << " is point "
          << neighbors[i] << " and the distance is " << distances[i] << ".\n";
    }
    */

    arma::mat trainLabels = arma::zeros<arma::mat>(1, _labels.n_cols);
    for (size_t i = 0; i < _labels.n_cols; ++i)
    {
      trainLabels(i) = arma::as_scalar(arma::find(
          arma::max(_labels.col(i)) == _labels.col(i), 1)) + 1;
    }

    //mlpack::ann::FFN<> model;
    //model.Add<mlpack::ann::Linear<> >(matrix.n_rows, matrix.n_rows);
    //model.Add<mlpack::ann::Linear<> >(matrix.n_rows, matrix.n_rows);
    //model.Add<mlpack::ann::Linear<> >(matrix.n_rows, matrix.n_rows);
    _ffn.Add<mlpack::ann::Linear<> >(_data.n_rows, 128);
    //model.Add<mlpack::ann::SigmoidLayer<> >();
    //model.Add<mlpack::ann::Linear<> >(128, 128);
    //model.Add<mlpack::ann::SigmoidLayer<> >();
    //model.Add<mlpack::ann::Linear<> >(12, 1);
    //for (int i=0 ; i < 20 ; ++i)
    //    model.Add<mlpack::ann::Linear<> >(12, 12);
    //model.Add<mlpack::ann::SigmoidLayer<> >();
    //model.Add<mlpack::ann::LogSoftMax<> >();

    for (int i=1 ; i <= 4 ; ++i) {
        _ffn.Train(_data, trainLabels);
        std::cout << std::endl << "Iteration " << i << " finished." ;
    }

    arma::mat assignments;
    _ffn.Predict(_data, assignments);
    assignments.save("/tmp/assignments.csv", arma::csv_ascii);

    //mlpack::data::Save("/tmp/model.xml", "model", model);
}

void DevModel::predicting(arma::mat &data, arma::mat &assignments)
{
    _ffn.Predict(data, assignments);
}

void DevModel::saving(const std::string &filename)
{
    mlpack::data::Save(filename, "mlpackmodel", _ffn, true);
}

void DevModel::loading(const std::string &filename)
{
    mlpack::data::Load(filename, "mlpackmodel", _ffn, true);
}

