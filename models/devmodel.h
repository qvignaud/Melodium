/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef DEVMODEL_H
#define DEVMODEL_H

#include "mlpackmodel.h"
#include <mlpack/methods/ann/ffn.hpp>

/*!
 * \brief Development model class.
 */
class DevModel : public MlpackModel
{
    Q_OBJECT
public:
    explicit DevModel(QObject *parent = nullptr);
    ~DevModel();

    static Model *create();

    static ModelType modelType();

protected:
    void training();

    void predicting(arma::mat &data, arma::mat &assignments);

    void saving(const std::string &filename);
    void loading(const std::string &filename);

private:
    mlpack::ann::FFN<> _ffn;
};

#endif // DEVMODEL_H
