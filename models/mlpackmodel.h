/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MLPACKMODEL_H
#define MLPACKMODEL_H

#include "model.h"
#include <QMutex>
#include <QMutexLocker>
#include <mlpack/core.hpp>

#include "models/mlpackrunnables/mlpackmodelfeeder.h"
#include "models/mlpackrunnables/mlpackmodeltrainer.h"
#include "models/mlpackrunnables/mlpackmodelpredictor.h"
#include "models/mlpackrunnables/mlpackmodelsaver.h"
#include "models/mlpackrunnables/mlpackmodelloader.h"

/*!
 * \brief Mlpack models base class.
 *
 * This class mainly makes use of a Mlpack model thread-safe.
 */
class MlpackModel : public Model
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a new Mlpack model.
     * \param parent
     */
    explicit MlpackModel(const ModelType &type, QObject *parent = nullptr);
    ~MlpackModel();

    /*!
     * \brief Add data to fed model with.
     * \param data
     * \param labels
     *
     * This is essentially used by feeding treatments.
     */
    void feed(const arma::mat &data, const arma::mat &labels);
    void train();
    void predict(arma::mat &data, arma::mat &assignments);
    void save(const std::string &filename);
    void load(const std::string &filename);

    double sequencePrecisionScore(double tolerance = 1e-4);

protected:
    /*!
     * \brief Mutex used to make this class thread-safe.
     */
    QMutex _mutex;
    /// Data to fed with.
    arma::mat _data;
    /// Labels of the data.
    arma::mat _labels;

    virtual Runnable *createFeedingRunnable(Track *track, QObject *parent);
    virtual Runnable *createTrainingRunnable(QObject *parent);
    virtual Runnable *createPredictionRunnable(Track *track, QObject *parent);
    virtual Runnable *createSavingRunnable(QObject *parent);
    virtual Runnable *createLoadingRunnable(QObject *parent);

    /*!
     * \brief Make the training of the Mlpack model.
     * \note For models that don't have any training step, juste define it with empty body.
     */
    virtual void training() = 0;
    /*!
     * \brief Make the predictions from the Mlpack model.
     * \param data
     * \param assignments
     */
    virtual void predicting(arma::mat &data, arma::mat &assignments) = 0;
    /*!
     * \brief Save the Mlpack model.
     * \param filename
     */
    virtual void saving(const std::string &filename) = 0;
    /*!
     * \brief Load the Mlpack model.
     * \param filename
     */
    virtual void loading(const std::string &filename) = 0;
};

#endif // MLPACKMODEL_H
