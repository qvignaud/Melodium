/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef CONFIGURABLEVARFFN_H
#define CONFIGURABLEVARFFN_H

#include "ffnmodel.h"

class ConfigurableVarFfn : public FfnModel
{
    Q_OBJECT
    Q_PROPERTY(QStringList layers READ layers WRITE setLayers)
    Q_PROPERTY(QList<QVariant> layersOutputSizes READ layersOutputSizes WRITE setLayersOutputSizes)
public:
    explicit ConfigurableVarFfn(QObject *parent = nullptr);

    QStringList layers() const;
    void setLayers(const QStringList &layers);

    QList<QVariant> layersOutputSizes() const;
    void setLayersOutputSizes(const QList<QVariant> &layersOutputSizes);

    static Model *create();

    static ModelType modelType();

signals:

public slots:

protected:
    virtual void building();

private:
    QStringList _layers;
    QList<QVariant> _layersOutputSizes;

};

#endif // CONFIGURABLEVARFFN_H
