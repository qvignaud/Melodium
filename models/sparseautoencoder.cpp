/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "sparseautoencoder.h"

SparseAutoencoder::SparseAutoencoder(QObject *parent) : MlpackModel(modelType(), parent)
{
    _visibleSize = 1;
    _hiddenSize = 1;
    _lambda = 0.0001;
    _beta = 3;
    _rho = 0.01;
}

SparseAutoencoder::~SparseAutoencoder()
{
    delete _autoEncoder;
    delete _autoEncoderFunction;
}

unsigned int SparseAutoencoder::visibleSize() const
{
    return _visibleSize;
}

void SparseAutoencoder::setVisibleSize(unsigned int visibleSize)
{
    if (_autoEncoder == nullptr)
        _visibleSize = visibleSize;
}

unsigned int SparseAutoencoder::hiddenSize() const
{
    return _hiddenSize;
}

void SparseAutoencoder::setHiddenSize(unsigned int hiddenSize)
{
    if (_autoEncoder == nullptr)
        _hiddenSize = hiddenSize;
}

float SparseAutoencoder::lambda() const
{
    return _lambda;
}

void SparseAutoencoder::setLambda(float lambda)
{
    if (_autoEncoder == nullptr)
        _lambda = lambda;
}

float SparseAutoencoder::beta() const
{
    return _beta;
}

void SparseAutoencoder::setBeta(float beta)
{
    if (_autoEncoder == nullptr)
        _beta = beta;
}

float SparseAutoencoder::rho() const
{
    return _rho;
}

void SparseAutoencoder::setRho(float rho)
{
    if (_autoEncoder == nullptr)
        _rho = rho;
}

Model *SparseAutoencoder::create()
{
    return new SparseAutoencoder();
}

ModelType SparseAutoencoder::modelType()
{
    ModelType mt("SparseAutoencoder", ModelType::ModelAbilities(ModelType::Feeding | ModelType::Training | ModelType::Predicting));
    mt.setDescription("Sparse autoencoder, training needed for use, no loading nor saving available at that time.");

    mt.setFeedingTreatmentType(SparseAutoencoderFeeder::treatmentType());
    mt.setTrainingTreatmentType(SparseAutoencoderTrainer::treatmentType());
    mt.setPredictingTreatmentType(SparseAutoencoderPredictor::treatmentType());

    return mt;
}

Runnable *SparseAutoencoder::createFeedingRunnable(Track *track, QObject *parent)
{
    return new SparseAutoencoderFeeder(track, this, parent);
}

Runnable *SparseAutoencoder::createTrainingRunnable(QObject *parent)
{
    return new SparseAutoencoderTrainer(this, parent);
}

Runnable *SparseAutoencoder::createPredictionRunnable(Track *track, QObject *parent)
{
    return new SparseAutoencoderPredictor(track, this, parent);
}

void SparseAutoencoder::training()
{
    if (_autoEncoder != nullptr)
        throw std::logic_error("The autoencoder has already been trained.");

    _autoEncoder = new mlpack::nn::SparseAutoencoder(_data, _visibleSize, _hiddenSize, _lambda, _beta, _rho);
}

void SparseAutoencoder::predicting(arma::mat &data, arma::mat &assignments)
{
    if (_autoEncoder == nullptr)
        throw std::logic_error("The autoencoder has not been trained.");

    _autoEncoder->GetNewFeatures(data, assignments);
}

void SparseAutoencoder::saving(const std::string &filename)
{
    Q_UNUSED(filename)
}

void SparseAutoencoder::loading(const std::string &filename)
{
    Q_UNUSED(filename)
}

