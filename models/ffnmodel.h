/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef FFNMODEL_H
#define FFNMODEL_H

#include "mlpackmodel.h"
#include <mlpack/methods/ann/ffn.hpp>
#include <mlpack/methods/ann/loss_functions/mean_squared_error.hpp>

/*!
 * \brief FFN model class.
 *
 * This class can be used as base for FFN classifiers.
 */
class FfnModel : public MlpackModel
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a new FFN model.
     * \param parent
     */
    explicit FfnModel(const ModelType &type, QObject *parent = nullptr);
    ~FfnModel();

protected:
    /*!
     * \brief The Fast-Forward Network.
     */
    mlpack::ann::FFN<mlpack::ann::MeanSquaredError<>> _ffn;

    /*!
     * \brief Tells if the model is built or not.
     */
    bool _built;
    /*!
     * \brief Check wether the model is built or not.
     *
     * Throw an logic error if already built, else turn _built to true.
     */
    void checkBuilt();
    /*!
     * \brief Build the model.
     *
     * This is automatically called when feeding is finished, data and labels are then available and complete.
     */
    virtual void building() = 0;

    virtual void training();
    virtual void predicting(arma::mat &data, arma::mat &assignments);
    virtual void saving(const std::string &filename);
    virtual void loading(const std::string &filename);
};

#endif // FFNMODEL_H
