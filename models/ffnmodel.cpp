/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "ffnmodel.h"

FfnModel::FfnModel(const ModelType &type, QObject *parent) : MlpackModel(type, parent)
{
    _built = false;
    connect(this, &Model::feedingFinished, this, &FfnModel::building);
}

FfnModel::~FfnModel()
{

}

void FfnModel::checkBuilt()
{
    if (_built)
        throw std::logic_error("The model is already built.");
    _built = true;
}

void FfnModel::training()
{
    _ffn.Train(_data, _labels);
}

void FfnModel::predicting(arma::mat &data, arma::mat &assignments)
{
    _ffn.Predict(data, assignments);
}

void FfnModel::saving(const std::string &filename)
{
    mlpack::data::Save(filename, type().name().toStdString(), _ffn, true);
}

void FfnModel::loading(const std::string &filename)
{
    building();
    std::cout << type().name().toStdString() << std::endl;
    mlpack::data::Load(filename, type().name().toStdString(), _ffn, true);
}
