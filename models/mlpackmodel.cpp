/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "mlpackmodel.h"

#include "mlpack/methods/ann/augmented/tasks/score.hpp"

#include "models/mlpackrunnables/mlpackmodelfeeder.h"
#include "models/mlpackrunnables/mlpackmodeltrainer.h"
#include "models/mlpackrunnables/mlpackmodelpredictor.h"
#include "models/mlpackrunnables/mlpackmodelsaver.h"
#include "models/mlpackrunnables/mlpackmodelloader.h"

MlpackModel::MlpackModel(const ModelType &type, QObject *parent) : Model(type, parent)
{

}

MlpackModel::~MlpackModel()
{
}

void MlpackModel::feed(const arma::mat &data, const arma::mat &labels)
{
    QMutexLocker locker(&_mutex);
    _data.insert_cols(_data.n_cols, data);
    _labels.insert_cols(_labels.n_cols, labels);
}

void MlpackModel::train()
{
    QMutexLocker locker(&_mutex);
    training();
}

void MlpackModel::predict(arma::mat &data, arma::mat &assignments)
{
    QMutexLocker locker(&_mutex);
    predicting(data, assignments);
}

void MlpackModel::save(const std::string &filename)
{
    QMutexLocker locker(&_mutex);
    saving(filename);
}

void MlpackModel::load(const std::string &filename)
{
    QMutexLocker locker(&_mutex);
    loading(filename);
}

double MlpackModel::sequencePrecisionScore(double tolerance)
{
    arma::mat assignments;
    predict(_data, assignments);

    arma::field<arma::mat> trueOutputs(1);
    trueOutputs(0) = _labels;
    arma::field<arma::mat> predOutputs(1);
    predOutputs(0) = assignments;

    return mlpack::ann::augmented::scorers::SequencePrecision(trueOutputs, predOutputs, tolerance);
}

Runnable *MlpackModel::createFeedingRunnable(Track *track, QObject *parent)
{
    return new MlpackModelFeeder(track, this, parent);
}

Runnable *MlpackModel::createTrainingRunnable(QObject *parent)
{
    return new MlpackModelTrainer(this, parent);
}

Runnable *MlpackModel::createPredictionRunnable(Track *track, QObject *parent)
{
    return new MlpackModelPredictor(track, this, parent);
}

Runnable *MlpackModel::createSavingRunnable(QObject *parent)
{
    return new MlpackModelSaver(this, parent);
}

Runnable *MlpackModel::createLoadingRunnable(QObject *parent)
{
    return new MlpackModelLoader(this, parent);
}

void MlpackModel::training()
{
    // Do nothing and it's normal.
}
