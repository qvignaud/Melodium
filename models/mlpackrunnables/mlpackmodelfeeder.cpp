/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "mlpackmodelfeeder.h"
#include "models/mlpackmodel.h"

MlpackModelFeeder::MlpackModelFeeder(Track *track, MlpackModel *model, QObject *parent) :
    Runnable("MlpackModelFeeder", track, parent),
    _model(model)
{

}

TreatmentType MlpackModelFeeder::treatmentType()
{
    TreatmentType tt("MlpackModelFeeder", TreatmentType::ModelFeeder);

    TreatmentType::DataInfo label;
    label.name = "label";
    label.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(label);

    TreatmentType::DataInfo data;
    data.name = "data";
    data.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(data);

    return  tt;
}

bool MlpackModelFeeder::init()
{
    QMutex *mutex = nullptr;
    auto &qualifiers = track()->detailedRealQualifiers(mutex);
    _data = qualifiers.value(inputName("data"));
    _labels = qualifiers.value(inputName("label"));
    mutex->unlock();

    return true;
}

bool MlpackModelFeeder::work()
{
    const size_t data_columns = _data.size();
    const size_t data_rows = _data.at(0).size();
    arma::mat data_matrix(data_rows, data_columns);
    for (size_t x = 0 ; x < data_columns ; ++x) {
        for (size_t y = 0 ; y < data_rows ; ++y) {
            data_matrix(y, x) = _data.at(x).at(y);
        }
    }

    const size_t labels_columns = _labels.size();
    const size_t labels_rows = _labels.at(0).size();
    arma::mat labels_matrix(labels_rows, labels_columns);
    for (size_t x = 0 ; x < labels_columns ; ++x) {
        for (size_t y = 0 ; y < labels_rows ; ++y) {
            labels_matrix(y, x) = _labels.at(x).at(y);
        }
    }

    _model->feed(data_matrix, labels_matrix);

    return true;
}
