/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "mlpackmodelpredictor.h"
#include "models/mlpackmodel.h"

MlpackModelPredictor::MlpackModelPredictor(Track *track, MlpackModel *model, QObject *parent) :
    Runnable("MlpackModelPredictor", track, parent),
    _model(model)
{

}

TreatmentType MlpackModelPredictor::treatmentType()
{
    TreatmentType tt("MlpackModelPredictor", TreatmentType::ModelPredictor);

    TreatmentType::DataInfo data;
    data.name = "data";
    data.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addRequire(data);

    TreatmentType::DataInfo assignments;
    assignments.name = "assignments";
    assignments.type = TreatmentType::DataType::VECTOR_VECTOR_REAL;
    tt.addProvide(assignments);

    return  tt;
}

bool MlpackModelPredictor::init()
{
    QMutex *mutex = nullptr;
    auto &qualifiers = track()->detailedRealQualifiers(mutex);
    _data = qualifiers.value(inputName("data"));
    mutex->unlock();

    return true;
}

bool MlpackModelPredictor::work()
{

    const size_t data_columns = _data.size();
    const size_t data_rows = _data.at(0).size();
    arma::mat data_matrix(data_rows, data_columns);
    for (size_t x = 0 ; x < data_columns ; ++x) {
        for (size_t y = 0 ; y < data_rows ; ++y) {
            data_matrix(y, x) = _data.at(x).at(y);
        }
    }

    arma::mat assignments_matrix;

    _model->predict(data_matrix, assignments_matrix);

    std::vector<std::vector<float>> assignments(assignments_matrix.n_cols,
                                                std::vector<float>(assignments_matrix.n_rows, 0));
    for (size_t c = 0 ; c < assignments_matrix.n_cols ; ++c) {
        for (size_t r = 0 ; r < assignments_matrix.n_rows ; ++r) {
            assignments.at(c).at(r) = assignments_matrix(r, c);
        }
    }

    track()->storeQualifier(outputName("assignments"), assignments);

    return true;
}
