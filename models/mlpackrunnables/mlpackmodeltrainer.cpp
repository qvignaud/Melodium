/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "mlpackmodeltrainer.h"
#include "models/mlpackmodel.h"

MlpackModelTrainer::MlpackModelTrainer(MlpackModel *model, QObject *parent) :
    Runnable("MlpackModelTrainer", parent),
    _model(model)
{
    _trainings = 1;
}

TreatmentType MlpackModelTrainer::treatmentType()
{
    TreatmentType tt("MlpackModelTrainer", TreatmentType::ModelTrainer, TreatmentType::NoTrack);

    TreatmentType::ParamInfo trainings;
    trainings.name = "trainings";
    trainings.description = "Set the maximum number of iterations the training must have.";
    trainings.type = TreatmentType::INT;
    trainings.values = {1, std::numeric_limits<int>::max()};
    trainings.defaultValue = 1;
    tt.addParameter(trainings);

    TreatmentType::ParamInfo trainingOutput;
    trainingOutput.name = "trainingOutput";
    trainingOutput.description = "File where writing training stats output. Leave empty to not have output.";
    trainingOutput.type = TreatmentType::STRING;
    trainingOutput.defaultValue = "";
    tt.addParameter(trainingOutput);

    return  tt;
}

int MlpackModelTrainer::trainings() const
{
    return _trainings;
}

void MlpackModelTrainer::setTrainings(int trainings)
{
    if (status() == Waiting)
        _trainings = trainings;
}

QString MlpackModelTrainer::trainingOutput() const
{
    return _trainingOutput;
}

void MlpackModelTrainer::setTrainingOutput(const QString &trainingOutput)
{
    if (status() == Waiting)
        _trainingOutput = trainingOutput;
}

bool MlpackModelTrainer::init()
{
    if (!_trainingOutput.isEmpty()) {
        _fileOutput.setFileName(_trainingOutput);

        if (!_fileOutput.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Unbuffered))
            return false;
    }

    return _model->isFeedingFinished();
}

bool MlpackModelTrainer::work()
{
    bool produceOutput = !_trainingOutput.isEmpty();

    for (int i = 0 ; i < _trainings ; ++i) {
        auto start = std::chrono::steady_clock::now();

        _model->train();

        auto end = std::chrono::steady_clock::now();

        /*
        size_t p = 100;
        double precisions[p];
        for (size_t j = 0 ; j < p ; ++j)
            precisions[j] = _model->sequencePrecisionScore(j);
        */

        if (produceOutput) {
            std::chrono::duration<double, std::milli> duration = end - start;

            QString outputString;
            outputString += "#" + QString::number(i) + ": " + QString::number(duration.count(), 'f', 6) + "ms";// + "ms, ";// + QString::number(precision * 100, 'f') + "%\n";
            /*for (size_t j = 0 ; j < p ; ++j)
                outputString += QString::number(precisions[j] * 100, 'f') + "%, ";*/
            outputString += '\n';

            _fileOutput.write(outputString.toUtf8());
            _fileOutput.flush();
        }
    }

    _fileOutput.close();

    return true;
}
