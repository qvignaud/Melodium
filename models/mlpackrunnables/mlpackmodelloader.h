/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MLPACKMODELLOADER_H
#define MLPACKMODELLOADER_H

#include "runnable.h"
#include "treatmenttype.h"

class MlpackModel;

/*!
 * \brief Mlpack model default loading runnable.
 */
class MlpackModelLoader : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(QString filename READ filename WRITE setFilename)
public:
    /*!
     * \brief Instanciates a new loader.
     * \param model
     * \param parent
     */
    explicit MlpackModelLoader(MlpackModel *model, QObject *parent = nullptr);

    QString filename() const;
    void setFilename(const QString &filename);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

protected:
    /// Model connected to this loader.
    MlpackModel *const _model;

    QString _filename;

    virtual bool init();
    virtual bool work();

};

#endif // MLPACKMODELLOADER_H
