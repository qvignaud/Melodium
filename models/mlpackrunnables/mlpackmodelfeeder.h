/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MLPACKMODELFEEDER_H
#define MLPACKMODELFEEDER_H

#include "runnable.h"
#include "treatmenttype.h"

class MlpackModel;

/*!
 * \brief Mlpack model default feeding runnable.
 */
class MlpackModelFeeder : public Runnable
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a new feeder.
     * \param track
     * \param model
     * \param parent
     */
    explicit MlpackModelFeeder(Track *track, MlpackModel *model, QObject *parent = nullptr);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

protected:
    /// Model connected to this feeder.
    MlpackModel *const _model;

    virtual bool init();
    virtual bool work();

private:
    std::vector<std::vector<float>> _data;
    std::vector<std::vector<float>> _labels;
};

#endif // MLPACKMODELFEEDER_H
