/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MLPACKMODELTRAINER_H
#define MLPACKMODELTRAINER_H

#include "runnable.h"
#include "treatmenttype.h"

class MlpackModel;

/*!
 * \brief Mlpack model default trainer runnable.
 */
class MlpackModelTrainer : public Runnable
{
    Q_OBJECT
    Q_PROPERTY(int trainings READ trainings WRITE setTrainings)
    Q_PROPERTY(QString trainingOutput READ trainingOutput WRITE setTrainingOutput)
public:
    /*!
     * \brief Instanciates a new trainer.
     * \param model
     * \param parent
     */
    explicit MlpackModelTrainer(MlpackModel *model, QObject *parent = nullptr);

    int trainings() const;
    void setTrainings(int trainings);

    QString trainingOutput() const;
    void setTrainingOutput(const QString &trainingOutput);

    /*!
     * \brief Get treatment type.
     * \return
     */
    static TreatmentType treatmentType();

protected:
    /// Model connected to this trainer.
    MlpackModel *const _model;

    virtual bool init();
    virtual bool work();

private:
    int _trainings;

    QString _trainingOutput;
    QFile _fileOutput;
};

#endif // MLPACKMODELTRAINER_H
