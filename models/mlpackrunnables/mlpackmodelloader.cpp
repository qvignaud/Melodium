/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "mlpackmodelloader.h"
#include "models/mlpackmodel.h"

MlpackModelLoader::MlpackModelLoader(MlpackModel *model, QObject *parent) :
    Runnable("MlpackModelLoader", parent),
    _model(model)
{

}

QString MlpackModelLoader::filename() const
{
    return _filename;
}

void MlpackModelLoader::setFilename(const QString &filename)
{
    _filename = filename;
}

TreatmentType MlpackModelLoader::treatmentType()
{
    TreatmentType tt("MlpackModelLoader", TreatmentType::ModelLoader, TreatmentType::NoTrack);

    TreatmentType::ParamInfo filename;
    filename.name = "filename";
    filename.description = "File name used to load model.";
    filename.type = TreatmentType::STRING;
    tt.addParameter(filename);

    return  tt;
}

bool MlpackModelLoader::init()
{
    return true;
}

bool MlpackModelLoader::work()
{
    _model->load(_filename.toStdString());
    return true;
}
