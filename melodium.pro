QT += core

TEMPLATE = app

TARGET = Melodium

mac {
    QT_CONFIG -= no-pkg-config
    PKG_CONFIG = /usr/local/bin/pkg-config
}

CONFIG += c++11 console link_pkgconfig
CONFIG -= app_bundle

PKGCONFIG += mlpack essentia taglib

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

include(QMidi/src/QMidi.pri)

SOURCES += main.cpp \
    initializers/hpcptrainer.cpp \
    initializers/instrumentfilteringexperiments.cpp \
    initializers/midigenerator.cpp \
    initializers/modelevaluator.cpp \
    initializers/notesestimationexperiment.cpp \
    initializers/scriptinitializer.cpp \
    models/configurablerectffn.cpp \
    models/configurablevarffn.cpp \
    models/ffnrect3hl.cpp \
    models/mlpackrunnables/mlpackmodelfeeder.cpp \
    models/mlpackrunnables/mlpackmodelloader.cpp \
    models/mlpackrunnables/mlpackmodelpredictor.cpp \
    models/mlpackrunnables/mlpackmodelsaver.cpp \
    models/mlpackrunnables/mlpackmodeltrainer.cpp \
    models/sparseautoencoderrunnables/sparseautoencoderfeeder.cpp \
    models/sparseautoencoderrunnables/sparseautoencoderpredictor.cpp \
    models/sparseautoencoderrunnables/sparseautoencodertrainer.cpp \
    modeltype.cpp \
    runnables/analysts/accumulator.cpp \
    runnables/analysts/concatenator.cpp \
    runnables/analysts/differentiator.cpp \
    runnables/analysts/evaluator.cpp \
    runnables/analysts/miditracksfiller.cpp \
    runnables/analysts/scaler.cpp \
    runnables/analysts/thresholder.cpp \
    runnables/audiorenderer/audiorender.cpp \
    runnables/audiorenderer/timidityrender.cpp \
    melodium.cpp \
    runnables/exporters/midiexporter.cpp \
    track.cpp \
    treatment.cpp \
    runnable.cpp \
    treatmentsfactory.cpp \
    runnables/audiorenderer/fluidsynthrender.cpp \
    treatmenttype.cpp \
    runnables/analysts/essentiaanalyst.cpp \
    runnables/analysts/essentiaregistrar.cpp \
    globalregistrar.cpp \
    runnables/analysts/essentiastraightanalyst.cpp \
    runnables/analysts/essentianestedanalyst.cpp \
    runnables/analysts/essentianestedoutanalyst.cpp \
    treatmentinfoprinter.cpp \
    runnables/converters/ffmpegconverter.cpp \
    runnables/exporters/jsonexporter.cpp \
    runnables/exporters/csvexporter.cpp \
    scheduler.cpp \
    schedulers/monothreadscheduler.cpp \
    schedulers/multithreadscheduler.cpp \
    preparator.cpp \
    preparators/audiofilepreparator.cpp \
    preparators/midifilepreparator.cpp \
    initializer.cpp \
    initializers/devinitializer.cpp \
    initializersfactory.cpp \
    trackexception.cpp \
    runnables/analysts/midianalyst.cpp \
    runnables/analysts/miditracksanalyst.cpp \
    runnables/analysts/midivoicesanalyst.cpp \
    preparators/soundfontpreparator.cpp \
    runnables/analysts/soundfontanalyst.cpp \
    model.cpp \
    models/devmodel.cpp \
    runnables/exporters/imageexporter.cpp \
    runnablescheme.cpp \
    models/mlpackmodel.cpp \
    models/ffnmodel.cpp \
    modelsfactory.cpp \
    initializers/visualizerinitializer.cpp \
    initializers/hpcpvisualizer.cpp \
    models/sparseautoencoder.cpp \
    treatmentsfactoryexception.cpp \
    preparatorexception.cpp

HEADERS += \
    initializers/hpcptrainer.h \
    initializers/instrumentfilteringexperiments.h \
    initializers/midigenerator.h \
    initializers/modelevaluator.h \
    initializers/notesestimationexperiment.h \
    initializers/scriptinitializer.h \
    models/configurablerectffn.h \
    models/configurablevarffn.h \
    models/ffnrect3hl.h \
    models/mlpackrunnables/mlpackmodelfeeder.h \
    models/mlpackrunnables/mlpackmodelloader.h \
    models/mlpackrunnables/mlpackmodelpredictor.h \
    models/mlpackrunnables/mlpackmodelsaver.h \
    models/mlpackrunnables/mlpackmodeltrainer.h \
    models/sparseautoencoderrunnables/sparseautoencoderfeeder.h \
    models/sparseautoencoderrunnables/sparseautoencoderpredictor.h \
    models/sparseautoencoderrunnables/sparseautoencodertrainer.h \
    modeltype.h \
    runnables/analysts/accumulator.h \
    runnables/analysts/concatenator.h \
    runnables/analysts/differentiator.h \
    runnables/analysts/evaluator.h \
    runnables/analysts/miditracksfiller.h \
    runnables/analysts/scaler.h \
    runnables/analysts/thresholder.h \
    runnables/audiorenderer/audiorender.h \
    runnables/audiorenderer/timidityrender.h \
    melodium.h \
    runnables/exporters/midiexporter.h \
    track.h \
    treatment.h \
    runnable.h \
    treatmentsfactory.h \
    runnables/audiorenderer/fluidsynthrender.h \
    treatmenttype.h \
    runnables/analysts/essentiaanalyst.h \
    runnables/analysts/essentiaregistrar.h \
    globalregistrar.h \
    runnables/analysts/essentiastraightanalyst.h \
    runnables/analysts/essentianestedanalyst.h \
    runnables/analysts/essentianestedoutanalyst.h \
    treatmentinfoprinter.h \
    runnables/converters/ffmpegconverter.h \
    runnables/exporters/jsonexporter.h \
    runnables/exporters/csvexporter.h \
    scheduler.h \
    schedulers/monothreadscheduler.h \
    schedulers/multithreadscheduler.h \
    preparator.h \
    preparators/audiofilepreparator.h \
    preparators/midifilepreparator.h \
    initializer.h \
    initializers/devinitializer.h \
    initializersfactory.h \
    trackexception.h \
    runnables/analysts/midianalyst.h \
    runnables/analysts/miditracksanalyst.h \
    runnables/analysts/midivoicesanalyst.h \
    preparators/soundfontpreparator.h \
    runnables/analysts/soundfontanalyst.h \
    model.h \
    models/devmodel.h \
    runnables/exporters/imageexporter.h \
    runnablescheme.h \
    models/mlpackmodel.h \
    models/ffnmodel.h \
    modelsfactory.h \
    initializers/visualizerinitializer.h \
    initializers/hpcpvisualizer.h \
    models/sparseautoencoder.h \
    treatmentsfactoryexception.h \
    preparatorexception.h

