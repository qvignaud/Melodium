/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "treatmentsfactory.h"
#include <QDebug>

QList<TreatmentsFactory::AvailableTreatment> TreatmentsFactory::_availableTreatments = QList<TreatmentsFactory::AvailableTreatment>();

TreatmentsFactory::TreatmentsFactory(QObject *parent) : QObject(parent)
{

}

TreatmentsFactory::~TreatmentsFactory()
{

}

Treatment *TreatmentsFactory::createTreatment(const QString &name, QObject *parent)
{
    AvailableTreatment at = findAvailableTreatment(name);

    if (at.trackOperation() != TreatmentType::NoTrack or at.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::WrongInitializationCall,
                                         "Wrong creation trial for '" + at.name() + "': don't work with tracks and not from model.");

    Runnable *runnable = at.initializer.noTrack(at, parent);

    return createTreatment(at, runnable);
}

Treatment *TreatmentsFactory::createTreatment(const QString &name, Track *track, QObject *parent)
{
    AvailableTreatment at = findAvailableTreatment(name);

    if (at.trackOperation() != TreatmentType::MonoTrack or at.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::WrongInitializationCall,
                                         "Wrong creation trial for '" + at.name() + "': is mono-track and not from model.");

    Runnable *runnable = at.initializer.monoTrack(at, track, parent);

    return createTreatment(at, runnable);
}

Treatment *TreatmentsFactory::createTreatment(const QString &name, const QVector<Track *> &tracks, QObject *parent)
{
    AvailableTreatment at = findAvailableTreatment(name);

    if (at.trackOperation() != TreatmentType::MultiTrack or at.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::WrongInitializationCall,
                                         "Wrong creation trial for '" + at.name() + "': is multi-track and not from model.");

    Runnable *runnable = at.initializer.multiTrack(at, tracks, parent);

    return createTreatment(at, runnable);
}

Treatment *TreatmentsFactory::createTreatment(const QString &name, Model *model, QObject *parent)
{
    AvailableTreatment at = findAvailableTreatment(name);

    if (at.trackOperation() != TreatmentType::NoTrack or !at.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::WrongInitializationCall,
                                         "Wrong creation trial for '" + at.name() + "': don't work with tracks and need model.");

    Runnable *runnable = (model->*at.initializer.noTrackModel)(at, parent);

    return createTreatment(at, runnable);
}

Treatment *TreatmentsFactory::createTreatment(const QString &name, Model *model, Track *track, QObject *parent)
{
    AvailableTreatment at = findAvailableTreatment(name);

    if (at.trackOperation() != TreatmentType::MonoTrack or !at.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::WrongInitializationCall,
                                         "Wrong creation trial for '" + at.name() + "': is mono-track and need model.");

    Runnable *runnable = (model->*at.initializer.monoTrackModel)(at, track, parent);

    return createTreatment(at, runnable);
}

Treatment *TreatmentsFactory::createTreatment(const QString &name, Model *model, const QVector<Track *> &tracks, QObject *parent)
{
    AvailableTreatment at = findAvailableTreatment(name);

    if (at.trackOperation() != TreatmentType::MultiTrack or !at.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::WrongInitializationCall,
                                         "Wrong creation trial for '" + at.name() + "': is multi-track and need model.");

    Runnable *runnable = (model->*at.initializer.multiTrackModel)(at, tracks, parent);

    return createTreatment(at, runnable);
}

QList<Treatment *> TreatmentsFactory::treatments() const
{
    return _treatments;
}

QList<Treatment *> TreatmentsFactory::treatments(TreatmentType::Role role) const
{
    QList<Treatment *> list;

    for (int i=0 ; i < _treatments.size() ; i++) {
        if (_treatments.at(i)->type().role() == role) list << _treatments.at(i);
    }

    return list;
}

QList<TreatmentType> TreatmentsFactory::availableTreatments()
{
    QList<TreatmentType> list;

    for (int i=0 ; i < TreatmentsFactory::_availableTreatments.size() ; i++)
        list.append(TreatmentsFactory::_availableTreatments.at(i));

    return list;
}

QList<TreatmentType> TreatmentsFactory::availableTreatments(TreatmentType::Role role)
{
    QList<TreatmentType> list;

    for (int i=0 ; i < TreatmentsFactory::_availableTreatments.size() ; i++)
        if (TreatmentsFactory::_availableTreatments.at(i).role() == role)
            list.append(TreatmentsFactory::_availableTreatments.at(i));

    return list;
}

void TreatmentsFactory::addAvailableTreatment(TreatmentType type, Runnable *(*initializer)(TreatmentType, QObject *))
{
    if (type.trackOperation() != TreatmentType::NoTrack or type.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::InvalidInitializer,
                                         "Initializer for '" + type.name() + "' is invalid.");

    if (!registeredAvailableTreatment(type.name())) {
        AvailableTreatment at(type);
        at.initializer.noTrack = initializer;
        TreatmentsFactory::_availableTreatments << at;
    }
}

void TreatmentsFactory::addAvailableTreatment(TreatmentType type, Runnable *(*initializer)(TreatmentType, Track *, QObject *))
{
    if (type.trackOperation() != TreatmentType::MonoTrack or type.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::InvalidInitializer,
                                         "Initializer for '" + type.name() + "' is invalid.");

    if (!registeredAvailableTreatment(type.name())) {
        AvailableTreatment at(type);
        at.initializer.monoTrack = initializer;
        TreatmentsFactory::_availableTreatments << at;
    }
}

void TreatmentsFactory::addAvailableTreatment(TreatmentType type, Runnable *(*initializer)(TreatmentType, const QVector<Track *> &, QObject *))
{
    if (type.trackOperation() != TreatmentType::MultiTrack or type.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::InvalidInitializer,
                                         "Initializer for '" + type.name() + "' is invalid.");

    if (!registeredAvailableTreatment(type.name())) {
        AvailableTreatment at(type);
        at.initializer.multiTrack = initializer;
        TreatmentsFactory::_availableTreatments << at;
    }
}

void TreatmentsFactory::addAvailableTreatment(TreatmentType type, Runnable *(Model::*initializer)(TreatmentType, QObject *))
{
    if (type.trackOperation() != TreatmentType::NoTrack or !type.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::InvalidInitializer,
                                         "Initializer for '" + type.name() + "' is invalid.");

    if (!registeredAvailableTreatment(type.name())) {
        AvailableTreatment at(type);
        at.initializer.noTrackModel = initializer;
        TreatmentsFactory::_availableTreatments << at;
    }
}

void TreatmentsFactory::addAvailableTreatment(TreatmentType type, Runnable *(Model::*initializer)(TreatmentType, Track *, QObject *))
{
    if (type.trackOperation() != TreatmentType::MonoTrack or !type.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::InvalidInitializer,
                                         "Initializer for '" + type.name() + "' is invalid.");

    if (!registeredAvailableTreatment(type.name())) {
        AvailableTreatment at(type);
        at.initializer.monoTrackModel = initializer;
        TreatmentsFactory::_availableTreatments << at;
    }
}

void TreatmentsFactory::addAvailableTreatment(TreatmentType type, Runnable *(Model::*initializer)(TreatmentType, const QVector<Track *> &, QObject *))
{
    if (type.trackOperation() != TreatmentType::MultiTrack or !type.isFromModel())
        throw TreatmentsFactoryException(TreatmentsFactoryException::InvalidInitializer,
                                         "Initializer for '" + type.name() + "' is invalid.");

    if (!registeredAvailableTreatment(type.name())) {
        AvailableTreatment at(type);
        at.initializer.multiTrackModel = initializer;
        TreatmentsFactory::_availableTreatments << at;
    }
}

TreatmentsFactory &TreatmentsFactory::globalInstance()
{
    static TreatmentsFactory instance;
    return instance;
}

Treatment *TreatmentsFactory::createTreatment(TreatmentType tt, Runnable *runnable)
{
    Treatment *treatment = new Treatment(tt, runnable, runnable->parent());

    connect(treatment, &QObject::destroyed, this, &TreatmentsFactory::treatmentDestroyed);
    _treatments << treatment;

    return treatment;
}

bool TreatmentsFactory::registeredAvailableTreatment(const QString &name)
{
    for (int i=0 ; i < TreatmentsFactory::_availableTreatments.size() ; i++) {
        auto &at = TreatmentsFactory::_availableTreatments.at(i);
        if (name.compare(at.name(), Qt::CaseInsensitive) == 0) {
            return true;
        }
    }
    // else
    return false;
}

TreatmentsFactory::AvailableTreatment TreatmentsFactory::findAvailableTreatment(const QString &name)
{
    for (int i=0 ; i < TreatmentsFactory::_availableTreatments.size() ; i++) {
        auto &at = TreatmentsFactory::_availableTreatments.at(i);
        if (name.compare(at.name(), Qt::CaseInsensitive) == 0) {
            return at;
        }
    }
    // else
    throw TreatmentsFactoryException(TreatmentsFactoryException::UnkownTreatment,
                                     "'" + name + "' is not a known treatment.");
}

void TreatmentsFactory::treatmentDestroyed(QObject *treatment)
{
    _treatments.removeAll(static_cast<Treatment *>(treatment));
}

TreatmentsFactory::AvailableTreatment::AvailableTreatment(TreatmentType &type) : TreatmentType (type)
{
    initializer.noTrackModel = nullptr;
}
