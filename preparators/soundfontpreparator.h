/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef SOUNDFONTPREPARATOR_H
#define SOUNDFONTPREPARATOR_H

#include "preparator.h"

/*!
 * \brief Prepare audio samples as tracks from sound font.
 *
 * This class is aimed to instanciate tracks using audio samples of a SFZ sound font.
 */
class SoundFontPreparator : public Preparator
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates a sound font preparator.
     * \param parent
     */
    explicit SoundFontPreparator(QObject *parent = nullptr);

public slots:
    /*!
     * \brief Prepare tracks.
     */
    void prepare();

private:
    /*!
     * \brief Treat a SFZ file.
     * \param fileInfo
     */
    void treatFile(const QFileInfo &fileInfo);
    /*!
     * \brief Treat an entry/sample of SFZ file.
     * \param file
     * \param properties
     */
    void treatEntry(const QString &file, const QMap<QString, QString> &properties);

    /*!
     * \brief Parse SFZ string in regions properties.
     * \param sfzData
     * \return
     */
    static QList<QMap<QString, QString>> parseRegions(QString sfzData);

    /*!
     * \brief Tells if the line is a group-start tag.
     * \param line
     * \return
     */
    static bool startGroup(const QString &line);
    /*!
     * \brief Tells if the line is a region-start tag.
     * \param line
     * \return
     */
    static bool startRegion(const QString &line);
    /*!
     * \brief Parse the SFZ properties contained in the line.
     * \param line  Line to parse.
     * \param properties    Map where properties will be added.
     */
    static void parseProperties(const QString &line, QMap<QString, QString> &properties);
};

#endif // SOUNDFONTPREPARATOR_H
