/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "audiofilepreparator.h"

AudioFilePreparator::AudioFilePreparator(QObject *parent) : Preparator(parent)
{
    _associateMidi = true;
}

bool AudioFilePreparator::associateMidi() const
{
    return _associateMidi;
}

void AudioFilePreparator::setAssociateMidi(bool associateMidi)
{
    _associateMidi = associateMidi;
}

bool AudioFilePreparator::isFileConvertible(const QFileInfo &file)
{
    QMimeType mime = QMimeDatabase().mimeTypeForFile(file);
    if (    mime.name().startsWith("audio/") or
            mime.name().startsWith("video/"))
        return true;
    else
        return false;
}

void AudioFilePreparator::prepare()
{
    // 'Absolution' of all entries.
    for (auto fileInfo: _filesList)
        fileInfo.makeAbsolute();

    recurseDirectories();

    for (auto file: _filesList)
        treatEntry(file);
}

void AudioFilePreparator::treatEntry(const QFileInfo &file)
{
    // Check if file can be treated.
    if (    !file.exists() or
            !file.isReadable() or
            !file.isFile())
        return;

    Track *track = newTrack();
    Treatment *conversionTreatment = nullptr;
    Treatment *loadingTreatment = nullptr;

    // If file is not already good for a track…
    if (!track->setAudioFile(file.absoluteFilePath())) {
        // …we check if it can be converted…
        if (isFileConvertible(file)) {
            // …if so we prepare a conversion.
            QString flacFilename = file.absolutePath() + '/' + file.completeBaseName() + ".flac";
            conversionTreatment = TreatmentsFactory::globalInstance().createTreatment("FfmpegConverter", track);
            conversionTreatment->setParameter("inputFilename", file.absoluteFilePath());
            conversionTreatment->setParameter("outputFilename", flacFilename);
        }
        else {
            // …else this file is not usable and it is ignored.
            delete track;
            return;
        }
    }

    if (_associateMidi) {
        // A MIDI file is associated with.
        QString midiFile = matchingMidiFile(file);
        if (!midiFile.isEmpty())
            track->setOriginalMidiFile(midiFile);
    }

    // Loading the audio file.
    loadingTreatment = TreatmentsFactory::globalInstance().createTreatment("MonoLoader", track);
    loadingTreatment->setParameter("sampleRate", tracksSampleRate());
    if (conversionTreatment != nullptr) {
        // If it comes from the converter.
        loadingTreatment->setParameter("filename", conversionTreatment->parameter("outputFilename"));
        conversionTreatment->addNext(loadingTreatment);
    }
    else
        loadingTreatment->setParameter("filename", file.absoluteFilePath());

   Melodium::instance()->addTrack(track);
    if (conversionTreatment != nullptr)
        Melodium::instance()->addTreatment(conversionTreatment);
    Melodium::instance()->addTreatment(loadingTreatment);

    PreparingTrack preparingTrack;
    preparingTrack.track = track;
    preparingTrack.treatments.insert("MonoLoader", loadingTreatment);

    _preparedTracks.append(PreparedTrack(preparingTrack));
}

QString AudioFilePreparator::matchingMidiFile(const QFileInfo &file)
{
    QDir dir(file.absoluteDir());

    QStringList filters;
    filters
            << file.completeBaseName() + ".midi"
            << file.completeBaseName() + ".mid"
               ;

    QStringList midiFiles = dir.entryList(filters, QDir::Files | QDir::Readable);

    if (!midiFiles.isEmpty())
        return midiFiles.at(0);
    else
        return QString();
}
