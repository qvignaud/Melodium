/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MIDIFILEPREPARATOR_H
#define MIDIFILEPREPARATOR_H

#include "preparator.h"

/*!
 * \brief Prepares original MIDI files for treatment.
 *
 * This class is aimed to prepare original MIDI files to be rendered and used in Mélodium.
 *
 * This do a render of original MIDI files in FLAC using Fluidsynth, if none are existing or override is ordered.
 * Then audio signal is loaded in Mélodium.
 */
class MidiFilePreparator : public Preparator
{
    Q_OBJECT
public:
    enum Analysis {
        Tracks,
        Voices
    };

    /*!
     * \brief Instanciates MidiFilePreparator.
     * \param parent
     */
    explicit MidiFilePreparator(QObject *parent = nullptr);

    /*!
     * \brief Tells if audio files have to be overrided by new render.
     * \return
     *
     * By default it is false.
     */
    bool overrideAudio() const;
    /*!
     * \brief Set if render audio files have to be overrided.
     * \param overrideAudio
     */
    void setOverrideAudio(bool overrideAudio);

    Analysis analysis() const;
    void setAnalysis(const Analysis &analysis);
    QString analyst() const;

public slots:
    /*!
     * \brief Prepare tracks.
     */
    void prepare();

private:
    /*!
     * \brief Audio files rendered have to be overrided.
     */
    bool _overrideAudio;

    /*!
     * \brief Analysis to use for MIDI.
     */
    Analysis _analysis;

    /*!
     * \brief Treat one file.
     * \param file
     *
     * If fileinfo is not usable (directory, not readable, not MIDI, etc), this does nothing.
     */
    void treatEntry(const QFileInfo &file);
};

#endif // MIDIFILEPREPARATOR_H
