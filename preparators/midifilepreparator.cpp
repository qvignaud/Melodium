/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "midifilepreparator.h"

MidiFilePreparator::MidiFilePreparator(QObject *parent) : Preparator(parent)
{
    _overrideAudio = false;
    _analysis = Voices;
}

void MidiFilePreparator::prepare()
{
    // 'Absolution' of all entries.
    for (auto fileInfo: _filesList)
        fileInfo.makeAbsolute();

    recurseDirectories();

    for (auto file: _filesList)
        treatEntry(file);
}

MidiFilePreparator::Analysis MidiFilePreparator::analysis() const
{
    return _analysis;
}

void MidiFilePreparator::setAnalysis(const Analysis &analysis)
{
    _analysis = analysis;
}

QString MidiFilePreparator::analyst() const
{
    switch(_analysis){
    case MidiFilePreparator::Tracks:
        return "MidiTracksAnalyst";
    case MidiFilePreparator::Voices:
        return "MidiVoicesAnalyst";
    default:
        return "";
    }
}

bool MidiFilePreparator::overrideAudio() const
{
    return _overrideAudio;
}

void MidiFilePreparator::setOverrideAudio(bool overrideAudio)
{
    _overrideAudio = overrideAudio;
}

void MidiFilePreparator::treatEntry(const QFileInfo &file)
{
    // Check if file can be treated.
    if (    !file.exists() or
            !file.isReadable() or
            !file.isFile())
        return;

    Track *track = newTrack();
    Treatment *renderingTreatment = nullptr;
    Treatment *loadingTreatment = nullptr;
    Treatment *midiAnalysisTreatment = nullptr;

    // If file is not MIDI.
    if (!track->setOriginalMidiFile(file.absoluteFilePath())) {
        delete track;
        return;
    }

    QString flacFilename = file.absolutePath() + '/' + file.completeBaseName() + ".flac";
    QString waveFilename = file.absolutePath() + '/' + file.completeBaseName() + ".wave";
    QString wavFilename  = file.absolutePath() + '/' + file.completeBaseName() + ".wav";
    QString audioFilename;
    bool audioExists = false;
    if (QFileInfo::exists(flacFilename)) {
        audioFilename = flacFilename;
        audioExists = true;
    }
    else if (QFileInfo::exists(waveFilename)) {
        audioFilename = waveFilename;
        audioExists = true;
    }
    else if (QFileInfo::exists(wavFilename)) {
        audioFilename = wavFilename;
        audioExists = true;
    }

    if (    _overrideAudio or
            !audioExists) {
        // Rendering is requested.
        audioFilename = flacFilename;

        renderingTreatment = TreatmentsFactory::globalInstance().createTreatment("FluidsynthRender", track);
        renderingTreatment->setParameter("flacFilename", audioFilename);
    }
    else {
        track->setAudioFile(audioFilename);
    }

    // Loading the audio file.
    loadingTreatment = TreatmentsFactory::globalInstance().createTreatment("MonoLoader", track);
    loadingTreatment->setParameter("filename", audioFilename);
    loadingTreatment->setParameter("sampleRate", tracksSampleRate());
    if (renderingTreatment != nullptr)
        renderingTreatment->addNext(loadingTreatment);

    midiAnalysisTreatment = TreatmentsFactory::globalInstance().createTreatment(analyst(), track);
    loadingTreatment->addNext(midiAnalysisTreatment);

    Melodium::instance()->addTrack(track);
    if (renderingTreatment != nullptr)
        Melodium::instance()->addTreatment(renderingTreatment);
    Melodium::instance()->addTreatment(loadingTreatment);
    Melodium::instance()->addTreatment(midiAnalysisTreatment);

    PreparingTrack preparingTrack;
    preparingTrack.track = track;
    preparingTrack.treatments.insert(analyst(), midiAnalysisTreatment);
    preparingTrack.treatments.insert("MonoLoader", loadingTreatment);

    _preparedTracks.append(PreparedTrack(preparingTrack));
}
