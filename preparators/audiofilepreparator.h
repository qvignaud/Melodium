/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef AUDIOFILEPREPARATOR_H
#define AUDIOFILEPREPARATOR_H

#include "preparator.h"
#include <QMimeDatabase>

/*!
 * \brief Prepares audio files for analysis.
 *
 * This class is aimed to prepare audio files, or any assimilable file type (lossy audio, video file), to be used.
 * It prepares the conversion if needed, and the loading of audio signal in Mélodium.
 *
 */
class AudioFilePreparator : public Preparator
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates AudioFilePreparator.
     * \param parent
     */
    explicit AudioFilePreparator(QObject *parent = nullptr);

    /*!
     * \brief Tells if MIDI file will be associated, if any.
     * \return
     *
     * By default it is true.
     */
    bool associateMidi() const;
    /*!
     * \brief Set if MIDI file should be associated.
     * \param associateMidi
     *
     * If true (default), a matching MIDI file will be associated as original MIDI file.
     * A matching MIDI file is a file located in the same directory, with the same base name than the audio file, ending with '.midi' or '.mid' instead of format extension.
     */
    void setAssociateMidi(bool associateMidi);

    /*!
     * \brief Tells if a file is convertible.
     * \param file
     * \return
     *
     * Checks if the file have MIME type 'audio/' or 'video/'.
     */
    static bool isFileConvertible(const QFileInfo &file);

public slots:
    /*!
     * \brief Prepare tracks.
     */
    void prepare();

private:
    /*!
     * \brief MIDI files should be associated.
     * \default true
     */
    bool _associateMidi;

    /*!
     * \brief Treat one file.
     * \param file
     *
     * If fileinfo is not usable (directory, not readable, not convertible, etc), this does nothing.
     */
    void treatEntry(const QFileInfo &file);

    /*!
     * \brief Return file path of MIDI file matching.
     * \param file
     * \return A file path, or empty string if nothing is found.
     */
    QString matchingMidiFile(const QFileInfo &file);
};

#endif // AUDIOFILEPREPARATOR_H
