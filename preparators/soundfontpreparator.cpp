/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "soundfontpreparator.h"
#include <QRegularExpression>

SoundFontPreparator::SoundFontPreparator(QObject *parent) : Preparator(parent)
{

}

void SoundFontPreparator::prepare()
{
    // 'Absolution' of all entries.
    for (auto fileInfo: _filesList)
        fileInfo.makeAbsolute();

    recurseDirectories();

    for (auto file: _filesList)
        treatFile(file);
}

void SoundFontPreparator::treatFile(const QFileInfo &fileInfo)
{
    // Check if file can be treated.
    if (    !fileInfo.exists() or
            !fileInfo.isReadable() or
            !fileInfo.isFile() or
            fileInfo.suffix().toLower() != "sfz")
       return;

    QFile file(fileInfo.absoluteFilePath());
    if (file.open(QIODevice::ReadOnly)) {

        QList<QMap<QString, QString>> regions = parseRegions(file.readAll());

        file.close();

        // Removal of all duplicates regions using the same sample.
        QStringList existingSamples;
        for (int i=0 ; i < regions.size() ; ++i) {
            if (existingSamples.contains(regions.at(i).value("sample"))) {
                regions.removeAt(i);
                --i;
                continue;
            }
            else
                existingSamples.append(regions.at(i).value("sample"));
        }

        QString voiceNumber = QRegularExpression("[0-9]{3}")
                .match(fileInfo.fileName())
                .captured();

        for (auto region: regions) {
            region.insert("voice", voiceNumber);

            QString sampleFile = fileInfo.absolutePath();
            sampleFile += '/' + region.value("sample");
            sampleFile.replace('\\', '/');

            treatEntry(sampleFile, region);
        }
    }

}

void SoundFontPreparator::treatEntry(const QString &file, const QMap<QString, QString> &properties)
{
    QFileInfo fileInfo(file);

    if (    !fileInfo.exists() or
            !fileInfo.isReadable() or
            !fileInfo.isFile())
        return;

    Track *track = newTrack();
    Treatment *loadingTreatment = nullptr;
    Treatment *sfAnalysisTreatment = nullptr;

    try {
        if (!track->setAudioFile(fileInfo.absoluteFilePath())) {
            delete track;
            return;
        }
    }
    catch (...) {
        delete track;
        return;
    }

    // Loading the audio file.
    loadingTreatment = TreatmentsFactory::globalInstance().createTreatment("MonoLoader", track);
    loadingTreatment->setParameter("filename", track->audioFile());
    loadingTreatment->setParameter("sampleRate", tracksSampleRate());

    sfAnalysisTreatment = TreatmentsFactory::globalInstance().createTreatment("SoundFontAnalyst", track);
    for (auto prop = properties.begin() ; prop != properties.end() ; ++prop) {
        sfAnalysisTreatment->setParameter(prop.key(), prop.value());
    }
    loadingTreatment->addNext(sfAnalysisTreatment);

    Melodium::instance()->addTrack(track);
    Melodium::instance()->addTreatment(loadingTreatment);
    Melodium::instance()->addTreatment(sfAnalysisTreatment);

    PreparingTrack preparingTrack;
    preparingTrack.track = track;
    preparingTrack.treatments.insert("SoundFontAnalyst", sfAnalysisTreatment);

    _preparedTracks.append(PreparedTrack(preparingTrack));
}

QList<QMap<QString, QString> > SoundFontPreparator::parseRegions(QString sfzData)
{
    QList<QMap<QString, QString>> regions;

    // Removing comments.
    sfzData.remove(QRegularExpression("//+.*$"));

    QStringList sfzLines = sfzData.split('\n');
    QMap<QString, QString> groupProperties;
    QMap<QString, QString> regionProperties;

    auto commitRegion = [&regions,&regionProperties]{
        regions.append(regionProperties);
    };

    bool inGroup = false;
    bool inRegion = false;
    for (QString line: sfzLines) {
        if (startGroup(line)) {
            inGroup = true;

            if (inRegion)
                commitRegion();
            inRegion = false;

            groupProperties.clear();
        }
        else if (startRegion(line)) {
            if (inRegion)
                commitRegion();
            inRegion = true;

            regionProperties.clear();
            regionProperties.unite(groupProperties);
        }
        else if (inGroup && !inRegion) {
            parseProperties(line, groupProperties);
        }
        else if (inRegion) {
            parseProperties(line, regionProperties);
        }
    }

    if (!regionProperties.empty())
        commitRegion();

    return regions;
}

bool SoundFontPreparator::startGroup(const QString &line)
{
    return (line.trimmed().compare("<group>") == 0);
}

bool SoundFontPreparator::startRegion(const QString &line)
{
    return (line.trimmed().compare("<region>") == 0);
}

void SoundFontPreparator::parseProperties(const QString &line, QMap<QString, QString> &properties)
{
    QRegularExpression regex("(((\\w+)=([^=]+)$)|((\\w+)=([^= ]+)))");
    auto iterator = regex.globalMatch(line);

    while (iterator.hasNext()) {
        auto match = iterator.next();
        properties.insert(match.captured(match.lastCapturedIndex()-1), match.captured(match.lastCapturedIndex()));
    }
}
