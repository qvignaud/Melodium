/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "runnable.h"

Runnable::Runnable(QString name, QObject *parent) :
    Runnable(name, QVector<Track*>(), parent)
{

}

Runnable::Runnable(QString name, Track *track, QObject *parent) :
    Runnable(name, QVector<Track*>(1, track), parent)
{

}

Runnable::Runnable(QString name, const QVector<Track *> &tracks, QObject *parent) :
    QObject(parent),
    _name(name),
    _status(Runnable::Waiting),
    _tracks(tracks),
    _initError(false),
    _workError(false)
{
    setAutoDelete(false);
}

QString Runnable::name() const
{
    return _name;
}

Runnable::Status Runnable::status() const
{
    return _status;
}

Track *Runnable::track() const
{
    return _tracks.first();
}

QVector<Track *> Runnable::tracks() const
{
    return _tracks;
}

QMap<QString, QString> Runnable::inputsMap() const
{
    return _inputsMap;
}

QString Runnable::inputName(const QString &ownName) const
{
    return _inputsMap.value(ownName, ownName);
}

void Runnable::addInputsMap(const QString &ownName, const QString &externalName)
{
    if (_status != Runnable::Waiting) return;
    _inputsMap.insert(ownName, externalName);
}

void Runnable::addInputsMap(const QMap<QString, QString> &inputsMap)
{
    if (_status != Runnable::Waiting) return;
    for (auto it = inputsMap.begin() ;
         it != inputsMap.end() ;
         ++it)
        _inputsMap.insert(it.key(), it.value());
}

void Runnable::setInputsMap(const QMap<QString, QString> &inputsMap)
{
    if (_status != Runnable::Waiting) return;
    _inputsMap = inputsMap;
}

QMap<QString, QString> Runnable::outputsMap() const
{
    return _outputsMap;
}

QString Runnable::outputName(const QString &ownName) const
{
    return _outputsMap.value(ownName, ownName);
}

void Runnable::addOutputsMap(const QString &ownName, const QString &externalName)
{
    if (_status != Runnable::Waiting) return;
    _outputsMap.insert(ownName, externalName);
}

void Runnable::addOutputsMap(const QMap<QString, QString> &outputsMap)
{
    if (_status != Runnable::Waiting) return;
    for (auto it = outputsMap.begin() ;
         it != outputsMap.end() ;
         ++it)
        _outputsMap.insert(it.key(), it.value());
}

void Runnable::setOutputsMap(const QMap<QString, QString> &outputsMap)
{
    if (_status != Runnable::Waiting) return;
    _outputsMap = outputsMap;
}

void Runnable::run()
{
    if (_status != Runnable::Waiting) return;

    setStatus(Runnable::Running);

    try {
        _initError = !init();
    } catch (const std::exception &ex) {
        _initError = true;
        setInitErrorMessage(ex.what());
    } catch (...) {
        _initError = true;
        setInitErrorMessage("An exception has been raised during initialisation.");
    }
    if (_initError) {
        setStatus(Runnable::Failed);
        return;
    }

    try {
        _workError = !work();
    } catch (const TrackException &te) {
        _workError = true;
        switch (te.type()) {
        case TrackException::ZeroFrames:
            setWorkErrorMessage("File '" + te.causeName() + "' is too short (" + QString::number(static_cast<quint64>(te.invalidSize())) + " samples) and doesn't allow to create at least one frame.");
            break;
        case TrackException::InvalidQualifierLength:
            setWorkErrorMessage("Qualifier '" + te.causeName() + "' have an invalid length, " +
                                QString::number(track()->samples()) + " (samples) or " +
                                QString::number(track()->frames()) + " (frames) expected, but " +
                                QString::number(static_cast<quint64>(te.invalidSize())) + " provided.");
            break;
        case TrackException::QualifierDepthNonConstant:
            setWorkErrorMessage("Qualifier '" + te.causeName() + "' have a non-constant depth.");
            break;
        default:
            break;
        }
    } catch (const std::exception &ex) {
        _workError = true;
        setWorkErrorMessage(ex.what());
    } catch (...) {
        _workError = true;
        setWorkErrorMessage("An exception has been raised during work.");
    }
    if (_workError) {
        setStatus(Runnable::Failed);
    }
    else {
        setStatus(Runnable::Finished);
    }
}

bool Runnable::initError() const
{
    return _initError;
}

QString Runnable::initErrorMessage() const
{
    return _initErrorMessage;
}

bool Runnable::workError() const
{
    return _workError;
}

QString Runnable::workErrorMessage() const
{
    return _workErrorMessage;
}

void Runnable::setInitErrorMessage(const QString &initErrorMessage)
{
    _initErrorMessage = initErrorMessage;
}

void Runnable::setWorkErrorMessage(const QString &workErrorMessage)
{
    _workErrorMessage = workErrorMessage;
}

void Runnable::setStatus(const Status status)
{
    _status = status;

    emit statusChanged(_status);

    switch (_status) {
    case Runnable::Running:
        emit started();
        break;
    case Runnable::Finished:
        emit finished();
        break;
    case Runnable::Failed:
        emit failed();
        break;
    default:
        break;
    }
}

