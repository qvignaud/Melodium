/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "multithreadscheduler.h"

MultiThreadScheduler::MultiThreadScheduler(QObject *parent) : Scheduler(parent)
{
    _pool = QThreadPool::globalInstance();

    _treatmentsStarted = 0;
    _treatmentsFinished = 0;
    _treatmentsFailed = 0;

    connect(Melodium::instance(), &Melodium::treatmentAdded, this, &MultiThreadScheduler::treatmentAdded);
    connect(Melodium::instance(), &Melodium::treatmentReady, this, &MultiThreadScheduler::treatmentReady);
    connect(Melodium::instance(), &Melodium::treatmentFinished, this, &MultiThreadScheduler::treatmentFinished);
    connect(Melodium::instance(), &Melodium::treatmentFailed, this, &MultiThreadScheduler::treatmentFailed);

}

void MultiThreadScheduler::work()
{
    auto treatments = Melodium::instance()->treatments();
    for (Treatment *t: treatments) {
        if (t->isReady()) {
            _treatmentsStarted++;
            _pool->start(t->runnable());
        }
    }
}

void MultiThreadScheduler::treatmentAdded(Treatment *treatment)
{
    if (isLaunched()) {
        if (treatment->isReady()) {
            _treatmentsStarted++;
            _pool->start(treatment->runnable());
        }
    }
}

void MultiThreadScheduler::treatmentReady(Treatment *treatment)
{
    if (isLaunched()) {
        _treatmentsStarted++;
        _pool->start(treatment->runnable(), 10);
    }
}

void MultiThreadScheduler::treatmentFinished(Treatment *treatment)
{
    Q_UNUSED(treatment)

    _treatmentsFinished++;
    checkFinished();
}

void MultiThreadScheduler::treatmentFailed(Treatment *treatment)
{
    Q_UNUSED(treatment)

    _treatmentsFailed++;
    checkFinished();
}

void MultiThreadScheduler::checkFinished()
{
    if (_treatmentsStarted == _treatmentsFinished + _treatmentsFailed)
        emit finished();
}
