/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MONOTHREADSCHEDULER_H
#define MONOTHREADSCHEDULER_H

#include "scheduler.h"

/*!
 * \brief Schedule treatments execution for monothreaded process.
 *
 * This class let Qt event loop working when possible.
 */
class MonoThreadScheduler : public Scheduler
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates MonoThreadScheduler.
     * \param parent
     */
    explicit MonoThreadScheduler(QObject *parent = nullptr);

protected:
    /*!
     * \brief Execute treatments.
     */
    void work();

private slots:
    /*!
     * \brief Manage the finishing of a treatment.
     * \param treatment
     *
     * Launch execution of next treatment or look for another ready treatment in Melodium.
     */
    void treatmentFinished(Treatment *treatment);

private:
    /*!
     * \brief Launch the first ready treatment found in Melodium.
     *
     * Emit finished() when none is found.
     */
    void runFirstReadyTreatment();

    /*!
     * \brief Run a treatment.
     * \param treatment
     */
    void runTreatment(Treatment *treatment);
};

#endif // MONOTHREADSCHEDULER_H
