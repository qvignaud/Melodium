/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef MULTITHREADSCHEDULER_H
#define MULTITHREADSCHEDULER_H

#include "scheduler.h"

/*!
 * \brief Schedule treatments in multithreaded application.
 *
 * This class uses QThreadPool::globalInstance as thread pool for doing work.
 */
class MultiThreadScheduler : public Scheduler
{
    Q_OBJECT
public:
    /*!
     * \brief Instanciates MultiThreadScheduler.
     * \param parent
     */
    explicit MultiThreadScheduler(QObject *parent = nullptr);

signals:

public slots:

protected:
    /*!
     * \brief Execute treatments.
     */
    void work();

private:
    /*!
     * \brief Thread pool used to work.
     *
     * Currently this one always refers to QThreadPool::globalInstance.
     */
    QThreadPool *_pool;

    /*!
     * \brief Store number of treatments started by the scheduler.
     */
    unsigned long long int _treatmentsStarted;
    /*!
     * \brief Store number of treatments finished.
     */
    unsigned long long int _treatmentsFinished;
    /*!
     * \brief Store number of treatments failed.
     */
    unsigned long long int _treatmentsFailed;

private slots:
    /*!
     * \brief Add treatment to schedule if it is ready and scheduler is launched.
     * \param treatment
     */
    void treatmentAdded(Treatment *treatment);

    /*!
     * \brief Starts the treatment if the shceduler is launched.
     * \param treatment
     */
    void treatmentReady(Treatment *treatment);

    /*!
     * \brief Emit finished if that was last treatment to finish.
     * \param treatment
     */
    void treatmentFinished(Treatment *treatment);
    /*!
     * \brief Manage failed treatment and emit finished if that make no more treatments executable.
     * \param treatment
     */
    void treatmentFailed(Treatment *treatment);

    /*!
     * \brief Check if the scheduler finished.
     */
    void checkFinished();
};

#endif // MULTITHREADSCHEDULER_H
