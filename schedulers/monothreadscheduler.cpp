/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#include "monothreadscheduler.h"

MonoThreadScheduler::MonoThreadScheduler(QObject *parent) : Scheduler(parent)
{

}

void MonoThreadScheduler::work()
{
    runFirstReadyTreatment();
}

void MonoThreadScheduler::treatmentFinished(Treatment *treatment)
{
    disconnect(treatment, &Treatment::finished, this, &MonoThreadScheduler::treatmentFinished);

    for (auto t: treatment->next()) {
        if (t->isReady()) {
            runTreatment(t);
            return;
        }
    }
    // else
    runFirstReadyTreatment();
}

void MonoThreadScheduler::runFirstReadyTreatment()
{
    auto treatments = Melodium::instance()->treatments();
    for (auto t: treatments) {
        if (t->isReady()) {
            runTreatment(t);
            return;
        }
    }
    // else
    emit finished();
}

void MonoThreadScheduler::runTreatment(Treatment *treatment)
{
    connect(treatment, &Treatment::finished, this, &MonoThreadScheduler::treatmentFinished);
    treatment->runnable()->run();
}
