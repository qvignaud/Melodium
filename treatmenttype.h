/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef TREATMENTTYPE_H
#define TREATMENTTYPE_H

#include <QString>
#include <QVector>
#include <QVariant>
#include <QVariantList>
#include <QObject>
#include <iostream>

/*!
 * \brief Describe a treatment type.
 */
class TreatmentType
{
    Q_GADGET
public:

    /*!
     * \brief Roles of treatments.
     */
    enum Role
    {
        ///Treatment is audio format conversion.
        Converter,
        ///Treatment is audio render.
        AudioRender,
        ///Treatment is analyst.
        Analysis,
        ///Treatment is a model feeder.
        ModelFeeder,
        ///Treatment is a model trainer.
        ModelTrainer,
        ///Treatment is a predictor.
        ModelPredictor,
        ///Treatment is a model saver.
        ModelSaver,
        ///Treatment is a model loader.
        ModelLoader,
        ///Treatment is data exporter.
        DataExporter
    };
    Q_ENUM(Role)

    /*!
     * \brief How many tracks are operated by the treatment.
     */
    enum TrackOperation {
        ///Treatment apply only for one track.
        MonoTrack,
        ///Treatment apply for multiple tracks.
        MultiTrack,
        ///Treatment don't apply on tracks.
        NoTrack
    };
    Q_ENUM(TrackOperation)

    /*!
     * \brief Type of data.
     */
    enum DataType {
        ///No data type, considered as invalid in most cases.
        NONE,
        ///Boolean
        BOOL,
        ///Integer, type C++ int.
        INT,
        ///Real, type C++ float.
        REAL,
        ///String, type QString or std::string.
        STRING,
        ///Vector of real, type QVector<float> or std::vector<float>.
        VECTOR_REAL,
        ///Vector of string, type QVector<QString> or std::vector<std::string>.
        VECTOR_STRING,
        ///Bidimentionnal vector of real, type QVector<QVector<float>> or std::vector<std::vector<float>>.
        VECTOR_VECTOR_REAL,
        ///Bidimentionnal vector of string, type QVector<QVector<QString>> or std::vector<std::vector<std::string>>.
        VECTOR_VECTOR_STRING
    };
    Q_ENUM(DataType)

    /*!
     * \brief Describes data processed by the treatment.
     */
    struct DataInfo {
        ///Name of the data.
        QString name;
        ///Description of the data.
        QString description;
        ///Type of data.
        DataType type;
    };

    /*!
     * \brief Describe a parameter used by the treatment.
     */
    struct ParamInfo : public DataInfo {
        ///List of the possibles values.
        QVariantList values;
        ///Value used by default.
        QVariant defaultValue;
    };

    /*!
     * \brief Constructor.
     * \param name  Name of the treatment.
     * \param role  Role of the treatment.
     * \param trackOperation
     */
    TreatmentType(QString name, Role role, TrackOperation trackOperation=TrackOperation::MonoTrack);

    /*!
     * \brief Get the name of treatment.
     * \return The name.
     */
    QString name() const;

    /*!
     * \brief Get the role of treatment.
     * \return Return the role of treatment.
     */
    Role role() const;

    /*!
     * \brief Get the track operation of treatment.
     * \return
     */
    TrackOperation trackOperation() const;

    /*!
     * \brief Tells if treatment is related to a model.
     * \return
     */
    bool isFromModel() const;

    /*!
     * \brief Get the description about the treatment.
     * \return
     */
    QString description() const;
    /*!
     * \brief Set description about the treatment.
     * \param description
     */
    void setDescription(const QString &description);

    /*!
     * \brief Get the informations about required data.
     * \return
     */
    QVector<DataInfo> require() const;
    /*!
     * \brief Add a required data.
     * \param require
     */
    void addRequire(const DataInfo &require);
    /*!
     * \brief Set requirements needed by the treatment.
     * \param require
     */
    void setRequire(const QVector<DataInfo> &require);

    /*!
     * \brief Get the parameters used by the treatment.
     * \return
     */
    QVector<ParamInfo> parameters() const;
    /*!
     * \brief Add a parameter.
     * \param parameter
     */
    void addParameter(const ParamInfo &parameter);
    /*!
     * \brief Set parameters exposed by the treatment.
     * \param parameters
     */
    void setParameters(const QVector<ParamInfo> &parameters);

    /*!
     * \brief Get the informations about data provided by the treatment.
     * \return
     */
    QVector<DataInfo> provide() const;
    /*!
     * \brief Add a provided data.
     * \param provide
     */
    void addProvide(const DataInfo &provide);
    /*!
     * \brief Set the outputs provided by the treatment.
     * \param provide
     */
    void setProvide(const QVector<DataInfo> &provide);

    //TreatmentType &operator=(const TreatmentType&);

private:
    ///Name of treatment.
    QString _name;
    ///Role of treatment.
    Role _role;
    ///Track operation.
    TrackOperation _trackOperation;
    ///Description of the treatment.
    QString _description;
    ///Data required by the treatment.
    QVector<DataInfo> _require;
    ///Parameters of the treatment.
    QVector<ParamInfo> _parameters;
    ///Data provided by the treatment.
    QVector<DataInfo> _provide;

};

std::ostream& operator<<(std::ostream& out, const TreatmentType::Role role);
std::ostream& operator<<(std::ostream& out, const TreatmentType::DataType type);

#endif // TREATMENTTYPE_H
