/*
 * Copyright 2018-2019 Quentin Vignaud
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef TREATMENTSFACTORYEXCEPTION_H
#define TREATMENTSFACTORYEXCEPTION_H

#include <QException>

class TreatmentsFactoryException : public QException
{
public:
    enum Type {
        InvalidInitializer,
        UnkownTreatment,
        WrongInitializationCall
    };

    TreatmentsFactoryException(const Type type, const QString cause);

    void raise() const override;
    TreatmentsFactoryException *clone() const override;

    Type type() const;

    QString cause() const;
    const char *what() const noexcept override;

private:
    const Type _type;
    const QString _cause;

};

#endif // TREATMENTSFACTORYEXCEPTION_H
